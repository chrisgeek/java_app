/*
 * To change this license header, choose License Headers in Project Properties.
 * author Chrisgeeq@gmail.com
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquadike;

import com.placeholder.PlaceHolder;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Chrisgeeq@gmail.com
 */
public class XyLineChart extends JPanel{
    private JFrame frame;
    private JScrollPane sp;
    private JPanel panel1,panel2,panel3,addchartPanel;
    private MigLayout ml;
    private JButton xaxisButton,yaxisButton,back2Menu,back2Fields,backFromChart,submit,submit2,view_chart,add_chart;
    private JTextField num_of_dataset_field,title_field,category_field,x_axis_field,y_axis_field,addchartField;
    private JLabel num_of_dataset_label,title_label,category_label,x_axis_label,y_axis_label;
    private TitledBorder titledBorder = titledBorder = BorderFactory.createTitledBorder(null,
                "<html><b>XYLINE CHART</b></html>", TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);

    public XyLineChart(JFrame frame , JScrollPane sp){
        this.frame = frame;
        this.sp = sp;
         initComponents();
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", 
                javax.swing.border.TitledBorder.CENTER,javax.swing.border.TitledBorder.DEFAULT_POSITION)); 
    }
    //initialise components
    private void initComponents(){
        ml = new MigLayout();
        file_chooser = new JFileChooser("Equation File");
        yfile_chooser = new JFileChooser("Equation File");
        xaxisButton = new JButton("<html><b>X AXIS FILE</b></html>");
        yaxisButton = new JButton("<html><b>Y AXIS FILE</b></html>");
        xaxisButton.addActionListener(new FileChooser());
        yaxisButton.addActionListener(new YFileChooser());
        add_chart =  new JButton("<html><b>ADD CHART</b></html>");
        add_chart.addActionListener((ActionEvent ae ) -> {addChart();});
        back2Fields = new JButton("<html><b>BACK</b></html>");
        back2Fields.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            back2Menu.setEnabled(true);
            view_chart.setEnabled(true);
            panel1.setVisible(true);            
            num_of_dataset_field.setEnabled(true);
            panel2.setVisible(false);
            panel2.removeAll();
            remove(panel2);
            add(panel1,BorderLayout.CENTER);
            setVisible(true);
            repaint();
            frame.remove(scrollPane);
            frame.add(this);
            frame.repaint();
            frame.revalidate();
        });
        back2Menu = new JButton("<html><b>MAIN MENU</b></html>");
        back2Menu.addActionListener((ActionEvent ae ) -> {
            setVisible(false);
            frame.remove(this);
            sp.setVisible(true);
            frame.add(sp);
            frame.repaint();
        });
        submit = new JButton("<html><b>SUBMIT</b></html>");
        submit2 = new JButton("<html><b>SUBMIT</b></html>");
        view_chart = new JButton("<html><b>VIEW CHART</b></html>");
        view_chart.addActionListener((ActionEvent ae ) -> {drawChart();});
        backFromChart = new JButton("<html><b>BACK</b></html>");
        backFromChart.addActionListener((ActionEvent ae ) -> {
            closeChart();
        });
        num_of_dataset_label =  new JLabel("<html><b>NUMBER OF DATASET</b></html>");
        title_label = new JLabel("<html><b>TITLE</b></html>");
        category_label = new JLabel("<html><b>CATEGORY</b></html>");
        x_axis_label = new JLabel("<html><b>X-AXIS</b></html>");
        y_axis_label = new JLabel("<html><b>Y-AXIS</b></html>");
        num_of_dataset_field = new JTextField(5);
        title_field = new JTextField(10);
        addchartField = new JTextField(10);
        category_field = new JTextField(10);
        x_axis_field = new JTextField(10);
        y_axis_field = new JTextField(10);
        placeHolder2 = new PlaceHolder(title_field, "chart title");
        placeHolder2 = new PlaceHolder(category_field, "e.g Velocity");
        placeHolder2 = new PlaceHolder(x_axis_field, "x parameter");
        placeHolder2 = new PlaceHolder(y_axis_field, "y parameter");
        
        //panel for adding new charts
//        addchartPanel = new JPanel();        
//        addchartPanel.setBorder(titledBorder);
//        addchartPanel.setLayout(ml);
//        
//        addchartPanel.add(addchartField,"span,growx,wrap");
//        addchartPanel.add(xaxisButton,"span,growx,wrap");
//        addchartPanel.add(yaxisButton,"growx ,span");
        
        panel2 = new JPanel();
        panel2.setBorder(titledBorder);
        panel2.setLayout(ml);
        
        panel1 = new JPanel();
        panel1.setBorder(titledBorder);
        panel1.setLayout(ml);
        
        //Add components to panel1(initial view panel)        
        panel1.add(title_label,"gap unrelated");
        panel1.add(title_field,"wrap");
        panel1.add(category_label,"gap unrelated");
        panel1.add(category_field,"wrap");
        panel1.add(x_axis_label,"gap unrelated");
        panel1.add(x_axis_field,"wrap");
        panel1.add(y_axis_label,"gap unrelated");
        panel1.add(y_axis_field,"wrap");
        panel1.add(xaxisButton);
        panel1.add(yaxisButton,"wrap");
        panel1.add(back2Menu,"span,growx,wrap");
        panel1.add(view_chart,"growx ,span");
        panel1.setVisible(true);
        add(panel1,BorderLayout.CENTER);
        setVisible(true);
        regEx = Pattern.compile("[a-zA-Z0-9 ]+");
        
    }
    
        //close chart
        private void closeChart(){
            panel3.setVisible(false);
            panel3.removeAll();
            remove(panel3);
            panel1.setVisible(true);
            back2Menu.setEnabled(true);
            view_chart.setEnabled(true);
            add(panel1);    
            frame.setSize(641,520);
            frame.repaint();
            frame.revalidate();

            
        }

        //method to super impose charts
        private void addChart(){        
         panel1.setVisible(true);
         
//        num_of_dataset_field.setEnabled(false);
        back2Menu.setEnabled(false);
        view_chart.setEnabled(false);
        submit.setEnabled(false);
         int result = JOptionPane.showConfirmDialog(null, panel1, 
               "ADD CHART", JOptionPane.OK_CANCEL_OPTION);               
          XYSeries firefo = new XYSeries(category_field.getText(),false,true);
          
         if (result == JOptionPane.OK_OPTION) {                      
         
         //String array_name = category_field.getText();
            Scanner scan1;
            Scanner scan2;
            
            int counter = 0;
            try {
                 scan1 = new Scanner(new File(filepath));                 
                    int z = 0;
                    while(scan1.hasNextDouble())
                    {
                           add_x[z] = scan1.nextDouble();
                           z++;
                    }                   
                   scan2 = new Scanner(new File(yfilepath));                                       
                   int a = 0;
                   
                    while(scan2.hasNextDouble())
                    {
                           add_y[a] = scan2.nextDouble();
                           a++;
                           counter++;
                    }
                    
                    for (int i=0; i<add_x.length; i++){
            firefo.add(add_x[i], add_y[i] );            
        }

        // datasets = new XYSeriesCollection();
         datasets.addSeries( firefo );
            panel3.repaint();
            } 
            catch (IllegalArgumentException ex) {
             //JOptionPane.showMessageDialog(frame, "PLEASE ADD NEW CATEGORY ","FILE ERROR",JOptionPane.ERROR_MESSAGE);            
//                 Logger.getLogger(XyLineChart.class.getName()).log(Level.SEVERE, null, ex); 
                  JOptionPane.showMessageDialog(frame, "CATEGORY" +"  "+category_field.getText()+ " ALREADY EXIST","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
            }
            
            catch (FileNotFoundException ex) {
                 JOptionPane.showMessageDialog(frame, "FILE NOT FOUND","FILE ERROR",JOptionPane.ERROR_MESSAGE);
                  }
            catch (ArrayIndexOutOfBoundsException ex) {
               JOptionPane.showMessageDialog(frame, "MAXIMUM DATA EXCEEDED ","FILE ERROR",JOptionPane.ERROR_MESSAGE);
             }
         //   System.out.printf("counter = %d",counter);
                 
      }
          
        }
        
    
    //draw chart with given x,y values
    private void drawChart(){        
    try {
        submitData();
        System.out.println("data submitted");
    } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(frame, "FILE NOT FOUND","FILE ERROR",JOptionPane.ERROR_MESSAGE);
    }
        try{
        JFreeChart chart = ChartFactory.createXYLineChart(get_title, get_x_axis,get_y_axis,
                createDataset(), PlotOrientation.VERTICAL, true, true, false);
        ChartPanel chartpanel = new ChartPanel(chart);
        chartpanel.setDomainZoomable(true);
                
         final XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesPaint(2, Color.YELLOW);
        renderer.setSeriesStroke(0, new BasicStroke(4.0f));
        renderer.setSeriesStroke(1, new BasicStroke(3.0f));
        renderer.setSeriesStroke(2, new BasicStroke(2.0f));
        plot.setRenderer(renderer);
        panel3 = new JPanel();
        panel3.setLayout(ml);
        panel3.add(chartpanel,"wrap");
        panel3.add(backFromChart,"growx , span,wrap");
        panel3.add(add_chart,"growx , span");
        //panel1.setVisible(false);
        //frame.remove(panel1);
        panel3.setVisible(true);        
        add(panel3);
        frame.add(this);
        //System.out.println(panel3.isVisible());
        frame.repaint();
        frame.revalidate();
        frame.pack();
        }
        catch(NumberFormatException | NullPointerException nfe){
            JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
         Logger.getLogger(XyLineChart.class.getName()).log(Level.SEVERE, null, nfe);
        System.err.print(nfe);
        }
    }
    String get_x,get_y;
     XYSeriesCollection datasets;
      XYSeries series ;
          private XYDataset createDataset() {
          series = new XYSeries(get_category,false,true);
        for (int i=0; i<x_axis.length; i++){
               series.add(x_axis[i], y_axis[i]);
               
        }
        datasets = new XYSeriesCollection();
        datasets.addSeries(series);   
        return datasets;
        }
          
    
    private JFileChooser file_chooser,yfile_chooser;
    double [] x_axis = new double [100];
    double [] y_axis = new double [100];
    double [] add_x = new double [100];
    double [] add_y = new double [100]; 
    private Pattern regEx ;
    private JScrollPane scrollPane;
    int dataset;
    String get_title,get_category,get_x_axis,get_y_axis;
    PlaceHolder placeHolder,placeHolder2;
    
    //Submit chart parameters
    private void submitData() throws FileNotFoundException{            
        try{
            //get  number of data set
            get_title = title_field.getText();
            get_category = category_field.getText();
            get_x_axis = x_axis_field.getText();
            get_y_axis = y_axis_field.getText();
           //dataset = Integer.parseInt(num_of_dataset_field.getText());                 
//            if(((regEx.matcher(get_title = title_field.getText()).matches()) &&(!get_title.equalsIgnoreCase("")))&&
//                ((regEx.matcher(get_category = category_field.getText()).matches()) &&(!get_category.equalsIgnoreCase("")))&&
//                    ((regEx.matcher(get_x_axis = x_axis_field.getText()).matches()) &&(!get_x_axis.equalsIgnoreCase("")))&&
//                    ((regEx.matcher(get_y_axis = y_axis_field.getText()).matches()) &&(!get_y_axis.equalsIgnoreCase("")))
//                    )
//                    {
                    panel1.setVisible(false);
                    remove(panel1);
                   //String desktop_path = System.getProperty("user.home") + "/Desktop";                   
                   Scanner scanner = new Scanner(new File(filepath));                    
                    int z = 0;
                    while(scanner.hasNextDouble())
                    {
                           x_axis[z] = scanner.nextDouble();
                           z++;
                    }
                   
                   Scanner scan = new Scanner(new File(yfilepath));                    
                    int a = 0;
                    while(scan.hasNextDouble())
                    {
                           y_axis[a] = scan.nextDouble();
                           a++;
                    }
                //}         
//             else{
//                //JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
//                
//            }
        }
            catch(NumberFormatException | NullPointerException nfe){
             JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
         //    System.err.print(nfe);
           //   Logger.getLogger(XyLineChart.class.getName()).log(Level.SEVERE, null, nfe);
            }
            catch(FileNotFoundException fnf){
             JOptionPane.showMessageDialog(frame, "FILE NOT FOUND","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
             //System.err.print(fnf);
              //Logger.getLogger(XyLineChart.class.getName()).log(Level.SEVERE, null, fnf);
            }
        catch (IllegalArgumentException ex) {
             JOptionPane.showMessageDialog(frame, "PLEASE ADD NEW CATEGORY ","FILE ERROR",JOptionPane.ERROR_MESSAGE);            
             //    Logger.getLogger(XyLineChart.class.getName()).log(Level.SEVERE, null, ex); 
               //   JOptionPane.showMessageDialog(frame, "CATEGORY" +category_field.getText()+ "ALREADY EXIST","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
            }
    } 
//    
        protected class FileChooser implements ActionListener{        
        @Override
        public void actionPerformed(ActionEvent ae){            
            file_chooser.removeChoosableFileFilter(file_chooser.getAcceptAllFileFilter());
            filter = new FileNameExtensionFilter("Text Files", "txt");
            file_chooser.setFileFilter(filter);
            int returnFile = file_chooser.showOpenDialog(null);
            if(returnFile == JFileChooser.APPROVE_OPTION){
                file = file_chooser.getSelectedFile();
                filepath=file.getAbsolutePath();
                
            }            
        }
    }
                protected class YFileChooser implements ActionListener{        
        @Override
        public void actionPerformed(ActionEvent ae){            
            yfile_chooser.removeChoosableFileFilter(yfile_chooser.getAcceptAllFileFilter());
            yfilter = new FileNameExtensionFilter("Text Files", "txt");
            yfile_chooser.setFileFilter(filter);
            int yreturnFile = yfile_chooser.showOpenDialog(null);
            if(yreturnFile == JFileChooser.APPROVE_OPTION){
                yfile = yfile_chooser.getSelectedFile();
                yfilepath=yfile.getAbsolutePath();
                
            }            
        }
    }
        FileNameExtensionFilter filter,yfilter;
        File file,yfile;
        String filepath,yfilepath;
}

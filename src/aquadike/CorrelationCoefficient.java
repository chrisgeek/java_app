/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquadike;

import com.placeholder.PlaceHolder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Chrisgeeq@gmail.com
 */
public class CorrelationCoefficient extends JPanel{

    public CorrelationCoefficient(JFrame frame, JScrollPane sp){
        this.frame = frame;
        this.sp = sp;
        initComponents();
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", 
                javax.swing.border.TitledBorder.CENTER,javax.swing.border.TitledBorder.DEFAULT_POSITION)); 

    }
    
    private void initComponents(){
        //initialise components
        
        answerFont = new Font("Tahoma", Font.BOLD, 12);
        ml= new MigLayout();
        mainPanel = new JPanel();
        answerPanel = new JPanel();
        answer_label = new JLabel("<html><b>CC : </b><html>");
        answer_label.setFont(answerFont);
        answer_label.setVisible(false);
        correlation_answer = new JLabel();
        correlation_answer.setFont(answerFont);
        correlation_answer.setForeground(Color.BLUE);
        correlation_answer.setVisible(false);
        answerPanel.setBorder(titledBorder);
        answerPanel.setLayout(ml);
        mainPanel.setBorder(titledBorder);
        mainPanel.setLayout(ml);        
        num_of_data_label = new JLabel("<html><b>NUMBER OF DATA </b></html>");
        num_of_data = new JTextField(5);
        back2Menu = new JButton("<html><b>BACK</b></html>");
        back2Menu.addActionListener((ActionEvent ae) -> {
            setVisible(false);
            frame.remove(this);
            sp.setVisible(true);
            frame.add(sp);
            frame.repaint();
        });
        confirm = new JButton("<html><b>GENERATE COEFS</b></html>");
        confirm.addActionListener(new ConfirmData());
        calc_correlation = new JButton("<html><b>CALCULATE</b></html>");
        calc_correlation.addActionListener(new CalculateCorrelation());
        back2MainPanel = new JButton("<html><b>BACK</b></html>");
        back2MainPanel.addActionListener((ActionEvent ae) -> {
           titledBorder.setTitle("<html><b>CORRELATION COEFFICIENT</b></html>");          
            scrollPane.setVisible(false);
            //scrollPane.remove(this);            
            mainPanel.setVisible(true);
            answerPanel.setVisible(false);
            answerPanel.removeAll();
            remove(answerPanel);
            add(mainPanel,BorderLayout.CENTER);
            setVisible(true);
            //repaint();
            frame.remove(scrollPane);
            frame.add(this);
            frame.repaint();
            frame.revalidate();
        });
        mainPanel.add(num_of_data_label,"gap unrelated");
        mainPanel.add(num_of_data,"wrap");
        mainPanel.add(back2Menu,"span, growx ,wrap");
        mainPanel.add(confirm,"span,growx");
        mainPanel.setVisible(true);
        add(mainPanel,BorderLayout.CENTER);
    }
    //Calculate button
    private void calculate(){
         try{
             for(int i=0; i<coef ; i++){
         get_x=x_coefs[i].getText();
         conv_x = Double.parseDouble(get_x);

         sum_x = sum_x + conv_x; //sum all values of x
         sum_x_sqr += Math.pow(conv_x, 2);//sum of xsquare

        get_y=y_coefs[i].getText();
        conv_y = Double.parseDouble(get_y);
        sum_y = sum_y + conv_y;//sum all values of y
        sum_y_sqr += Math.pow(conv_y, 2);//sum of ysquare

        sum_xy += conv_x * conv_y;
        //sum_xy += xy;
         }
         sqr_sum_x = Math.pow(sum_x, 2);//square sum_x
         sqr_sum_y = Math.pow(sum_y, 2);//square sum_y
          cc = (((coef*sum_xy)-(sum_x * sum_y)) / (Math.sqrt(((coef*sum_x_sqr)-(sqr_sum_x)))* Math.sqrt(((coef*sum_y_sqr)-(sqr_sum_y)))));
          correlation_answer.setText(String.valueOf(cc));
          correlation_answer.setVisible(true); //Show
          answer_label.setVisible(true);       //Answer
        //Reset variables
          sum_x=0;
        sum_y=0;
        sum_y_sqr=0;
        sum_x_sqr=0;
        sum_xy=0;
        conv_x=0;
        conv_y=0;
        cc=0;

         }
        catch(NumberFormatException | NullPointerException nfe){
             JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void getNumberOfData(){
        try{
            titledBorder.setTitle("<html><b>CORRELATION <br>COEFFICIENT</b></html>");
            get_num_of_data = num_of_data.getText();
            coef = Integer.parseInt(get_num_of_data);
            
            for(int i=0; i<coef; i++){
                answerPanel.add( x_coefs[i]=new JTextField(5));
                answerPanel.add( y_coefs[i]=new JTextField(5),"wrap");
                placeHolder = new PlaceHolder(x_coefs[i], "x value");
                placeHolder = new PlaceHolder(y_coefs[i], "y value");
            }
            
            mainPanel.setVisible(false);
            remove(mainPanel);
            answerPanel.setVisible(true);
            add(answerPanel);
            repaint();
            
            answerPanel.add(answer_label,"gap unrelated");
            answerPanel.add(correlation_answer,"wrap");
            answerPanel.add(back2MainPanel,"span ,growx,wrap");
            answerPanel.add(calc_correlation,"span,growx");
            
            scrollPane = new JScrollPane(this,
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            frame.remove(this);
            frame.add(scrollPane);            
            frame.repaint();
            frame.revalidate();
            
        }
         catch(NumberFormatException | NullPointerException nfe){
             JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
         catch(ArrayIndexOutOfBoundsException nfe){
             JOptionPane.showMessageDialog(frame, "<html><b>MAXIMUM VALUE is 50</b></html>","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
            
        }
    
    protected class CalculateCorrelation implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae){
            calculate();
        } 
    }

    protected class ConfirmData implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae){
            getNumberOfData();
        }
    }
    
    private String get_num_of_data;
   JTextField [] x_coefs = get_xFields();
   JTextField [] y_coefs = get_yFields();

 public JTextField[] get_xFields(){
    JTextField xfields[]={
        x1	,
    x2	,x3	,x4	,x5	,x6	,x7	,x8	,
    x9	,x10	,x11	,x12	,x13	,x14	,x15	,x16	,x17	,x18	,x19	,x20	,x21	,x22	,x23	,
    x24	,x25	,x26	,x27	,x28	,x29	,x30	,x31	,x32	,x33	,x34	,x35	,x36	,x37	,x38	,
    x39	,x40	,x41	,x42	,x43	,x44	,x45	,x46	,x47	,x48	,x49	,x50


};
    return xfields;
 }
//field array 
public JTextField[] get_yFields(){
   JTextField yfields[]={y1	,
    y2	,y3	,y4	,y5	,y6	,y7	,y8	,
    y9	,y10	,y11	,y12	,y13	,y14	,y15	,y16	,y17	,y18	,y19	,y20	,y21	,y22	,y23	,
    y24	,y25	,y26	,y27	,y28	,y29	,y30	,y31	,y32	,y33	,y34	,y35	,y36	,y37	,y38	,
    y39	,y40	,y41	,y42	,y43	,y44	,y45	,y46	,y47	,y48	,y49	,y50
};
return yfields;
}
//declare field variables
JTextField x1	,
x2	,x3	,x4	,x5	,x6	,x7	,x8	,x9	,x10	,x11	,x12	,x13	,x14	,x15	,x16	,x17	,x18	,x19	,x20	,x21	,x22	,x23	,
x24	,x25	,x26	,x27	,x28	,x29	,x30	,x31	,x32	,x33	,x34	,x35	,x36	,x37	,x38	,
x39	,x40	,x41	,x42	,x43	,x44	,x45	,x46	,x47	,x48	,x49	,x50,y1	,
y2	,y3	,y4	,y5	,y6	,y7	,y8	,y9	,y10	,y11	,y12	,y13	,y14	,y15	,y16	,y17	,y18	,y19	,y20	,y21	,y22	,y23	,
y24	,y25	,y26	,y27	,y28	,y29	,y30	,y31	,y32	,y33	,y34	,y35	,y36	,y37	,y38	,
y39	,y40	,y41	,y42	,y43	,y44	,y45	,y46	,y47	,y48	,y49	,y50;

   private JFrame frame;
   private JScrollPane sp;
   private JPanel mainPanel,answerPanel;
   private JTextField num_of_data;
   private JButton confirm,back2Menu,calc_correlation,back2MainPanel;
   private JLabel num_of_data_label,answer_label;
   private MigLayout ml;
   private PlaceHolder placeHolder;
   int coef;
   private Font answerFont; 
   private TitledBorder titledBorder = titledBorder = BorderFactory.createTitledBorder(null,
                "<html><b>CORRELATION COEFFICIENT</b></html>", TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
   private JLabel correlation_answer;
    private String get_x,get_y;
    private double sum_x,conv_x,sum_y,sum_x_sqr,conv_y,sum_y_sqr,sum_xy,sqr_sum_x,sqr_sum_y,cc;
        JScrollPane scrollPane;
    
}

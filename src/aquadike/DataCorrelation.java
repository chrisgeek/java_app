/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquadike;

import com.placeholder.PlaceHolder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Chirgeeq@gmail.com
 */
public class DataCorrelation extends JPanel{
    
        TitledBorder titledBorder = titledBorder = BorderFactory.createTitledBorder(null,
                "<html><b><b>DATA CORRELATION</b></html>", TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    private MigLayout ml;
    
    public DataCorrelation(JFrame frame,JScrollPane sp){
        this.frame = frame;
        this.sp = sp;
        initDataCorrComponents();
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", 
                javax.swing.border.TitledBorder.CENTER,javax.swing.border.TitledBorder.DEFAULT_POSITION)); 
    }
    
    private void initDataCorrComponents(){
        //initialise components            
          answerFont = new Font("Tahoma", Font.BOLD, 12);
          ml = new MigLayout();
          mean_answer = new JLabel();
          mean_answer.setFont(answerFont);
          mean_answer.setForeground(Color.BLUE);
          design_answer= new JLabel();
          design_answer.setFont(answerFont);
          design_answer.setForeground(Color.BLUE);
          variance_answer= new JLabel();
          variance_answer.setFont(answerFont);
          variance_answer.setForeground(Color.BLUE);
          s_deviation_answer= new JLabel();
          s_deviation_answer.setFont(answerFont);
          s_deviation_answer.setForeground(Color.BLUE);
          coef_variation_answer= new JLabel();
          coef_variation_answer.setFont(answerFont);
          coef_variation_answer.setForeground(Color.BLUE);
          design_value_label = new JLabel("DESIGN VALUE :");
          design_value_label.setFont(answerFont);
          s_deviation_label = new JLabel("STANDARD DEVIATION :");
          s_deviation_label.setFont(answerFont);
        mean_label = new JLabel("MEAN :");
        mean_label.setFont(answerFont);
        variance_label = new JLabel("VARIANCE : ");
        variance_label.setFont(answerFont);
        coef_variation_label = new JLabel("COEF OF VARIATION : ");
        coef_variation_label.setFont(answerFont);
        dataPanel = new JPanel();//panel to get number of data
        calc_dataPanel = new JPanel();
        calc_dataPanel.setLayout(ml);
        calc_dataPanel.setBorder(titledBorder);
        answerPanel = new JPanel();
        answerPanel.setLayout(ml);
        answerPanel.setBorder(titledBorder);
        dataPanel.setLayout(ml);
        dataPanel.setBorder(titledBorder);
        data_label = new JLabel("<html><b>Number of Data</b></htm>");
        data_field = new JTextField(4);
        answerPanelButton = new JButton("<html><b>BACK</b></html>");
        answerPanelButton.addActionListener((ActionEvent ae) -> {
            removeAnswerPanel();
        });
        copy = new JButton("<html><b>COPY TO CLIPBOARD</b></html>");
        copy.addActionListener((ActionEvent ae) -> {
            String mean,variance,deviation;
              mean = mean_answer.getText();
              variance = variance_answer.getText();
              deviation = s_deviation_answer.getText();
                Toolkit toolkit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = toolkit.getSystemClipboard();
		StringSelection strSel = new StringSelection(mean);
                StringSelection strSel2 = new StringSelection(variance);
		clipboard.setContents(strSel, null);
                clipboard.setContents(strSel2, null);
        });
        
        back = new JButton("<html><b>BACK</b></html>");
        back.addActionListener((ActionEvent ae) -> {
            removePanel();
        });
        confirm = new JButton("<html><b>SUBMIT</b></html>");
        confirm.addActionListener(new ConfirmData());
        dataPanel.add(data_label);
        dataPanel.add(data_field,"wrap");
        dataPanel.add(back," span,growx ,wrap");
        dataPanel.add(confirm,"span,growx ");
        dataPanel.setVisible(true);        
        add(dataPanel,BorderLayout.CENTER);
        regEx = Pattern.compile("[0-9]*\\.?[0-9]*");
    }
    private void removeAnswerPanel(){
        answerPanel.setVisible(false);
        answerPanel.removeAll();
        calc_dataPanel.setVisible(true);
        remove(answerPanel);
        add(calc_dataPanel);
        repaint();
        revalidate();
    }
    private JButton answerPanelButton; //Am feeling a little sick right now
    public JTextField[] get_xFields(){
        JTextField xfields[]={
            x1	,
        x2	,x3	,x4	,x5	,x6	,x7	,x8	,
        x9	,x10	,x11	,x12	,x13	,x14	,x15	,x16	,x17	,x18	,x19	,x20	,x21	,x22	,x23	,
        x24	,x25	,x26	,x27	,x28	,x29	,x30	,x31	,x32	,x33	,x34	,x35	,x36	,x37	,x38	,
        x39	,x40	,x41	,x42	,x43	,x44	,x45	,x46	,x47	,x48	,x49	,x50

        };
        return xfields;
    }
    
        public void removePanel(){
            //go back to main menu
             setVisible(false);
            frame.remove(this);
            sp.setVisible(true);
            frame.add(sp,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();                        
        }
        
    private void returnToDataPanel(){
        //go back to dataPanel        
        calc_dataPanel.setVisible(false);        
        remove(calc_dataPanel);
        calc_dataPanel.removeAll();
        dataPanel.setVisible(true);
        //setVisible(true);
        add(dataPanel);
        repaint();
        revalidate();
        setVisible(true);
    }
        JTextField [] x_coefs;
        JScrollPane scrollPane;        
        
    private void generateFields(){
        x_coefs = get_xFields();
        n_field = new JTextField(5);
        g_field = new JTextField(5);
        k_field = new JTextField(5);
        getCoefs = data_field.getText();
        holder = new PlaceHolder(n_field, "n value");
        holder = new PlaceHolder(g_field, "g value");
        holder = new PlaceHolder(k_field, "k value");
        try{
            coef = Integer.parseInt(getCoefs);
           
            cancel_datacorrelation = new JButton("<html><b>BACK</b></html>");
            cancel_datacorrelation.addActionListener((ActionEvent ae) -> {
                returnToDataPanel();
            });
        //calculate button
        calc_datacorrelation = new JButton("<html><b>CALCULATE</b></html>");
        calc_datacorrelation.addActionListener(new CalculateDataCorrelation());
        int q=1;
        for(int i=0; i<coef; i++){
            if (q++ % 4 == 0){//arrange generated textfields in pairs
                calc_dataPanel.add( x_coefs[i]=new JTextField(5),"wrap");
            }
            else{
                calc_dataPanel.add( x_coefs[i]=new JTextField(5));
            }
            PlaceHolder placeHolder = new PlaceHolder(x_coefs[i], "x value");
        }
        //add and position other textfields(n,g,k) and buttons
        calc_dataPanel.add(n_field,"span 3 , split 4");
        calc_dataPanel.add(g_field);
        calc_dataPanel.add(k_field);
        calc_dataPanel.add(new JLabel(),"wrap");
        calc_dataPanel.add(cancel_datacorrelation,"span, grow,wrap");
        calc_dataPanel.add(calc_datacorrelation,"span,growx");        
        
           dataPanel.setVisible(false);
            remove(dataPanel);
            calc_dataPanel.setVisible(true);
            add(calc_dataPanel,BorderLayout.CENTER);
            repaint();
         
        
        if(coef > 30){
            frame.pack();
        }
        
        }
        catch(NumberFormatException | NullPointerException nfe){
             JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
        catch(ArrayIndexOutOfBoundsException nfe){
             JOptionPane.showMessageDialog(frame, "MAXIMUM VALUE,EXCEEDED","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }

    }
    protected class ConfirmData implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae){
            generateFields();
        }
    }
    private void calculateDataCorr(){
        get_n_field = n_field.getText();
        get_g_field = g_field.getText();
        get_k_field = k_field.getText();
        try{
            n = Double.parseDouble(get_n_field);
            g = Double.parseDouble(get_g_field);
            k = Double.parseDouble(get_k_field);
            
            for (int i=0; i<coef; i++){
                get_x = x_coefs[i].getText();//get n values of x
                conv_x = Double.parseDouble(get_x); //convert x values to double
                sum_x = sum_x + conv_x;//Sum the values of x
            }
            mean_value = sum_x / coef;//calculate mean 
                
            
            for (int a=0; a<coef; a++){
                    get_x = x_coefs[a].getText();//get n values of x
                    conv_x = Double.parseDouble(get_x); 
                    sum_x_u = Math.pow((conv_x - mean_value),2);//get (x-u)
                    get_sum_x_u += sum_x_u; //sum of (x-u)
                    sum_x_sqr += Math.pow(conv_x, 2);//sum of x raised to 2
                }

                    variance =((get_sum_x_u)/(coef-1));
                    s_deviation = Math.sqrt(variance);
                    coef_variation = s_deviation / mean_value;
                    design_value = ((n/g)*mean_value*(1-k*coef_variation));
                
                //Reset variables
                sum_x = 0;
                conv_x =0;
                sum_x_u = 0;
                get_sum_x_u = 0;
                sum_x_sqr = 0;
                
                //set label values
                mean_answer.setText(String.valueOf(mean_value));
                variance_answer.setText(String.valueOf(variance));
                s_deviation_answer.setText(String.valueOf(s_deviation));
                coef_variation_answer.setText(String.valueOf(coef_variation));
                design_answer.setText(String.valueOf(design_value));
                
                answerPanel.setVisible(true);
                //add labels to answerPanel
                answerPanel.add(mean_label,"gap unrelated");
                answerPanel.add(mean_answer,"wrap");
                answerPanel.add(variance_label,"gap unrelated");
                answerPanel.add(variance_answer,"wrap");
                answerPanel.add(s_deviation_label,"gap unrelated");
                answerPanel.add(s_deviation_answer,"wrap");
                answerPanel.add(coef_variation_label,"gap unrelated");
                answerPanel.add(coef_variation_answer,"wrap");
                answerPanel.add(design_value_label,"gap unrelated");
                answerPanel.add(design_answer,"wrap");
                answerPanel.add(answerPanelButton,"span , growx ");                
                answerPanel.add(copy,"span , growx,wrap ");
                calc_dataPanel.setVisible(false);
                remove(calc_dataPanel);
                add(answerPanel);
                repaint();
                revalidate();
                
                
        }
        catch(NumberFormatException | NullPointerException nfe){
             JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
    }
    protected class CalculateDataCorrelation implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae){
            calculateDataCorr();
        }
    }
    private String getCoefs,get_n_field,get_k_field,get_g_field,get_x;
    private double n,g,k,conv_x,sum_x,mean_value,sum_x_u,sum_x_sqr,get_sum_x_u,variance,s_deviation,coef_variation,design_value;
    Pattern regEx;
    private PlaceHolder holder;
    private JLabel data_label,mean_label,design_value_label,mean_answer,design_answer,variance_answer,s_deviation_answer,
    coef_variation_answer,variance_label,s_deviation_label,coef_variation_label;
    //Declare textfield variables
    private JTextField data_field,n_field,g_field,k_field,x1	,
            x2	,x3	,x4	,x5	,x6	,x7	,x8	,x9	,x10	,x11	,x12	,x13	,x14	,x15	,x16	,x17	,x18	,x19	,x20	,x21	,x22	,x23	,
            x24	,x25	,x26	,x27	,x28	,x29	,x30	,x31	,x32	,x33	,x34	,x35	,x36	,x37	,x38	,
            x39	,x40	,x41	,x42	,x43	,x44	,x45	,x46	,x47	,x48	,x49	,x50,y1	,
            y2	,y3	,y4	,y5	,y6	,y7	,y8	,y9	,y10	,y11	,y12	,y13	,y14	,y15	,y16	,y17	,y18	,y19	,y20	,y21	,y22	,y23	,
            y24	,y25	,y26	,y27	,y28	,y29	,y30	,y31	,y32	,y33	,y34	,y35	,y36	,y37	,y38	,
            y39	,y40	,y41	,y42	,y43	,y44	,y45	,y46	,y47	,y48	,y49	,y50;
    private JButton back, confirm,cancel_datacorrelation,copy,calc_datacorrelation;
    private int coef;
    private JPanel dataPanel,calc_dataPanel,answerPanel;
    private Font answerFont;
    private JFrame frame;
    private JScrollPane sp;
}








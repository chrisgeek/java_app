
package aquadike;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import net.miginfocom.layout.Grid;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import com.placeholder.PlaceHolder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DocumentFilter;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
/**
 *
 * @author chrisgeeq@gmail.com
 */
//TidalVelocity and BedLevel
public class SedimentPanel extends JPanel{
     String new_title;   
  private JFrame frame;
  private JScrollPane sp;
    Image image;
    ImageIcon icon;
    public SedimentPanel(JFrame frame, JScrollPane sp){
       super();      
       this.frame = frame;
        this.sp = sp;
        image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/res/aqua_icon32.jpg"));
        //icon = new ImageIcon();
        frame.setIconImage(image);
        initComponents();
        initPlaceHolders();
                setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", 
                javax.swing.border.TitledBorder.CENTER,javax.swing.border.TitledBorder.DEFAULT_POSITION));         
    }
    String getEquation = "VELOCITY(X-DIRECTION)";
    public void setTitle(String title){
        titledBorder.setTitle(title);
        getEquation = title;
        main_panel.repaint();        
    }
    File temp_file;
    String file_path;
    //Save file method,called when user clicks "save file"
    private void saveFile() throws IOException{
        try{
            JFileChooser file_Chooser = new JFileChooser();
            filter = new FileNameExtensionFilter("Text Files", "txt");
            file_Chooser.setFileFilter(filter);
            int retval = file_Chooser.showSaveDialog(save_file);
            
            if (retval == JFileChooser.APPROVE_OPTION) {
            File new_file = file_Chooser.getSelectedFile(); //get file name
               file_path = new_file.getAbsolutePath(); // get file path
            }
            temp_file = new File(desktop_path + "\\aqua_values.txt");
            InputStream in = new FileInputStream(temp_file);
           // OutputStream out = new FileOutputStream(new File(desktop_path + "\\saved_values.txt"));
            OutputStream out = new FileOutputStream(new File(file_path));
           byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
               out.write(buf, 0, len);
            }
            in.close();            
            out.close();
            temp_file.delete();
            JOptionPane.showMessageDialog(this, "<html><b>FILE SAVED </b></html>","FILE STATUS",JOptionPane.INFORMATION_MESSAGE);
              }
        catch(FileNotFoundException  fn){
             JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
    }
    //create method to get field values and do the calculation
    public void getFieldValues() throws FileNotFoundException, ScriptException, IOException{
        double [] f_values = getArray();
        if (d3.isSelected()){
            dimension="3D";
        }
        else if(d2.isSelected()){
            dimension="2D";
        }
        else if(d1.isSelected()){
            dimension="1D";
        }
        else{ dimension="3D";}
    
    get_p_field=p_field.getText();
    //get_u_field = u_field.getText();
    get_h_field=h_field.getText();
    get_g_field=g_field.getText();
    get_k_field=k_field.getText();
    get_x_field=x_field.getText();
    get_l_field=l_field.getText();
    //get_v_field=v_field.getText();
    get_t_field=t_field.getText();
    
    get_m1_field=m1_field.getText();
    get_m2_field=m2_field.getText();
    get_m3_field=m3_field.getText();
    get_m4_field=m4_field.getText();
     get_m5_field=m5_field.getText();
    get_m6_field=m6_field.getText();
                          
 
        //parse strings to double    
    try{
        p = Double.parseDouble(get_p_field);
        //u = Double.parseDouble(get_u_field);
        h = Double.parseDouble(get_h_field);
        k = Double.parseDouble(get_k_field);
        x=  Double.parseDouble(get_x_field);
        l = Double.parseDouble(get_l_field);
        //v = Double.parseDouble(get_v_field);
        t = Double.parseDouble(get_t_field);
        g = Double.parseDouble(get_g_field);
        m1 = Double.parseDouble(get_m1_field);
        m2 = Double.parseDouble(get_m2_field);
        m3 = Double.parseDouble(get_m3_field);
        m4 = Double.parseDouble(get_m4_field);
        m5 = Double.parseDouble(get_m5_field);
        m6 = Double.parseDouble(get_m6_field);
        
            switch (getEquation) {
                case "SEDIMENT CONCENTRATION":
                    get_ds_field=ds_field.getText();
                    get_d50_field=d50_field.getText();
                    ds = Double.parseDouble(get_ds_field);
                    d50 = Double.parseDouble(get_d50_field);
                    get_u_field = u_field.getText();
                    u = Double.parseDouble(get_u_field);    
                    get_v_field=v_field.getText();
                    v = Double.parseDouble(get_v_field);
                    w1=(-u) +(-v);
                    c = (0.9*ds/(Math.pow (h, 2))+0.9*ds/(Math.pow (k,2)) + 0.42* ds/(Math.pow (l,2)));                    
                    e =  (u/(2*h) - (0.45*ds)/(Math.pow (h,2)))*g;
                    w = (-u/(2*h)-(0.45*ds)/(Math.pow(h,2)))*g;
           switch (dimension) {
            case "3D":                
                n =( v/(2*k) + (0.45*ds)/(2*(Math.pow(k,2))))*g;
                s = (-v/(2*k) + (0.45*ds)/(2*(Math.pow(k,2))))*g;
                a = (w1/(2*l)-(0.21*ds)/(Math.pow(l,2))-((Math.sqrt((4*9.81*(1-0.5)*d50)/( 3*0.34)))/(2*l)))*g;
                b = (-w1/(2*l) -(0.21*ds)/(Math.pow(l,2))+((Math.sqrt((4*9.81*(1-0.5)*d50)/( 3*0.34)))/(2*l)))*g;       
            break;
            
            case "2D":
                n = (v/(2*k) + (0.45*ds)/(2*(Math.pow(k,2))))*g;
                s = (-v/(2*k) + (0.45*ds)/(2*(Math.pow(k,2))))*g;                
                a = 0;
                b = 0;                
                //System.out.println("2D");
                break;
            case "1D":
                n = 0;
                s = 0;
                a = 0;
                b = 0;
                break;
            }                    
                    break;
                case "VELOCITY(X-DIRECTION)":
                    get_u_field = u_field.getText();
                    u = Double.parseDouble(get_u_field);    
                    get_v_field=v_field.getText();
                    v = Double.parseDouble(get_v_field);
                    w1= -u-v;
                    c = ((2 * p / Math.pow(h, 2)) + (2 * p) / (Math.pow(k, 2)) + (2 * p / (Math.pow(l, 2))));
                    e = ((u/(2*h)) + (p)/(Math.pow (h, 2)) - (0.2*(Math.pow(u, 2))/4) + (9.81*(Math.sin(x))/2))*g;
                    w = (((-u)/(2*h)) + (p)/(Math.pow(h, 2))- ((0.2*(Math.pow(u, 2)))/4) + ((9.81*(Math.sin(x)))/2))*g ;
            switch (dimension) {
            case "3D":
                n =(((v)/(2*k)-(p)/(Math.pow(k,2))))*g;
                s =(((-v)/(2*k))-(p)/(Math.pow(k,2)))*g;
                a = (((w1)/(2*l))-(p)/(Math. pow(l,2)))*g;
                b = (((-w1)/(2*l))-(p)/(Math.pow(l,2)))*g;   
                
            break;
            
            case "2D":
                n =(((v)/(2*k)-(p)/(Math.pow(k,2))))*g;
                s =(((-v)/(2*k))-(p)/(Math.pow(k,2)))*g;
                a = 0;
                b = 0;                
                
                //System.out.println("2D");
                break;
            case "1D":
                n = 0;
                s = 0;
                a = 0;
                b = 0;
                break;
            }
                break;
                
                case "VELOCITY(Y-DIRECTION)":
                    get_u_field = u_field.getText();
                    u = Double.parseDouble(get_u_field);    
                    get_v_field=v_field.getText();
                    v = Double.parseDouble(get_v_field);
                    w1= -u-v;
            c = ((2 * p / Math.pow(h, 2)) + (2 * p) / (Math.pow(k, 2)) + (2 * p / (Math.pow(l, 2))));
            e = (((u/(2*h)) - (p)/(Math.pow (h, 2)))*g);
            w = (((-u)/(2*h)) - (p)/(Math.pow(h, 2))*g);
            switch (dimension) {
            case "3D":
                n =((v)/(2*k)-(p)/(Math.pow(k,2))+(0.2*(Math.pow(u, 2))/4))*g;
                s =(((-v)/(2*k))-(p)/(Math.pow(k,2))-(0.2*(Math.pow(u, 2))/4))*g;
                a = (((w1)/(2*l))-(p)/(Math. pow(l,2)))*g;
                b = (((-w1)/(2*l))-(p)/(Math.pow(l,2)))*g;  
                
            break;
            
            case "2D":
                n =((v)/(2*k)-(p)/(Math.pow(k,2))+(0.2*(Math.pow(u, 2))/4))*g;
                s =(((-v)/(2*k))-(p)/(Math.pow(k,2))-(0.2*(Math.pow(u, 2))/4))*g;
                a = 0;
                b = 0;              
                
                //System.out.println("2D");
                break;
            case "1D":
                n = 0;
                s = 0;
                a = 0;
                b = 0;
                break;
            }
            break;
            case "TIDAL VELOCITY PROFILE":
                        um = Double.parseDouble(um_field.getText());
                        ua = Double.parseDouble(ua_field.getText());
                        rp = Double.parseDouble(rp_field.getText());
                        vm = Double.parseDouble(vm_field.getText());
                        va = Double.parseDouble(va_field.getText());
                        time = Double.parseDouble(time_field.getText());

                        u = (um + ua * Math.cos((Math.PI * time)/rp));
                        v = (vm + va * Math.cos((Math.PI * time))/rp);
                        //System.out.printf("multiple velocity selected time=%f and um=%f",time,um);                        
                        w1= -u-v;
                        c = ((2 * p / Math.pow(h, 2)) + (2 * p) / (Math.pow(k, 2)) + (2 * p / (Math.pow(l, 2))));
                        e = ((u/(2*h)) + (p)/(Math.pow (h, 2)) - (0.2*(Math.pow(u, 2))/4) + (9.81*(Math.sin(x))/2))*g;
                        w = (((-u)/(2*h)) + (p)/(Math.pow(h, 2))- ((0.2*(Math.pow(u, 2)))/4) + ((9.81*(Math.sin(x)))/2))*g ;                        
                    
                        switch (dimension) {
                    case "3D":
                        n =(((v)/(2*k)-(p)/(Math.pow(k,2))))*g;
                        s =(((-v)/(2*k))-(p)/(Math.pow(k,2)))*g;
                        a = (((w1)/(2*l))-(p)/(Math. pow(l,2)))*g;
                        b = (((-w1)/(2*l))-(p)/(Math.pow(l,2)))*g;   
                    break;
                    case "2D":
                        n =(((v)/(2*k)-(p)/(Math.pow(k,2))))*g;
                        s =(((-v)/(2*k))-(p)/(Math.pow(k,2)))*g;
                        a = 0;
                        b = 0;                
                        //System.out.println("2D");
                        break;
                    case "1D":
                        n = 0;
                        s = 0;
                        a = 0;
                        b = 0;
                        break;
                    }                   
                       
                }
                                
//            switch (dimension) {
//            case "3D":
//                n =(((v)/(2*k)-(p)/(Math.pow(k,2))))*g;
//                s =(((-v)/(2*k))-(p)/(Math.pow(k,2)))*g;
//                a = (((w1)/(2*l))-(p)/(Math. pow(l,2)))*g;
//                b = (((-w1)/(2*l))-(p)/(Math.pow(l,2)))*g;   
//                
//            break;
//            
//            case "2D":
//                n =(((v)/(2*k)-(p)/(Math.pow(k,2))))*g;
//                s =(((-v)/(2*k))-(p)/(Math.pow(k,2)))*g;
//                a = 0;
//                b = 0;                
//                //System.out.println("2D");
//                break;
//            case "1D":
//                n = 0;
//                s = 0;
//                a = 0;
//                b = 0;
//                break;
//            default:
//                n =(((v)/(2*k)-(p)/(Math.pow(k,2))))*g;
//                s =(((-v)/(2*k))-(p)/(Math.pow(k,2)))*g;
//                a = (((w1)/(2*l))-(p)/(Math. pow(l,2)))*g;
//                b = (((-w1)/(2*l))-(p)/(Math.pow(l,2)))*g;
//            //System.out.print("default is active");
//            }

            //Locate and add textfile via scanner
             //fScn = new Scanner(new File(desktop_path + "\\"+"equations.txt"));
             fScn = new Scanner(new File(filepath));
            ScriptEngineManager mgr = new ScriptEngineManager();
            ScriptEngine engine = mgr.getEngineByName("JavaScript");
            engine.put("t",t);
            engine.put("n",n);
            engine.put("a",a);
            engine.put("e",e);
            engine.put("c",c);
            engine.put("w",w);
            engine.put("s",s);
            engine.put("b",b);
             engine.put("m1",m1);
             engine.put("m2",m2);
             engine.put("m3",m3);
             engine.put("m4",m4);
             engine.put("m5",m5);
             engine.put("m6",m6);
                  for (int i=0; i<f_values.length; i++){
                        engine.put("f"+i,f_values[i]);
                  }

            int i=1;
           Date dNow = new Date( );
           SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a");
           
           //add time and equation properties to panel
            sediment_answer_panel.add(new JLabel("Time :"),"gap unrelated");
            sediment_answer_panel.add(new JLabel( ft.format(dNow)),"wrap");
            sediment_answer_panel.add(new JLabel("Equation :"),"gap unrelated");
            sediment_answer_panel.add(new JLabel(getEquation),"wrap");
            sediment_answer_panel.add(new JLabel("DIMENSION :"),"gap unrelated");
            sediment_answer_panel.add(new JLabel(dimension),"wrap");
 
           int counter=1;
        //iterate and get values from  textfile
        temp_file = new File(desktop_path + "\\aqua_values.txt");
        //PrintWriter writer = new PrintWriter(desktop_path + "\\aqua_values.txt");
        PrintWriter writer = new PrintWriter(temp_file);  
        while( fScn.hasNextLine()){
           data = fScn.nextLine();
           if (!data.isEmpty() ) {
           answers = engine.eval(data).toString();
           getAnswers = Double.parseDouble(answers);
           }
            if(remove_coefs.isSelected()){
                writer.println(getAnswers);
            }
            else{
                writer.println("f"+ String.valueOf(counter++) + " ="+getAnswers);
            }
            
            //double not_binding=1.2*t;
            //String not_binding_text="";
            
            JLabel not_binding_label = new JLabel();
            not_binding_label.setForeground(Color.red);
            //set not_binding text 
                if (( getAnswers < (1.2 * -t) ) || ( getAnswers > (1.2 * t) ) ){
                    not_binding_label.setText("Not Binding");
                }
                else{
                    not_binding_label.setText("");                    
                }
                
                //Add values to panel
                if (remove_coefs.isSelected()){
                    sediment_answer_panel.add(new JLabel(String.valueOf(getAnswers)));
                    sediment_answer_panel.add(not_binding_label,"wrap");
                }
                else{
                    sediment_answer_panel.add(new JLabel("f"+ String.valueOf(i++) + "   ="));
                    sediment_answer_panel.add(new JLabel(String.valueOf(getAnswers)));
                    sediment_answer_panel.add(not_binding_label,"wrap");           
                } 
           }
         fScn.close();
          writer.close();
         
          
                main_panel.setVisible(false);
                remove(main_panel);
                sediment_answer_panel.setVisible(true);                
                add(sediment_answer_panel,BorderLayout.CENTER);
                sediment_answer_panel.add(back,"gap unrelated");
                sediment_answer_panel.add(save_file);
                repaint();
         }
    catch(NumberFormatException nfe){
             JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);        
             //    Logger.getLogger(SedimentPanel.class.getName()).log(Level.SEVERE, null, nfe); 
                    sediment_answer_panel.removeAll();
    }
    catch(FileNotFoundException fn){
        JOptionPane.showMessageDialog(this, "FILE NOT FOUND","FILE ERROR",JOptionPane.ERROR_MESSAGE);
        //Logger.getLogger(SedimentPanel.class.getName()).log(Level.SEVERE, null, fn);
           sediment_answer_panel.removeAll();
    }
    catch(  NullPointerException | ScriptException np){
        JOptionPane.showMessageDialog(this, "INVALID INPUTS(S)","FILE ERROR",JOptionPane.ERROR_MESSAGE);
        //Logger.getLogger(SedimentPanel.class.getName()).log(Level.SEVERE, null, np);
        sediment_answer_panel.removeAll();
    }            
    }
    
    protected class FileChooser implements ActionListener{        
        @Override
        public void actionPerformed(ActionEvent ae){            
            file_chooser.removeChoosableFileFilter(file_chooser.getAcceptAllFileFilter());
            filter = new FileNameExtensionFilter("Text Files", "txt");
            file_chooser.setFileFilter(filter);
            int returnFile = file_chooser.showOpenDialog(null);
            if(returnFile == JFileChooser.APPROVE_OPTION){
                file = file_chooser.getSelectedFile();
                filepath=file.getAbsolutePath();
                
            }
            
        }
    }
    private final ButtonGroup group = new ButtonGroup();
    private JRadioButton d1,d2,d3;
     TitledBorder titledBorder;
     JMenuItem sedimentMenuItem ;
     JMenuItem velocityMenuItem ;
     
    private void initComponents(){
         
        titledBorder = BorderFactory.createTitledBorder(null,getEquation, TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
        
        answerLabel = new JLabel();
        f_label = new JLabel();
        new_title = "VELOCITY";
        main_panel = new JPanel();
        main_panel.setLayout(new MigLayout());
        sediment_answer_panel = new JPanel();
        sediment_answer_panel.setVisible(true);
        sediment_answer_panel.setLayout(new MigLayout());
        main_panel.setBorder(titledBorder);
        sediment_answer_panel.setBorder(titledBorder);
        fileChooser = new FileChooser();
        back = new JButton("<html><b> BACK</b></html>");
        save_file = new JButton("<html><b>SAVE </b></html>");
        save_file.addActionListener((ActionEvent ae) -> {
        try {
            saveFile();
            //System.out.print("file saved successfullly");
            } 
        catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
            //Logger.getLogger(SedimentPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        calculate = new JButton("<html><b>CALCULATE</b></html>");
        calculate.addActionListener((ActionEvent ae) -> {
            try {
                //sediment_panel.setTitle("VELOCITY");
                getFieldValues();
            } catch (FileNotFoundException | ScriptException  ex) {
                JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
              } 
              catch (IOException ex) {
                Logger.getLogger(SedimentPanel.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
// System.err.print(ex);
              }
        });
        
        back.addActionListener((ActionEvent ae) -> {
            if(temp_file.exists()){
                temp_file.delete();
            }
        sediment_answer_panel.setVisible(false);
        sediment_answer_panel.removeAll();
        remove(sediment_answer_panel);
        main_panel.setVisible(true);
        add(main_panel,BorderLayout.CENTER);
        repaint();
              
        });
        //radio buttons
        d1 = new JRadioButton("<html><b>1D</b></html>");
        d2 = new JRadioButton("<html><b>2D</b></html>");
        d3 = new JRadioButton("<html><b>3D</b></html>",true);
        file_chooser_btn = new JButton("<html><b>Select Equation File</b></html>");
        group.add(d1);
        group.add(d2);
        group.add(d3);
        file_chooser_btn.addActionListener(fileChooser);
        file_chooser = new JFileChooser("Equation File");
        //border_title = new PanelTitle().title;
        //setLayout(new MigLayout());
        regEx = Pattern.compile("[0-9]*\\.?[0-9]*");//[^\\s]
        //layout = new MigLayout("gap, insets 0, wrap 2", "","");    
        p_field = new JTextField(4);
        u_field = new JTextField(4);
        h_field = new JTextField(4);
        k_field = new JTextField(4);
        x_field = new JTextField(4);
        l_field = new JTextField(4);
        v_field = new JTextField(4);
        g_field = new JTextField(4);
        t_field = new JTextField(4);
        um_field = new JTextField(4);
        ua_field = new JTextField(4);
        rp_field = new JTextField(4);
        va_field = new JTextField(4);
        vm_field = new JTextField(4);
        time_field = new JTextField(4);
        file_field = new JTextField(4);
        m1_field = new JTextField(4);
        m2_field = new JTextField(4);
        m3_field = new JTextField(4);
        m4_field = new JTextField(4);
        m5_field = new JTextField(4);
        m6_field = new JTextField(4);
        ds_field = new JTextField(4);
        d50_field = new JTextField(4);
        p_label = new JLabel("p");
        u_label = new JLabel("u");
        h_label = new JLabel("h");
        k_label = new JLabel("k");
        x_label = new JLabel("x");
        l_label = new JLabel("l");
        v_label = new JLabel("v");
        g_label = new JLabel("g");
        t_label = new JLabel("tv");
        um_label = new JLabel("um");
        ua_label = new JLabel("ua");
        rp_label = new JLabel("rp");
        time_label = new JLabel("t");
        file_label = new JLabel("text file");
        m1_label = new JLabel("m1");
        m2_label = new JLabel("m2");
        m3_label = new JLabel("m3");
        m4_label = new JLabel("m4");
        m5_label = new JLabel("m5");
        m6_label = new JLabel("m6");
        ds_label = new JLabel("ds");
        d50_label = new JLabel("d50");
        vm_label = new JLabel("vm");
        va_label  = new JLabel("va");
        checkBox = new JCheckBox("<html><b>Tidal Multiple Points</b></html>");
        remove_coefs = new JCheckBox("<html><b>No Coefs</b></html>");
        //Add itemListener to checkbox
        checkBox.addItemListener((ItemEvent ie) -> {
            if(checkBox.isSelected()){                           
                disable_d_Fields(false);
                setTitle("TIDAL VELOCITY PROFILE");
                getEquation = "TIDAL VELOCITY PROFILE";
                toggle_tidal_fields(true);
                uv_fields(false);
//                um_field.setEnabled(true);
//                ua_field.setEnabled(true);
//                rp_field.setEnabled(true);
//                va_field.setEnabled(true);
//                vm_field.setEnabled(true);
//                time_field.setEnabled(true);
//                
                //System.out.printf("equation = %s",getEquation);
            } 
            else if(checkBox.isSelected() == false) {                      
                toggle_tidal_fields(false);
                uv_fields(true);
                setTitle("VELOCITY PROFILE");
                getEquation = "VELOCITY PROFILE";
//                um_field.setEnabled(false);
//                ua_field.setEnabled(false);
//                rp_field.setEnabled(false);
//                va_field.setEnabled(false);
//                vm_field.setEnabled(false);
//                time_field.setEnabled(false);
            }
        });
        
       //Add labels and textfields to jpanel
        main_panel.add(p_label);
        main_panel.add(p_field);
        main_panel.add(m1_label,"gap unrelated");
        main_panel.add(m1_field);
        main_panel.add(t_label,"gap unrelated");
        main_panel.add(t_field,"wrap");
        main_panel.add(u_label);
        main_panel.add(u_field);
        main_panel.add(m2_label,"gap unrelated");
        main_panel.add(m2_field);
        main_panel.add(ds_label,"gap unrelated");
        main_panel.add(ds_field,"wrap");
        main_panel.add(h_label);
        main_panel.add(h_field);
        main_panel.add(m3_label,"gap unrelated");
        main_panel.add(m3_field);
        main_panel.add(d50_label,"gap unrelated");
        main_panel.add(d50_field,"wrap");
        main_panel.add(k_label);
        main_panel.add(k_field);
        main_panel.add(m4_label,"gap unrelated");
        main_panel.add(m4_field);
        main_panel.add(time_label,"gap unrelated");
        main_panel.add(time_field,"wrap");
        main_panel.add(x_label);
        main_panel.add(x_field);
        main_panel.add(m5_label,"gap unrelated");
        main_panel.add(m5_field);
        main_panel.add(um_label,"gap unrelated");
        main_panel.add(um_field,"wrap");
        main_panel.add(l_label);
        main_panel.add(l_field);
        main_panel.add(m6_label,"gap unrelated");
        main_panel.add(m6_field);
        main_panel.add(ua_label,"gap unrelated");
        main_panel.add(ua_field,"wrap");
        main_panel.add(v_label);
        main_panel.add(v_field);
        main_panel.add(g_label,"gap unrelated");
        main_panel.add(g_field);
        main_panel.add(rp_label,"gap unrelated");
        main_panel.add(rp_field,"wrap");        
        main_panel.add(vm_label);
        main_panel.add(vm_field);        
        main_panel.add(va_label,"gap unrelated");
        main_panel.add(va_field,"wrap");
        //main_panel.add(checkBox,"span");
        main_panel.add(checkBox,"span 5,split 2");
        //main_panel.add(new JCheckBox("remove coefs"),"span,growx,wrap");
        main_panel.add(remove_coefs,"wrap");
        main_panel.add(d3,"span 5,split 3,growx");
        main_panel.add(d2,"growx");
        main_panel.add(d1,"wrap");
        main_panel.add(file_chooser_btn,"span,grow,wrap");
        main_panel.add(calculate,"span,growx");
        toggle_tidal_fields(false);
//        um_field.setEnabled(false);
//        ua_field.setEnabled(false);
//        rp_field.setEnabled(false);
//        va_field.setEnabled(false);
//        vm_field.setEnabled(false);
//        time_field.setEnabled(false);
         disable_d_Fields(false);
//        ds_field.setEnabled(false);
//        d50_field.setEnabled(false);
         add(main_panel,BorderLayout.CENTER);        
    }
    //method to enable and disable tidal fields
    private void toggle_tidal_fields(boolean toggle){
                um_field.setEnabled(toggle);
                ua_field.setEnabled(toggle);
                rp_field.setEnabled(toggle);
                va_field.setEnabled(toggle);
                vm_field.setEnabled(toggle);
                time_field.setEnabled(toggle);
    }
    
    private void uv_fields(boolean toggle){
        v_field.setEnabled(toggle);
        u_field.setEnabled(toggle);
    }
    
    //disable and enable ds and d50 fields
    public void disable_d_Fields(boolean disable){
        ds_field.setEnabled(disable);
        d50_field.setEnabled(disable);
    }
    private void initPlaceHolders(){    
         holder = new PlaceHolder(time_field, "t(time)");
    }
       
    
    //Declare Variables
    Scanner fScn;
    String desktop_path= System.getProperty("user.home") + "/Desktop";
    private JTextField p_field,u_field,h_field,k_field,x_field,l_field,v_field,g_field,t_field,file_field,m1_field,m2_field,m3_field
            ,m4_field,m5_field,m6_field,ds_field,d50_field,rp_field,um_field,ua_field,time_field,vm_field,va_field;
    private JLabel p_label,u_label,h_label,k_label,x_label,l_label,v_label,g_label,t_label,file_label,m1_label,m2_label,m3_label,
            m4_label,m5_label,m6_label,ds_label,d50_label,um_label,ua_label,rp_label,time_label,f_label,answerLabel,vm_label,va_label;
    private MigLayout layout ;
    String border_title,dimension,data,answers;
    private PlaceHolder holder;
    private JFileChooser file_chooser;
    private String get_p_field,get_u_field,get_h_field,get_k_field,get_x_field,get_l_field,get_v_field,get_g_field,
            get_t_field,get_file_field,get_m1_field,get_m2_field,get_m3_field,get_m4_field,get_m5_field,get_m6_field,get_ds_field,get_d50_field,
            get_um_field,get_ua_field,get_rp_field;
    //PanelTitle param_title;
    public JButton calculate,back,file_chooser_btn,save_file;
    public JPanel sediment_answer_panel,main_panel;
    FileChooser fileChooser;
    File file;
    FileNameExtensionFilter filter;
   // WrapperPanel wrapper_panel;
    String filepath;
    private JCheckBox remove_coefs;
    JCheckBox checkBox;
    private Pattern regEx;
    double array_value[];
    private double getAnswers,p,u,uv,h,k,x,l,v,g,t,m1,m2,m3,m4,m5,m6,ds,d50,um,ua,rp,time,c,e,w,n,s,a,b,w1,vm,va;
    
      public double[] getArray(){
 double array_value[]={f0,f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29,f30,f31,f32,f33,f34,f35,f36,f37,f38,f39,f40,f41,f42,f43,f44,f45,f46,f47,f48,f49,f50,f51,f52,f53,f54,f55,f56,f57,f58,f59,f60,f61,f62,f63,f64,f65,f66,f67,f68,f69,f70,f71,f72,
f73,f74,f75,f76,f77,f78,f79,f80,f81,f82,f83,f84,f85,f86,f87,f88,f89,f90,f91,f92,f93,f94,f95,f96,f97,f98,f99,f100,f101,f102,f103,f104,f105,f106,f107,f108,f109,
f110,f111,f112,f113,f114,f115,f116,f117,f118,f119,f120,f121,f122,f123,f124,f125,f126,f127,f128,f129,f130,f131,f132,f133,f134,f135,f136,f137,f138,f139,f140,f141,
f142,f143,f144,f145,f146,f147,f148,f149,f150,f151,f152,f153,f154,f155,f156,f157,f158,f159,f160,f161,f162,f163,f164,f165,f166,f167,f168,f169,f170,f171,f172,f173,
f174,f175,f176,f177,f178,f179,f180,f181,f182,f183,f184,f185,f186,f187,f188,f189,f190,f191,f192,f193,f194,f195,f196,f197,f198,f199,f200,f201,f202,f203,f204,
f205,f206,f207,f208,f209,f210,f211,f212,f213,f214,f215,f216,f217,f218,f219,f220,f221,f222,f223,f224,f225,f226,f227,f228,f229,f230,f231,f232,f233,f234,f235,
f236,f237,f238,f239,f240,f241,f242,f243,f244,f245,f246,f247,f248,f249,f250,f251,f252,f253,f254,f255,f256,f257,f258,f259,f260,f261,f262,f263,f264,f265,f266,
f267,f268,f269,f270,f271,f272,f273,f274,f275,f276,f277,f278,f279,f280,f281,f282,f283,f284,f285,f286,f287,f288,f289,f290,f291,f292,f293,f294,f295,f296,f297,
f298,f299,f300,f301,f302,f303,f304,f305,f306,f307,f308,f309,f310,f311,f312,f313,f314,f315,f316,f317,f318,f319,f320,f321,f322,f323,f324,f325,f326,f327,f328,
f329,f330,f331,f332,f333,f334,f335,f336,f337,f338,f339,f340,f341,f342,f343,f344,f345,f346,f347,f348,f349,f350,f351,f352,f353,f354,f355,f356,f357,f358,f359,
f360,f361,f362,f363,f364,f365,f366,f367,f368,f369,f370,f371,f372,f373,f374,f375,f376,f377,f378,f379,f380,f381,f382,f383,f384,f385,f386,f387,f388,f389,f390,
f391,f392,f393,f394,f395,f396,f397,f398,f399,f400,f401,f402,f403,f404,f405,f406,f407,f408,f409,f410,f411,f412,f413,f414,f415,f416,f417,f418,f419,f420,f421,
f422,f423,f424,f425,f426,f427,f428,f429,f430,f431,f432,f433,f434,f435,f436,f437,f438,f439,f440,f441,f442,f443,f444,f445,f446,f447,f448,f449,f450,f451,f452,f453,f454,f455,
f456,f457,f458,f459,f460,f461,f462,f463,f464,f465,f466,f467,f468,f469,f470,f471,f472,f473,f474,f475,f476,f477,f478,f479,f480,f481,f482,f483,f484,f485,f486,f487,f488,f489,f490,
f491,f492,f493,f494,f495,f496,f497,f498,f499,f500,f501,f502,f503,f504,f505,f506,f507,f508,f509,f510,f511,f512,f513,f514,f515,f516,f517,f518,f519,f520,f521,
f522,f523,f524,f525,f526,f527,f528,f529,f530,f531,f532,f533,f534,f535,f536,f537,f538,f539,f540,f541,f542,f543,f544,f545,f546,f547,f548,f549,f550,f551,f552,
f553,f554,f555,f556,f557,f558,f559,f560,f561,f562,f563,f564,f565,f566,f567,f568,f569,f570,f571,f572,f573,f574,f575,f576,f577,f578,f579,f580,f581,f582,f583,
f584,f585,f586,f587,f588,f589,f590,f591,f592,f593,f594,f595,f596,f597,f598,f599,f600,f601,f602,f603,f604,f605,f606,f607,f608,f609,f610,f611,f612,f613,f614,
f615,f616,f617,f618,f619,f620,f621,f622,f623,f624,f625,f626,f627,f628,f629,f630,f631,f632,f633,f634,f635,f636,f637,f638,f639,f640,f641,f642,f643,f644,f645,
f646	,
f647	,
f648	,
f649	,
f650	,
f651	,
f652	,
f653	,
f654	,
f655	,
f656	,
f657	,
f658	,
f659	,
f660	,
f661	,
f662	,
f663	,
f664	,
f665	,
f666	,
f667	,
f668	,
f669	,
f670	,
f671	,
f672	,
f673	,
f674	,
f675	,
f676	,
f677	,
f678	,
f679	,
f680	,
f681	,
f682	,
f683	,
f684	,
f685	,
f686	,
f687	,
f688	,
f689	,
f690	,
f691	,
f692	,
f693	,
f694	,
f695	,
f696	,
f697	,
f698	,
f699	,
f700	,
f701	,
f702	,
f703	,
f704	,
f705	,
f706	,
f707	,
f708	,
f709	,
f710	,
f711	,
f712	,
f713	,
f714	,
f715	,
f716	,
f717	,
f718	,
f719	,
f720	,
f721	,
f722	,
f723	,
f724	,
f725	,
f726	,
f727	,
f728	,
f729	,
f730	,
f731	,
f732	,
f733	,
f734	,
f735	,
f736	,
f737	,
f738	,
f739	,
f740	,
f741	,
f742	,
f743	,
f744	,
f745	,
f746	,
f747	,
f748	,
f749	,
f750	,
f751	,
f752	,
f753	,
f754	,
f755	,
f756	,
f757	,
f758	,
f759	,
f760	,
f761	,
f762	,
f763	,
f764	,
f765	,
f766	,
f767	,
f768	,
f769	,
f770	,
f771	,
f772	,
f773	,
f774	,
f775	,
f776	,
f777	,
f778	,
f779	,
f780	,
f781	,
f782	,
f783	,
f784	,
f785	,
f786	,
f787	,
f788	,
f789	,
f790	,
f791	,
f792	,
f793	,
f794	,
f795	,
f796	,
f797	,
f798	,
f799	,
f800	,
f801	,
f802	,
f803	,
f804	,
f805	,
f806	,
f807	,
f808	,
f809	,
f810	,
f811	,
f812	,
f813	,
f814	,
f815	,
f816	,
f817	,
f818	,
f819	,
f820	,
f821	,
f822	,
f823	,
f824	,
f825	,
f826	,
f827	,
f828	,
f829	,
f830	,
f831	,
f832	,
f833	,
f834	,
f835	,
f836	,
f837	,
f838	,
f839	,
f840	,
f841	,
f842	,
f843	,
f844	,
f845	,
f846	,
f847	,
f848	,
f849	,
f850	,
f851	,
f852	,
f853	,
f854	,
f855	,
f856	,
f857	,
f858	,
f859	,
f860	,
f861	,
f862	,
f863	,
f864	,
f865	,
f866	,
f867	,
f868	,
f869	,
f870	,
f871	,
f872	,
f873	,
f874	,
f875	,
f876	,
f877	,
f878	,
f879	,
f880	,
f881	,
f882	,
f883	,
f884	,
f885	,
f886	,
f887	,
f888	,
f889	,
f890	,
f891	,
f892	,
f893	,
f894	,
f895	,
f896	,
f897	,
f898	,
f899	,
f900	,
f901	,
f902	,
f903	,
f904	,
f905	,
f906	,
f907	,
f908	,
f909	,
f910	,
f911	,
f912	,
f913	,
f914	,
f915	,
f916	,
f917	,
f918	,
f919	,
f920	,
f921	,
f922	,
f923	,
f924	,
f925	,
f926	,
f927	,
f928	,
f929	,
f930	,
f931	,
f932	,
f933	,
f934	,
f935	,
f936	,
f937	,
f938	,
f939	,
f940	,
f941	,
f942	,
f943	,
f944	,
f945	,
f946	,
f947	,
f948	,
f949	,
f950	,
f951	,
f952	,
f953	,
f954	,
f955	,
f956	,
f957	,
f958	,
f959	,
f960	,
f961	,
f962	,
f963	,
f964	,
f965	,
f966	,
f967	,
f968	,
f969	,
f970	,
f971	,
f972	,
f973	,
f974	,
f975	,
f976	,
f977	,
f978	,
f979	,
f980	,
f981	,
f982	,
f983	,
f984	,
f985	,
f986	,
f987	,
f988	,
f989	,
f990	,
f991	,
f992	,
f993	,
f994	,
f995	,
f996	,
f997	,
f998	,
f999	,
f1000	,
f1001	,
f1002	,
f1003	,
f1004	,
f1005	,
f1006	,
f1007	,
f1008	,
f1009	,
f1010	,
f1011	,
f1012	,
f1013	,
f1014	,
f1015	,
f1016	,
f1017	,
f1018	,
f1019	,
f1020	,
f1021	,
f1022	,
f1023	,
f1024	,
f1025	,
f1026	,
f1027	,
f1028	,
f1029	,
f1030	,
f1031	,
f1032	,
f1033	,
f1034	,
f1035	,
f1036	,
f1037	,
f1038	,
f1039	,
f1040	,
f1041	,
f1042	,
f1043	,
f1044	,
f1045	,
f1046	,
f1047	,
f1048	,
f1049	,
f1050	,
f1051	,
f1052	,
f1053	,
f1054	,
f1055	,
f1056	,
f1057	,
f1058	,
f1059	,
f1060	,
f1061	,
f1062	,
f1063	,
f1064	,
f1065	,
f1066	,
f1067	,
f1068	,
f1069	,
f1070	,
f1071	,
f1072	,
f1073	,
f1074	,
f1075	,
f1076	,
f1077	,
f1078	,
f1079	,
f1080	,
f1081	,
f1082	,
f1083	,
f1084	,
f1085	,
f1086	,
f1087	,
f1088	,
f1089	,
f1090	,
f1091	,
f1092	,
f1093	,
f1094	,
f1095	,
f1096	,
f1097	,
f1098	,
f1099	,
f1100	,
f1101	,
f1102	,
f1103	,
f1104	,
f1105	,
f1106	,
f1107	,
f1108	,
f1109	,
f1110	,
f1111	,
f1112	,
f1113	,
f1114	,
f1115	,
f1116	,
f1117	,
f1118	,
f1119	,
f1120	,
f1121	,
f1122	,
f1123	,
f1124	,
f1125	,
f1126	,
f1127	,
f1128	,
f1129	,
f1130	,
f1131	,
f1132	,
f1133	,
f1134	,
f1135	,
f1136	,
f1137	,
f1138	,
f1139	,
f1140	,
f1141	,
f1142	,
f1143	,
f1144	,
f1145	,
f1146	,
f1147	,
f1148	,
f1149	,
f1150	,
f1151	,
f1152	,
f1153	,
f1154	,
f1155	,
f1156	,
f1157	,
f1158	,
f1159	,
f1160	,
f1161	,
f1162	,
f1163	,
f1164	,
f1165	,
f1166	,
f1167	,
f1168	,
f1169	,
f1170	,
f1171	,
f1172	,
f1173	,
f1174	,
f1175	,
f1176	,
f1177	,
f1178	,
f1179	,
f1180	,
f1181	,
f1182	,
f1183	,
f1184	,
f1185	,
f1186	,
f1187	,
f1188	,
f1189	,
f1190	,
f1191	,
f1192	,
f1193	,
f1194	,
f1195	,
f1196	,
f1197	,
f1198	,
f1199	,
f1200	,
f1201	,
f1202	,
f1203	,
f1204	,
f1205	,
f1206	,
f1207	,
f1208	,
f1209	,
f1210	,
f1211	,
f1212	,
f1213	,
f1214	,
f1215	,
f1216	,
f1217	,
f1218	,
f1219	,
f1220	,
f1221	,
f1222	,
f1223	,
f1224	,
f1225	,
f1226	,
f1227	,
f1228	,
f1229	,
f1230	,
f1231	,
f1232	,
f1233	,
f1234	,
f1235	,
f1236	,
f1237	,
f1238	,
f1239	,
f1240	,
f1241	,
f1242	,
f1243	,
f1244	,
f1245	,
f1246	,
f1247	,
f1248	,
f1249	,
f1250	,
f1251	,
f1252	,
f1253	,
f1254	,
f1255	,
f1256	,
f1257	,
f1258	,
f1259	,
f1260	,
f1261	,
f1262	,
f1263	,
f1264	,
f1265	,
f1266	,
f1267	,
f1268	,
f1269	,
f1270	,
f1271	,
f1272	,
f1273	,
f1274	,
f1275	,
f1276	,
f1277	,
f1278	,
f1279	,
f1280	,
f1281	,
f1282	,
f1283	,
f1284	,
f1285	,
f1286	,
f1287	,
f1288	,
f1289	,
f1290	,
f1291	,
f1292	,
f1293	,
f1294	,
f1295	,
f1296	,
f1297	,
f1298	,
f1299	,
f1300	,
f1301	,
f1302	,
f1303	,
f1304	,
f1305	,
f1306	,
f1307	,
f1308	,
f1309	,
f1310	,
f1311	,
f1312	,
f1313	,
f1314	,
f1315	,
f1316	,
f1317	,
f1318	,
f1319	,
f1320	,
f1321	,
f1322	,
f1323	,
f1324	,
f1325	,
f1326	,
f1327	,
f1328	,
f1329	,
f1330	,
f1331	,
f1332	,
f1333	,
f1334	,
f1335	,
f1336	,
f1337	,
f1338	,
f1339	,
f1340	,
f1341	,
f1342	,
f1343	,
f1344	,
f1345	,
f1346	,
f1347	,
f1348	,
f1349	,
f1350	,
f1351	,
f1352	,
f1353	,
f1354	,
f1355	,
f1356	,
f1357	,
f1358	,
f1359	,
f1360	,
f1361	,
f1362	,
f1363	,
f1364	,
f1365	,
f1366	,
f1367	,
f1368	,
f1369	,
f1370	,
f1371	,
f1372	,
f1373	,
f1374	,
f1375	,
f1376	,
f1377	,
f1378	,
f1379	,
f1380	,
f1381	,
f1382	,
f1383	,
f1384	,
f1385	,
f1386	,
f1387	,
f1388	,
f1389	,
f1390	,
f1391	,
f1392	,
f1393	,
f1394	,
f1395	,
f1396	,
f1397	,
f1398	,
f1399	,
f1400	,
f1401	,
f1402	,
f1403	,
f1404	,
f1405	,
f1406	,
f1407	,
f1408	,
f1409	,
f1410	,
f1411	,
f1412	,
f1413	,
f1414	,
f1415	,
f1416	,
f1417	,
f1418	,
f1419	,
f1420	,
f1421	,
f1422	,
f1423	,
f1424	,
f1425	,
f1426	,
f1427	,
f1428	,
f1429	,
f1430	,
f1431	,
f1432	,
f1433	,
f1434	,
f1435	,
f1436	,
f1437	,
f1438	,
f1439	,
f1440	,
f1441	,
f1442	,
f1443	,
f1444	,
f1445	,
f1446	,
f1447	,
f1448	,
f1449	,
f1450	,
f1451	,
f1452	,
f1453	,
f1454	,
f1455	,
f1456	,
f1457	,
f1458	,
f1459	,
f1460	,
f1461	,
f1462	,
f1463	,
f1464	,
f1465	,
f1466	,
f1467	,
f1468	,
f1469	,
f1470	,
f1471	,
f1472	,
f1473	,
f1474	,
f1475	,
f1476	,
f1477	,
f1478	,
f1479	,
f1480	,
f1481	,
f1482	,
f1483	,
f1484	,
f1485	,
f1486	,
f1487	,
f1488	,
f1489	,
f1490	,
f1491	,
f1492	,
f1493	,
f1494	,
f1495	,
f1496	,
f1497	,
f1498	,
f1499	,
f1500	,
f1501	,
f1502	,
f1503	,
f1504	,
f1505	,
f1506	,
f1507	,
f1508	,
f1509	,
f1510	,
f1511	,
f1512	,
f1513	,
f1514	,
f1515	,
f1516	,
f1517	,
f1518	,
f1519	,
f1520	,
f1521	,
f1522	,
f1523	,
f1524	,
f1525	,
f1526	,
f1527	,
f1528	,
f1529	,
f1530	,
f1531	,
f1532	,
f1533	,
f1534	,
f1535	,
f1536	,
f1537	,
f1538	,
f1539	,
f1540	,
f1541	,
f1542	,
f1543	,
f1544	,
f1545	,
f1546	,
f1547	,
f1548	,
f1549	,
f1550	,
f1551	,
f1552	,
f1553	,
f1554	,
f1555	,
f1556	,
f1557	,
f1558	,
f1559	,
f1560	,
f1561	,
f1562	,
f1563	,
f1564	,
f1565	,
f1566	,
f1567	,
f1568	,
f1569	,
f1570	,
f1571	,
f1572	,
f1573	,
f1574	,
f1575	,
f1576	,
f1577	,
f1578	,
f1579	,
f1580	,
f1581	,
f1582	,
f1583	,
f1584	,
f1585	,
f1586	,
f1587	,
f1588	,
f1589	,
f1590	,
f1591	,
f1592	,
f1593	,
f1594	,
f1595	,
f1596	,
f1597	,
f1598	,
f1599	,
f1600	,
f1601	,
f1602	,
f1603	,
f1604	,
f1605	,
f1606	,
f1607	,
f1608	,
f1609	,
f1610	,
f1611	,
f1612	,
f1613	,
f1614	,
f1615	,
f1616	,
f1617	,
f1618	,
f1619	,
f1620	,
f1621	,
f1622	,
f1623	,
f1624	,
f1625	,
f1626	,
f1627	,
f1628	,
f1629	,
f1630	,
f1631	,
f1632	,
f1633	,
f1634	,
f1635	,
f1636	,
f1637	,
f1638	,
f1639	,
f1640	,
f1641	,
f1642	,
f1643	,
f1644	,
f1645	,
f1646	,
f1647	,
f1648	,
f1649	,
f1650	,
f1651	,
f1652	,
f1653	,
f1654	,
f1655	,
f1656	,
f1657	,
f1658	,
f1659	,
f1660	,
f1661	,
f1662	,
f1663	,
f1664	,
f1665	,
f1666	,
f1667	,
f1668	,
f1669	,
f1670	,
f1671	,
f1672	,
f1673	,
f1674	,
f1675	,
f1676	,
f1677	,
f1678	,
f1679	,
f1680	,
f1681	,
f1682	,
f1683	,
f1684	,
f1685	,
f1686	,
f1687	,
f1688	,
f1689	,
f1690	,
f1691	,
f1692	,
f1693	,
f1694	,
f1695	,
f1696	,
f1697	,
f1698	,
f1699	,
f1700	,
f1701	,
f1702	,
f1703	,
f1704	,
f1705	,
f1706	,
f1707	,
f1708	,
f1709	,
f1710	,
f1711	,
f1712	,
f1713	,
f1714	,
f1715	,
f1716	,
f1717	,
f1718	,
f1719	,
f1720	,
f1721	,
f1722	,
f1723	,
f1724	,
f1725	,
f1726	,
f1727	,
f1728	,
f1729	,
f1730	,
f1731	,
f1732	,
f1733	,
f1734	,
f1735	,
f1736	,
f1737	,
f1738	,
f1739	,
f1740	,
f1741	,
f1742	,
f1743	,
f1744	,
f1745	,
f1746	,
f1747	,
f1748	,
f1749	,
f1750	,
f1751	,
f1752	,
f1753	,
f1754	,
f1755	,
f1756	,
f1757	,
f1758	,
f1759	,
f1760	,
f1761	,
f1762	,
f1763	,
f1764	,
f1765	,
f1766	,
f1767	,
f1768	,
f1769	,
f1770	,
f1771	,
f1772	,
f1773	,
f1774	,
f1775	,
f1776	,
f1777	,
f1778	,
f1779	,
f1780	,
f1781	,
f1782	,
f1783	,
f1784	,
f1785	,
f1786	,
f1787	,
f1788	,
f1789	,
f1790	,
f1791	,
f1792	,
f1793	,
f1794	,
f1795	,
f1796	,
f1797	,
f1798	,
f1799	,
f1800	,
f1801	,
f1802	,
f1803	,
f1804	,
f1805	,
f1806	,
f1807	,
f1808	,
f1809	,
f1810	,
f1811	,
f1812	,
f1813	,
f1814	,
f1815	,
f1816	,
f1817	,
f1818	,
f1819	,
f1820	,
f1821	,
f1822	,
f1823	,
f1824	,
f1825	,
f1826	,
f1827	,
f1828	,
f1829	,
f1830	,
f1831	,
f1832	,
f1833	,
f1834	,
f1835	,
f1836	,
f1837	,
f1838	,
f1839	,
f1840	,
f1841	,
f1842	,
f1843	,
f1844	,
f1845	,
f1846	,
f1847	,
f1848	,
f1849	,
f1850	,
f1851	,
f1852	,
f1853	,
f1854	,
f1855	,
f1856	,
f1857	,
f1858	,
f1859	,
f1860	,
f1861	,
f1862	,
f1863	,
f1864	,
f1865	,
f1866	,
f1867	,
f1868	,
f1869	,
f1870	,
f1871	,
f1872	,
f1873	,
f1874	,
f1875	,
f1876	,
f1877	,
f1878	,
f1879	,
f1880	,
f1881	,
f1882	,
f1883	,
f1884	,
f1885	,
f1886	,
f1887	,
f1888	,
f1889	,
f1890	,
f1891	,
f1892	,
f1893	,
f1894	,
f1895	,
f1896	,
f1897	,
f1898	,
f1899	,
f1900	,
f1901	,
f1902	,
f1903	,
f1904	,
f1905	,
f1906	,
f1907	,
f1908	,
f1909	,
f1910	,
f1911	,
f1912	,
f1913	,
f1914	,
f1915	,
f1916	,
f1917	,
f1918	,
f1919	,
f1920	,
f1921	,
f1922	,
f1923	,
f1924	,
f1925	,
f1926	,
f1927	,
f1928	,
f1929	,
f1930	,
f1931	,
f1932	,
f1933	,
f1934	,
f1935	,
f1936	,
f1937	,
f1938	,
f1939	,
f1940	,
f1941	,
f1942	,
f1943	,
f1944	,
f1945	,
f1946	,
f1947	,
f1948	,
f1949	,
f1950	,
f1951	,
f1952	,
f1953	,
f1954	,
f1955	,
f1956	,
f1957	,
f1958	,
f1959	,
f1960	,
f1961	,
f1962	,
f1963	,
f1964	,
f1965	,
f1966	,
f1967	,
f1968	,
f1969	,
f1970	,
f1971	,
f1972	,
f1973	,
f1974	,
f1975	,
f1976	,
f1977	,
f1978	,
f1979	,
f1980	,
f1981	,
f1982	,
f1983	,
f1984	,
f1985	,
f1986	,
f1987	,
f1988	,
f1989	,
f1990	,
f1991	,
f1992	,
f1993	,
f1994	,
f1995	,
f1996	,
f1997	,
f1998	,
f1999	,
f2000	,
f2001	,
f2002	,
f2003	,
f2004	,
f2005	,
f2006	,
f2007	,
f2008	,
f2009	,
f2010	,
f2011	,
f2012	,
f2013	,
f2014	,
f2015	,
f2016	,
f2017	,
f2018	,
f2019	,
f2020	,
f2021	,
f2022	,
f2023	,
f2024	,
f2025	,
f2026	,
f2027	,
f2028	,
f2029	,
f2030	,
f2031	,
f2032	,
f2033	,
f2034	,
f2035	,
f2036	,
f2037	,
f2038	,
f2039	,
f2040	,
f2041	,
f2042	,
f2043	,
f2044	,
f2045	,
f2046	,
f2047	,
f2048	,
f2049	,
f2050	,
f2051	,
f2052	,
f2053	,
f2054	,
f2055	,
f2056	,
f2057	,
f2058	,
f2059	,
f2060	,
f2061	,
f2062	,
f2063	,
f2064	,
f2065	,
f2066	,
f2067	,
f2068	,
f2069	,
f2070	,
f2071	,
f2072	,
f2073	,
f2074	,
f2075	,
f2076	,
f2077	,
f2078	,
f2079	,
f2080	,
f2081	,
f2082	,
f2083	,
f2084	,
f2085	,
f2086	,
f2087	,
f2088	,
f2089	,
f2090	,
f2091	,
f2092	,
f2093	,
f2094	,
f2095	,
f2096	,
f2097	,
f2098	,
f2099	,
f2100	,
f2101	,
f2102	,
f2103	,
f2104	,
f2105	,
f2106	,
f2107	,
f2108	,
f2109	,
f2110	,
f2111	,
f2112	,
f2113	,
f2114	,
f2115	,
f2116	,
f2117	,
f2118	,
f2119	,
f2120	,
f2121	,
f2122	,
f2123	,
f2124	,
f2125	,
f2126	,
f2127	,
f2128	,
f2129	,
f2130	,
f2131	,
f2132	,
f2133	,
f2134	,
f2135	,
f2136	,
f2137	,
f2138	,
f2139	,
f2140	,
f2141	,
f2142	,
f2143	,
f2144	,
f2145	,
f2146	,
f2147	,
f2148	,
f2149	,
f2150	,
f2151	,
f2152	,
f2153	,
f2154	,
f2155	,
f2156	,
f2157	,
f2158	,
f2159	,
f2160	,
f2161	,
f2162	,
f2163	,
f2164	,
f2165	,
f2166	,
f2167	,
f2168	,
f2169	,
f2170	,
f2171	,
f2172	,
f2173	,
f2174	,
f2175	,
f2176	,
f2177	,
f2178	,
f2179	,
f2180	,
f2181	,
f2182	,
f2183	,
f2184	,
f2185	,
f2186	,
f2187	,
f2188	,
f2189	,
f2190	,
f2191	,
f2192	,
f2193	,
f2194	,
f2195	,
f2196	,
f2197	,
f2198	,
f2199	,
f2200	,
f2201	,
f2202	,
f2203	,
f2204	,
f2205	,
f2206	,
f2207	,
f2208	,
f2209	,
f2210	,
f2211	,
f2212	,
f2213	,
f2214	,
f2215	,
f2216	,
f2217	,
f2218	,
f2219	,
f2220	,
f2221	,
f2222	,
f2223	,
f2224	,
f2225	,
f2226	,
f2227	,
f2228	,
f2229	,
f2230	,
f2231	,
f2232	,
f2233	,
f2234	,
f2235	,
f2236	,
f2237	,
f2238	,
f2239	,
f2240	,
f2241	,
f2242	,
f2243	,
f2244	,
f2245	,
f2246	,
f2247	,
f2248	,
f2249	,
f2250	,
f2251	,
f2252	,
f2253	,
f2254	,
f2255	,
f2256	,
f2257	,
f2258	,
f2259	,
f2260	,
f2261	,
f2262	,
f2263	,
f2264	,
f2265	,
f2266	,
f2267	,
f2268	,
f2269	,
f2270	,
f2271	,
f2272	,
f2273	,
f2274	,
f2275	,
f2276	,
f2277	,
f2278	,
f2279	,
f2280	,
f2281	,
f2282	,
f2283	,
f2284	,
f2285	,
f2286	,
f2287	,
f2288	,
f2289	,
f2290	,
f2291	,
f2292	,
f2293	,
f2294	,
f2295	,
f2296	,
f2297	,
f2298	,
f2299	,
f2300	,
f2301	,
f2302	,
f2303	,
f2304	,
f2305	,
f2306	,
f2307	,
f2308	,
f2309	,
f2310	,
f2311	,
f2312	,
f2313	,
f2314	,
f2315	,
f2316	,
f2317	,
f2318	,
f2319	,
f2320	,
f2321	,
f2322	,
f2323	,
f2324	,
f2325	,
f2326	,
f2327	,
f2328	,
f2329	,
f2330	,
f2331	,
f2332	,
f2333	,
f2334	,
f2335	,
f2336	,
f2337	,
f2338	,
f2339	,
f2340	,
f2341	,
f2342	,
f2343	,
f2344	,
f2345	,
f2346	,
f2347	,
f2348	,
f2349	,
f2350	,
f2351	,
f2352	,
f2353	,
f2354	,
f2355	,
f2356	,
f2357	,
f2358	,
f2359	,
f2360	,
f2361	,
f2362	,
f2363	,
f2364	,
f2365	,
f2366	,
f2367	,
f2368	,
f2369	,
f2370	,
f2371	,
f2372	,
f2373	,
f2374	,
f2375	,
f2376	,
f2377	,
f2378	,
f2379	,
f2380	,
f2381	,
f2382	,
f2383	,
f2384	,
f2385	,
f2386	,
f2387	,
f2388	,
f2389	,
f2390	,
f2391	,
f2392	,
f2393	,
f2394	,
f2395	,
f2396	,
f2397	,
f2398	,
f2399	,
f2400	,
f2401	,
f2402	,
f2403	,
f2404	,
f2405	,
f2406	,
f2407	,
f2408	,
f2409	,
f2410	,
f2411	,
f2412	,
f2413	,
f2414	,
f2415	,
f2416	,
f2417	,
f2418	,
f2419	,
f2420	,
f2421	,
f2422	,
f2423	,
f2424	,
f2425	,
f2426	,
f2427	,
f2428	,
f2429	,
f2430	, f2431	, f2432	,f2433	,f2434	,
f2435	,
f2436	, f2437	,
f2438	,
f2439	,
f2440	,
f2441	,
f2442	,
f2443	,
f2444	,
f2445	,
f2446	,
 f2447,
 
f2448	,
f2449	,
f2450	,
f2451	,
f2452	,
f2453	,
f2454	,
f2455	,
f2456	,
f2457	,
f2458	,
f2459	,
f2460	,
f2461	,
f2462	,
f2463	,
f2464	,
f2465	,
f2466	,
f2467	,
f2468	,
f2469	,
f2470	,
f2471	,
f2472	,
f2473	,
f2474	,
f2475	,
f2476	,
f2477	,
f2478	,
f2479	,
f2480	,
f2481	,
f2482	,
f2483	,
f2484	,
f2485	,
f2486	,
f2487	,
f2488	,
f2489	,
f2490	,
f2491	,
f2492	,
f2493	,
f2494	,
f2495	,
f2496	,
f2497	,
f2498	,
f2499	,
f2500	,
f2501	,
f2502	,
f2503	,
f2504	,
f2505	,
f2506	,
f2507	,
f2508	,
f2509	,
f2510	,
f2511	,
f2512	,
f2513	,
f2514	,
f2515	,
f2516	,
f2517	,
f2518	,
f2519	,
f2520	,
f2521	,
f2522	,
f2523	,
f2524	,
f2525	,
f2526	,
f2527	,
f2528	,
f2529	,
f2530	,
f2531	,
f2532	,
f2533	,
f2534	,
f2535	,
f2536	,
f2537	,
f2538	,
f2539	,
f2540	,
f2541	,
f2542	,
f2543	,
f2544	,
f2545	,
f2546	,
f2547	,
f2548	,
f2549	,
f2550	,
f2551	,
f2552	,
f2553	,
f2554	,
f2555	,
f2556	,
f2557	,
f2558	,
f2559	,
f2560	,
f2561	,
f2562	,
f2563	,
f2564	,
f2565	,
f2566	,
f2567	,
f2568	,
f2569	,
f2570	,
f2571	,
f2572	,
f2573	,
f2574	,
f2575	,
f2576	,
f2577	,
f2578	,
f2579	,
f2580	,
f2581	,
f2582	,
f2583	,
f2584	,
f2585	,
f2586	,
f2587	,
f2588	,
f2589	,
f2590	,
f2591	,
f2592	,
f2593	,
f2594	,
f2595	,
f2596	,
f2597	,
f2598	,
f2599	,
f2600	,
f2601	,
f2602	,
f2603	,
f2604	,
f2605	,
f2606	,
f2607	,
f2608	,
f2609	,
f2610	,
f2611	,
f2612	,
f2613	,
f2614	,
f2615	,
f2616	,
f2617	,
f2618	,
f2619	,
f2620	,
f2621	,
f2622	,
f2623	,
f2624	,
f2625	,
f2626	,
f2627	,
f2628	,
f2629	,
f2630	,
f2631	,
f2632	,
f2633	,
f2634	,
f2635	,
f2636	,
f2637	,
f2638	,
f2639	,
f2640	,
f2641	,
f2642	,
f2643	,
f2644	,
f2645	,
f2646	,
f2647	,
f2648	,
f2649	,
f2650	,
f2651	,
f2652	,
f2653	,
f2654	,
f2655	,
f2656	,
f2657	,
f2658	,
f2659	,
f2660	,
f2661	,
f2662	,
f2663	,
f2664	,
f2665	,
f2666	,
f2667	,
f2668	,
f2669	,
f2670	,
f2671	,
f2672	,
f2673	,
f2674	,
f2675	,
f2676	,
f2677	,
f2678	,
f2679	,
f2680	,
f2681	,
f2682	,
f2683	,
f2684	,
f2685	,
f2686	,
f2687	,
f2688	,
f2689	,
f2690	,
f2691	,
f2692	,
f2693	,
f2694	,
f2695	,
f2696	,
f2697	,
f2698	,
f2699	,
f2700	,
f2701	,
f2702	,
f2703	,
f2704	,
f2705	,
f2706	,
f2707	,
f2708	,
f2709	,
f2710	,
f2711	,
f2712	,
f2713	,
f2714	,
f2715	,
f2716	,
f2717	,
f2718	,
f2719	,
f2720	,
f2721	,
f2722	,
f2723	,
f2724	,
f2725	,
f2726	,
f2727	,
f2728	,
f2729	,
f2730	,
f2731	,
f2732	,
f2733	,
f2734	,
f2735	,
f2736	,
f2737	,
f2738	,
f2739	,
f2740	,
f2741	,
f2742	,
f2743	,
f2744	,
f2745	,
f2746	,
f2747	,
f2748	,
f2749	,
f2750	,
f2751	,
f2752	,
f2753	,
f2754	,
f2755	,
f2756	,
f2757	,
f2758	,
f2759	,
f2760	,
f2761	,
f2762	,
f2763	,
f2764	,
f2765	,
f2766	,
f2767	,
f2768	,
f2769	,
f2770	,
f2771	,
f2772	,
f2773	,
f2774	,
f2775	,
f2776	,
f2777	,
f2778	,
f2779	,
f2780	,
f2781	,
f2782	,
f2783	,
f2784	,
f2785	,
f2786	,
f2787	,
f2788	,
f2789	,
f2790	,
f2791	,
f2792	,
f2793	,
f2794	,
f2795	,
f2796	,
f2797	,
f2798	,
f2799	,
f2800	,
f2801	,
f2802	,
f2803	,
f2804	,
f2805	,
f2806	,
f2807	,
f2808	,
f2809	,
f2810	,
f2811	,
f2812	,
f2813	,
f2814	,
f2815	,
f2816	,
f2817	,
f2818	,
f2819	,
f2820	,
f2821	,
f2822	,
f2823	,
f2824	,
f2825	,
f2826	,
f2827	,
f2828	,
f2829	,
f2830	,
f2831	,
f2832	,
f2833	,
f2834	,
f2835	,
f2836	,
f2837	,
f2838	,
f2839	,
f2840	,
f2841	,
f2842	,
f2843	,
f2844	,
f2845	,
f2846	,
f2847	,
f2848	,
f2849	,
f2850	,
f2851	,
f2852	,
f2853	,
f2854	,
f2855	,
f2856	,
f2857	,
f2858	,
f2859	,
f2860	,
f2861	,
f2862	,
f2863	,
f2864	,
f2865	,
f2866	,
f2867	,
f2868	,
f2869	,
f2870	,
f2871	,
f2872	,
f2873	,
f2874	,
f2875	,
f2876	,
f2877	,
f2878	,
f2879	,
f2880	,
f2881	,
f2882	,
f2883	,
f2884	,
f2885	,
f2886	,
f2887	,
f2888	,
f2889	,
f2890	,
f2891	,
f2892	,
f2893	,
f2894	,
f2895	,
f2896	,
f2897	,
f2898	,
f2899	,
f2900	,
f2901	,
f2902	,
f2903	,
f2904	,
f2905	,
f2906	,
f2907	,
f2908	,
f2909	,
f2910	,
f2911	,
f2912	,
f2913	,
f2914	,
f2915	,
f2916	,
f2917	,
f2918	,
f2919	,
f2920	,
f2921	,
f2922	,
f2923	,
f2924	,
f2925	,
f2926	,
f2927	,
f2928	,
f2929	,
f2930	,
f2931	,
f2932	,
f2933	,
f2934	,
f2935	,
f2936	,
f2937	,
f2938	,
f2939	,
f2940	,
f2941	,
f2942	,
f2943	,
f2944	,
f2945	,
f2946	,
f2947	,
f2948	,
f2949	,
f2950	,
f2951	,
f2952	,
f2953	,
f2954	,
f2955	,
f2956	,
f2957	,
f2958	,
f2959	,
f2960	,
f2961	,
f2962	,
f2963	,
f2964	,
f2965	,
f2966	,
f2967	,
f2968	,
f2969	,
f2970	,
f2971	,
f2972	,
f2973	,
f2974	,
f2975	,
f2976	,
f2977	,
f2978	,
f2979	,
f2980	,
f2981	,
f2982	,
f2983	,
f2984	,
f2985	,
f2986	,
f2987	,
f2988	,
f2989	,
f2990	,
f2991	,
f2992	,
f2993	,
f2994	,
f2995	,
f2996	,
f2997	,
f2998	,
f2999	,
f3000	,
f3001	,
f3002	,
f3003	,
f3004	,
f3005	,
f3006	,
f3007	,
f3008	,
f3009	,
f3010	,
f3011	,
f3012	,
f3013	,
f3014	,
f3015	,
f3016	,
f3017	,
f3018	,
f3019	,
f3020	,
f3021	,
f3022	,
f3023	,
f3024	,
f3025	,
f3026	,
f3027	,
f3028	,
f3029	,
f3030	,
f3031	,
f3032	,
f3033	,
f3034	,
f3035	,
f3036	,
f3037	,
f3038	,
f3039	,
f3040	,
f3041	,
f3042	,
f3043	,
f3044	,
f3045	,
f3046	,
f3047	,
f3048	,
f3049	,
f3050	,
f3051	,
f3052	,
f3053	,
f3054	,
f3055	,
f3056	,
f3057	,
f3058	,
f3059	,
f3060	,
f3061	,
f3062	,
f3063	,
f3064	,
f3065	,
f3066	,
f3067	,
f3068	,
f3069	,
f3070	,
f3071	,
f3072	,
f3073	,
f3074	,
f3075	,
f3076	,
f3077	,
f3078	,
f3079	,
f3080	,
f3081	,
f3082	,
f3083	,
f3084	,
f3085	,
f3086	,
f3087	,
f3088	,
f3089	,
f3090	,
f3091	,
f3092	,
f3093	,
f3094	,
f3095	,
f3096	,
f3097	,
f3098	,
f3099	,
f3100	,
f3101	,
f3102	,
f3103	,
f3104	,
f3105	,
f3106	,
f3107	,
f3108	,
f3109	,
f3110	,
f3111	,
f3112	,
f3113	,
f3114	,
f3115	,
f3116	,
f3117	,
f3118	,
f3119	,
f3120	,
f3121	,
f3122	,
f3123	,
f3124	,
f3125	,
f3126	,
f3127	,
f3128	,
f3129	,
f3130	,
f3131	,
f3132	,
f3133	,
f3134	,
f3135	,
f3136	,
f3137	,
f3138	,
f3139	,
f3140	,
f3141	,
f3142	,
f3143	,
f3144	,
f3145	,
f3146	,
f3147	,
f3148	,
f3149	,
f3150	,
f3151	,
f3152	,
f3153	,
f3154	,
f3155	,
f3156	,
f3157	,
f3158	,
f3159	,
f3160	,
f3161	,
f3162	,
f3163	,
f3164	,
f3165	,
f3166	,
f3167	,
f3168	,
f3169	,
f3170	,
f3171	,
f3172	,
f3173	,
f3174	,
f3175	,
f3176	,
f3177	,
f3178	,
f3179	,
f3180	,
f3181	,
f3182	,
f3183	,
f3184	,
f3185	,
f3186	,
f3187	,
f3188	,
f3189	,
f3190	,
f3191	,
f3192	,
f3193	,
f3194	,
f3195	,
f3196	,
f3197	,
f3198	,
f3199	,
f3200	,
f3201	,
f3202	,
f3203	,
f3204	,
f3205	,
f3206	,
f3207	,
f3208	,
f3209	,
f3210	,
f3211	,
f3212	,
f3213	,
f3214	,
f3215	,
f3216	,
f3217	,
f3218	,
f3219	,
f3220	,
f3221	,
f3222	,
f3223	,
f3224	,
f3225	,
f3226	,
f3227	,
f3228	,
f3229	,
f3230	,
f3231	,
f3232	,
f3233	,
f3234	,
f3235	,
f3236	,
f3237	,
f3238	,
f3239	,
f3240	,
f3241	,
f3242	,
f3243	,
f3244	,
f3245	,
f3246	,
f3247	,
f3248	,
f3249	,
f3250	,
f3251	,
f3252	,
f3253	,
f3254	,
f3255	,
f3256	,
f3257	,
f3258	,
f3259	,
f3260	,
f3261	,
f3262	,
f3263	,
f3264	,
f3265	,
f3266	,
f3267	,
f3268	,
f3269	,
f3270	,
f3271	,
f3272	,
f3273	,
f3274	,
f3275	,
f3276	,
f3277	,
f3278	,
f3279	,
f3280	,
f3281	,
f3282	,
f3283	,
f3284	,
f3285	,
f3286	,
f3287	,
f3288	,
f3289	,
f3290	,
f3291	,
f3292	,
f3293	,
f3294	,
f3295	,
f3296	,
f3297	,
f3298	,
f3299	,
f3300	,
f3301	,
f3302	,
f3303	,
f3304	,
f3305	,
f3306	,
f3307	,
f3308	,
f3309	,
f3310	,
f3311	,
f3312	,
f3313	,
f3314	,
f3315	,
f3316	,
f3317	,
f3318	,
f3319	,
f3320	,
f3321	,
f3322	,
f3323	,
f3324	,
f3325	,
f3326	,
f3327	,
f3328	,
f3329	,
f3330	,
f3331	,
f3332	,
f3333	,
f3334	,
f3335	,
f3336	,
f3337	,
f3338	,
f3339	,
f3340	,
f3341	,
f3342	,
f3343	,
f3344	,
f3345	,
f3346	,
f3347	,
f3348	,
f3349	,
f3350	,
f3351	,
f3352	,
f3353	,
f3354	,
f3355	,
f3356	,
f3357	,
f3358	,
f3359	,
f3360	,
f3361	,
f3362	,
f3363	,
f3364	,
f3365	,
f3366	,
f3367	,
f3368	,
f3369	,
f3370	,
f3371	,
f3372	,
f3373	,
f3374	,
f3375	,
f3376	,
f3377	,
f3378	,
f3379	,
f3380	,
f3381	,
f3382	,
f3383	,
f3384	,
f3385	,
f3386	,
f3387	,
f3388	,
f3389	,
f3390	,
f3391	,
f3392	,
f3393	,
f3394	,
f3395	,
f3396	,
f3397	,
f3398	,
f3399	,
f3400	,
f3401	,
f3402	,
f3403	,
f3404	,
f3405	,
f3406	,
f3407	,
f3408	,
f3409	,
f3410	,
f3411	,
f3412	,
f3413	,
f3414	,
f3415	,
f3416	,
f3417	,
f3418	,
f3419	,
f3420	,
f3421	,
f3422	,
f3423	,
f3424	,
f3425	,
f3426	,
f3427	,
f3428	,
f3429	,
f3430	,
f3431	,
f3432	,
f3433	,
f3434	,
f3435	,
f3436	,
f3437	,
f3438	,
f3439	,
f3440	,
f3441	,
f3442	,
f3443	,
f3444	,
f3445	,
f3446	,
f3447	,
f3448	,
f3449	,
f3450	,
f3451	,
f3452	,
f3453	,
f3454	,
f3455	,
f3456	,
f3457	,
f3458	,
f3459	,
f3460	,
f3461	,
f3462	,
f3463	,
f3464	,
f3465	,
f3466	,
f3467	,
f3468	,
f3469	,
f3470	,
f3471	,
f3472	,
f3473	,
f3474	,
f3475	,
f3476	,
f3477	,
f3478	,
f3479	,
f3480	,
f3481	,
f3482	,
f3483	,
f3484	,
f3485	,
f3486	,
f3487	,
f3488	,
f3489	,
f3490	,
f3491	,
f3492	,
f3493	,
f3494	,
f3495	,
f3496	,
f3497	,
f3498	,
f3499	,
f3500	,
f3501	,
f3502	,
f3503	,
f3504	,
f3505	,
f3506	,
f3507	,
f3508	,
f3509	,
f3510	,
f3511	,
f3512	,
f3513	,
f3514	,
f3515	,
f3516	,
f3517	,
f3518	,
f3519	,
f3520	,
f3521	,
f3522	,
f3523	,
f3524	,
f3525	,
f3526	,
f3527	,
f3528	,
f3529	,
f3530	,
f3531	,
f3532	,
f3533	,
f3534	,
f3535	,
f3536	,
f3537	,
f3538	,
f3539	,
f3540	,
f3541	,
f3542	,
f3543	,
f3544	,
f3545	,
f3546	,
f3547	,
f3548	,
f3549	,
f3550	,
f3551	,
f3552	,
f3553	,
f3554	,
f3555	,
f3556	,
f3557	,
f3558	,
f3559	,
f3560	,
f3561	,
f3562	,
f3563	,
f3564	,
f3565	,
f3566	,
f3567	,
f3568	,
f3569	,
f3570	,
f3571	,
f3572	,
f3573	,
f3574	,
f3575	,
f3576	,
f3577	,
f3578	,
f3579	,
f3580	,
f3581	,
f3582	,
f3583	,
f3584	,
f3585	,
f3586	,
f3587	,
f3588	,
f3589	,
f3590	,
f3591	,
f3592	,
f3593	,
f3594	,
f3595	,
f3596	,
f3597	,
f3598	,
f3599	,
f3600	,
f3601	,
f3602	,
f3603	,
f3604	,
f3605	,
f3606	,
f3607	,
f3608	,
f3609	,
f3610	,
f3611	,
f3612	,
f3613	,
f3614	,
f3615	,
f3616	,
f3617	,
f3618	,
f3619	,
f3620	,
f3621	,
f3622	,
f3623	,
f3624	,
f3625	,
f3626	,
f3627	,
f3628	,
f3629	,
f3630	,
f3631	,
f3632	,
f3633	,
f3634	,
f3635	,
f3636	,
f3637	,
f3638	,
f3639	,
f3640	,
f3641	,
f3642	,
f3643	,
f3644	,
f3645	,
f3646	,
f3647	,
f3648	,
f3649	,
f3650	,
f3651	,
f3652	,
f3653	,
f3654	,
f3655	,
f3656	,
f3657	,
f3658	,
f3659	,
f3660	,
f3661	,
f3662	,
f3663	,
f3664	,
f3665	,
f3666	,
f3667	,
f3668	,
f3669	,
f3670	,
f3671	,
f3672	,
f3673	,
f3674	,
f3675	,
f3676	,
f3677	,
f3678	,
f3679	,
f3680	,
f3681	,
f3682	,
f3683	,
f3684	,
f3685	,
f3686	,
f3687	,
f3688	,
f3689	,
f3690	,
f3691	,
f3692	,
f3693	,
f3694	,
f3695	,
f3696	,
f3697	,
f3698	,
f3699	,
f3700	,
f3701	,
f3702	,
f3703	,
f3704	,
f3705	,
f3706	,
f3707	,
f3708	,
f3709	,
f3710	,
f3711	,
f3712	,
f3713	,
f3714	,
f3715	,
f3716	,
f3717	,
f3718	,
f3719	,
f3720	,
f3721	,
f3722	,
f3723	,
f3724	,
f3725	,
f3726	,
f3727	,
f3728	,
f3729	,
f3730	,
f3731	,
f3732	,
f3733	,
f3734	,
f3735	,
f3736	,
f3737	,
f3738	,
f3739	,
f3740	,
f3741	,
f3742	,
f3743	,
f3744	,
f3745	,
f3746	,
f3747	,
f3748	,
f3749	,
f3750	,
f3751	,
f3752	,
f3753	,
f3754	,
f3755	,
f3756	,
f3757	,
f3758	,
f3759	,
f3760	,
f3761	,
f3762	,
f3763	,
f3764	,
f3765	,
f3766	,
f3767	,
f3768	,
f3769	,
f3770	,
f3771	,
f3772	,
f3773	,
f3774	,
f3775	,
f3776	,
f3777	,
f3778	,
f3779	,
f3780	,
f3781	,
f3782	,
f3783	,
f3784	,
f3785	,
f3786	,
f3787	,
f3788	,
f3789	,
f3790	,
f3791	,
f3792	,
f3793	,
f3794	,
f3795	,
f3796	,
f3797	,
f3798	,
f3799	,
f3800	,
f3801	,
f3802	,
f3803	,
f3804	,
f3805	,
f3806	,
f3807	,
f3808	,
f3809	,
f3810	,
f3811	,
f3812	,
f3813	,
f3814	,
f3815	,
f3816	,
f3817	,
f3818	,
f3819	,
f3820	,
f3821	,
f3822	,
f3823	,
f3824	,
f3825	,
f3826	,
f3827	,
f3828	,
f3829	,
f3830	,
f3831	,
f3832	,
f3833	,
f3834	,
f3835	,
f3836	,
f3837	,
f3838	,
f3839	,
f3840	,
f3841	,
f3842	,
f3843	,
f3844	,
f3845	,
f3846	,
f3847	,
f3848	,
f3849	,
f3850	,
f3851	,
f3852	,
f3853	,
f3854	,
f3855	,
f3856	,
f3857	,
f3858	,
f3859	,
f3860	,
f3861	,
f3862	,
f3863	,
f3864	,
f3865	,
f3866	,
f3867	,
f3868	,
f3869	,
f3870	,
f3871	,
f3872	,
f3873	,
f3874	,
f3875	,
f3876	,
f3877	,
f3878	,
f3879	,
f3880	,
f3881	,
f3882	,
f3883	,
f3884	,
f3885	,
f3886	,
f3887	,
f3888	,
f3889	,
f3890	,
f3891	,
f3892	,
f3893	,
f3894	,
f3895	,
f3896	,
f3897	,
f3898	,
f3899	,
f3900	,
f3901	,
f3902	,
f3903	,
f3904	,
f3905	,
f3906	,
f3907	,
f3908	,
f3909	,
f3910	,
f3911	,
f3912	,
f3913	,
f3914	,
f3915	,
f3916	,
f3917	,
f3918	,
f3919	,
f3920	,
f3921	,
f3922	,
f3923	,
f3924	,
f3925	,
f3926	,
f3927	,
f3928	,
f3929	,
f3930	,
f3931	,
f3932	,
f3933	,
f3934	,
f3935	,
f3936	,
f3937	,
f3938	,
f3939	,
f3940	,
f3941	,
f3942	,
f3943	,
f3944	,
f3945	,
f3946	,
f3947	,
f3948	,
f3949	,
f3950	,
f3951	,
f3952	,
f3953	,
f3954	,
f3955	,
f3956	,
f3957	,
f3958	,
f3959	,
f3960	,
f3961	,
f3962	,
f3963	,
f3964	,
f3965	,
f3966	,
f3967	,
f3968	,
f3969	,
f3970	,
f3971	,
f3972	,
f3973	,
f3974	,
f3975	,
f3976	,
f3977	,
f3978	,
f3979	,
f3980	,
f3981	,
f3982	,
f3983	,
f3984	,
f3985	,
f3986	,
f3987	,
f3988	,
f3989	,
f3990	,
f3991	,
f3992	,
f3993	,
f3994	,
f3995	,
f3996	,
f3997	,
f3998	,
f3999	,
f4000	,
f4001	,
f4002	,
f4003	,
f4004	,
f4005	,
f4006	,
f4007	,
f4008	,
f4009	,
f4010	,
f4011	,
f4012	,
f4013	,
f4014	,
f4015	,
f4016	,
f4017	,
f4018	,
f4019	,
f4020	,
f4021	,
f4022	,
f4023	,
f4024	,
f4025	,
f4026	,
f4027	,
f4028	,
f4029	,
f4030	,
f4031	,
f4032	,
f4033	,
f4034	,
f4035	,
f4036	,
f4037	,
f4038	,
f4039	,
f4040	,
f4041	,
f4042	,
f4043	,
f4044	,
f4045	,
f4046	,
f4047	,
f4048	,
f4049	,
f4050	,
f4051	,
f4052	,
f4053	,
f4054	,
f4055	,
f4056	,
f4057	,
f4058	,
f4059	,
f4060	,
f4061	,
f4062	,
f4063	,
f4064	,
f4065	,
f4066	,
f4067	,
f4068	,
f4069	,
f4070	,
f4071	,
f4072	,
f4073	,
f4074	,
f4075	,
f4076	,
f4077	,
f4078	,
f4079	,
f4080	,
f4081	,
f4082	,
f4083	,
f4084	,
f4085	,
f4086	,
f4087	,
f4088	,
f4089	,
f4090	,
f4091	,
f4092	,
f4093	,
f4094	,
f4095	,
f4096	,
f4097	,
f4098	,
f4099	,
f4100	,
f4101	,
f4102	,
f4103	,
f4104	,
f4105	,
f4106	,
f4107	,
f4108	,
f4109	,
f4110	,
f4111	,
f4112	,
f4113	,
f4114	,
f4115	,
f4116	,
f4117	,
f4118	,
f4119	,
f4120	,
f4121	,
f4122	,
f4123	,
f4124	,
f4125	,
f4126	,
f4127	,
f4128	,
f4129	,
f4130	,
f4131	,
f4132	,
f4133	,
f4134	,
f4135	,
f4136	,
f4137	,
f4138	,
f4139	,
f4140	,
f4141	,
f4142	,
f4143	,
f4144	,
f4145	,
f4146	,
f4147	,
f4148	,
f4149	,
f4150	,
f4151	,
f4152	,
f4153	,
f4154	,
f4155	,
f4156	,
f4157	,
f4158	,
f4159	,
f4160	,
f4161	,
f4162	,
f4163	,
f4164	,
f4165	,
f4166	,
f4167	,
f4168	,
f4169	,
f4170	,
f4171	,
f4172	,
f4173	,
f4174	,
f4175	,
f4176	,
f4177	,
f4178	,
f4179	,
f4180	,
f4181	,
f4182	,
f4183	,
f4184	,
f4185	,
f4186	,
f4187	,
f4188	,
f4189	,
f4190	,
f4191	,
f4192	,
f4193	,
f4194	,
f4195	,
f4196	,
f4197	,
f4198	,
f4199	,
f4200	,
f4201	,
f4202	,
f4203	,
f4204	,
f4205	,
f4206	,
f4207	,
f4208	,
f4209	,
f4210	,
f4211	,
f4212	,
f4213	,
f4214	,
f4215	,
f4216	,
f4217	,
f4218	,
f4219	,
f4220	,
f4221	,
f4222	,
f4223	,
f4224	,
f4225	,
f4226	,
f4227	,
f4228	,
f4229	,
f4230	,
f4231	,
f4232	,
f4233	,
f4234	,
f4235	,
f4236	,
f4237	,
f4238	,
f4239	,
f4240	,
f4241	,
f4242	,
f4243	,
f4244	,
f4245	,
f4246	,
f4247	,
f4248	,
f4249	,
f4250	,
f4251	,
f4252	,
f4253	,
f4254	,
f4255	,
f4256	,
f4257	,
f4258	,
f4259	,
f4260	,
f4261	,
f4262	,
f4263	,
f4264	,
f4265	,
f4266	,
f4267	,
f4268	,
f4269	,
f4270	,
f4271	,
f4272	,
f4273	,
f4274	,
f4275	,
f4276	,
f4277	,
f4278	,
f4279	,
f4280	,
f4281	,
f4282	,
f4283	,
f4284	,
f4285	,
f4286	,
f4287	,
f4288	,
f4289	,
f4290	,
f4291	,
f4292	,
f4293	,
f4294	,
f4295	,
f4296	,
f4297	,
f4298	,
f4299	,
f4300	,
f4301	,
f4302	,
f4303	,
f4304	,
f4305	,
f4306	,
f4307	,
f4308	,
f4309	,
f4310	,
f4311	,
f4312	,
f4313	,
f4314	,
f4315	,
f4316	,
f4317	,
f4318	,
f4319	,
f4320	,
f4321	,
f4322	,
f4323	,
f4324	,
f4325	,
f4326	,
f4327	,
f4328	,
f4329	,
f4330	,
f4331	,
f4332	,
f4333	,
f4334	,
f4335	,
f4336	,
f4337	,
f4338	,
f4339	,
f4340	,
f4341	,
f4342	,
f4343	,
f4344	,
f4345	,
f4346	,
f4347	,
f4348	,
f4349	,
f4350	,
f4351	,
f4352	,
f4353	,
f4354	,
f4355	,
f4356	,
f4357	,
f4358	,
f4359	,
f4360	,
f4361	,
f4362	,
f4363	,
f4364	,
f4365	,
f4366	,
f4367	,
f4368	,
f4369	,
f4370	,
f4371	,
f4372	,
f4373	,
f4374	,
f4375	,
f4376	,
f4377	,
f4378	,
f4379	,
f4380	,
f4381	,
f4382	,
f4383	,
f4384	,
f4385	,
f4386	,
f4387	,
f4388	,
f4389	,
f4390	,
f4391	,
f4392	,
f4393	,
f4394	,
f4395	,
f4396	,
f4397	,
f4398	,
f4399	,
f4400	,
f4401	,
f4402	,
f4403	,
f4404	,
f4405	,
f4406	,
f4407	,
f4408	,
f4409	,
f4410	,
f4411	,
f4412	,
f4413	,
f4414	,
f4415	,
f4416	,
f4417	,
f4418	,
f4419	,
f4420	,
f4421	,
f4422	,
f4423	,
f4424	,
f4425	,
f4426	,
f4427	,
f4428	,
f4429	,
f4430	,
f4431	,
f4432	,
f4433	,
f4434	,
f4435	,
f4436	,
f4437	,
f4438	,
f4439	,
f4440	,
f4441	,
f4442	,
f4443	,
f4444	,
f4445	,
f4446	,
f4447	,
f4448	,
f4449	,
f4450	,
f4451	,
f4452	,
f4453	,
f4454	,
f4455	,
f4456	,
f4457	,
f4458	,
f4459	,
f4460	,
f4461	,
f4462	,
f4463	,
f4464	,
f4465	,
f4466	,
f4467	,
f4468	,
f4469	,
f4470	,
f4471	,
f4472	,
f4473	,
f4474	,
f4475	,
f4476	,
f4477	,
f4478	,
f4479	,
f4480	,
f4481	,
f4482	,
f4483	,
f4484	,
f4485	,
f4486	,
f4487	,
f4488	,
f4489	,
f4490	,
f4491	,
f4492	,
f4493	,
f4494	,
f4495	,
f4496	,
f4497	,
f4498	,
f4499	,
f4500	,
f4501	,
f4502	,
f4503	,
f4504	,
f4505	,
f4506	,
f4507	,
f4508	,
f4509	,
f4510	,
f4511	,
f4512	,
f4513	,
f4514	,
f4515	,
f4516	,
f4517	,
f4518	,
f4519	,
f4520	,
f4521	,
f4522	,
f4523	,
f4524	,
f4525	,
f4526	,
f4527	,
f4528	,
f4529	,
f4530	,
f4531	,
f4532	,
f4533	,
f4534	,
f4535	,
f4536	,
f4537	,
f4538	,
f4539	,
f4540	,
f4541	,
f4542	,
f4543	,
f4544	,
f4545	,
f4546	,
f4547	,
f4548	,
f4549	,
f4550	,
f4551	,
f4552	,
f4553	,
f4554	,
f4555	,
f4556	,
f4557	,
f4558	,
f4559	,
f4560	,
f4561	,
f4562	,
f4563	,
f4564	,
f4565	,
f4566	,
f4567	,
f4568	,
f4569	,
f4570	,
f4571	,
f4572	,
f4573	,
f4574	,
f4575	,
f4576	,
f4577	,
f4578	,
f4579	,
f4580	,
f4581	,
f4582	,
f4583	,
f4584	,
f4585	,
f4586	,
f4587	,
f4588	,
f4589	,
f4590	,
f4591	,
f4592	,
f4593	,
f4594	,
f4595	,
f4596	,
f4597	,
f4598	,
f4599	,
f4600	,
f4601	,
f4602	,
f4603	,
f4604	,
f4605	,
f4606	,
f4607	,
f4608	,
f4609	,
f4610	,
f4611	,
f4612	,
f4613	,
f4614	,
f4615	,
f4616	,
f4617	,
f4618	,
f4619	,
f4620	,
f4621	,
f4622	,
f4623	,
f4624	,
f4625	,
f4626	,
f4627	,
f4628	,
f4629	,
f4630	,
f4631	,
f4632	,
f4633	,
f4634	,
f4635	,
f4636	,
f4637	,
f4638	,
f4639	,
f4640	,
f4641	,
f4642	,
f4643	,
f4644	,
f4645	,
f4646	,
f4647	,
f4648	,
f4649	,
f4650	,
f4651	,
f4652	,
f4653	,
f4654	,
f4655	,
f4656	,
f4657	,
f4658	,
f4659	,
f4660	,
f4661	,
f4662	,
f4663	,
f4664	,
f4665	,
f4666	,
f4667	,
f4668	,
f4669	,
f4670	,
f4671	,
f4672	,
f4673	,
f4674	,
f4675	,
f4676	,
f4677	,
f4678	,
f4679	,
f4680	,
f4681	,
f4682	,
f4683	,
f4684	,
f4685	,
f4686	,
f4687	,
f4688	,
f4689	,
f4690	,
f4691	,
f4692	,
f4693	,
f4694	,
f4695	,
f4696	,
f4697	,
f4698	,
f4699	,
f4700	,
f4701	,
f4702	,
f4703	,
f4704	,
f4705	,
f4706	,
f4707	,
f4708	,
f4709	,
f4710	,
f4711	,
f4712	,
f4713	,
f4714	,
f4715	,
f4716	,
f4717	,
f4718	,
f4719	,
f4720	,
f4721	,
f4722	,
f4723	,
f4724	,
f4725	,
f4726	,
f4727	,
f4728	,
f4729	,
f4730	,
f4731	,
f4732	,
f4733	,
f4734	,
f4735	,
f4736	,
f4737	,
f4738	,
f4739	,
f4740	,
f4741	,
f4742	,
f4743	,
f4744	,
f4745	,
f4746	,
f4747	,
f4748	,
f4749	,
f4750	,
f4751	,
f4752	,
f4753	,
f4754	,
f4755	,
f4756	,
f4757	,
f4758	,
f4759	,
f4760	,
f4761	,
f4762	,
f4763	,
f4764	,
f4765	,
f4766	,
f4767	,
f4768	,
f4769	,
f4770	,
f4771	,
f4772	,
f4773	,
f4774	,
f4775	,
f4776	,
f4777	,
f4778	,
f4779	,
f4780	,
f4781	,
f4782	,
f4783	,
f4784	,
f4785	,
f4786	,
f4787	,
f4788	,
f4789	,
f4790	,
f4791	,
f4792	,
f4793	,
f4794	,
f4795	,
f4796	,
f4797	,
f4798	,
f4799	,
f4800	,
f4801	,
f4802	,
f4803	,
f4804	,
f4805	,
f4806	,
f4807	,
f4808	,
f4809	,
f4810	,
f4811	,
f4812	,
f4813	,
f4814	,
f4815	,
f4816	,
f4817	,
f4818	,
f4819	,
f4820	,
f4821	,
f4822	,
f4823	,
f4824	,
f4825	,
f4826	,
f4827	,
f4828	,
f4829	,
f4830	,
f4831	,
f4832	,
f4833	,
f4834	,
f4835	,
f4836	,
f4837	,
f4838	,
f4839	,
f4840	,
f4841	,
f4842	,
f4843	,
f4844	,
f4845	,
f4846	,
f4847	,
f4848	,
f4849	,
f4850	,
f4851	,
f4852	,
f4853	,
f4854	,
f4855	,
f4856	,
f4857	,
f4858	,
f4859	,
f4860	,
f4861	,
f4862	,
f4863	,
f4864	,
f4865	,
f4866	,
f4867	,
f4868	,
f4869	,
f4870	,
f4871	,
f4872	,
f4873	,
f4874	,
f4875	,
f4876	,
f4877	,
f4878	,
f4879	,
f4880	,
f4881	,
f4882	,
f4883	,
f4884	,
f4885	,
f4886	,
f4887	,
f4888	,
f4889	,
f4890	,
f4891	,
f4892	,
f4893	,
f4894	,
f4895	,
f4896	,
f4897	,
f4898	,
f4899	,
f4900	,
f4901	,
f4902	,
f4903	,
f4904	,
f4905	,
f4906	,
f4907	,
f4908	,
f4909	,
f4910	,
f4911	,
f4912	,
f4913	,
f4914	,
f4915	,
f4916	,
f4917	,
f4918	,
f4919	,
f4920	,
f4921	,
f4922	,
f4923	,
f4924	,
f4925	,
f4926	,
f4927	,
f4928	,
f4929	,
f4930	,
f4931	,
f4932	,
f4933	,
f4934	,
f4935	,
f4936	,
f4937	,
f4938	,
f4939	,
f4940	,
f4941	,
f4942	,
f4943	,
f4944	,
f4945	,
f4946	,
f4947	,
f4948	,
f4949	,
f4950	,
f4951	,
f4952	,
f4953	,
f4954	,
f4955	,
f4956	,
f4957	,
f4958	,
f4959	,
f4960	,
f4961	,
f4962	,
f4963	,
f4964	,
f4965	,
f4966	,
f4967	,
f4968	,
f4969	,
f4970	,
f4971	,
f4972	,
f4973	,
f4974	,
f4975	,
f4976	,
f4977	,
f4978	,
f4979	,
f4980	,
f4981	,
f4982	,
f4983	,
f4984	,
f4985	,
f4986	,
f4987	,
f4988	,
f4989	,
f4990	,
f4991	,
f4992	,
f4993	,
f4994	,
f4995	,
f4996	,
f4997	,
f4998	,
f4999	,
f5000	

};
 return array_value;
      }
 
  
	double	f0	=	0	;

	double	f1	=	t	;
	double	f2	=	t	;
	double	f3	=	t	;
	double	f4	=	t	;
	double	f5	=	t	;
	double	f6	=	t	;
	double	f7	=	t	;
	double	f8	=	t	;
	double	f9	=	t	;
	double	f10	=	t	;
	double	f11	=	t	;
	double	f12	=	t	;
	double	f13	=	t	;
	double	f14	=	t	;
	double	f15	=	t	;
	double	f16	=	t	;
	double	f17	=	t	;
	double	f18	=	t	;
	double	f19	=	t	;
	double	f20	=	t	;
	double	f21	=	t	;
	double	f22	=	t	;
	double	f23	=	t	;
	double	f24	=	t	;
	double	f25	=	t	;
	double	f26	=	t	;
	double	f27	=	t	;
	double	f28	=	t	;
	double	f29	=	t	;
	double	f30	=	t	;
	double	f31	=	t	;
	double	f32	=	t	;
	double	f33	=	t	;
	double	f34	=	t	;
	double	f35	=	t	;
	double	f36	=	t	;
	double	f37	=	t	;
	double	f38	=	t	;
	double	f39	=	t	;
	double	f40	=	t	;
	double	f41	=	t	;
	double	f42	=	t	;
	double	f43	=	t	;
	double	f44	=	t	;
	double	f45	=	t	;
	double	f46	=	t	;
	double	f47	=	t	;
	double	f48	=	t	;
	double	f49	=	t	;
	double	f50	=	t	;
	double	f51	=	t	;
	double	f52	=	t	;
	double	f53	=	t	;
	double	f54	=	t	;
	double	f55	=	t	;
	double	f56	=	t	;
	double	f57	=	t	;
	double	f58	=	t	;
	double	f59	=	t	;
	double	f60	=	t	;
	double	f61	=	t	;
	double	f62	=	t	;
	double	f63	=	t	;
	double	f64	=	t	;
	double	f65	=	t	;
	double	f66	=	t	;
	double	f67	=	t	;
	double	f68	=	t	;
	double	f69	=	t	;
	double	f70	=	t	;
	double	f71	=	t	;
	double	f72	=	t	;
	double	f73	=	t	;
	double	f74	=	t	;
	double	f75	=	t	;
	double	f76	=	t	;
	double	f77	=	t	;
	double	f78	=	t	;
	double	f79	=	t	;
	double	f80	=	t	;
	double	f81	=	t	;
	double	f82	=	t	;
	double	f83	=	t	;
	double	f84	=	t	;
	double	f85	=	t	;
	double	f86	=	t	;
	double	f87	=	t	;
	double	f88	=	t	;
	double	f89	=	t	;
	double	f90	=	t	;
	double	f91	=	t	;
	double	f92	=	t	;
	double	f93	=	t	;
	double	f94	=	t	;
	double	f95	=	t	;
	double	f96	=	t	;
	double	f97	=	t	;
	double	f98	=	t	;
	double	f99	=	t	;
	double	f100	=	t	;
	double	f101	=	t	;
	double	f102	=	t	;
	double	f103	=	t	;
	double	f104	=	t	;
	double	f105	=	t	;
	double	f106	=	t	;
	double	f107	=	t	;
	double	f108	=	t	;
	double	f109	=	t	;
	double	f110	=	t	;
	double	f111	=	t	;
	double	f112	=	t	;
	double	f113	=	t	;
	double	f114	=	t	;
	double	f115	=	t	;
	double	f116	=	t	;
	double	f117	=	t	;
	double	f118	=	t	;
	double	f119	=	t	;
	double	f120	=	t	;
	double	f121	=	t	;
	double	f122	=	t	;
	double	f123	=	t	;
	double	f124	=	t	;
	double	f125	=	t	;
	double	f126	=	t	;
	double	f127	=	t	;
	double	f128	=	t	;
	double	f129	=	t	;
	double	f130	=	t	;
	double	f131	=	t	;
	double	f132	=	t	;
	double	f133	=	t	;
	double	f134	=	t	;
	double	f135	=	t	;
	double	f136	=	t	;
	double	f137	=	t	;
	double	f138	=	t	;
	double	f139	=	t	;
	double	f140	=	t	;
	double	f141	=	t	;
	double	f142	=	t	;
	double	f143	=	t	;
	double	f144	=	t	;
	double	f145	=	t	;
	double	f146	=	t	;
	double	f147	=	t	;
	double	f148	=	t	;
	double	f149	=	t	;
	double	f150	=	t	;
	double	f151	=	t	;
	double	f152	=	t	;
	double	f153	=	t	;
	double	f154	=	t	;
	double	f155	=	t	;
	double	f156	=	t	;
	double	f157	=	t	;
	double	f158	=	t	;
	double	f159	=	t	;
	double	f160	=	t	;
	double	f161	=	t	;
	double	f162	=	t	;
	double	f163	=	t	;
	double	f164	=	t	;
	double	f165	=	t	;
	double	f166	=	t	;
	double	f167	=	t	;
	double	f168	=	t	;
	double	f169	=	t	;
	double	f170	=	t	;
	double	f171	=	t	;
	double	f172	=	t	;
	double	f173	=	t	;
	double	f174	=	t	;
	double	f175	=	t	;
	double	f176	=	t	;
	double	f177	=	t	;
	double	f178	=	t	;
	double	f179	=	t	;
	double	f180	=	t	;
	double	f181	=	t	;
	double	f182	=	t	;
	double	f183	=	t	;
	double	f184	=	t	;
	double	f185	=	t	;
	double	f186	=	t	;
	double	f187	=	t	;
	double	f188	=	t	;
	double	f189	=	t	;
	double	f190	=	t	;
	double	f191	=	t	;
	double	f192	=	t	;
	double	f193	=	t	;
	double	f194	=	t	;
	double	f195	=	t	;
	double	f196	=	t	;
	double	f197	=	t	;
	double	f198	=	t	;
	double	f199	=	t	;
	double	f200	=	t	;
	double	f201	=	t	;
	double	f202	=	t	;
	double	f203	=	t	;
	double	f204	=	t	;
	double	f205	=	t	;
	double	f206	=	t	;
	double	f207	=	t	;
	double	f208	=	t	;
	double	f209	=	t	;
	double	f210	=	t	;
	double	f211	=	t	;
	double	f212	=	t	;
	double	f213	=	t	;
	double	f214	=	t	;
	double	f215	=	t	;
	double	f216	=	t	;
	double	f217	=	t	;
	double	f218	=	t	;
	double	f219	=	t	;
	double	f220	=	t	;
	double	f221	=	t	;
	double	f222	=	t	;
	double	f223	=	t	;
	double	f224	=	t	;
	double	f225	=	t	;
	double	f226	=	t	;
	double	f227	=	t	;
	double	f228	=	t	;
	double	f229	=	t	;
	double	f230	=	t	;
	double	f231	=	t	;
	double	f232	=	t	;
	double	f233	=	t	;
	double	f234	=	t	;
	double	f235	=	t	;
	double	f236	=	t	;
	double	f237	=	t	;
	double	f238	=	t	;
	double	f239	=	t	;
	double	f240	=	t	;
	double	f241	=	t	;
	double	f242	=	t	;
	double	f243	=	t	;
	double	f244	=	t	;
	double	f245	=	t	;
	double	f246	=	t	;
	double	f247	=	t	;
	double	f248	=	t	;
	double	f249	=	t	;
	double	f250	=	t	;
	double	f251	=	t	;
	double	f252	=	t	;
	double	f253	=	t	;
	double	f254	=	t	;
	double	f255	=	t	;
	double	f256	=	t	;
	double	f257	=	t	;
	double	f258	=	t	;
	double	f259	=	t	;
	double	f260	=	t	;
	double	f261	=	t	;
	double	f262	=	t	;
	double	f263	=	t	;
	double	f264	=	t	;
	double	f265	=	t	;
	double	f266	=	t	;
	double	f267	=	t	;
	double	f268	=	t	;
	double	f269	=	t	;
	double	f270	=	t	;
	double	f271	=	t	;
	double	f272	=	t	;
	double	f273	=	t	;
	double	f274	=	t	;
	double	f275	=	t	;
	double	f276	=	t	;
	double	f277	=	t	;
	double	f278	=	t	;
	double	f279	=	t	;
	double	f280	=	t	;
	double	f281	=	t	;
	double	f282	=	t	;
	double	f283	=	t	;
	double	f284	=	t	;
	double	f285	=	t	;
	double	f286	=	t	;
	double	f287	=	t	;
	double	f288	=	t	;
	double	f289	=	t	;
	double	f290	=	t	;
	double	f291	=	t	;
	double	f292	=	t	;
	double	f293	=	t	;
	double	f294	=	t	;
	double	f295	=	t	;
	double	f296	=	t	;
	double	f297	=	t	;
	double	f298	=	t	;
	double	f299	=	t	;
	double	f300	=	t	;
	double	f301	=	t	;
	double	f302	=	t	;
	double	f303	=	t	;
	double	f304	=	t	;
	double	f305	=	t	;
	double	f306	=	t	;
	double	f307	=	t	;
	double	f308	=	t	;
	double	f309	=	t	;
	double	f310	=	t	;
	double	f311	=	t	;
	double	f312	=	t	;
	double	f313	=	t	;
	double	f314	=	t	;
	double	f315	=	t	;
	double	f316	=	t	;
	double	f317	=	t	;
	double	f318	=	t	;
	double	f319	=	t	;
	double	f320	=	t	;
	double	f321	=	t	;
	double	f322	=	t	;
	double	f323	=	t	;
	double	f324	=	t	;
	double	f325	=	t	;
	double	f326	=	t	;
	double	f327	=	t	;
	double	f328	=	t	;
	double	f329	=	t	;
	double	f330	=	t	;
	double	f331	=	t	;
	double	f332	=	t	;
	double	f333	=	t	;
	double	f334	=	t	;
	double	f335	=	t	;
	double	f336	=	t	;
	double	f337	=	t	;
	double	f338	=	t	;
	double	f339	=	t	;
	double	f340	=	t	;
	double	f341	=	t	;
	double	f342	=	t	;
	double	f343	=	t	;
	double	f344	=	t	;
	double	f345	=	t	;
	double	f346	=	t	;
	double	f347	=	t	;
	double	f348	=	t	;
	double	f349	=	t	;
	double	f350	=	t	;
	double	f351	=	t	;
	double	f352	=	t	;
	double	f353	=	t	;
	double	f354	=	t	;
	double	f355	=	t	;
	double	f356	=	t	;
	double	f357	=	t	;
	double	f358	=	t	;
	double	f359	=	t	;
	double	f360	=	t	;
	double	f361	=	t	;
	double	f362	=	t	;
	double	f363	=	t	;
	double	f364	=	t	;
	double	f365	=	t	;
	double	f366	=	t	;
	double	f367	=	t	;
	double	f368	=	t	;
	double	f369	=	t	;
	double	f370	=	t	;
	double	f371	=	t	;
	double	f372	=	t	;
	double	f373	=	t	;
	double	f374	=	t	;
	double	f375	=	t	;
	double	f376	=	t	;
	double	f377	=	t	;
	double	f378	=	t	;
	double	f379	=	t	;
	double	f380	=	t	;
	double	f381	=	t	;
	double	f382	=	t	;
	double	f383	=	t	;
	double	f384	=	t	;
	double	f385	=	t	;
	double	f386	=	t	;
	double	f387	=	t	;
	double	f388	=	t	;
	double	f389	=	t	;
	double	f390	=	t	;
	double	f391	=	t	;
	double	f392	=	t	;
	double	f393	=	t	;
	double	f394	=	t	;
	double	f395	=	t	;
	double	f396	=	t	;
	double	f397	=	t	;
	double	f398	=	t	;
	double	f399	=	t	;
	double	f400	=	t	;
	double	f401	=	t	;
	double	f402	=	t	;
	double	f403	=	t	;
	double	f404	=	t	;
	double	f405	=	t	;
	double	f406	=	t	;
	double	f407	=	t	;
	double	f408	=	t	;
	double	f409	=	t	;
	double	f410	=	t	;
	double	f411	=	t	;
	double	f412	=	t	;
	double	f413	=	t	;
	double	f414	=	t	;
	double	f415	=	t	;
	double	f416	=	t	;
	double	f417	=	t	;
	double	f418	=	t	;
	double	f419	=	t	;
	double	f420	=	t	;
	double	f421	=	t	;
	double	f422	=	t	;
	double	f423	=	t	;
	double	f424	=	t	;
	double	f425	=	t	;
	double	f426	=	t	;
	double	f427	=	t	;
	double	f428	=	t	;
	double	f429	=	t	;
	double	f430	=	t	;
	double	f431	=	t	;
	double	f432	=	t	;
	double	f433	=	t	;
	double	f434	=	t	;
	double	f435	=	t	;
	double	f436	=	t	;
	double	f437	=	t	;
	double	f438	=	t	;
	double	f439	=	t	;
	double	f440	=	t	;
	double	f441	=	t	;
	double	f442	=	t	;
	double	f443	=	t	;
	double	f444	=	t	;
	double	f445	=	t	;
	double	f446	=	t	;
	double	f447	=	t	;
	double	f448	=	t	;
	double	f449	=	t	;
	double	f450	=	t	;
	double	f451	=	t	;
	double	f452	=	t	;
	double	f453	=	t	;
	double	f454	=	t	;
	double	f455	=	t	;
	double	f456	=	t	;
	double	f457	=	t	;
	double	f458	=	t	;
	double	f459	=	t	;
	double	f460	=	t	;
	double	f461	=	t	;
	double	f462	=	t	;
	double	f463	=	t	;
	double	f464	=	t	;
	double	f465	=	t	;
	double	f466	=	t	;
	double	f467	=	t	;
	double	f468	=	t	;
	double	f469	=	t	;
	double	f470	=	t	;
	double	f471	=	t	;
	double	f472	=	t	;
	double	f473	=	t	;
	double	f474	=	t	;
	double	f475	=	t	;
	double	f476	=	t	;
	double	f477	=	t	;
	double	f478	=	t	;
	double	f479	=	t	;
	double	f480	=	t	;
	double	f481	=	t	;
	double	f482	=	t	;
	double	f483	=	t	;
	double	f484	=	t	;
	double	f485	=	t	;
	double	f486	=	t	;
	double	f487	=	t	;
	double	f488	=	t	;
	double	f489	=	t	;
	double	f490	=	t	;
	double	f491	=	t	;
	double	f492	=	t	;
	double	f493	=	t	;
	double	f494	=	t	;
	double	f495	=	t	;
	double	f496	=	t	;
	double	f497	=	t	;
	double	f498	=	t	;
	double	f499	=	t	;
	double	f500	=	t	;
	double	f501	=	t	;
	double	f502	=	t	;
	double	f503	=	t	;
	double	f504	=	t	;
	double	f505	=	t	;
	double	f506	=	t	;
	double	f507	=	t	;
	double	f508	=	t	;
	double	f509	=	t	;
	double	f510	=	t	;
	double	f511	=	t	;
	double	f512	=	t	;
	double	f513	=	t	;
	double	f514	=	t	;
	double	f515	=	t	;
	double	f516	=	t	;
	double	f517	=	t	;
	double	f518	=	t	;
	double	f519	=	t	;
	double	f520	=	t	;
	double	f521	=	t	;
	double	f522	=	t	;
	double	f523	=	t	;
	double	f524	=	t	;
	double	f525	=	t	;
	double	f526	=	t	;
	double	f527	=	t	;
	double	f528	=	t	;
	double	f529	=	t	;
	double	f530	=	t	;
	double	f531	=	t	;
	double	f532	=	t	;
	double	f533	=	t	;
	double	f534	=	t	;
	double	f535	=	t	;
	double	f536	=	t	;
	double	f537	=	t	;
	double	f538	=	t	;
	double	f539	=	t	;
	double	f540	=	t	;
	double	f541	=	t	;
	double	f542	=	t	;
	double	f543	=	t	;
	double	f544	=	t	;
	double	f545	=	t	;
	double	f546	=	t	;
	double	f547	=	t	;
	double	f548	=	t	;
	double	f549	=	t	;
	double	f550	=	t	;
	double	f551	=	t	;
	double	f552	=	t	;
	double	f553	=	t	;
	double	f554	=	t	;
	double	f555	=	t	;
	double	f556	=	t	;
	double	f557	=	t	;
	double	f558	=	t	;
	double	f559	=	t	;
	double	f560	=	t	;
	double	f561	=	t	;
	double	f562	=	t	;
	double	f563	=	t	;
	double	f564	=	t	;
	double	f565	=	t	;
	double	f566	=	t	;
	double	f567	=	t	;
	double	f568	=	t	;
	double	f569	=	t	;
	double	f570	=	t	;
	double	f571	=	t	;
	double	f572	=	t	;
	double	f573	=	t	;
	double	f574	=	t	;
	double	f575	=	t	;
	double	f576	=	t	;
	double	f577	=	t	;
	double	f578	=	t	;
	double	f579	=	t	;
	double	f580	=	t	;
	double	f581	=	t	;
	double	f582	=	t	;
	double	f583	=	t	;
	double	f584	=	t	;
	double	f585	=	t	;
	double	f586	=	t	;
	double	f587	=	t	;
	double	f588	=	t	;
	double	f589	=	t	;
	double	f590	=	t	;
	double	f591	=	t	;
	double	f592	=	t	;
	double	f593	=	t	;
	double	f594	=	t	;
	double	f595	=	t	;
	double	f596	=	t	;
	double	f597	=	t	;
	double	f598	=	t	;
	double	f599	=	t	;
	double	f600	=	t	;
	double	f601	=	t	;
	double	f602	=	t	;
	double	f603	=	t	;
	double	f604	=	t	;
	double	f605	=	t	;
	double	f606	=	t	;
	double	f607	=	t	;
	double	f608	=	t	;
	double	f609	=	t	;
	double	f610	=	t	;
	double	f611	=	t	;
	double	f612	=	t	;
	double	f613	=	t	;
	double	f614	=	t	;
	double	f615	=	t	;
	double	f616	=	t	;
	double	f617	=	t	;
	double	f618	=	t	;
	double	f619	=	t	;
	double	f620	=	t	;
	double	f621	=	t	;
	double	f622	=	t	;
	double	f623	=	t	;
	double	f624	=	t	;
	double	f625	=	t	;
	double	f626	=	t	;
	double	f627	=	t	;
	double	f628	=	t	;
	double	f629	=	t	;
	double	f630	=	t	;
	double	f631	=	t	;
	double	f632	=	t	;
	double	f633	=	t	;
	double	f634	=	t	;
	double	f635	=	t	;
	double	f636	=	t	;
	double	f637	=	t	;
	double	f638	=	t	;
	double	f639	=	t	;
	double	f640	=	t	;
	double	f641	=	t	;
	double	f642	=	t	;
	double	f643	=	t	;
	double	f644	=	t	;
	double	f645	=	t	;
	double	f646	=	t	;
	double	f647	=	t	;
	double	f648	=	t	;
	double	f649	=	t	;
	double	f650	=	t	;
	double	f651	=	t	;
	double	f652	=	t	;
	double	f653	=	t	;
	double	f654	=	t	;
	double	f655	=	t	;
	double	f656	=	t	;
	double	f657	=	t	;
	double	f658	=	t	;
	double	f659	=	t	;
	double	f660	=	t	;
	double	f661	=	t	;
	double	f662	=	t	;
	double	f663	=	t	;
	double	f664	=	t	;
	double	f665	=	t	;
	double	f666	=	t	;
	double	f667	=	t	;
	double	f668	=	t	;
	double	f669	=	t	;
	double	f670	=	t	;
	double	f671	=	t	;
	double	f672	=	t	;
	double	f673	=	t	;
	double	f674	=	t	;
	double	f675	=	t	;
	double	f676	=	t	;
	double	f677	=	t	;
	double	f678	=	t	;
	double	f679	=	t	;
	double	f680	=	t	;
	double	f681	=	t	;
	double	f682	=	t	;
	double	f683	=	t	;
	double	f684	=	t	;
	double	f685	=	t	;
	double	f686	=	t	;
	double	f687	=	t	;
	double	f688	=	t	;
	double	f689	=	t	;
	double	f690	=	t	;
	double	f691	=	t	;
	double	f692	=	t	;
	double	f693	=	t	;
	double	f694	=	t	;
	double	f695	=	t	;
	double	f696	=	t	;
	double	f697	=	t	;
	double	f698	=	t	;
	double	f699	=	t	;
	double	f700	=	t	;
	double	f701	=	t	;
	double	f702	=	t	;
	double	f703	=	t	;
	double	f704	=	t	;
	double	f705	=	t	;
	double	f706	=	t	;
	double	f707	=	t	;
	double	f708	=	t	;
	double	f709	=	t	;
	double	f710	=	t	;
	double	f711	=	t	;
	double	f712	=	t	;
	double	f713	=	t	;
	double	f714	=	t	;
	double	f715	=	t	;
	double	f716	=	t	;
	double	f717	=	t	;
	double	f718	=	t	;
	double	f719	=	t	;
	double	f720	=	t	;
	double	f721	=	t	;
	double	f722	=	t	;
	double	f723	=	t	;
	double	f724	=	t	;
	double	f725	=	t	;
	double	f726	=	t	;
	double	f727	=	t	;
	double	f728	=	t	;
	double	f729	=	t	;
	double	f730	=	t	;
	double	f731	=	t	;
	double	f732	=	t	;
	double	f733	=	t	;
	double	f734	=	t	;
	double	f735	=	t	;
	double	f736	=	t	;
	double	f737	=	t	;
	double	f738	=	t	;
	double	f739	=	t	;
	double	f740	=	t	;
	double	f741	=	t	;
	double	f742	=	t	;
	double	f743	=	t	;
	double	f744	=	t	;
	double	f745	=	t	;
	double	f746	=	t	;
	double	f747	=	t	;
	double	f748	=	t	;
	double	f749	=	t	;
	double	f750	=	t	;
	double	f751	=	t	;
	double	f752	=	t	;
	double	f753	=	t	;
	double	f754	=	t	;
	double	f755	=	t	;
	double	f756	=	t	;
	double	f757	=	t	;
	double	f758	=	t	;
	double	f759	=	t	;
	double	f760	=	t	;
	double	f761	=	t	;
	double	f762	=	t	;
	double	f763	=	t	;
	double	f764	=	t	;
	double	f765	=	t	;
	double	f766	=	t	;
	double	f767	=	t	;
	double	f768	=	t	;
	double	f769	=	t	;
	double	f770	=	t	;
	double	f771	=	t	;
	double	f772	=	t	;
	double	f773	=	t	;
	double	f774	=	t	;
	double	f775	=	t	;
	double	f776	=	t	;
	double	f777	=	t	;
	double	f778	=	t	;
	double	f779	=	t	;
	double	f780	=	t	;
	double	f781	=	t	;
	double	f782	=	t	;
	double	f783	=	t	;
	double	f784	=	t	;
	double	f785	=	t	;
	double	f786	=	t	;
	double	f787	=	t	;
	double	f788	=	t	;
	double	f789	=	t	;
	double	f790	=	t	;
	double	f791	=	t	;
	double	f792	=	t	;
	double	f793	=	t	;
	double	f794	=	t	;
	double	f795	=	t	;
	double	f796	=	t	;
	double	f797	=	t	;
	double	f798	=	t	;
	double	f799	=	t	;
	double	f800	=	t	;
	double	f801	=	t	;
	double	f802	=	t	;
	double	f803	=	t	;
	double	f804	=	t	;
	double	f805	=	t	;
	double	f806	=	t	;
	double	f807	=	t	;
	double	f808	=	t	;
	double	f809	=	t	;
	double	f810	=	t	;
	double	f811	=	t	;
	double	f812	=	t	;
	double	f813	=	t	;
	double	f814	=	t	;
	double	f815	=	t	;
	double	f816	=	t	;
	double	f817	=	t	;
	double	f818	=	t	;
	double	f819	=	t	;
	double	f820	=	t	;
	double	f821	=	t	;
	double	f822	=	t	;
	double	f823	=	t	;
	double	f824	=	t	;
	double	f825	=	t	;
	double	f826	=	t	;
	double	f827	=	t	;
	double	f828	=	t	;
	double	f829	=	t	;
	double	f830	=	t	;
	double	f831	=	t	;
	double	f832	=	t	;
	double	f833	=	t	;
	double	f834	=	t	;
	double	f835	=	t	;
	double	f836	=	t	;
	double	f837	=	t	;
	double	f838	=	t	;
	double	f839	=	t	;
	double	f840	=	t	;
	double	f841	=	t	;
	double	f842	=	t	;
	double	f843	=	t	;
	double	f844	=	t	;
	double	f845	=	t	;
	double	f846	=	t	;
	double	f847	=	t	;
	double	f848	=	t	;
	double	f849	=	t	;
	double	f850	=	t	;
	double	f851	=	t	;
	double	f852	=	t	;
	double	f853	=	t	;
	double	f854	=	t	;
	double	f855	=	t	;
	double	f856	=	t	;
	double	f857	=	t	;
	double	f858	=	t	;
	double	f859	=	t	;
	double	f860	=	t	;
	double	f861	=	t	;
	double	f862	=	t	;
	double	f863	=	t	;
	double	f864	=	t	;
	double	f865	=	t	;
	double	f866	=	t	;
	double	f867	=	t	;
	double	f868	=	t	;
	double	f869	=	t	;
	double	f870	=	t	;
	double	f871	=	t	;
	double	f872	=	t	;
	double	f873	=	t	;
	double	f874	=	t	;
	double	f875	=	t	;
	double	f876	=	t	;
	double	f877	=	t	;
	double	f878	=	t	;
	double	f879	=	t	;
	double	f880	=	t	;
	double	f881	=	t	;
	double	f882	=	t	;
	double	f883	=	t	;
	double	f884	=	t	;
	double	f885	=	t	;
	double	f886	=	t	;
	double	f887	=	t	;
	double	f888	=	t	;
	double	f889	=	t	;
	double	f890	=	t	;
	double	f891	=	t	;
	double	f892	=	t	;
	double	f893	=	t	;
	double	f894	=	t	;
	double	f895	=	t	;
	double	f896	=	t	;
	double	f897	=	t	;
	double	f898	=	t	;
	double	f899	=	t	;
	double	f900	=	t	;
	double	f901	=	t	;
	double	f902	=	t	;
	double	f903	=	t	;
	double	f904	=	t	;
	double	f905	=	t	;
	double	f906	=	t	;
	double	f907	=	t	;
	double	f908	=	t	;
	double	f909	=	t	;
	double	f910	=	t	;
	double	f911	=	t	;
	double	f912	=	t	;
	double	f913	=	t	;
	double	f914	=	t	;
	double	f915	=	t	;
	double	f916	=	t	;
	double	f917	=	t	;
	double	f918	=	t	;
	double	f919	=	t	;
	double	f920	=	t	;
	double	f921	=	t	;
	double	f922	=	t	;
	double	f923	=	t	;
	double	f924	=	t	;
	double	f925	=	t	;
	double	f926	=	t	;
	double	f927	=	t	;
	double	f928	=	t	;
	double	f929	=	t	;
	double	f930	=	t	;
	double	f931	=	t	;
	double	f932	=	t	;
	double	f933	=	t	;
	double	f934	=	t	;
	double	f935	=	t	;
	double	f936	=	t	;
	double	f937	=	t	;
	double	f938	=	t	;
	double	f939	=	t	;
	double	f940	=	t	;
	double	f941	=	t	;
	double	f942	=	t	;
	double	f943	=	t	;
	double	f944	=	t	;
	double	f945	=	t	;
	double	f946	=	t	;
	double	f947	=	t	;
	double	f948	=	t	;
	double	f949	=	t	;
	double	f950	=	t	;
	double	f951	=	t	;
	double	f952	=	t	;
	double	f953	=	t	;
	double	f954	=	t	;
	double	f955	=	t	;
	double	f956	=	t	;
	double	f957	=	t	;
	double	f958	=	t	;
	double	f959	=	t	;
	double	f960	=	t	;
	double	f961	=	t	;
	double	f962	=	t	;
	double	f963	=	t	;
	double	f964	=	t	;
	double	f965	=	t	;
	double	f966	=	t	;
	double	f967	=	t	;
	double	f968	=	t	;
	double	f969	=	t	;
	double	f970	=	t	;
	double	f971	=	t	;
	double	f972	=	t	;
	double	f973	=	t	;
	double	f974	=	t	;
	double	f975	=	t	;
	double	f976	=	t	;
	double	f977	=	t	;
	double	f978	=	t	;
	double	f979	=	t	;
	double	f980	=	t	;
	double	f981	=	t	;
	double	f982	=	t	;
	double	f983	=	t	;
	double	f984	=	t	;
	double	f985	=	t	;
	double	f986	=	t	;
	double	f987	=	t	;
	double	f988	=	t	;
	double	f989	=	t	;
	double	f990	=	t	;
	double	f991	=	t	;
	double	f992	=	t	;
	double	f993	=	t	;
	double	f994	=	t	;
	double	f995	=	t	;
	double	f996	=	t	;
	double	f997	=	t	;
	double	f998	=	t	;
	double	f999	=	t	;
	double	f1000	=	t	;
	double	f1001	=	t	;
	double	f1002	=	t	;
	double	f1003	=	t	;
	double	f1004	=	t	;
	double	f1005	=	t	;
	double	f1006	=	t	;
	double	f1007	=	t	;
	double	f1008	=	t	;
	double	f1009	=	t	;
	double	f1010	=	t	;
	double	f1011	=	t	;
	double	f1012	=	t	;
	double	f1013	=	t	;
	double	f1014	=	t	;
	double	f1015	=	t	;
	double	f1016	=	t	;
	double	f1017	=	t	;
	double	f1018	=	t	;
	double	f1019	=	t	;
	double	f1020	=	t	;
	double	f1021	=	t	;
	double	f1022	=	t	;
	double	f1023	=	t	;
	double	f1024	=	t	;
	double	f1025	=	t	;
	double	f1026	=	t	;
	double	f1027	=	t	;
	double	f1028	=	t	;
	double	f1029	=	t	;
	double	f1030	=	t	;
	double	f1031	=	t	;
	double	f1032	=	t	;
	double	f1033	=	t	;
	double	f1034	=	t	;
	double	f1035	=	t	;
	double	f1036	=	t	;
	double	f1037	=	t	;
	double	f1038	=	t	;
	double	f1039	=	t	;
	double	f1040	=	t	;
	double	f1041	=	t	;
	double	f1042	=	t	;
double f1043 = t;
    double f1044 = t;
    double f1045 = t;
    double f1046 = t;
    double f1047 = t;
    double f1048 = t;
    double f1049 = t;
    double f1050 = t;
    double f1051 = t;
    double f1052 = t;
    double f1053 = t;
    double f1054 = t;
    double f1055 = t;
    double f1056 = t;
    double f1057 = t;
    double f1058 = t;
    double f1059 = t;
    double f1060 = t;
    double f1061 = t;
    double f1062 = t;
    double f1063 = t;
    double f1064 = t;
    double f1065 = t;
    double f1066 = t;
    double f1067 = t;
    double f1068 = t;
    double f1069 = t;
    double f1070 = t;
    double f1071 = t;
    double f1072 = t;
    double f1073 = t;
    double f1074 = t;
    double f1075 = t;
    double f1076 = t;
    double f1077 = t;
    double f1078 = t;
    double f1079 = t;
    double f1080 = t;
    double f1081 = t;
    double f1082 = t;
    double f1083 = t;
    double f1084 = t;
    double f1085 = t;
    double f1086 = t;
    double f1087 = t;
    double f1088 = t;
    double f1089 = t;
    double f1090 = t;
    double f1091 = t;
    double f1092 = t;
    double f1093 = t;
    double f1094 = t;
    double f1095 = t;
    double f1096 = t;
    double f1097 = t;
    double f1098 = t;
    double f1099 = t;
    double f1100 = t;
    double f1101 = t;
    double f1102 = t;
    double f1103 = t;
    double f1104 = t;
    double f1105 = t;
    double f1106 = t;
    double f1107 = t;
    double f1108 = t;
    double f1109 = t;
    double f1110 = t;
    double f1111 = t;
    double f1112 = t;
    double f1113 = t;
    double f1114 = t;
    double f1115 = t;
    double f1116 = t;
    double f1117 = t;
    double f1118 = t;
    double f1119 = t;
    double f1120 = t;
    double f1121 = t;
    double f1122 = t;
    double f1123 = t;
    double f1124 = t;
    double f1125 = t;
    double f1126 = t;
    double f1127 = t;
    double f1128 = t;
    double f1129 = t;
    double f1130 = t;
    double f1131 = t;
    double f1132 = t;
    double f1133 = t;
    double f1134 = t;
    double f1135 = t;
    double f1136 = t;
    double f1137 = t;
    double f1138 = t;
    double f1139 = t;
    double f1140 = t;
    double f1141 = t;
    double f1142 = t;
    double f1143 = t;
    double f1144 = t;
    double f1145 = t;
    double f1146 = t;
    double f1147 = t;
    double f1148 = t;
    double f1149 = t;
    double f1150 = t;
    double f1151 = t;
    double f1152 = t;
    double f1153 = t;
    double f1154 = t;
    double f1155 = t;
    double f1156 = t;
    double f1157 = t;
    double f1158 = t;
    double f1159 = t;
    double f1160 = t;
    double f1161 = t;
    double f1162 = t;
    double f1163 = t;
    double f1164 = t;
    double f1165 = t;
    double f1166 = t;
    double f1167 = t;
    double f1168 = t;
    double f1169 = t;
    double f1170 = t;
    double f1171 = t;
    double f1172 = t;
    double f1173 = t;
    double f1174 = t;
    double f1175 = t;
    double f1176 = t;
    double f1177 = t;
    double f1178 = t;
    double f1179 = t;
    double f1180 = t;
    double f1181 = t;
    double f1182 = t;
    double f1183 = t;
    double f1184 = t;
    double f1185 = t;
    double f1186 = t;
    double f1187 = t;
    double f1188 = t;
    double f1189 = t;
    double f1190 = t;
    double f1191 = t;
    double f1192 = t;
    double f1193 = t;
    double f1194 = t;
    double f1195 = t;
    double f1196 = t;
    double f1197 = t;
    double f1198 = t;
    double f1199 = t;
    double f1200 = t;
    double f1201 = t;
    double f1202 = t;
    double f1203 = t;
    double f1204 = t;
    double f1205 = t;
    double f1206 = t;
    double f1207 = t;
    double f1208 = t;
    double f1209 = t;
    double f1210 = t;
    double f1211 = t;
    double f1212 = t;
    double f1213 = t;
    double f1214 = t;
    double f1215 = t;
    double f1216 = t;
    double f1217 = t;
    double f1218 = t;
    double f1219 = t;
    double f1220 = t;
    double f1221 = t;
    double f1222 = t;
    double f1223 = t;
    double f1224 = t;
    double f1225 = t;
    double f1226 = t;
    double f1227 = t;
    double f1228 = t;
    double f1229 = t;
    double f1230 = t;
    double f1231 = t;
    double f1232 = t;
    double f1233 = t;
    double f1234 = t;
    double f1235 = t;
    double f1236 = t;
    double f1237 = t;
    double f1238 = t;
    double f1239 = t;
    double f1240 = t;
    double f1241 = t;
    double f1242 = t;
    double f1243 = t;
    double f1244 = t;
    double f1245 = t;
    double f1246 = t;
    double f1247 = t;
    double f1248 = t;
    double f1249 = t;
    double f1250 = t;
    double f1251 = t;
    double f1252 = t;
    double f1253 = t;
    double f1254 = t;
    double f1255 = t;
    double f1256 = t;
    double f1257 = t;
    double f1258 = t;
    double f1259 = t;
    double f1260 = t;
    double f1261 = t;
    double f1262 = t;
    double f1263 = t;
    double f1264 = t;
    double f1265 = t;
    double f1266 = t;
    double f1267 = t;
    double f1268 = t;
    double f1269 = t;
    double f1270 = t;
    double f1271 = t;
    double f1272 = t;
    double f1273 = t;
    double f1274 = t;
    double f1275 = t;
    double f1276 = t;
    double f1277 = t;
    double f1278 = t;
    double f1279 = t;
    double f1280 = t;
    double f1281 = t;
    double f1282 = t;
    double f1283 = t;
    double f1284 = t;
    double f1285 = t;
    double f1286 = t;
    double f1287 = t;
    double f1288 = t;
    double f1289 = t;
    double f1290 = t;
    double f1291 = t;
    double f1292 = t;
    double f1293 = t;
    double f1294 = t;
    double f1295 = t;
    double f1296 = t;
    double f1297 = t;
    double f1298 = t;
    double f1299 = t;
    double f1300 = t;
    double f1301 = t;
    double f1302 = t;
    double f1303 = t;
    double f1304 = t;
    double f1305 = t;
    double f1306 = t;
    double f1307 = t;
    double f1308 = t;
    double f1309 = t;
    double f1310 = t;
    double f1311 = t;
    double f1312 = t;
    double f1313 = t;
    double f1314 = t;
    double f1315 = t;
    double f1316 = t;
    double f1317 = t;
    double f1318 = t;
    double f1319 = t;
    double f1320 = t;
    double f1321 = t;
    double f1322 = t;
    double f1323 = t;
    double f1324 = t;
    double f1325 = t;
    double f1326 = t;
    double f1327 = t;
    double f1328 = t;
    double f1329 = t;
    double f1330 = t;
    double f1331 = t;
    double f1332 = t;
    double f1333 = t;
    double f1334 = t;
    double f1335 = t;
    double f1336 = t;
    double f1337 = t;
    double f1338 = t;
    double f1339 = t;
    double f1340 = t;
    double f1341 = t;
    double f1342 = t;
    double f1343 = t;
    double f1344 = t;
    double f1345 = t;
    double f1346 = t;
    double f1347 = t;
    double f1348 = t;
    double f1349 = t;
    double f1350 = t;
    double f1351 = t;
    double f1352 = t;
    double f1353 = t;
    double f1354 = t;
    double f1355 = t;
    double f1356 = t;
    double f1357 = t;
    double f1358 = t;
    double f1359 = t;
    double f1360 = t;
    double f1361 = t;
    double f1362 = t;
    double f1363 = t;
    double f1364 = t;
    double f1365 = t;
    double f1366 = t;
    double f1367 = t;
    double f1368 = t;
    double f1369 = t;
    double f1370 = t;
    double f1371 = t;
    double f1372 = t;
    double f1373 = t;
    double f1374 = t;
    double f1375 = t;
    double f1376 = t;
    double f1377 = t;
    double f1378 = t;
    double f1379 = t;
    double f1380 = t;
    double f1381 = t;
    double f1382 = t;
    double f1383 = t;
    double f1384 = t;
    double f1385 = t;
    double f1386 = t;
    double f1387 = t;
    double f1388 = t;
    double f1389 = t;
    double f1390 = t;
    double f1391 = t;
    double f1392 = t;
    double f1393 = t;
    double f1394 = t;
    double f1395 = t;
    double f1396 = t;
    double f1397 = t;
    double f1398 = t;
    double f1399 = t;
    double f1400 = t;
    double f1401 = t;
    double f1402 = t;
    double f1403 = t;
    double f1404 = t;
    double f1405 = t;
    double f1406 = t;
    double f1407 = t;
    double f1408 = t;
    double f1409 = t;
    double f1410 = t;
    double f1411 = t;
    double f1412 = t;
    double f1413 = t;
    double f1414 = t;
    double f1415 = t;
    double f1416 = t;
    double f1417 = t;
    double f1418 = t;
    double f1419 = t;
    double f1420 = t;
    double f1421 = t;
    double f1422 = t;
    double f1423 = t;
    double f1424 = t;
    double f1425 = t;
    double f1426 = t;
    double f1427 = t;
    double f1428 = t;
    double f1429 = t;
    double f1430 = t;
    double f1431 = t;
    double f1432 = t;
    double f1433 = t;
    double f1434 = t;
    double f1435 = t;
    double f1436 = t;
    double f1437 = t;
    double f1438 = t;
    double f1439 = t;
    double f1440 = t;
    double f1441 = t;
    double f1442 = t;
    double f1443 = t;
    double f1444 = t;
    double f1445 = t;
    double f1446 = t;
    double f1447 = t;
    double f1448 = t;
    double f1449 = t;
    double f1450 = t;
    double f1451 = t;
    double f1452 = t;
    double f1453 = t;
    double f1454 = t;
    double f1455 = t;
    double f1456 = t;
    double f1457 = t;
    double f1458 = t;
    double f1459 = t;
    double f1460 = t;
    double f1461 = t;
    double f1462 = t;
    double f1463 = t;
    double f1464 = t;
    double f1465 = t;
    double f1466 = t;
    double f1467 = t;
    double f1468 = t;
    double f1469 = t;
    double f1470 = t;
    double f1471 = t;
    double f1472 = t;
    double f1473 = t;
    double f1474 = t;
    double f1475 = t;
    double f1476 = t;
    double f1477 = t;
    double f1478 = t;
    double f1479 = t;
    double f1480 = t;
    double f1481 = t;
    double f1482 = t;
    double f1483 = t;
    double f1484 = t;
    double f1485 = t;
    double f1486 = t;
    double f1487 = t;
    double f1488 = t;
    double f1489 = t;
    double f1490 = t;
    double f1491 = t;
    double f1492 = t;
    double f1493 = t;
    double f1494 = t;
    double f1495 = t;
    double f1496 = t;
    double f1497 = t;
    double f1498 = t;
    double f1499 = t;
    double f1500 = t;
    double f1501 = t;
    double f1502 = t;
    double f1503 = t;
    double f1504 = t;
    double f1505 = t;
    double f1506 = t;
    double f1507 = t;
    double f1508 = t;
    double f1509 = t;
    double f1510 = t;
    double f1511 = t;
    double f1512 = t;
    double f1513 = t;
    double f1514 = t;
    double f1515 = t;
    double f1516 = t;
    double f1517 = t;
    double f1518 = t;
    double f1519 = t;
    double f1520 = t;
    double f1521 = t;
    double f1522 = t;
    double f1523 = t;
    double f1524 = t;
    double f1525 = t;
    double f1526 = t;
    double f1527 = t;
    double f1528 = t;
    double f1529 = t;
    double f1530 = t;
    double f1531 = t;
    double f1532 = t;
    double f1533 = t;
    double f1534 = t;
    double f1535 = t;
    double f1536 = t;
    double f1537 = t;
    double f1538 = t;
    double f1539 = t;
    double f1540 = t;
    double f1541 = t;
    double f1542 = t;
    double f1543 = t;
    double f1544 = t;
    double f1545 = t;
    double f1546 = t;
    double f1547 = t;
    double f1548 = t;
    double f1549 = t;
    double f1550 = t;
    double f1551 = t;
    double f1552 = t;
    double f1553 = t;
    double f1554 = t;
    double f1555 = t;
    double f1556 = t;
    double f1557 = t;
    double f1558 = t;
    double f1559 = t;
    double f1560 = t;
    double f1561 = t;
    double f1562 = t;
    double f1563 = t;
    double f1564 = t;
    double f1565 = t;
    double f1566 = t;
    double f1567 = t;
    double f1568 = t;
    double f1569 = t;
    double f1570 = t;
    double f1571 = t;
    double f1572 = t;
    double f1573 = t;
    double f1574 = t;
    double f1575 = t;
    double f1576 = t;
    double f1577 = t;
    double f1578 = t;
    double f1579 = t;
    double f1580 = t;
    double f1581 = t;
    double f1582 = t;
    double f1583 = t;
    double f1584 = t;
    double f1585 = t;
    double f1586 = t;
    double f1587 = t;
    double f1588 = t;
    double f1589 = t;
    double f1590 = t;
    double f1591 = t;
    double f1592 = t;
    double f1593 = t;
    double f1594 = t;
    double f1595 = t;
    double f1596 = t;
    double f1597 = t;
    double f1598 = t;
    double f1599 = t;
    double f1600 = t;
    double f1601 = t;
    double f1602 = t;
    double f1603 = t;
    double f1604 = t;
    double f1605 = t;
    double f1606 = t;
    double f1607 = t;
    double f1608 = t;
    double f1609 = t;
    double f1610 = t;
    double f1611 = t;
    double f1612 = t;
    double f1613 = t;
    double f1614 = t;
    double f1615 = t;
    double f1616 = t;
    double f1617 = t;
    double f1618 = t;
    double f1619 = t;
    double f1620 = t;
    double f1621 = t;
    double f1622 = t;
    double f1623 = t;
    double f1624 = t;
    double f1625 = t;
    double f1626 = t;
    double f1627 = t;
    double f1628 = t;
    double f1629 = t;
    double f1630 = t;
    double f1631 = t;
    double f1632 = t;
    double f1633 = t;
    double f1634 = t;
    double f1635 = t;
    double f1636 = t;
    double f1637 = t;
    double f1638 = t;
    double f1639 = t;
    double f1640 = t;
    double f1641 = t;
    double f1642 = t;
    double f1643 = t;
    double f1644 = t;
    double f1645 = t;
    double f1646 = t;
    double f1647 = t;
    double f1648 = t;
    double f1649 = t;
    double f1650 = t;
    double f1651 = t;
    double f1652 = t;
    double f1653 = t;
    double f1654 = t;
    double f1655 = t;
    double f1656 = t;
    double f1657 = t;
    double f1658 = t;
    double f1659 = t;
    double f1660 = t;
    double f1661 = t;
    double f1662 = t;
    double f1663 = t;
    double f1664 = t;
    double f1665 = t;
    double f1666 = t;
    double f1667 = t;
    double f1668 = t;
    double f1669 = t;
    double f1670 = t;
    double f1671 = t;
    double f1672 = t;
    double f1673 = t;
    double f1674 = t;
    double f1675 = t;
    double f1676 = t;
    double f1677 = t;
    double f1678 = t;
    double f1679 = t;
    double f1680 = t;
    double f1681 = t;
    double f1682 = t;
    double f1683 = t;
    double f1684 = t;
    double f1685 = t;
    double f1686 = t;
    double f1687 = t;
    double f1688 = t;
    double f1689 = t;
    double f1690 = t;
    double f1691 = t;
    double f1692 = t;
    double f1693 = t;
    double f1694 = t;
    double f1695 = t;
    double f1696 = t;
    double f1697 = t;
    double f1698 = t;
    double f1699 = t;
    double f1700 = t;
    double f1701 = t;
    double f1702 = t;
    double f1703 = t;
    double f1704 = t;
    double f1705 = t;
    double f1706 = t;
    double f1707 = t;
    double f1708 = t;
    double f1709 = t;
    double f1710 = t;
    double f1711 = t;
    double f1712 = t;
    double f1713 = t;
    double f1714 = t;
    double f1715 = t;
    double f1716 = t;
    double f1717 = t;
    double f1718 = t;
    double f1719 = t;
    double f1720 = t;
    double f1721 = t;
    double f1722 = t;
    double f1723 = t;
    double f1724 = t;
    double f1725 = t;
    double f1726 = t;
    double f1727 = t;
    double f1728 = t;
    double f1729 = t;
    double f1730 = t;
    double f1731 = t;
    double f1732 = t;
    double f1733 = t;
    double f1734 = t;
    double f1735 = t;
    double f1736 = t;
    double f1737 = t;
    double f1738 = t;
    double f1739 = t;
    double f1740 = t;
    double f1741 = t;
    double f1742 = t;
    double f1743 = t;
    double f1744 = t;
    double f1745 = t;
    double f1746 = t;
    double f1747 = t;
    double f1748 = t;
    double f1749 = t;
    double f1750 = t;
    double f1751 = t;
    double f1752 = t;
    double f1753 = t;
    double f1754 = t;
    double f1755 = t;
    double f1756 = t;
    double f1757 = t;
    double f1758 = t;
    double f1759 = t;
    double f1760 = t;
    double f1761 = t;
    double f1762 = t;
    double f1763 = t;
    double f1764 = t;
    double f1765 = t;
    double f1766 = t;
    double f1767 = t;
    double f1768 = t;
    double f1769 = t;
    double f1770 = t;
    double f1771 = t;
    double f1772 = t;
    double f1773 = t;
    double f1774 = t;
    double f1775 = t;
    double f1776 = t;
    double f1777 = t;
    double f1778 = t;
    double f1779 = t;
    double f1780 = t;
    double f1781 = t;
    double f1782 = t;
    double f1783 = t;
    double f1784 = t;
    double f1785 = t;
    double f1786 = t;
    double f1787 = t;
    double f1788 = t;
    double f1789 = t;
    double f1790 = t;
    double f1791 = t;
    double f1792 = t;
    double f1793 = t;
    double f1794 = t;
    double f1795 = t;
    double f1796 = t;
    double f1797 = t;
    double f1798 = t;
    double f1799 = t;
    double f1800 = t;
    double f1801 = t;
    double f1802 = t;
    double f1803 = t;
    double f1804 = t;
    double f1805 = t;
    double f1806 = t;
    double f1807 = t;
    double f1808 = t;
    double f1809 = t;
    double f1810 = t;
    double f1811 = t;
    double f1812 = t;
    double f1813 = t;
    double f1814 = t;
    double f1815 = t;
    double f1816 = t;
    double f1817 = t;
    double f1818 = t;
    double f1819 = t;
    double f1820 = t;
    double f1821 = t;
    double f1822 = t;
    double f1823 = t;
    double f1824 = t;
    double f1825 = t;
    double f1826 = t;
    double f1827 = t;
    double f1828 = t;
    double f1829 = t;
    double f1830 = t;
    double f1831 = t;
    double f1832 = t;
    double f1833 = t;
    double f1834 = t;
    double f1835 = t;
    double f1836 = t;
    double f1837 = t;
    double f1838 = t;
    double f1839 = t;
    double f1840 = t;
    double f1841 = t;
    double f1842 = t;
    double f1843 = t;
    double f1844 = t;
    double f1845 = t;
    double f1846 = t;
    double f1847 = t;
    double f1848 = t;
    double f1849 = t;
    double f1850 = t;
    double f1851 = t;
    double f1852 = t;
    double f1853 = t;
    double f1854 = t;
    double f1855 = t;
    double f1856 = t;
    double f1857 = t;
    double f1858 = t;
    double f1859 = t;
    double f1860 = t;
    double f1861 = t;
    double f1862 = t;
    double f1863 = t;
    double f1864 = t;
    double f1865 = t;
    double f1866 = t;
    double f1867 = t;
    double f1868 = t;
    double f1869 = t;
    double f1870 = t;
    double f1871 = t;
    double f1872 = t;
    double f1873 = t;
    double f1874 = t;
    double f1875 = t;
    double f1876 = t;
    double f1877 = t;
    double f1878 = t;
    double f1879 = t;
    double f1880 = t;
    double f1881 = t;
    double f1882 = t;
    double f1883 = t;
    double f1884 = t;
    double f1885 = t;
    double f1886 = t;
    double f1887 = t;
    double f1888 = t;
    double f1889 = t;
    double f1890 = t;
    double f1891 = t;
    double f1892 = t;
    double f1893 = t;
    double f1894 = t;
    double f1895 = t;
    double f1896 = t;
    double f1897 = t;
    double f1898 = t;
    double f1899 = t;
    double f1900 = t;
    double f1901 = t;
    double f1902 = t;
    double f1903 = t;
    double f1904 = t;
    double f1905 = t;
    double f1906 = t;
    double f1907 = t;
    double f1908 = t;
    double f1909 = t;
    double f1910 = t;
    double f1911 = t;
    double f1912 = t;
    double f1913 = t;
    double f1914 = t;
    double f1915 = t;
    double f1916 = t;
    double f1917 = t;
    double f1918 = t;
    double f1919 = t;
    double f1920 = t;
    double f1921 = t;
    double f1922 = t;
    double f1923 = t;
    double f1924 = t;
    double f1925 = t;
    double f1926 = t;
    double f1927 = t;
    double f1928 = t;
    double f1929 = t;
    double f1930 = t;
    double f1931 = t;
    double f1932 = t;
    double f1933 = t;
    double f1934 = t;
    double f1935 = t;
    double f1936 = t;
    double f1937 = t;
    double f1938 = t;
    double f1939 = t;
    double f1940 = t;
    double f1941 = t;
    double f1942 = t;
    double f1943 = t;
    double f1944 = t;
    double f1945 = t;
    double f1946 = t;
    double f1947 = t;
    double f1948 = t;
    double f1949 = t;
    double f1950 = t;
    double f1951 = t;
    double f1952 = t;
    double f1953 = t;
    double f1954 = t;
    double f1955 = t;
    double f1956 = t;
    double f1957 = t;
    double f1958 = t;
    double f1959 = t;
    double f1960 = t;
    double f1961 = t;
    double f1962 = t;
    double f1963 = t;
    double f1964 = t;
    double f1965 = t;
    double f1966 = t;
    double f1967 = t;
    double f1968 = t;
    double f1969 = t;
    double f1970 = t;
    double f1971 = t;
    double f1972 = t;
    double f1973 = t;
    double f1974 = t;
    double f1975 = t;
    double f1976 = t;
    double f1977 = t;
    double f1978 = t;
    double f1979 = t;
    double f1980 = t;
    double f1981 = t;
    double f1982 = t;
    double f1983 = t;
    double f1984 = t;
    double f1985 = t;
    double f1986 = t;
    double f1987 = t;
    double f1988 = t;
    double f1989 = t;
    double f1990 = t;
    double f1991 = t;
    double f1992 = t;
    double f1993 = t;
    double f1994 = t;
    double f1995 = t;
    double f1996 = t;
    double f1997 = t;
    double f1998 = t;
    double f1999 = t;
    double f2000 = t;
    double f2001 = t;
    double f2002 = t;
    double f2003 = t;
    double f2004 = t;
    double f2005 = t;
    double f2006 = t;
    double f2007 = t;
    double f2008 = t;
    double f2009 = t;
    double f2010 = t;
    double f2011 = t;
    double f2012 = t;
    double f2013 = t;
    double f2014 = t;
    double f2015 = t;
    double f2016 = t;
    double f2017 = t;
    double f2018 = t;
    double f2019 = t;
    double f2020 = t;
    double f2021 = t;
    double f2022 = t;
    double f2023 = t;
    double f2024 = t;
    double f2025 = t;
    double f2026 = t;
    double f2027 = t;
    double f2028 = t;
    double f2029 = t;
    double f2030 = t;
    double f2031 = t;
    double f2032 = t;
    double f2033 = t;
    double f2034 = t;
    double f2035 = t;
    double f2036 = t;
    double f2037 = t;
    double f2038 = t;
    double f2039 = t;
    double f2040 = t;
    double f2041 = t;
    double f2042 = t;
    double f2043 = t;
    double f2044 = t;
    double f2045 = t;
    double f2046 = t;
    double f2047 = t;
    double f2048 = t;
    double f2049 = t;
    double f2050 = t;
    double f2051 = t;
    double f2052 = t;
    double f2053 = t;
    double f2054 = t;
    double f2055 = t;
    double f2056 = t;
    double f2057 = t;
    double f2058 = t;
    double f2059 = t;
    double f2060 = t;
    double f2061 = t;
    double f2062 = t;
    double f2063 = t;
    double f2064 = t;
    double f2065 = t;
    double f2066 = t;
    double f2067 = t;
    double f2068 = t;
    double f2069 = t;
    double f2070 = t;
    double f2071 = t;
    double f2072 = t;
    double f2073 = t;
    double f2074 = t;
    double f2075 = t;
    double f2076 = t;
    double f2077 = t;
    double f2078 = t;
    double f2079 = t;
    double f2080 = t;
    double f2081 = t;
    double f2082 = t;
    double f2083 = t;
    double f2084 = t;
    double f2085 = t;
    double f2086 = t;
    double f2087 = t;
    double f2088 = t;
    double f2089 = t;
    double f2090 = t;
    double f2091 = t;
    double f2092 = t;
    double f2093 = t;
    double f2094 = t;
    double f2095 = t;
    double f2096 = t;
    double f2097 = t;
    double f2098 = t;
    double f2099 = t;
    double f2100 = t;
    double f2101 = t;
    double f2102 = t;
    double f2103 = t;
    double f2104 = t;
    double f2105 = t;
    double f2106 = t;
    double f2107 = t;
    double f2108 = t;
    double f2109 = t;
    double f2110 = t;
    double f2111 = t;
    double f2112 = t;
    double f2113 = t;
    double f2114 = t;
    double f2115 = t;
    double f2116 = t;
    double f2117 = t;
    double f2118 = t;
    double f2119 = t;
    double f2120 = t;
    double f2121 = t;
    double f2122 = t;
    double f2123 = t;
    double f2124 = t;
    double f2125 = t;
    double f2126 = t;
    double f2127 = t;
    double f2128 = t;
    double f2129 = t;
    double f2130 = t;
    double f2131 = t;
    double f2132 = t;
    double f2133 = t;
    double f2134 = t;
    double f2135 = t;
    double f2136 = t;
    double f2137 = t;
    double f2138 = t;
    double f2139 = t;
    double f2140 = t;
    double f2141 = t;
    double f2142 = t;
    double f2143 = t;
    double f2144 = t;
    double f2145 = t;
    double f2146 = t;
    double f2147 = t;
    double f2148 = t;
    double f2149 = t;
    double f2150 = t;
    double f2151 = t;
    double f2152 = t;
    double f2153 = t;
    double f2154 = t;
    double f2155 = t;
    double f2156 = t;
    double f2157 = t;
    double f2158 = t;
    double f2159 = t;
    double f2160 = t;
    double f2161 = t;
    double f2162 = t;
    double f2163 = t;
    double f2164 = t;
    double f2165 = t;
    double f2166 = t;
    double f2167 = t;
    double f2168 = t;
    double f2169 = t;
    double f2170 = t;
    double f2171 = t;
    double f2172 = t;
    double f2173 = t;
    double f2174 = t;
    double f2175 = t;
    double f2176 = t;
    double f2177 = t;
    double f2178 = t;
    double f2179 = t;
    double f2180 = t;
    double f2181 = t;
    double f2182 = t;
    double f2183 = t;
    double f2184 = t;
    double f2185 = t;
    double f2186 = t;
    double f2187 = t;
    double f2188 = t;
    double f2189 = t;
    double f2190 = t;
    double f2191 = t;
    double f2192 = t;
    double f2193 = t;
    double f2194 = t;
    double f2195 = t;
    double f2196 = t;
    double f2197 = t;
    double f2198 = t;
    double f2199 = t;
    double f2200 = t;
    double f2201 = t;
    double f2202 = t;
    double f2203 = t;
    double f2204 = t;
    double f2205 = t;
    double f2206 = t;
    double f2207 = t;
    double f2208 = t;
    double f2209 = t;
    double f2210 = t;
    double f2211 = t;
    double f2212 = t;
    double f2213 = t;
    double f2214 = t;
    double f2215 = t;
    double f2216 = t;
    double f2217 = t;
    double f2218 = t;
    double f2219 = t;
    double f2220 = t;
    double f2221 = t;
    double f2222 = t;
    double f2223 = t;
    double f2224 = t;
    double f2225 = t;
    double f2226 = t;
    double f2227 = t;
    double f2228 = t;
    double f2229 = t;
    double f2230 = t;
    double f2231 = t;
    double f2232 = t;
    double f2233 = t;
    double f2234 = t;
    double f2235 = t;
    double f2236 = t;
    double f2237 = t;
    double f2238 = t;
    double f2239 = t;
    double f2240 = t;
    double f2241 = t;
    double f2242 = t;
    double f2243 = t;
    double f2244 = t;
    double f2245 = t;
    double f2246 = t;
    double f2247 = t;
    double f2248 = t;
    double f2249 = t;
    double f2250 = t;
    double f2251 = t;
    double f2252 = t;
    double f2253 = t;
    double f2254 = t;
    double f2255 = t;
    double f2256 = t;
    double f2257 = t;
    double f2258 = t;
    double f2259 = t;
    double f2260 = t;
    double f2261 = t;
    double f2262 = t;
    double f2263 = t;
    double f2264 = t;
    double f2265 = t;
    double f2266 = t;
    double f2267 = t;
    double f2268 = t;
    double f2269 = t;
    double f2270 = t;
    double f2271 = t;
    double f2272 = t;
    double f2273 = t;
    double f2274 = t;
    double f2275 = t;
    double f2276 = t;
    double f2277 = t;
    double f2278 = t;
    double f2279 = t;
    double f2280 = t;
    double f2281 = t;
    double f2282 = t;
    double f2283 = t;
    double f2284 = t;
    double f2285 = t;
    double f2286 = t;
    double f2287 = t;
    double f2288 = t;
    double f2289 = t;
    double f2290 = t;
    double f2291 = t;
    double f2292 = t;
    double f2293 = t;
    double f2294 = t;
    double f2295 = t;
    double f2296 = t;
    double f2297 = t;
    double f2298 = t;
    double f2299 = t;
    double f2300 = t;
    double f2301 = t;
    double f2302 = t;
    double f2303 = t;
    double f2304 = t;
    double f2305 = t;
    double f2306 = t;
    double f2307 = t;
    double f2308 = t;
    double f2309 = t;
    double f2310 = t;
    double f2311 = t;
    double f2312 = t;
    double f2313 = t;
    double f2314 = t;
    double f2315 = t;
    double f2316 = t;
    double f2317 = t;
    double f2318 = t;
    double f2319 = t;
    double f2320 = t;
    double f2321 = t;
    double f2322 = t;
    double f2323 = t;
    double f2324 = t;
    double f2325 = t;
    double f2326 = t;
    double f2327 = t;
    double f2328 = t;
    double f2329 = t;
    double f2330 = t;
    double f2331 = t;
    double f2332 = t;
    double f2333 = t;
    double f2334 = t;
    double f2335 = t;
    double f2336 = t;
    double f2337 = t;
    double f2338 = t;
    double f2339 = t;
    double f2340 = t;
    double f2341 = t;
    double f2342 = t;
    double f2343 = t;
    double f2344 = t;
    double f2345 = t;
    double f2346 = t;
    double f2347 = t;
    double f2348 = t;
    double f2349 = t;
    double f2350 = t;
    double f2351 = t;
    double f2352 = t;
    double f2353 = t;
    double f2354 = t;
    double f2355 = t;
    double f2356 = t;
    double f2357 = t;
    double f2358 = t;
    double f2359 = t;
    double f2360 = t;
    double f2361 = t;
    double f2362 = t;
    double f2363 = t;
    double f2364 = t;
    double f2365 = t;
    double f2366 = t;
    double f2367 = t;
    double f2368 = t;
    double f2369 = t;
    double f2370 = t;
    double f2371 = t;
    double f2372 = t;
    double f2373 = t;
    double f2374 = t;
    double f2375 = t;
    double f2376 = t;
    double f2377 = t;
    double f2378 = t;
    double f2379 = t;
    double f2380 = t;
    double f2381 = t;
    double f2382 = t;
    double f2383 = t;
    double f2384 = t;
    double f2385 = t;
    double f2386 = t;
    double f2387 = t;
    double f2388 = t;
    double f2389 = t;
    double f2390 = t;
    double f2391 = t;
    double f2392 = t;
    double f2393 = t;
    double f2394 = t;
    double f2395 = t;
    double f2396 = t;
    double f2397 = t;
    double f2398 = t;
    double f2399 = t;
    double f2400 = t;
    double f2401 = t;
    double f2402 = t;
    double f2403 = t;
    double f2404 = t;
    double f2405 = t;
    double f2406 = t;
    double f2407 = t;
    double f2408 = t;
    double f2409 = t;
    double f2410 = t;
    double f2411 = t;
    double f2412 = t;
    double f2413 = t;
    double f2414 = t;
    double f2415 = t;
    double f2416 = t;
    double f2417 = t;
    double f2418 = t;
    double f2419 = t;
    double f2420 = t;
    double f2421 = t;
    double f2422 = t;
    double f2423 = t;
    double f2424 = t;
    double f2425 = t;
    double f2426 = t;
    double f2427 = t;
    double f2428 = t;
    double f2429 = t;
    double f2430 = t;
    double f2431 = t;
    double f2432 = t;
    double f2433 = t;
    double f2434 = t;
    double f2435 = t;
    double f2436 = t;
    double f2437 = t;
    double f2438 = t;
    double f2439 = t;
    double f2440 = t;
    double f2441 = t;
    double f2442 = t;
    double f2443 = t;
    double f2444 = t;
    double f2445 = t;
    double f2446 = t;
    double f2447 = t;
    
 	double	f2448	=	t	;
 	double	f2449	=	t	;
 	double	f2450	=	t	;
 	double	f2451	=	t	;
 	double	f2452	=	t	;
 	double	f2453	=	t	;
 	double	f2454	=	t	;
 	double	f2455	=	t	;
 	double	f2456	=	t	;
 	double	f2457	=	t	;
 	double	f2458	=	t	;
 	double	f2459	=	t	;
 	double	f2460	=	t	;
 	double	f2461	=	t	;
 	double	f2462	=	t	;
 	double	f2463	=	t	;
 	double	f2464	=	t	;
 	double	f2465	=	t	;
 	double	f2466	=	t	;
 	double	f2467	=	t	;
 	double	f2468	=	t	;
 	double	f2469	=	t	;
 	double	f2470	=	t	;
 	double	f2471	=	t	;
 	double	f2472	=	t	;
 	double	f2473	=	t	;
 	double	f2474	=	t	;
 	double	f2475	=	t	;
 	double	f2476	=	t	;
 	double	f2477	=	t	;
 	double	f2478	=	t	;
 	double	f2479	=	t	;
 	double	f2480	=	t	;
 	double	f2481	=	t	;
 	double	f2482	=	t	;
 	double	f2483	=	t	;
 	double	f2484	=	t	;
 	double	f2485	=	t	;
 	double	f2486	=	t	;
 	double	f2487	=	t	;
 	double	f2488	=	t	;
 	double	f2489	=	t	;
 	double	f2490	=	t	;
 	double	f2491	=	t	;
 	double	f2492	=	t	;
 	double	f2493	=	t	;
 	double	f2494	=	t	;
 	double	f2495	=	t	;
 	double	f2496	=	t	;
 	double	f2497	=	t	;
 	double	f2498	=	t	;
 	double	f2499	=	t	;
 	double	f2500	=	t	;
 	double	f2501	=	t	;
 	double	f2502	=	t	;
 	double	f2503	=	t	;
 	double	f2504	=	t	;
 	double	f2505	=	t	;
 	double	f2506	=	t	;
 	double	f2507	=	t	;
 	double	f2508	=	t	;
 	double	f2509	=	t	;
 	double	f2510	=	t	;
 	double	f2511	=	t	;
 	double	f2512	=	t	;
 	double	f2513	=	t	;
 	double	f2514	=	t	;
 	double	f2515	=	t	;
 	double	f2516	=	t	;
 	double	f2517	=	t	;
 	double	f2518	=	t	;
 	double	f2519	=	t	;
 	double	f2520	=	t	;
 	double	f2521	=	t	;
 	double	f2522	=	t	;
 	double	f2523	=	t	;
 	double	f2524	=	t	;
 	double	f2525	=	t	;
 	double	f2526	=	t	;
 	double	f2527	=	t	;
 	double	f2528	=	t	;
 	double	f2529	=	t	;
 	double	f2530	=	t	;
 	double	f2531	=	t	;
 	double	f2532	=	t	;
 	double	f2533	=	t	;
 	double	f2534	=	t	;
 	double	f2535	=	t	;
 	double	f2536	=	t	;
 	double	f2537	=	t	;
 	double	f2538	=	t	;
 	double	f2539	=	t	;
 	double	f2540	=	t	;
 	double	f2541	=	t	;
 	double	f2542	=	t	;
 	double	f2543	=	t	;
 	double	f2544	=	t	;
 	double	f2545	=	t	;
 	double	f2546	=	t	;
 	double	f2547	=	t	;
 	double	f2548	=	t	;
 	double	f2549	=	t	;
 	double	f2550	=	t	;
 	double	f2551	=	t	;
 	double	f2552	=	t	;
 	double	f2553	=	t	;
 	double	f2554	=	t	;
 	double	f2555	=	t	;
 	double	f2556	=	t	;
 	double	f2557	=	t	;
 	double	f2558	=	t	;
 	double	f2559	=	t	;
 	double	f2560	=	t	;
 	double	f2561	=	t	;
 	double	f2562	=	t	;
 	double	f2563	=	t	;
 	double	f2564	=	t	;
 	double	f2565	=	t	;
 	double	f2566	=	t	;
 	double	f2567	=	t	;
 	double	f2568	=	t	;
 	double	f2569	=	t	;
 	double	f2570	=	t	;
 	double	f2571	=	t	;
 	double	f2572	=	t	;
 	double	f2573	=	t	;
 	double	f2574	=	t	;
 	double	f2575	=	t	;
 	double	f2576	=	t	;
 	double	f2577	=	t	;
 	double	f2578	=	t	;
 	double	f2579	=	t	;
 	double	f2580	=	t	;
 	double	f2581	=	t	;
 	double	f2582	=	t	;
 	double	f2583	=	t	;
 	double	f2584	=	t	;
 	double	f2585	=	t	;
 	double	f2586	=	t	;
 	double	f2587	=	t	;
 	double	f2588	=	t	;
 	double	f2589	=	t	;
 	double	f2590	=	t	;
 	double	f2591	=	t	;
 	double	f2592	=	t	;
 	double	f2593	=	t	;
 	double	f2594	=	t	;
 	double	f2595	=	t	;
 	double	f2596	=	t	;
 	double	f2597	=	t	;
 	double	f2598	=	t	;
 	double	f2599	=	t	;
 	double	f2600	=	t	;
 	double	f2601	=	t	;
 	double	f2602	=	t	;
 	double	f2603	=	t	;
 	double	f2604	=	t	;
 	double	f2605	=	t	;
 	double	f2606	=	t	;
 	double	f2607	=	t	;
 	double	f2608	=	t	;
 	double	f2609	=	t	;
 	double	f2610	=	t	;
 	double	f2611	=	t	;
 	double	f2612	=	t	;
 	double	f2613	=	t	;
 	double	f2614	=	t	;
 	double	f2615	=	t	;
 	double	f2616	=	t	;
 	double	f2617	=	t	;
 	double	f2618	=	t	;
 	double	f2619	=	t	;
 	double	f2620	=	t	;
 	double	f2621	=	t	;
 	double	f2622	=	t	;
 	double	f2623	=	t	;
 	double	f2624	=	t	;
 	double	f2625	=	t	;
 	double	f2626	=	t	;
 	double	f2627	=	t	;
 	double	f2628	=	t	;
 	double	f2629	=	t	;
 	double	f2630	=	t	;
 	double	f2631	=	t	;
 	double	f2632	=	t	;
 	double	f2633	=	t	;
 	double	f2634	=	t	;
 	double	f2635	=	t	;
 	double	f2636	=	t	;
 	double	f2637	=	t	;
 	double	f2638	=	t	;
 	double	f2639	=	t	;
 	double	f2640	=	t	;
 	double	f2641	=	t	;
 	double	f2642	=	t	;
 	double	f2643	=	t	;
 	double	f2644	=	t	;
 	double	f2645	=	t	;
 	double	f2646	=	t	;
 	double	f2647	=	t	;
 	double	f2648	=	t	;
 	double	f2649	=	t	;
 	double	f2650	=	t	;
 	double	f2651	=	t	;
 	double	f2652	=	t	;
 	double	f2653	=	t	;
 	double	f2654	=	t	;
 	double	f2655	=	t	;
 	double	f2656	=	t	;
 	double	f2657	=	t	;
 	double	f2658	=	t	;
 	double	f2659	=	t	;
 	double	f2660	=	t	;
 	double	f2661	=	t	;
 	double	f2662	=	t	;
 	double	f2663	=	t	;
 	double	f2664	=	t	;
 	double	f2665	=	t	;
 	double	f2666	=	t	;
 	double	f2667	=	t	;
 	double	f2668	=	t	;
 	double	f2669	=	t	;
 	double	f2670	=	t	;
 	double	f2671	=	t	;
 	double	f2672	=	t	;
 	double	f2673	=	t	;
 	double	f2674	=	t	;
 	double	f2675	=	t	;
 	double	f2676	=	t	;
 	double	f2677	=	t	;
 	double	f2678	=	t	;
 	double	f2679	=	t	;
 	double	f2680	=	t	;
 	double	f2681	=	t	;
 	double	f2682	=	t	;
 	double	f2683	=	t	;
 	double	f2684	=	t	;
 	double	f2685	=	t	;
 	double	f2686	=	t	;
 	double	f2687	=	t	;
 	double	f2688	=	t	;
 	double	f2689	=	t	;
 	double	f2690	=	t	;
 	double	f2691	=	t	;
 	double	f2692	=	t	;
 	double	f2693	=	t	;
 	double	f2694	=	t	;
 	double	f2695	=	t	;
 	double	f2696	=	t	;
 	double	f2697	=	t	;
 	double	f2698	=	t	;
 	double	f2699	=	t	;
 	double	f2700	=	t	;
 	double	f2701	=	t	;
 	double	f2702	=	t	;
 	double	f2703	=	t	;
 	double	f2704	=	t	;
 	double	f2705	=	t	;
 	double	f2706	=	t	;
 	double	f2707	=	t	;
 	double	f2708	=	t	;
 	double	f2709	=	t	;
 	double	f2710	=	t	;
 	double	f2711	=	t	;
 	double	f2712	=	t	;
 	double	f2713	=	t	;
 	double	f2714	=	t	;
 	double	f2715	=	t	;
 	double	f2716	=	t	;
 	double	f2717	=	t	;
 	double	f2718	=	t	;
 	double	f2719	=	t	;
 	double	f2720	=	t	;
 	double	f2721	=	t	;
 	double	f2722	=	t	;
 	double	f2723	=	t	;
 	double	f2724	=	t	;
 	double	f2725	=	t	;
 	double	f2726	=	t	;
 	double	f2727	=	t	;
 	double	f2728	=	t	;
 	double	f2729	=	t	;
 	double	f2730	=	t	;
 	double	f2731	=	t	;
 	double	f2732	=	t	;
 	double	f2733	=	t	;
 	double	f2734	=	t	;
 	double	f2735	=	t	;
 	double	f2736	=	t	;
 	double	f2737	=	t	;
 	double	f2738	=	t	;
 	double	f2739	=	t	;
 	double	f2740	=	t	;
 	double	f2741	=	t	;
 	double	f2742	=	t	;
 	double	f2743	=	t	;
 	double	f2744	=	t	;
 	double	f2745	=	t	;
 	double	f2746	=	t	;
 	double	f2747	=	t	;
 	double	f2748	=	t	;
 	double	f2749	=	t	;
 	double	f2750	=	t	;
 	double	f2751	=	t	;
 	double	f2752	=	t	;
 	double	f2753	=	t	;
 	double	f2754	=	t	;
 	double	f2755	=	t	;
 	double	f2756	=	t	;
 	double	f2757	=	t	;
 	double	f2758	=	t	;
 	double	f2759	=	t	;
 	double	f2760	=	t	;
 	double	f2761	=	t	;
 	double	f2762	=	t	;
 	double	f2763	=	t	;
 	double	f2764	=	t	;
 	double	f2765	=	t	;
 	double	f2766	=	t	;
 	double	f2767	=	t	;
 	double	f2768	=	t	;
 	double	f2769	=	t	;
 	double	f2770	=	t	;
 	double	f2771	=	t	;
 	double	f2772	=	t	;
 	double	f2773	=	t	;
 	double	f2774	=	t	;
 	double	f2775	=	t	;
 	double	f2776	=	t	;
 	double	f2777	=	t	;
 	double	f2778	=	t	;
 	double	f2779	=	t	;
 	double	f2780	=	t	;
 	double	f2781	=	t	;
 	double	f2782	=	t	;
 	double	f2783	=	t	;
 	double	f2784	=	t	;
 	double	f2785	=	t	;
 	double	f2786	=	t	;
 	double	f2787	=	t	;
 	double	f2788	=	t	;
 	double	f2789	=	t	;
 	double	f2790	=	t	;
 	double	f2791	=	t	;
 	double	f2792	=	t	;
 	double	f2793	=	t	;
 	double	f2794	=	t	;
 	double	f2795	=	t	;
 	double	f2796	=	t	;
 	double	f2797	=	t	;
 	double	f2798	=	t	;
 	double	f2799	=	t	;
 	double	f2800	=	t	;
 	double	f2801	=	t	;
 	double	f2802	=	t	;
 	double	f2803	=	t	;
 	double	f2804	=	t	;
 	double	f2805	=	t	;
 	double	f2806	=	t	;
 	double	f2807	=	t	;
 	double	f2808	=	t	;
 	double	f2809	=	t	;
 	double	f2810	=	t	;
 	double	f2811	=	t	;
 	double	f2812	=	t	;
 	double	f2813	=	t	;
 	double	f2814	=	t	;
 	double	f2815	=	t	;
 	double	f2816	=	t	;
 	double	f2817	=	t	;
 	double	f2818	=	t	;
 	double	f2819	=	t	;
 	double	f2820	=	t	;
 	double	f2821	=	t	;
 	double	f2822	=	t	;
 	double	f2823	=	t	;
 	double	f2824	=	t	;
 	double	f2825	=	t	;
 	double	f2826	=	t	;
 	double	f2827	=	t	;
 	double	f2828	=	t	;
 	double	f2829	=	t	;
 	double	f2830	=	t	;
 	double	f2831	=	t	;
 	double	f2832	=	t	;
 	double	f2833	=	t	;
 	double	f2834	=	t	;
 	double	f2835	=	t	;
 	double	f2836	=	t	;
 	double	f2837	=	t	;
 	double	f2838	=	t	;
 	double	f2839	=	t	;
 	double	f2840	=	t	;
 	double	f2841	=	t	;
 	double	f2842	=	t	;
 	double	f2843	=	t	;
 	double	f2844	=	t	;
 	double	f2845	=	t	;
 	double	f2846	=	t	;
 	double	f2847	=	t	;
 	double	f2848	=	t	;
 	double	f2849	=	t	;
 	double	f2850	=	t	;
 	double	f2851	=	t	;
 	double	f2852	=	t	;
 	double	f2853	=	t	;
 	double	f2854	=	t	;
 	double	f2855	=	t	;
 	double	f2856	=	t	;
 	double	f2857	=	t	;
 	double	f2858	=	t	;
 	double	f2859	=	t	;
 	double	f2860	=	t	;
 	double	f2861	=	t	;
 	double	f2862	=	t	;
 	double	f2863	=	t	;
 	double	f2864	=	t	;
 	double	f2865	=	t	;
 	double	f2866	=	t	;
 	double	f2867	=	t	;
 	double	f2868	=	t	;
 	double	f2869	=	t	;
 	double	f2870	=	t	;
 	double	f2871	=	t	;
 	double	f2872	=	t	;
 	double	f2873	=	t	;
 	double	f2874	=	t	;
 	double	f2875	=	t	;
 	double	f2876	=	t	;
 	double	f2877	=	t	;
 	double	f2878	=	t	;
 	double	f2879	=	t	;
 	double	f2880	=	t	;
 	double	f2881	=	t	;
 	double	f2882	=	t	;
 	double	f2883	=	t	;
 	double	f2884	=	t	;
 	double	f2885	=	t	;
 	double	f2886	=	t	;
 	double	f2887	=	t	;
 	double	f2888	=	t	;
 	double	f2889	=	t	;
 	double	f2890	=	t	;
 	double	f2891	=	t	;
 	double	f2892	=	t	;
 	double	f2893	=	t	;
 	double	f2894	=	t	;
 	double	f2895	=	t	;
 	double	f2896	=	t	;
 	double	f2897	=	t	;
 	double	f2898	=	t	;
 	double	f2899	=	t	;
 	double	f2900	=	t	;
 	double	f2901	=	t	;
 	double	f2902	=	t	;
 	double	f2903	=	t	;
 	double	f2904	=	t	;
 	double	f2905	=	t	;
 	double	f2906	=	t	;
 	double	f2907	=	t	;
 	double	f2908	=	t	;
 	double	f2909	=	t	;
 	double	f2910	=	t	;
 	double	f2911	=	t	;
 	double	f2912	=	t	;
 	double	f2913	=	t	;
 	double	f2914	=	t	;
 	double	f2915	=	t	;
 	double	f2916	=	t	;
 	double	f2917	=	t	;
 	double	f2918	=	t	;
 	double	f2919	=	t	;
 	double	f2920	=	t	;
 	double	f2921	=	t	;
 	double	f2922	=	t	;
 	double	f2923	=	t	;
 	double	f2924	=	t	;
 	double	f2925	=	t	;
 	double	f2926	=	t	;
 	double	f2927	=	t	;
 	double	f2928	=	t	;
 	double	f2929	=	t	;
 	double	f2930	=	t	;
 	double	f2931	=	t	;
 	double	f2932	=	t	;
 	double	f2933	=	t	;
 	double	f2934	=	t	;
 	double	f2935	=	t	;
 	double	f2936	=	t	;
 	double	f2937	=	t	;
 	double	f2938	=	t	;
 	double	f2939	=	t	;
 	double	f2940	=	t	;
 	double	f2941	=	t	;
 	double	f2942	=	t	;
 	double	f2943	=	t	;
 	double	f2944	=	t	;
 	double	f2945	=	t	;
 	double	f2946	=	t	;
 	double	f2947	=	t	;
 	double	f2948	=	t	;
 	double	f2949	=	t	;
 	double	f2950	=	t	;
 	double	f2951	=	t	;
 	double	f2952	=	t	;
 	double	f2953	=	t	;
 	double	f2954	=	t	;
 	double	f2955	=	t	;
 	double	f2956	=	t	;
 	double	f2957	=	t	;
 	double	f2958	=	t	;
 	double	f2959	=	t	;
 	double	f2960	=	t	;
 	double	f2961	=	t	;
 	double	f2962	=	t	;
 	double	f2963	=	t	;
 	double	f2964	=	t	;
 	double	f2965	=	t	;
 	double	f2966	=	t	;
 	double	f2967	=	t	;
 	double	f2968	=	t	;
 	double	f2969	=	t	;
 	double	f2970	=	t	;
 	double	f2971	=	t	;
 	double	f2972	=	t	;
 	double	f2973	=	t	;
 	double	f2974	=	t	;
 	double	f2975	=	t	;
 	double	f2976	=	t	;
 	double	f2977	=	t	;
 	double	f2978	=	t	;
 	double	f2979	=	t	;
 	double	f2980	=	t	;
 	double	f2981	=	t	;
 	double	f2982	=	t	;
 	double	f2983	=	t	;
 	double	f2984	=	t	;
 	double	f2985	=	t	;
 	double	f2986	=	t	;
 	double	f2987	=	t	;
 	double	f2988	=	t	;
 	double	f2989	=	t	;
 	double	f2990	=	t	;
 	double	f2991	=	t	;
 	double	f2992	=	t	;
 	double	f2993	=	t	;
 	double	f2994	=	t	;
 	double	f2995	=	t	;
 	double	f2996	=	t	;
 	double	f2997	=	t	;
 	double	f2998	=	t	;
 	double	f2999	=	t	;
 	double	f3000	=	t	;
 	double	f3001	=	t	;
 	double	f3002	=	t	;
 	double	f3003	=	t	;
 	double	f3004	=	t	;
 	double	f3005	=	t	;
 	double	f3006	=	t	;
 	double	f3007	=	t	;
 	double	f3008	=	t	;
 	double	f3009	=	t	;
 	double	f3010	=	t	;
 	double	f3011	=	t	;
 	double	f3012	=	t	;
 	double	f3013	=	t	;
 	double	f3014	=	t	;
 	double	f3015	=	t	;
 	double	f3016	=	t	;
 	double	f3017	=	t	;
 	double	f3018	=	t	;
 	double	f3019	=	t	;
 	double	f3020	=	t	;
 	double	f3021	=	t	;
 	double	f3022	=	t	;
 	double	f3023	=	t	;
 	double	f3024	=	t	;
 	double	f3025	=	t	;
 	double	f3026	=	t	;
 	double	f3027	=	t	;
 	double	f3028	=	t	;
 	double	f3029	=	t	;
 	double	f3030	=	t	;
 	double	f3031	=	t	;
 	double	f3032	=	t	;
 	double	f3033	=	t	;
 	double	f3034	=	t	;
 	double	f3035	=	t	;
 	double	f3036	=	t	;
 	double	f3037	=	t	;
 	double	f3038	=	t	;
 	double	f3039	=	t	;
 	double	f3040	=	t	;
 	double	f3041	=	t	;
 	double	f3042	=	t	;
 	double	f3043	=	t	;
 	double	f3044	=	t	;
 	double	f3045	=	t	;
 	double	f3046	=	t	;
 	double	f3047	=	t	;
 	double	f3048	=	t	;
 	double	f3049	=	t	;
 	double	f3050	=	t	;
 	double	f3051	=	t	;
 	double	f3052	=	t	;
 	double	f3053	=	t	;
 	double	f3054	=	t	;
 	double	f3055	=	t	;
 	double	f3056	=	t	;
 	double	f3057	=	t	;
 	double	f3058	=	t	;
 	double	f3059	=	t	;
 	double	f3060	=	t	;
 	double	f3061	=	t	;
 	double	f3062	=	t	;
 	double	f3063	=	t	;
 	double	f3064	=	t	;
 	double	f3065	=	t	;
 	double	f3066	=	t	;
 	double	f3067	=	t	;
 	double	f3068	=	t	;
 	double	f3069	=	t	;
 	double	f3070	=	t	;
 	double	f3071	=	t	;
 	double	f3072	=	t	;
 	double	f3073	=	t	;
 	double	f3074	=	t	;
 	double	f3075	=	t	;
 	double	f3076	=	t	;
 	double	f3077	=	t	;
 	double	f3078	=	t	;
 	double	f3079	=	t	;
 	double	f3080	=	t	;
 	double	f3081	=	t	;
 	double	f3082	=	t	;
 	double	f3083	=	t	;
 	double	f3084	=	t	;
 	double	f3085	=	t	;
 	double	f3086	=	t	;
 	double	f3087	=	t	;
 	double	f3088	=	t	;
 	double	f3089	=	t	;
 	double	f3090	=	t	;
 	double	f3091	=	t	;
 	double	f3092	=	t	;
 	double	f3093	=	t	;
 	double	f3094	=	t	;
 	double	f3095	=	t	;
 	double	f3096	=	t	;
 	double	f3097	=	t	;
 	double	f3098	=	t	;
 	double	f3099	=	t	;
 	double	f3100	=	t	;
 	double	f3101	=	t	;
 	double	f3102	=	t	;
 	double	f3103	=	t	;
 	double	f3104	=	t	;
 	double	f3105	=	t	;
 	double	f3106	=	t	;
 	double	f3107	=	t	;
 	double	f3108	=	t	;
 	double	f3109	=	t	;
 	double	f3110	=	t	;
 	double	f3111	=	t	;
 	double	f3112	=	t	;
 	double	f3113	=	t	;
 	double	f3114	=	t	;
 	double	f3115	=	t	;
 	double	f3116	=	t	;
 	double	f3117	=	t	;
 	double	f3118	=	t	;
 	double	f3119	=	t	;
 	double	f3120	=	t	;
 	double	f3121	=	t	;
 	double	f3122	=	t	;
 	double	f3123	=	t	;
 	double	f3124	=	t	;
 	double	f3125	=	t	;
 	double	f3126	=	t	;
 	double	f3127	=	t	;
 	double	f3128	=	t	;
 	double	f3129	=	t	;
 	double	f3130	=	t	;
 	double	f3131	=	t	;
 	double	f3132	=	t	;
 	double	f3133	=	t	;
 	double	f3134	=	t	;
 	double	f3135	=	t	;
 	double	f3136	=	t	;
 	double	f3137	=	t	;
 	double	f3138	=	t	;
 	double	f3139	=	t	;
 	double	f3140	=	t	;
 	double	f3141	=	t	;
 	double	f3142	=	t	;
 	double	f3143	=	t	;
 	double	f3144	=	t	;
 	double	f3145	=	t	;
 	double	f3146	=	t	;
 	double	f3147	=	t	;
 	double	f3148	=	t	;
 	double	f3149	=	t	;
 	double	f3150	=	t	;
 	double	f3151	=	t	;
 	double	f3152	=	t	;
 	double	f3153	=	t	;
 	double	f3154	=	t	;
 	double	f3155	=	t	;
 	double	f3156	=	t	;
 	double	f3157	=	t	;
 	double	f3158	=	t	;
 	double	f3159	=	t	;
 	double	f3160	=	t	;
 	double	f3161	=	t	;
 	double	f3162	=	t	;
 	double	f3163	=	t	;
 	double	f3164	=	t	;
 	double	f3165	=	t	;
 	double	f3166	=	t	;
 	double	f3167	=	t	;
 	double	f3168	=	t	;
 	double	f3169	=	t	;
 	double	f3170	=	t	;
 	double	f3171	=	t	;
 	double	f3172	=	t	;
 	double	f3173	=	t	;
 	double	f3174	=	t	;
 	double	f3175	=	t	;
 	double	f3176	=	t	;
 	double	f3177	=	t	;
 	double	f3178	=	t	;
 	double	f3179	=	t	;
 	double	f3180	=	t	;
 	double	f3181	=	t	;
 	double	f3182	=	t	;
 	double	f3183	=	t	;
 	double	f3184	=	t	;
 	double	f3185	=	t	;
 	double	f3186	=	t	;
 	double	f3187	=	t	;
 	double	f3188	=	t	;
 	double	f3189	=	t	;
 	double	f3190	=	t	;
 	double	f3191	=	t	;
 	double	f3192	=	t	;
 	double	f3193	=	t	;
 	double	f3194	=	t	;
 	double	f3195	=	t	;
 	double	f3196	=	t	;
 	double	f3197	=	t	;
 	double	f3198	=	t	;
 	double	f3199	=	t	;
 	double	f3200	=	t	;
 	double	f3201	=	t	;
 	double	f3202	=	t	;
 	double	f3203	=	t	;
 	double	f3204	=	t	;
 	double	f3205	=	t	;
 	double	f3206	=	t	;
 	double	f3207	=	t	;
 	double	f3208	=	t	;
 	double	f3209	=	t	;
 	double	f3210	=	t	;
 	double	f3211	=	t	;
 	double	f3212	=	t	;
 	double	f3213	=	t	;
 	double	f3214	=	t	;
 	double	f3215	=	t	;
 	double	f3216	=	t	;
 	double	f3217	=	t	;
 	double	f3218	=	t	;
 	double	f3219	=	t	;
 	double	f3220	=	t	;
 	double	f3221	=	t	;
 	double	f3222	=	t	;
 	double	f3223	=	t	;
 	double	f3224	=	t	;
 	double	f3225	=	t	;
 	double	f3226	=	t	;
 	double	f3227	=	t	;
 	double	f3228	=	t	;
 	double	f3229	=	t	;
 	double	f3230	=	t	;
 	double	f3231	=	t	;
 	double	f3232	=	t	;
 	double	f3233	=	t	;
 	double	f3234	=	t	;
 	double	f3235	=	t	;
 	double	f3236	=	t	;
 	double	f3237	=	t	;
 	double	f3238	=	t	;
 	double	f3239	=	t	;
 	double	f3240	=	t	;
 	double	f3241	=	t	;
 	double	f3242	=	t	;
 	double	f3243	=	t	;
 	double	f3244	=	t	;
 	double	f3245	=	t	;
 	double	f3246	=	t	;
 	double	f3247	=	t	;
 	double	f3248	=	t	;
 	double	f3249	=	t	;
 	double	f3250	=	t	;
 	double	f3251	=	t	;
 	double	f3252	=	t	;
 	double	f3253	=	t	;
 	double	f3254	=	t	;
 	double	f3255	=	t	;
 	double	f3256	=	t	;
 	double	f3257	=	t	;
 	double	f3258	=	t	;
 	double	f3259	=	t	;
 	double	f3260	=	t	;
 	double	f3261	=	t	;
 	double	f3262	=	t	;
 	double	f3263	=	t	;
 	double	f3264	=	t	;
 	double	f3265	=	t	;
 	double	f3266	=	t	;
 	double	f3267	=	t	;
 	double	f3268	=	t	;
 	double	f3269	=	t	;
 	double	f3270	=	t	;
 	double	f3271	=	t	;
 	double	f3272	=	t	;
 	double	f3273	=	t	;
 	double	f3274	=	t	;
 	double	f3275	=	t	;
 	double	f3276	=	t	;
 	double	f3277	=	t	;
 	double	f3278	=	t	;
 	double	f3279	=	t	;
 	double	f3280	=	t	;
 	double	f3281	=	t	;
 	double	f3282	=	t	;
 	double	f3283	=	t	;
 	double	f3284	=	t	;
 	double	f3285	=	t	;
 	double	f3286	=	t	;
 	double	f3287	=	t	;
 	double	f3288	=	t	;
 	double	f3289	=	t	;
 	double	f3290	=	t	;
 	double	f3291	=	t	;
 	double	f3292	=	t	;
 	double	f3293	=	t	;
 	double	f3294	=	t	;
 	double	f3295	=	t	;
 	double	f3296	=	t	;
 	double	f3297	=	t	;
 	double	f3298	=	t	;
 	double	f3299	=	t	;
 	double	f3300	=	t	;
 	double	f3301	=	t	;
 	double	f3302	=	t	;
 	double	f3303	=	t	;
 	double	f3304	=	t	;
 	double	f3305	=	t	;
 	double	f3306	=	t	;
 	double	f3307	=	t	;
 	double	f3308	=	t	;
 	double	f3309	=	t	;
 	double	f3310	=	t	;
 	double	f3311	=	t	;
 	double	f3312	=	t	;
 	double	f3313	=	t	;
 	double	f3314	=	t	;
 	double	f3315	=	t	;
 	double	f3316	=	t	;
 	double	f3317	=	t	;
 	double	f3318	=	t	;
 	double	f3319	=	t	;
 	double	f3320	=	t	;
 	double	f3321	=	t	;
 	double	f3322	=	t	;
 	double	f3323	=	t	;
 	double	f3324	=	t	;
 	double	f3325	=	t	;
 	double	f3326	=	t	;
 	double	f3327	=	t	;
 	double	f3328	=	t	;
 	double	f3329	=	t	;
 	double	f3330	=	t	;
 	double	f3331	=	t	;
 	double	f3332	=	t	;
 	double	f3333	=	t	;
 	double	f3334	=	t	;
 	double	f3335	=	t	;
 	double	f3336	=	t	;
 	double	f3337	=	t	;
 	double	f3338	=	t	;
 	double	f3339	=	t	;
 	double	f3340	=	t	;
 	double	f3341	=	t	;
 	double	f3342	=	t	;
 	double	f3343	=	t	;
 	double	f3344	=	t	;
 	double	f3345	=	t	;
 	double	f3346	=	t	;
 	double	f3347	=	t	;
 	double	f3348	=	t	;
 	double	f3349	=	t	;
 	double	f3350	=	t	;
 	double	f3351	=	t	;
 	double	f3352	=	t	;
 	double	f3353	=	t	;
 	double	f3354	=	t	;
 	double	f3355	=	t	;
 	double	f3356	=	t	;
 	double	f3357	=	t	;
 	double	f3358	=	t	;
 	double	f3359	=	t	;
 	double	f3360	=	t	;
 	double	f3361	=	t	;
 	double	f3362	=	t	;
 	double	f3363	=	t	;
 	double	f3364	=	t	;
 	double	f3365	=	t	;
 	double	f3366	=	t	;
 	double	f3367	=	t	;
 	double	f3368	=	t	;
 	double	f3369	=	t	;
 	double	f3370	=	t	;
 	double	f3371	=	t	;
 	double	f3372	=	t	;
 	double	f3373	=	t	;
 	double	f3374	=	t	;
 	double	f3375	=	t	;
 	double	f3376	=	t	;
 	double	f3377	=	t	;
 	double	f3378	=	t	;
 	double	f3379	=	t	;
 	double	f3380	=	t	;
 	double	f3381	=	t	;
 	double	f3382	=	t	;
 	double	f3383	=	t	;
 	double	f3384	=	t	;
 	double	f3385	=	t	;
 	double	f3386	=	t	;
 	double	f3387	=	t	;
 	double	f3388	=	t	;
 	double	f3389	=	t	;
 	double	f3390	=	t	;
 	double	f3391	=	t	;
 	double	f3392	=	t	;
 	double	f3393	=	t	;
 	double	f3394	=	t	;
 	double	f3395	=	t	;
 	double	f3396	=	t	;
 	double	f3397	=	t	;
 	double	f3398	=	t	;
 	double	f3399	=	t	;
 	double	f3400	=	t	;
 	double	f3401	=	t	;
 	double	f3402	=	t	;
 	double	f3403	=	t	;
 	double	f3404	=	t	;
 	double	f3405	=	t	;
 	double	f3406	=	t	;
 	double	f3407	=	t	;
 	double	f3408	=	t	;
 	double	f3409	=	t	;
 	double	f3410	=	t	;
 	double	f3411	=	t	;
 	double	f3412	=	t	;
 	double	f3413	=	t	;
 	double	f3414	=	t	;
 	double	f3415	=	t	;
 	double	f3416	=	t	;
 	double	f3417	=	t	;
 	double	f3418	=	t	;
 	double	f3419	=	t	;
 	double	f3420	=	t	;
 	double	f3421	=	t	;
 	double	f3422	=	t	;
 	double	f3423	=	t	;
 	double	f3424	=	t	;
 	double	f3425	=	t	;
 	double	f3426	=	t	;
 	double	f3427	=	t	;
 	double	f3428	=	t	;
 	double	f3429	=	t	;
 	double	f3430	=	t	;
 	double	f3431	=	t	;
 	double	f3432	=	t	;
 	double	f3433	=	t	;
 	double	f3434	=	t	;
 	double	f3435	=	t	;
 	double	f3436	=	t	;
 	double	f3437	=	t	;
 	double	f3438	=	t	;
 	double	f3439	=	t	;
 	double	f3440	=	t	;
 	double	f3441	=	t	;
 	double	f3442	=	t	;
 	double	f3443	=	t	;
 	double	f3444	=	t	;
 	double	f3445	=	t	;
 	double	f3446	=	t	;
 	double	f3447	=	t	;
 	double	f3448	=	t	;
 	double	f3449	=	t	;
 	double	f3450	=	t	;
 	double	f3451	=	t	;
 	double	f3452	=	t	;
 	double	f3453	=	t	;
 	double	f3454	=	t	;
 	double	f3455	=	t	;
 	double	f3456	=	t	;
 	double	f3457	=	t	;
 	double	f3458	=	t	;
 	double	f3459	=	t	;
 	double	f3460	=	t	;
 	double	f3461	=	t	;
 	double	f3462	=	t	;
 	double	f3463	=	t	;
 	double	f3464	=	t	;
 	double	f3465	=	t	;
 	double	f3466	=	t	;
 	double	f3467	=	t	;
 	double	f3468	=	t	;
 	double	f3469	=	t	;
 	double	f3470	=	t	;
 	double	f3471	=	t	;
 	double	f3472	=	t	;
 	double	f3473	=	t	;
 	double	f3474	=	t	;
 	double	f3475	=	t	;
 	double	f3476	=	t	;
 	double	f3477	=	t	;
 	double	f3478	=	t	;
 	double	f3479	=	t	;
 	double	f3480	=	t	;
 	double	f3481	=	t	;
 	double	f3482	=	t	;
 	double	f3483	=	t	;
 	double	f3484	=	t	;
 	double	f3485	=	t	;
 	double	f3486	=	t	;
 	double	f3487	=	t	;
 	double	f3488	=	t	;
 	double	f3489	=	t	;
 	double	f3490	=	t	;
 	double	f3491	=	t	;
 	double	f3492	=	t	;
 	double	f3493	=	t	;
 	double	f3494	=	t	;
 	double	f3495	=	t	;
 	double	f3496	=	t	;
 	double	f3497	=	t	;
 	double	f3498	=	t	;
 	double	f3499	=	t	;
 	double	f3500	=	t	;
 	double	f3501	=	t	;
 	double	f3502	=	t	;
 	double	f3503	=	t	;
 	double	f3504	=	t	;
 	double	f3505	=	t	;
 	double	f3506	=	t	;
 	double	f3507	=	t	;
 	double	f3508	=	t	;
 	double	f3509	=	t	;
 	double	f3510	=	t	;
 	double	f3511	=	t	;
 	double	f3512	=	t	;
 	double	f3513	=	t	;
 	double	f3514	=	t	;
 	double	f3515	=	t	;
 	double	f3516	=	t	;
 	double	f3517	=	t	;
 	double	f3518	=	t	;
 	double	f3519	=	t	;
 	double	f3520	=	t	;
 	double	f3521	=	t	;
 	double	f3522	=	t	;
 	double	f3523	=	t	;
 	double	f3524	=	t	;
 	double	f3525	=	t	;
 	double	f3526	=	t	;
 	double	f3527	=	t	;
 	double	f3528	=	t	;
 	double	f3529	=	t	;
 	double	f3530	=	t	;
 	double	f3531	=	t	;
 	double	f3532	=	t	;
 	double	f3533	=	t	;
 	double	f3534	=	t	;
 	double	f3535	=	t	;
 	double	f3536	=	t	;
 	double	f3537	=	t	;
 	double	f3538	=	t	;
 	double	f3539	=	t	;
 	double	f3540	=	t	;
 	double	f3541	=	t	;
 	double	f3542	=	t	;
 	double	f3543	=	t	;
 	double	f3544	=	t	;
 	double	f3545	=	t	;
 	double	f3546	=	t	;
 	double	f3547	=	t	;
 	double	f3548	=	t	;
 	double	f3549	=	t	;
 	double	f3550	=	t	;
 	double	f3551	=	t	;
 	double	f3552	=	t	;
 	double	f3553	=	t	;
 	double	f3554	=	t	;
 	double	f3555	=	t	;
 	double	f3556	=	t	;
 	double	f3557	=	t	;
 	double	f3558	=	t	;
 	double	f3559	=	t	;
 	double	f3560	=	t	;
 	double	f3561	=	t	;
 	double	f3562	=	t	;
 	double	f3563	=	t	;
 	double	f3564	=	t	;
 	double	f3565	=	t	;
 	double	f3566	=	t	;
 	double	f3567	=	t	;
 	double	f3568	=	t	;
 	double	f3569	=	t	;
 	double	f3570	=	t	;
 	double	f3571	=	t	;
 	double	f3572	=	t	;
 	double	f3573	=	t	;
 	double	f3574	=	t	;
 	double	f3575	=	t	;
 	double	f3576	=	t	;
 	double	f3577	=	t	;
 	double	f3578	=	t	;
 	double	f3579	=	t	;
 	double	f3580	=	t	;
 	double	f3581	=	t	;
 	double	f3582	=	t	;
 	double	f3583	=	t	;
 	double	f3584	=	t	;
 	double	f3585	=	t	;
 	double	f3586	=	t	;
 	double	f3587	=	t	;
 	double	f3588	=	t	;
 	double	f3589	=	t	;
 	double	f3590	=	t	;
 	double	f3591	=	t	;
 	double	f3592	=	t	;
 	double	f3593	=	t	;
 	double	f3594	=	t	;
 	double	f3595	=	t	;
 	double	f3596	=	t	;
 	double	f3597	=	t	;
 	double	f3598	=	t	;
 	double	f3599	=	t	;
 	double	f3600	=	t	;
 	double	f3601	=	t	;
 	double	f3602	=	t	;
 	double	f3603	=	t	;
 	double	f3604	=	t	;
 	double	f3605	=	t	;
 	double	f3606	=	t	;
 	double	f3607	=	t	;
 	double	f3608	=	t	;
 	double	f3609	=	t	;
 	double	f3610	=	t	;
 	double	f3611	=	t	;
 	double	f3612	=	t	;
 	double	f3613	=	t	;
 	double	f3614	=	t	;
 	double	f3615	=	t	;
 	double	f3616	=	t	;
 	double	f3617	=	t	;
 	double	f3618	=	t	;
 	double	f3619	=	t	;
 	double	f3620	=	t	;
 	double	f3621	=	t	;
 	double	f3622	=	t	;
 	double	f3623	=	t	;
 	double	f3624	=	t	;
 	double	f3625	=	t	;
 	double	f3626	=	t	;
 	double	f3627	=	t	;
 	double	f3628	=	t	;
 	double	f3629	=	t	;
 	double	f3630	=	t	;
 	double	f3631	=	t	;
 	double	f3632	=	t	;
 	double	f3633	=	t	;
 	double	f3634	=	t	;
 	double	f3635	=	t	;
 	double	f3636	=	t	;
 	double	f3637	=	t	;
 	double	f3638	=	t	;
 	double	f3639	=	t	;
 	double	f3640	=	t	;
 	double	f3641	=	t	;
 	double	f3642	=	t	;
 	double	f3643	=	t	;
 	double	f3644	=	t	;
 	double	f3645	=	t	;
 	double	f3646	=	t	;
 	double	f3647	=	t	;
 	double	f3648	=	t	;
 	double	f3649	=	t	;
 	double	f3650	=	t	;
 	double	f3651	=	t	;
 	double	f3652	=	t	;
 	double	f3653	=	t	;
 	double	f3654	=	t	;
 	double	f3655	=	t	;
 	double	f3656	=	t	;
 	double	f3657	=	t	;
 	double	f3658	=	t	;
 	double	f3659	=	t	;
 	double	f3660	=	t	;
 	double	f3661	=	t	;
 	double	f3662	=	t	;
 	double	f3663	=	t	;
 	double	f3664	=	t	;
 	double	f3665	=	t	;
 	double	f3666	=	t	;
 	double	f3667	=	t	;
 	double	f3668	=	t	;
 	double	f3669	=	t	;
 	double	f3670	=	t	;
 	double	f3671	=	t	;
 	double	f3672	=	t	;
 	double	f3673	=	t	;
 	double	f3674	=	t	;
 	double	f3675	=	t	;
 	double	f3676	=	t	;
 	double	f3677	=	t	;
 	double	f3678	=	t	;
 	double	f3679	=	t	;
 	double	f3680	=	t	;
 	double	f3681	=	t	;
 	double	f3682	=	t	;
 	double	f3683	=	t	;
 	double	f3684	=	t	;
 	double	f3685	=	t	;
 	double	f3686	=	t	;
 	double	f3687	=	t	;
 	double	f3688	=	t	;
 	double	f3689	=	t	;
 	double	f3690	=	t	;
 	double	f3691	=	t	;
 	double	f3692	=	t	;
 	double	f3693	=	t	;
 	double	f3694	=	t	;
 	double	f3695	=	t	;
 	double	f3696	=	t	;
 	double	f3697	=	t	;
 	double	f3698	=	t	;
 	double	f3699	=	t	;
 	double	f3700	=	t	;
 	double	f3701	=	t	;
 	double	f3702	=	t	;
 	double	f3703	=	t	;
 	double	f3704	=	t	;
 	double	f3705	=	t	;
 	double	f3706	=	t	;
 	double	f3707	=	t	;
 	double	f3708	=	t	;
 	double	f3709	=	t	;
 	double	f3710	=	t	;
 	double	f3711	=	t	;
 	double	f3712	=	t	;
 	double	f3713	=	t	;
 	double	f3714	=	t	;
 	double	f3715	=	t	;
 	double	f3716	=	t	;
 	double	f3717	=	t	;
 	double	f3718	=	t	;
 	double	f3719	=	t	;
 	double	f3720	=	t	;
 	double	f3721	=	t	;
 	double	f3722	=	t	;
 	double	f3723	=	t	;
 	double	f3724	=	t	;
 	double	f3725	=	t	;
 	double	f3726	=	t	;
 	double	f3727	=	t	;
 	double	f3728	=	t	;
 	double	f3729	=	t	;
 	double	f3730	=	t	;
 	double	f3731	=	t	;
 	double	f3732	=	t	;
 	double	f3733	=	t	;
 	double	f3734	=	t	;
 	double	f3735	=	t	;
 	double	f3736	=	t	;
 	double	f3737	=	t	;
 	double	f3738	=	t	;
 	double	f3739	=	t	;
 	double	f3740	=	t	;
 	double	f3741	=	t	;
 	double	f3742	=	t	;
 	double	f3743	=	t	;
 	double	f3744	=	t	;
 	double	f3745	=	t	;
 	double	f3746	=	t	;
 	double	f3747	=	t	;
 	double	f3748	=	t	;
 	double	f3749	=	t	;
 	double	f3750	=	t	;
 	double	f3751	=	t	;
 	double	f3752	=	t	;
 	double	f3753	=	t	;
 	double	f3754	=	t	;
 	double	f3755	=	t	;
 	double	f3756	=	t	;
 	double	f3757	=	t	;
 	double	f3758	=	t	;
 	double	f3759	=	t	;
 	double	f3760	=	t	;
 	double	f3761	=	t	;
 	double	f3762	=	t	;
 	double	f3763	=	t	;
 	double	f3764	=	t	;
 	double	f3765	=	t	;
 	double	f3766	=	t	;
 	double	f3767	=	t	;
 	double	f3768	=	t	;
 	double	f3769	=	t	;
 	double	f3770	=	t	;
 	double	f3771	=	t	;
 	double	f3772	=	t	;
 	double	f3773	=	t	;
 	double	f3774	=	t	;
 	double	f3775	=	t	;
 	double	f3776	=	t	;
 	double	f3777	=	t	;
 	double	f3778	=	t	;
 	double	f3779	=	t	;
 	double	f3780	=	t	;
 	double	f3781	=	t	;
 	double	f3782	=	t	;
 	double	f3783	=	t	;
 	double	f3784	=	t	;
 	double	f3785	=	t	;
 	double	f3786	=	t	;
 	double	f3787	=	t	;
 	double	f3788	=	t	;
 	double	f3789	=	t	;
 	double	f3790	=	t	;
 	double	f3791	=	t	;
 	double	f3792	=	t	;
 	double	f3793	=	t	;
 	double	f3794	=	t	;
 	double	f3795	=	t	;
 	double	f3796	=	t	;
 	double	f3797	=	t	;
 	double	f3798	=	t	;
 	double	f3799	=	t	;
 	double	f3800	=	t	;
 	double	f3801	=	t	;
 	double	f3802	=	t	;
 	double	f3803	=	t	;
 	double	f3804	=	t	;
 	double	f3805	=	t	;
 	double	f3806	=	t	;
 	double	f3807	=	t	;
 	double	f3808	=	t	;
 	double	f3809	=	t	;
 	double	f3810	=	t	;
 	double	f3811	=	t	;
 	double	f3812	=	t	;
 	double	f3813	=	t	;
 	double	f3814	=	t	;
 	double	f3815	=	t	;
 	double	f3816	=	t	;
 	double	f3817	=	t	;
 	double	f3818	=	t	;
 	double	f3819	=	t	;
 	double	f3820	=	t	;
 	double	f3821	=	t	;
 	double	f3822	=	t	;
 	double	f3823	=	t	;
 	double	f3824	=	t	;
 	double	f3825	=	t	;
 	double	f3826	=	t	;
 	double	f3827	=	t	;
 	double	f3828	=	t	;
 	double	f3829	=	t	;
 	double	f3830	=	t	;
 	double	f3831	=	t	;
 	double	f3832	=	t	;
 	double	f3833	=	t	;
 	double	f3834	=	t	;
 	double	f3835	=	t	;
 	double	f3836	=	t	;
 	double	f3837	=	t	;
 	double	f3838	=	t	;
 	double	f3839	=	t	;
 	double	f3840	=	t	;
 	double	f3841	=	t	;
 	double	f3842	=	t	;
 	double	f3843	=	t	;
 	double	f3844	=	t	;
 	double	f3845	=	t	;
 	double	f3846	=	t	;
 	double	f3847	=	t	;
 	double	f3848	=	t	;
 	double	f3849	=	t	;
 	double	f3850	=	t	;
 	double	f3851	=	t	;
 	double	f3852	=	t	;
 	double	f3853	=	t	;
 	double	f3854	=	t	;
 	double	f3855	=	t	;
 	double	f3856	=	t	;
 	double	f3857	=	t	;
 	double	f3858	=	t	;
 	double	f3859	=	t	;
 	double	f3860	=	t	;
 	double	f3861	=	t	;
 	double	f3862	=	t	;
 	double	f3863	=	t	;
 	double	f3864	=	t	;
 	double	f3865	=	t	;
 	double	f3866	=	t	;
 	double	f3867	=	t	;
 	double	f3868	=	t	;
 	double	f3869	=	t	;
 	double	f3870	=	t	;
 	double	f3871	=	t	;
 	double	f3872	=	t	;
 	double	f3873	=	t	;
 	double	f3874	=	t	;
 	double	f3875	=	t	;
 	double	f3876	=	t	;
 	double	f3877	=	t	;
 	double	f3878	=	t	;
 	double	f3879	=	t	;
 	double	f3880	=	t	;
 	double	f3881	=	t	;
 	double	f3882	=	t	;
 	double	f3883	=	t	;
 	double	f3884	=	t	;
 	double	f3885	=	t	;
 	double	f3886	=	t	;
 	double	f3887	=	t	;
 	double	f3888	=	t	;
 	double	f3889	=	t	;
 	double	f3890	=	t	;
 	double	f3891	=	t	;
 	double	f3892	=	t	;
 	double	f3893	=	t	;
 	double	f3894	=	t	;
 	double	f3895	=	t	;
 	double	f3896	=	t	;
 	double	f3897	=	t	;
 	double	f3898	=	t	;
 	double	f3899	=	t	;
 	double	f3900	=	t	;
 	double	f3901	=	t	;
 	double	f3902	=	t	;
 	double	f3903	=	t	;
 	double	f3904	=	t	;
 	double	f3905	=	t	;
 	double	f3906	=	t	;
 	double	f3907	=	t	;
 	double	f3908	=	t	;
 	double	f3909	=	t	;
 	double	f3910	=	t	;
 	double	f3911	=	t	;
 	double	f3912	=	t	;
 	double	f3913	=	t	;
 	double	f3914	=	t	;
 	double	f3915	=	t	;
 	double	f3916	=	t	;
 	double	f3917	=	t	;
 	double	f3918	=	t	;
 	double	f3919	=	t	;
 	double	f3920	=	t	;
 	double	f3921	=	t	;
 	double	f3922	=	t	;
 	double	f3923	=	t	;
 	double	f3924	=	t	;
 	double	f3925	=	t	;
 	double	f3926	=	t	;
 	double	f3927	=	t	;
 	double	f3928	=	t	;
 	double	f3929	=	t	;
 	double	f3930	=	t	;
 	double	f3931	=	t	;
 	double	f3932	=	t	;
 	double	f3933	=	t	;
 	double	f3934	=	t	;
 	double	f3935	=	t	;
 	double	f3936	=	t	;
 	double	f3937	=	t	;
 	double	f3938	=	t	;
 	double	f3939	=	t	;
 	double	f3940	=	t	;
 	double	f3941	=	t	;
 	double	f3942	=	t	;
 	double	f3943	=	t	;
 	double	f3944	=	t	;
 	double	f3945	=	t	;
 	double	f3946	=	t	;
 	double	f3947	=	t	;
 	double	f3948	=	t	;
 	double	f3949	=	t	;
 	double	f3950	=	t	;
 	double	f3951	=	t	;
 	double	f3952	=	t	;
 	double	f3953	=	t	;
 	double	f3954	=	t	;
 	double	f3955	=	t	;
 	double	f3956	=	t	;
 	double	f3957	=	t	;
 	double	f3958	=	t	;
 	double	f3959	=	t	;
 	double	f3960	=	t	;
 	double	f3961	=	t	;
 	double	f3962	=	t	;
 	double	f3963	=	t	;
 	double	f3964	=	t	;
 	double	f3965	=	t	;
 	double	f3966	=	t	;
 	double	f3967	=	t	;
 	double	f3968	=	t	;
 	double	f3969	=	t	;
 	double	f3970	=	t	;
 	double	f3971	=	t	;
 	double	f3972	=	t	;
 	double	f3973	=	t	;
 	double	f3974	=	t	;
 	double	f3975	=	t	;
 	double	f3976	=	t	;
 	double	f3977	=	t	;
 	double	f3978	=	t	;
 	double	f3979	=	t	;
 	double	f3980	=	t	;
 	double	f3981	=	t	;
 	double	f3982	=	t	;
 	double	f3983	=	t	;
 	double	f3984	=	t	;
 	double	f3985	=	t	;
 	double	f3986	=	t	;
 	double	f3987	=	t	;
 	double	f3988	=	t	;
 	double	f3989	=	t	;
 	double	f3990	=	t	;
 	double	f3991	=	t	;
 	double	f3992	=	t	;
 	double	f3993	=	t	;
 	double	f3994	=	t	;
 	double	f3995	=	t	;
 	double	f3996	=	t	;
 	double	f3997	=	t	;
 	double	f3998	=	t	;
 	double	f3999	=	t	;
 	double	f4000	=	t	;
 	double	f4001	=	t	;
 	double	f4002	=	t	;
 	double	f4003	=	t	;
 	double	f4004	=	t	;
 	double	f4005	=	t	;
 	double	f4006	=	t	;
 	double	f4007	=	t	;
 	double	f4008	=	t	;
 	double	f4009	=	t	;
 	double	f4010	=	t	;
 	double	f4011	=	t	;
 	double	f4012	=	t	;
 	double	f4013	=	t	;
 	double	f4014	=	t	;
 	double	f4015	=	t	;
 	double	f4016	=	t	;
 	double	f4017	=	t	;
 	double	f4018	=	t	;
 	double	f4019	=	t	;
 	double	f4020	=	t	;
 	double	f4021	=	t	;
 	double	f4022	=	t	;
 	double	f4023	=	t	;
 	double	f4024	=	t	;
 	double	f4025	=	t	;
 	double	f4026	=	t	;
 	double	f4027	=	t	;
 	double	f4028	=	t	;
 	double	f4029	=	t	;
 	double	f4030	=	t	;
 	double	f4031	=	t	;
 	double	f4032	=	t	;
 	double	f4033	=	t	;
 	double	f4034	=	t	;
 	double	f4035	=	t	;
 	double	f4036	=	t	;
 	double	f4037	=	t	;
 	double	f4038	=	t	;
 	double	f4039	=	t	;
 	double	f4040	=	t	;
 	double	f4041	=	t	;
 	double	f4042	=	t	;
 	double	f4043	=	t	;
 	double	f4044	=	t	;
 	double	f4045	=	t	;
 	double	f4046	=	t	;
 	double	f4047	=	t	;
 	double	f4048	=	t	;
 	double	f4049	=	t	;
 	double	f4050	=	t	;
 	double	f4051	=	t	;
 	double	f4052	=	t	;
 	double	f4053	=	t	;
 	double	f4054	=	t	;
 	double	f4055	=	t	;
 	double	f4056	=	t	;
 	double	f4057	=	t	;
 	double	f4058	=	t	;
 	double	f4059	=	t	;
 	double	f4060	=	t	;
 	double	f4061	=	t	;
 	double	f4062	=	t	;
 	double	f4063	=	t	;
 	double	f4064	=	t	;
 	double	f4065	=	t	;
 	double	f4066	=	t	;
 	double	f4067	=	t	;
 	double	f4068	=	t	;
 	double	f4069	=	t	;
 	double	f4070	=	t	;
 	double	f4071	=	t	;
 	double	f4072	=	t	;
 	double	f4073	=	t	;
 	double	f4074	=	t	;
 	double	f4075	=	t	;
 	double	f4076	=	t	;
 	double	f4077	=	t	;
 	double	f4078	=	t	;
 	double	f4079	=	t	;
 	double	f4080	=	t	;
 	double	f4081	=	t	;
 	double	f4082	=	t	;
 	double	f4083	=	t	;
 	double	f4084	=	t	;
 	double	f4085	=	t	;
 	double	f4086	=	t	;
 	double	f4087	=	t	;
 	double	f4088	=	t	;
 	double	f4089	=	t	;
 	double	f4090	=	t	;
 	double	f4091	=	t	;
 	double	f4092	=	t	;
 	double	f4093	=	t	;
 	double	f4094	=	t	;
 	double	f4095	=	t	;
 	double	f4096	=	t	;
 	double	f4097	=	t	;
 	double	f4098	=	t	;
 	double	f4099	=	t	;
 	double	f4100	=	t	;
 	double	f4101	=	t	;
 	double	f4102	=	t	;
 	double	f4103	=	t	;
 	double	f4104	=	t	;
 	double	f4105	=	t	;
 	double	f4106	=	t	;
 	double	f4107	=	t	;
 	double	f4108	=	t	;
 	double	f4109	=	t	;
 	double	f4110	=	t	;
 	double	f4111	=	t	;
 	double	f4112	=	t	;
 	double	f4113	=	t	;
 	double	f4114	=	t	;
 	double	f4115	=	t	;
 	double	f4116	=	t	;
 	double	f4117	=	t	;
 	double	f4118	=	t	;
 	double	f4119	=	t	;
 	double	f4120	=	t	;
 	double	f4121	=	t	;
 	double	f4122	=	t	;
 	double	f4123	=	t	;
 	double	f4124	=	t	;
 	double	f4125	=	t	;
 	double	f4126	=	t	;
 	double	f4127	=	t	;
 	double	f4128	=	t	;
 	double	f4129	=	t	;
 	double	f4130	=	t	;
 	double	f4131	=	t	;
 	double	f4132	=	t	;
 	double	f4133	=	t	;
 	double	f4134	=	t	;
 	double	f4135	=	t	;
 	double	f4136	=	t	;
 	double	f4137	=	t	;
 	double	f4138	=	t	;
 	double	f4139	=	t	;
 	double	f4140	=	t	;
 	double	f4141	=	t	;
 	double	f4142	=	t	;
 	double	f4143	=	t	;
 	double	f4144	=	t	;
 	double	f4145	=	t	;
 	double	f4146	=	t	;
 	double	f4147	=	t	;
 	double	f4148	=	t	;
 	double	f4149	=	t	;
 	double	f4150	=	t	;
 	double	f4151	=	t	;
 	double	f4152	=	t	;
 	double	f4153	=	t	;
 	double	f4154	=	t	;
 	double	f4155	=	t	;
 	double	f4156	=	t	;
 	double	f4157	=	t	;
 	double	f4158	=	t	;
 	double	f4159	=	t	;
 	double	f4160	=	t	;
 	double	f4161	=	t	;
 	double	f4162	=	t	;
 	double	f4163	=	t	;
 	double	f4164	=	t	;
 	double	f4165	=	t	;
 	double	f4166	=	t	;
 	double	f4167	=	t	;
 	double	f4168	=	t	;
 	double	f4169	=	t	;
 	double	f4170	=	t	;
 	double	f4171	=	t	;
 	double	f4172	=	t	;
 	double	f4173	=	t	;
 	double	f4174	=	t	;
 	double	f4175	=	t	;
 	double	f4176	=	t	;
 	double	f4177	=	t	;
 	double	f4178	=	t	;
 	double	f4179	=	t	;
 	double	f4180	=	t	;
 	double	f4181	=	t	;
 	double	f4182	=	t	;
 	double	f4183	=	t	;
 	double	f4184	=	t	;
 	double	f4185	=	t	;
 	double	f4186	=	t	;
 	double	f4187	=	t	;
 	double	f4188	=	t	;
 	double	f4189	=	t	;
 	double	f4190	=	t	;
 	double	f4191	=	t	;
 	double	f4192	=	t	;
 	double	f4193	=	t	;
 	double	f4194	=	t	;
 	double	f4195	=	t	;
 	double	f4196	=	t	;
 	double	f4197	=	t	;
 	double	f4198	=	t	;
 	double	f4199	=	t	;
 	double	f4200	=	t	;
 	double	f4201	=	t	;
 	double	f4202	=	t	;
 	double	f4203	=	t	;
 	double	f4204	=	t	;
 	double	f4205	=	t	;
 	double	f4206	=	t	;
 	double	f4207	=	t	;
 	double	f4208	=	t	;
 	double	f4209	=	t	;
 	double	f4210	=	t	;
 	double	f4211	=	t	;
 	double	f4212	=	t	;
 	double	f4213	=	t	;
 	double	f4214	=	t	;
 	double	f4215	=	t	;
 	double	f4216	=	t	;
 	double	f4217	=	t	;
 	double	f4218	=	t	;
 	double	f4219	=	t	;
 	double	f4220	=	t	;
 	double	f4221	=	t	;
 	double	f4222	=	t	;
 	double	f4223	=	t	;
 	double	f4224	=	t	;
 	double	f4225	=	t	;
 	double	f4226	=	t	;
 	double	f4227	=	t	;
 	double	f4228	=	t	;
 	double	f4229	=	t	;
 	double	f4230	=	t	;
 	double	f4231	=	t	;
 	double	f4232	=	t	;
 	double	f4233	=	t	;
 	double	f4234	=	t	;
 	double	f4235	=	t	;
 	double	f4236	=	t	;
 	double	f4237	=	t	;
 	double	f4238	=	t	;
 	double	f4239	=	t	;
 	double	f4240	=	t	;
 	double	f4241	=	t	;
 	double	f4242	=	t	;
 	double	f4243	=	t	;
 	double	f4244	=	t	;
 	double	f4245	=	t	;
 	double	f4246	=	t	;
 	double	f4247	=	t	;
 	double	f4248	=	t	;
 	double	f4249	=	t	;
 	double	f4250	=	t	;
 	double	f4251	=	t	;
 	double	f4252	=	t	;
 	double	f4253	=	t	;
 	double	f4254	=	t	;
 	double	f4255	=	t	;
 	double	f4256	=	t	;
 	double	f4257	=	t	;
 	double	f4258	=	t	;
 	double	f4259	=	t	;
 	double	f4260	=	t	;
 	double	f4261	=	t	;
 	double	f4262	=	t	;
 	double	f4263	=	t	;
 	double	f4264	=	t	;
 	double	f4265	=	t	;
 	double	f4266	=	t	;
 	double	f4267	=	t	;
 	double	f4268	=	t	;
 	double	f4269	=	t	;
 	double	f4270	=	t	;
 	double	f4271	=	t	;
 	double	f4272	=	t	;
 	double	f4273	=	t	;
 	double	f4274	=	t	;
 	double	f4275	=	t	;
 	double	f4276	=	t	;
 	double	f4277	=	t	;
 	double	f4278	=	t	;
 	double	f4279	=	t	;
 	double	f4280	=	t	;
 	double	f4281	=	t	;
 	double	f4282	=	t	;
 	double	f4283	=	t	;
 	double	f4284	=	t	;
 	double	f4285	=	t	;
 	double	f4286	=	t	;
 	double	f4287	=	t	;
 	double	f4288	=	t	;
 	double	f4289	=	t	;
 	double	f4290	=	t	;
 	double	f4291	=	t	;
 	double	f4292	=	t	;
 	double	f4293	=	t	;
 	double	f4294	=	t	;
 	double	f4295	=	t	;
 	double	f4296	=	t	;
 	double	f4297	=	t	;
 	double	f4298	=	t	;
 	double	f4299	=	t	;
 	double	f4300	=	t	;
 	double	f4301	=	t	;
 	double	f4302	=	t	;
 	double	f4303	=	t	;
 	double	f4304	=	t	;
 	double	f4305	=	t	;
 	double	f4306	=	t	;
 	double	f4307	=	t	;
 	double	f4308	=	t	;
 	double	f4309	=	t	;
 	double	f4310	=	t	;
 	double	f4311	=	t	;
 	double	f4312	=	t	;
 	double	f4313	=	t	;
 	double	f4314	=	t	;
 	double	f4315	=	t	;
 	double	f4316	=	t	;
 	double	f4317	=	t	;
 	double	f4318	=	t	;
 	double	f4319	=	t	;
 	double	f4320	=	t	;
 	double	f4321	=	t	;
 	double	f4322	=	t	;
 	double	f4323	=	t	;
 	double	f4324	=	t	;
 	double	f4325	=	t	;
 	double	f4326	=	t	;
 	double	f4327	=	t	;
 	double	f4328	=	t	;
 	double	f4329	=	t	;
 	double	f4330	=	t	;
 	double	f4331	=	t	;
 	double	f4332	=	t	;
 	double	f4333	=	t	;
 	double	f4334	=	t	;
 	double	f4335	=	t	;
 	double	f4336	=	t	;
 	double	f4337	=	t	;
 	double	f4338	=	t	;
 	double	f4339	=	t	;
 	double	f4340	=	t	;
 	double	f4341	=	t	;
 	double	f4342	=	t	;
 	double	f4343	=	t	;
 	double	f4344	=	t	;
 	double	f4345	=	t	;
 	double	f4346	=	t	;
 	double	f4347	=	t	;
 	double	f4348	=	t	;
 	double	f4349	=	t	;
 	double	f4350	=	t	;
 	double	f4351	=	t	;
 	double	f4352	=	t	;
 	double	f4353	=	t	;
 	double	f4354	=	t	;
 	double	f4355	=	t	;
 	double	f4356	=	t	;
 	double	f4357	=	t	;
 	double	f4358	=	t	;
 	double	f4359	=	t	;
 	double	f4360	=	t	;
 	double	f4361	=	t	;
 	double	f4362	=	t	;
 	double	f4363	=	t	;
 	double	f4364	=	t	;
 	double	f4365	=	t	;
 	double	f4366	=	t	;
 	double	f4367	=	t	;
 	double	f4368	=	t	;
 	double	f4369	=	t	;
 	double	f4370	=	t	;
 	double	f4371	=	t	;
 	double	f4372	=	t	;
 	double	f4373	=	t	;
 	double	f4374	=	t	;
 	double	f4375	=	t	;
 	double	f4376	=	t	;
 	double	f4377	=	t	;
 	double	f4378	=	t	;
 	double	f4379	=	t	;
 	double	f4380	=	t	;
 	double	f4381	=	t	;
 	double	f4382	=	t	;
 	double	f4383	=	t	;
 	double	f4384	=	t	;
 	double	f4385	=	t	;
 	double	f4386	=	t	;
 	double	f4387	=	t	;
 	double	f4388	=	t	;
 	double	f4389	=	t	;
 	double	f4390	=	t	;
 	double	f4391	=	t	;
 	double	f4392	=	t	;
 	double	f4393	=	t	;
 	double	f4394	=	t	;
 	double	f4395	=	t	;
 	double	f4396	=	t	;
 	double	f4397	=	t	;
 	double	f4398	=	t	;
 	double	f4399	=	t	;
 	double	f4400	=	t	;
 	double	f4401	=	t	;
 	double	f4402	=	t	;
 	double	f4403	=	t	;
 	double	f4404	=	t	;
 	double	f4405	=	t	;
 	double	f4406	=	t	;
 	double	f4407	=	t	;
 	double	f4408	=	t	;
 	double	f4409	=	t	;
 	double	f4410	=	t	;
 	double	f4411	=	t	;
 	double	f4412	=	t	;
 	double	f4413	=	t	;
 	double	f4414	=	t	;
 	double	f4415	=	t	;
 	double	f4416	=	t	;
 	double	f4417	=	t	;
 	double	f4418	=	t	;
 	double	f4419	=	t	;
 	double	f4420	=	t	;
 	double	f4421	=	t	;
 	double	f4422	=	t	;
 	double	f4423	=	t	;
 	double	f4424	=	t	;
 	double	f4425	=	t	;
 	double	f4426	=	t	;
 	double	f4427	=	t	;
 	double	f4428	=	t	;
 	double	f4429	=	t	;
 	double	f4430	=	t	;
 	double	f4431	=	t	;
 	double	f4432	=	t	;
 	double	f4433	=	t	;
 	double	f4434	=	t	;
 	double	f4435	=	t	;
 	double	f4436	=	t	;
 	double	f4437	=	t	;
 	double	f4438	=	t	;
 	double	f4439	=	t	;
 	double	f4440	=	t	;
 	double	f4441	=	t	;
 	double	f4442	=	t	;
 	double	f4443	=	t	;
 	double	f4444	=	t	;
 	double	f4445	=	t	;
 	double	f4446	=	t	;
 	double	f4447	=	t	;
 	double	f4448	=	t	;
 	double	f4449	=	t	;
 	double	f4450	=	t	;
 	double	f4451	=	t	;
 	double	f4452	=	t	;
 	double	f4453	=	t	;
 	double	f4454	=	t	;
 	double	f4455	=	t	;
 	double	f4456	=	t	;
 	double	f4457	=	t	;
 	double	f4458	=	t	;
 	double	f4459	=	t	;
 	double	f4460	=	t	;
 	double	f4461	=	t	;
 	double	f4462	=	t	;
 	double	f4463	=	t	;
 	double	f4464	=	t	;
 	double	f4465	=	t	;
 	double	f4466	=	t	;
 	double	f4467	=	t	;
 	double	f4468	=	t	;
 	double	f4469	=	t	;
 	double	f4470	=	t	;
 	double	f4471	=	t	;
 	double	f4472	=	t	;
 	double	f4473	=	t	;
 	double	f4474	=	t	;
 	double	f4475	=	t	;
 	double	f4476	=	t	;
 	double	f4477	=	t	;
 	double	f4478	=	t	;
 	double	f4479	=	t	;
 	double	f4480	=	t	;
 	double	f4481	=	t	;
 	double	f4482	=	t	;
 	double	f4483	=	t	;
 	double	f4484	=	t	;
 	double	f4485	=	t	;
 	double	f4486	=	t	;
 	double	f4487	=	t	;
 	double	f4488	=	t	;
 	double	f4489	=	t	;
 	double	f4490	=	t	;
 	double	f4491	=	t	;
 	double	f4492	=	t	;
 	double	f4493	=	t	;
 	double	f4494	=	t	;
 	double	f4495	=	t	;
 	double	f4496	=	t	;
 	double	f4497	=	t	;
 	double	f4498	=	t	;
 	double	f4499	=	t	;
 	double	f4500	=	t	;
 	double	f4501	=	t	;
 	double	f4502	=	t	;
 	double	f4503	=	t	;
 	double	f4504	=	t	;
 	double	f4505	=	t	;
 	double	f4506	=	t	;
 	double	f4507	=	t	;
 	double	f4508	=	t	;
 	double	f4509	=	t	;
 	double	f4510	=	t	;
 	double	f4511	=	t	;
 	double	f4512	=	t	;
 	double	f4513	=	t	;
 	double	f4514	=	t	;
 	double	f4515	=	t	;
 	double	f4516	=	t	;
 	double	f4517	=	t	;
 	double	f4518	=	t	;
 	double	f4519	=	t	;
 	double	f4520	=	t	;
 	double	f4521	=	t	;
 	double	f4522	=	t	;
 	double	f4523	=	t	;
 	double	f4524	=	t	;
 	double	f4525	=	t	;
 	double	f4526	=	t	;
 	double	f4527	=	t	;
 	double	f4528	=	t	;
 	double	f4529	=	t	;
 	double	f4530	=	t	;
 	double	f4531	=	t	;
 	double	f4532	=	t	;
 	double	f4533	=	t	;
 	double	f4534	=	t	;
 	double	f4535	=	t	;
 	double	f4536	=	t	;
 	double	f4537	=	t	;
 	double	f4538	=	t	;
 	double	f4539	=	t	;
 	double	f4540	=	t	;
 	double	f4541	=	t	;
 	double	f4542	=	t	;
 	double	f4543	=	t	;
 	double	f4544	=	t	;
 	double	f4545	=	t	;
 	double	f4546	=	t	;
 	double	f4547	=	t	;
 	double	f4548	=	t	;
 	double	f4549	=	t	;
 	double	f4550	=	t	;
 	double	f4551	=	t	;
 	double	f4552	=	t	;
 	double	f4553	=	t	;
 	double	f4554	=	t	;
 	double	f4555	=	t	;
 	double	f4556	=	t	;
 	double	f4557	=	t	;
 	double	f4558	=	t	;
 	double	f4559	=	t	;
 	double	f4560	=	t	;
 	double	f4561	=	t	;
 	double	f4562	=	t	;
 	double	f4563	=	t	;
 	double	f4564	=	t	;
 	double	f4565	=	t	;
 	double	f4566	=	t	;
 	double	f4567	=	t	;
 	double	f4568	=	t	;
 	double	f4569	=	t	;
 	double	f4570	=	t	;
 	double	f4571	=	t	;
 	double	f4572	=	t	;
 	double	f4573	=	t	;
 	double	f4574	=	t	;
 	double	f4575	=	t	;
 	double	f4576	=	t	;
 	double	f4577	=	t	;
 	double	f4578	=	t	;
 	double	f4579	=	t	;
 	double	f4580	=	t	;
 	double	f4581	=	t	;
 	double	f4582	=	t	;
 	double	f4583	=	t	;
 	double	f4584	=	t	;
 	double	f4585	=	t	;
 	double	f4586	=	t	;
 	double	f4587	=	t	;
 	double	f4588	=	t	;
 	double	f4589	=	t	;
 	double	f4590	=	t	;
 	double	f4591	=	t	;
 	double	f4592	=	t	;
 	double	f4593	=	t	;
 	double	f4594	=	t	;
 	double	f4595	=	t	;
 	double	f4596	=	t	;
 	double	f4597	=	t	;
 	double	f4598	=	t	;
 	double	f4599	=	t	;
 	double	f4600	=	t	;
 	double	f4601	=	t	;
 	double	f4602	=	t	;
 	double	f4603	=	t	;
 	double	f4604	=	t	;
 	double	f4605	=	t	;
 	double	f4606	=	t	;
 	double	f4607	=	t	;
 	double	f4608	=	t	;
 	double	f4609	=	t	;
 	double	f4610	=	t	;
 	double	f4611	=	t	;
 	double	f4612	=	t	;
 	double	f4613	=	t	;
 	double	f4614	=	t	;
 	double	f4615	=	t	;
 	double	f4616	=	t	;
 	double	f4617	=	t	;
 	double	f4618	=	t	;
 	double	f4619	=	t	;
 	double	f4620	=	t	;
 	double	f4621	=	t	;
 	double	f4622	=	t	;
 	double	f4623	=	t	;
 	double	f4624	=	t	;
 	double	f4625	=	t	;
 	double	f4626	=	t	;
 	double	f4627	=	t	;
 	double	f4628	=	t	;
 	double	f4629	=	t	;
 	double	f4630	=	t	;
 	double	f4631	=	t	;
 	double	f4632	=	t	;
 	double	f4633	=	t	;
 	double	f4634	=	t	;
 	double	f4635	=	t	;
 	double	f4636	=	t	;
 	double	f4637	=	t	;
 	double	f4638	=	t	;
 	double	f4639	=	t	;
 	double	f4640	=	t	;
 	double	f4641	=	t	;
 	double	f4642	=	t	;
 	double	f4643	=	t	;
 	double	f4644	=	t	;
 	double	f4645	=	t	;
 	double	f4646	=	t	;
 	double	f4647	=	t	;
 	double	f4648	=	t	;
 	double	f4649	=	t	;
 	double	f4650	=	t	;
 	double	f4651	=	t	;
 	double	f4652	=	t	;
 	double	f4653	=	t	;
 	double	f4654	=	t	;
 	double	f4655	=	t	;
 	double	f4656	=	t	;
 	double	f4657	=	t	;
 	double	f4658	=	t	;
 	double	f4659	=	t	;
 	double	f4660	=	t	;
 	double	f4661	=	t	;
 	double	f4662	=	t	;
 	double	f4663	=	t	;
 	double	f4664	=	t	;
 	double	f4665	=	t	;
 	double	f4666	=	t	;
 	double	f4667	=	t	;
 	double	f4668	=	t	;
 	double	f4669	=	t	;
 	double	f4670	=	t	;
 	double	f4671	=	t	;
 	double	f4672	=	t	;
 	double	f4673	=	t	;
 	double	f4674	=	t	;
 	double	f4675	=	t	;
 	double	f4676	=	t	;
 	double	f4677	=	t	;
 	double	f4678	=	t	;
 	double	f4679	=	t	;
 	double	f4680	=	t	;
 	double	f4681	=	t	;
 	double	f4682	=	t	;
 	double	f4683	=	t	;
 	double	f4684	=	t	;
 	double	f4685	=	t	;
 	double	f4686	=	t	;
 	double	f4687	=	t	;
 	double	f4688	=	t	;
 	double	f4689	=	t	;
 	double	f4690	=	t	;
 	double	f4691	=	t	;
 	double	f4692	=	t	;
 	double	f4693	=	t	;
 	double	f4694	=	t	;
 	double	f4695	=	t	;
 	double	f4696	=	t	;
 	double	f4697	=	t	;
 	double	f4698	=	t	;
 	double	f4699	=	t	;
 	double	f4700	=	t	;
 	double	f4701	=	t	;
 	double	f4702	=	t	;
 	double	f4703	=	t	;
 	double	f4704	=	t	;
 	double	f4705	=	t	;
 	double	f4706	=	t	;
 	double	f4707	=	t	;
 	double	f4708	=	t	;
 	double	f4709	=	t	;
 	double	f4710	=	t	;
 	double	f4711	=	t	;
 	double	f4712	=	t	;
 	double	f4713	=	t	;
 	double	f4714	=	t	;
 	double	f4715	=	t	;
 	double	f4716	=	t	;
 	double	f4717	=	t	;
 	double	f4718	=	t	;
 	double	f4719	=	t	;
 	double	f4720	=	t	;
 	double	f4721	=	t	;
 	double	f4722	=	t	;
 	double	f4723	=	t	;
 	double	f4724	=	t	;
 	double	f4725	=	t	;
 	double	f4726	=	t	;
 	double	f4727	=	t	;
 	double	f4728	=	t	;
 	double	f4729	=	t	;
 	double	f4730	=	t	;
 	double	f4731	=	t	;
 	double	f4732	=	t	;
 	double	f4733	=	t	;
 	double	f4734	=	t	;
 	double	f4735	=	t	;
 	double	f4736	=	t	;
 	double	f4737	=	t	;
 	double	f4738	=	t	;
 	double	f4739	=	t	;
 	double	f4740	=	t	;
 	double	f4741	=	t	;
 	double	f4742	=	t	;
 	double	f4743	=	t	;
 	double	f4744	=	t	;
 	double	f4745	=	t	;
 	double	f4746	=	t	;
 	double	f4747	=	t	;
 	double	f4748	=	t	;
 	double	f4749	=	t	;
 	double	f4750	=	t	;
 	double	f4751	=	t	;
 	double	f4752	=	t	;
 	double	f4753	=	t	;
 	double	f4754	=	t	;
 	double	f4755	=	t	;
 	double	f4756	=	t	;
 	double	f4757	=	t	;
 	double	f4758	=	t	;
 	double	f4759	=	t	;
 	double	f4760	=	t	;
 	double	f4761	=	t	;
 	double	f4762	=	t	;
 	double	f4763	=	t	;
 	double	f4764	=	t	;
 	double	f4765	=	t	;
 	double	f4766	=	t	;
 	double	f4767	=	t	;
 	double	f4768	=	t	;
 	double	f4769	=	t	;
 	double	f4770	=	t	;
 	double	f4771	=	t	;
 	double	f4772	=	t	;
 	double	f4773	=	t	;
 	double	f4774	=	t	;
 	double	f4775	=	t	;
 	double	f4776	=	t	;
 	double	f4777	=	t	;
 	double	f4778	=	t	;
 	double	f4779	=	t	;
 	double	f4780	=	t	;
 	double	f4781	=	t	;
 	double	f4782	=	t	;
 	double	f4783	=	t	;
 	double	f4784	=	t	;
 	double	f4785	=	t	;
 	double	f4786	=	t	;
 	double	f4787	=	t	;
 	double	f4788	=	t	;
 	double	f4789	=	t	;
 	double	f4790	=	t	;
 	double	f4791	=	t	;
 	double	f4792	=	t	;
 	double	f4793	=	t	;
 	double	f4794	=	t	;
 	double	f4795	=	t	;
 	double	f4796	=	t	;
 	double	f4797	=	t	;
 	double	f4798	=	t	;
 	double	f4799	=	t	;
 	double	f4800	=	t	;
 	double	f4801	=	t	;
 	double	f4802	=	t	;
 	double	f4803	=	t	;
 	double	f4804	=	t	;
 	double	f4805	=	t	;
 	double	f4806	=	t	;
 	double	f4807	=	t	;
 	double	f4808	=	t	;
 	double	f4809	=	t	;
 	double	f4810	=	t	;
 	double	f4811	=	t	;
 	double	f4812	=	t	;
 	double	f4813	=	t	;
 	double	f4814	=	t	;
 	double	f4815	=	t	;
 	double	f4816	=	t	;
 	double	f4817	=	t	;
 	double	f4818	=	t	;
 	double	f4819	=	t	;
 	double	f4820	=	t	;
 	double	f4821	=	t	;
 	double	f4822	=	t	;
 	double	f4823	=	t	;
 	double	f4824	=	t	;
 	double	f4825	=	t	;
 	double	f4826	=	t	;
 	double	f4827	=	t	;
 	double	f4828	=	t	;
 	double	f4829	=	t	;
 	double	f4830	=	t	;
 	double	f4831	=	t	;
 	double	f4832	=	t	;
 	double	f4833	=	t	;
 	double	f4834	=	t	;
 	double	f4835	=	t	;
 	double	f4836	=	t	;
 	double	f4837	=	t	;
 	double	f4838	=	t	;
 	double	f4839	=	t	;
 	double	f4840	=	t	;
 	double	f4841	=	t	;
 	double	f4842	=	t	;
 	double	f4843	=	t	;
 	double	f4844	=	t	;
 	double	f4845	=	t	;
 	double	f4846	=	t	;
 	double	f4847	=	t	;
 	double	f4848	=	t	;
 	double	f4849	=	t	;
 	double	f4850	=	t	;
 	double	f4851	=	t	;
 	double	f4852	=	t	;
 	double	f4853	=	t	;
 	double	f4854	=	t	;
 	double	f4855	=	t	;
 	double	f4856	=	t	;
 	double	f4857	=	t	;
 	double	f4858	=	t	;
 	double	f4859	=	t	;
 	double	f4860	=	t	;
 	double	f4861	=	t	;
 	double	f4862	=	t	;
 	double	f4863	=	t	;
 	double	f4864	=	t	;
 	double	f4865	=	t	;
 	double	f4866	=	t	;
 	double	f4867	=	t	;
 	double	f4868	=	t	;
 	double	f4869	=	t	;
 	double	f4870	=	t	;
 	double	f4871	=	t	;
 	double	f4872	=	t	;
 	double	f4873	=	t	;
 	double	f4874	=	t	;
 	double	f4875	=	t	;
 	double	f4876	=	t	;
 	double	f4877	=	t	;
 	double	f4878	=	t	;
 	double	f4879	=	t	;
 	double	f4880	=	t	;
 	double	f4881	=	t	;
 	double	f4882	=	t	;
 	double	f4883	=	t	;
 	double	f4884	=	t	;
 	double	f4885	=	t	;
 	double	f4886	=	t	;
 	double	f4887	=	t	;
 	double	f4888	=	t	;
 	double	f4889	=	t	;
 	double	f4890	=	t	;
 	double	f4891	=	t	;
 	double	f4892	=	t	;
 	double	f4893	=	t	;
 	double	f4894	=	t	;
 	double	f4895	=	t	;
 	double	f4896	=	t	;
 	double	f4897	=	t	;
 	double	f4898	=	t	;
 	double	f4899	=	t	;
 	double	f4900	=	t	;
 	double	f4901	=	t	;
 	double	f4902	=	t	;
 	double	f4903	=	t	;
 	double	f4904	=	t	;
 	double	f4905	=	t	;
 	double	f4906	=	t	;
 	double	f4907	=	t	;
 	double	f4908	=	t	;
 	double	f4909	=	t	;
 	double	f4910	=	t	;
 	double	f4911	=	t	;
 	double	f4912	=	t	;
 	double	f4913	=	t	;
 	double	f4914	=	t	;
 	double	f4915	=	t	;
 	double	f4916	=	t	;
 	double	f4917	=	t	;
 	double	f4918	=	t	;
 	double	f4919	=	t	;
 	double	f4920	=	t	;
 	double	f4921	=	t	;
 	double	f4922	=	t	;
 	double	f4923	=	t	;
 	double	f4924	=	t	;
 	double	f4925	=	t	;
 	double	f4926	=	t	;
 	double	f4927	=	t	;
 	double	f4928	=	t	;
 	double	f4929	=	t	;
 	double	f4930	=	t	;
 	double	f4931	=	t	;
 	double	f4932	=	t	;
 	double	f4933	=	t	;
 	double	f4934	=	t	;
 	double	f4935	=	t	;
 	double	f4936	=	t	;
 	double	f4937	=	t	;
 	double	f4938	=	t	;
 	double	f4939	=	t	;
 	double	f4940	=	t	;
 	double	f4941	=	t	;
 	double	f4942	=	t	;
 	double	f4943	=	t	;
 	double	f4944	=	t	;
 	double	f4945	=	t	;
 	double	f4946	=	t	;
 	double	f4947	=	t	;
 	double	f4948	=	t	;
 	double	f4949	=	t	;
 	double	f4950	=	t	;
 	double	f4951	=	t	;
 	double	f4952	=	t	;
 	double	f4953	=	t	;
 	double	f4954	=	t	;
 	double	f4955	=	t	;
 	double	f4956	=	t	;
 	double	f4957	=	t	;
 	double	f4958	=	t	;
 	double	f4959	=	t	;
 	double	f4960	=	t	;
 	double	f4961	=	t	;
 	double	f4962	=	t	;
 	double	f4963	=	t	;
 	double	f4964	=	t	;
 	double	f4965	=	t	;
 	double	f4966	=	t	;
 	double	f4967	=	t	;
 	double	f4968	=	t	;
 	double	f4969	=	t	;
 	double	f4970	=	t	;
 	double	f4971	=	t	;
 	double	f4972	=	t	;
 	double	f4973	=	t	;
 	double	f4974	=	t	;
 	double	f4975	=	t	;
 	double	f4976	=	t	;
 	double	f4977	=	t	;
 	double	f4978	=	t	;
 	double	f4979	=	t	;
 	double	f4980	=	t	;
 	double	f4981	=	t	;
 	double	f4982	=	t	;
 	double	f4983	=	t	;
 	double	f4984	=	t	;
 	double	f4985	=	t	;
 	double	f4986	=	t	;
 	double	f4987	=	t	;
 	double	f4988	=	t	;
 	double	f4989	=	t	;
 	double	f4990	=	t	;
 	double	f4991	=	t	;
 	double	f4992	=	t	;
 	double	f4993	=	t	;
 	double	f4994	=	t	;
 	double	f4995	=	t	;
 	double	f4996	=	t	;
 	double	f4997	=	t	;
 	double	f4998	=	t	;
 	double	f4999	=	t	;
 	double	f5000	=	t	;
// 	double	f5001	=	t	;
// 	double	f5002	=	t	;
// 	double	f5003	=	t	;
// 	double	f5004	=	t	;
// 	double	f5005	=	t	;
// 	double	f5006	=	t	;
// 	double	f5007	=	t	;
// 	double	f5008	=	t	;
// 	double	f5009	=	t	;
// 	double	f5010	=	t	;
// 	double	f5011	=	t	;
// 	double	f5012	=	t	;
// 	double	f5013	=	t	;
// 	double	f5014	=	t	;
// 	double	f5015	=	t	;
// 	double	f5016	=	t	;
// 	double	f5017	=	t	;
// 	double	f5018	=	t	;
// 	double	f5019	=	t	;
// 	double	f5020	=	t	;
// 	double	f5021	=	t	;
// 	double	f5022	=	t	;
// 	double	f5023	=	t	;
// 	double	f5024	=	t	;
// 	double	f5025	=	t	;
// 	double	f5026	=	t	;
// 	double	f5027	=	t	;
// 	double	f5028	=	t	;
// 	double	f5029	=	t	;
// 	double	f5030	=	t	;
// 	double	f5031	=	t	;
// 	double	f5032	=	t	;
// 	double	f5033	=	t	;
// 	double	f5034	=	t	;
// 	double	f5035	=	t	;
// 	double	f5036	=	t	;
// 	double	f5037	=	t	;
// 	double	f5038	=	t	;
// 	double	f5039	=	t	;
// 	double	f5040	=	t	;
// 	double	f5041	=	t	;
// 	double	f5042	=	t	;
// 	double	f5043	=	t	;
// 	double	f5044	=	t	;
// 	double	f5045	=	t	;
// 	double	f5046	=	t	;
// 	double	f5047	=	t	;
// 	double	f5048	=	t	;
// 	double	f5049	=	t	;
// 	double	f5050	=	t	;
// 	double	f5051	=	t	;
// 	double	f5052	=	t	;
// 	double	f5053	=	t	;
// 	double	f5054	=	t	;
// 	double	f5055	=	t	;
// 	double	f5056	=	t	;
// 	double	f5057	=	t	;
// 	double	f5058	=	t	;
// 	double	f5059	=	t	;
// 	double	f5060	=	t	;
// 	double	f5061	=	t	;
// 	double	f5062	=	t	;
// 	double	f5063	=	t	;
// 	double	f5064	=	t	;
// 	double	f5065	=	t	;
// 	double	f5066	=	t	;
// 	double	f5067	=	t	;
// 	double	f5068	=	t	;
// 	double	f5069	=	t	;
// 	double	f5070	=	t	;
// 	double	f5071	=	t	;
// 	double	f5072	=	t	;
// 	double	f5073	=	t	;
// 	double	f5074	=	t	;
// 	double	f5075	=	t	;
// 	double	f5076	=	t	;
// 	double	f5077	=	t	;
// 	double	f5078	=	t	;
// 	double	f5079	=	t	;
// 	double	f5080	=	t	;
// 	double	f5081	=	t	;
// 	double	f5082	=	t	;
// 	double	f5083	=	t	;
// 	double	f5084	=	t	;
// 	double	f5085	=	t	;
// 	double	f5086	=	t	;
// 	double	f5087	=	t	;
// 	double	f5088	=	t	;
// 	double	f5089	=	t	;
// 	double	f5090	=	t	;
// 	double	f5091	=	t	;
// 	double	f5092	=	t	;
// 	double	f5093	=	t	;
// 	double	f5094	=	t	;
// 	double	f5095	=	t	;
// 	double	f5096	=	t	;
// 	double	f5097	=	t	;
// 	double	f5098	=	t	;
//        
// 	double	f5099	=	t	;
// 	double	f5100	=	t	;
// 	double	f5101	=	t	;
// 	double	f5102	=	t	;
// 	double	f5103	=	t	;
// 	double	f5104	=	t	;
// 	double	f5105	=	t	;
// 	double	f5106	=	t	;
// 	double	f5107	=	t	;
// 	double	f5108	=	t	;
// 	double	f5109	=	t	;
// 	double	f5110	=	t	;
// 	double	f5111	=	t	;
// 	double	f5112	=	t	;
// 	double	f5113	=	t	;
// 	double	f5114	=	t	;
// 	double	f5115	=	t	;
// 	double	f5116	=	t	;
// 	double	f5117	=	t	;
// 	double	f5118	=	t	;
// 	double	f5119	=	t	;
// 	double	f5120	=	t	;
// 	double	f5121	=	t	;
// 	double	f5122	=	t	;
// 	double	f5123	=	t	;
// 	double	f5124	=	t	;
// 	double	f5125	=	t	;
// 	double	f5126	=	t	;
// 	double	f5127	=	t	;
// 	double	f5128	=	t	;
// 	double	f5129	=	t	;
// 	double	f5130	=	t	;
// 	double	f5131	=	t	;
// 	double	f5132	=	t	;
// 	double	f5133	=	t	;
// 	double	f5134	=	t	;
// 	double	f5135	=	t	;
// 	double	f5136	=	t	;
// 	double	f5137	=	t	;
// 	double	f5138	=	t	;
// 	double	f5139	=	t	;
// 	double	f5140	=	t	;
// 	double	f5141	=	t	;
// 	double	f5142	=	t	;
// 	double	f5143	=	t	;
// 	double	f5144	=	t	;
// 	double	f5145	=	t	;
// 	double	f5146	=	t	;
// 	double	f5147	=	t	;
// 	double	f5148	=	t	;
// 	double	f5149	=	t	;
// 	double	f5150	=	t	;
// 	double	f5151	=	t	;
// 	double	f5152	=	t	;
// 	double	f5153	=	t	;
// 	double	f5154	=	t	;
// 	double	f5155	=	t	;
// 	double	f5156	=	t	;
// 	double	f5157	=	t	;
// 	double	f5158	=	t	;
// 	double	f5159	=	t	;
// 	double	f5160	=	t	;
// 	double	f5161	=	t	;
// 	double	f5162	=	t	;
// 	double	f5163	=	t	;
// 	double	f5164	=	t	;
// 	double	f5165	=	t	;
// 	double	f5166	=	t	;
// 	double	f5167	=	t	;
// 	double	f5168	=	t	;
// 	double	f5169	=	t	;
// 	double	f5170	=	t	;
// 	double	f5171	=	t	;
// 	double	f5172	=	t	;
// 	double	f5173	=	t	;
// 	double	f5174	=	t	;
// 	double	f5175	=	t	;
// 	double	f5176	=	t	;
// 	double	f5177	=	t	;
// 	double	f5178	=	t	;
// 	double	f5179	=	t	;
// 	double	f5180	=	t	;
// 	double	f5181	=	t	;
// 	double	f5182	=	t	;
// 	double	f5183	=	t	;
// 	double	f5184	=	t	;
// 	double	f5185	=	t	;
// 	double	f5186	=	t	;
// 	double	f5187	=	t	;
// 	double	f5188	=	t	;
// 	double	f5189	=	t	;
// 	double	f5190	=	t	;
// 	double	f5191	=	t	;
// 	double	f5192	=	t	;
// 	double	f5193	=	t	;
// 	double	f5194	=	t	;
// 	double	f5195	=	t	;
// 	double	f5196	=	t	;
// 	double	f5197	=	t	;
// 	double	f5198	=	t	;
// 	double	f5199	=	t	;
// 	double	f5200	=	t	;
// 	double	f5201	=	t	;
// 	double	f5202	=	t	;
// 	double	f5203	=	t	;
// 	double	f5204	=	t	;
// 	double	f5205	=	t	;
// 	double	f5206	=	t	;
// 	double	f5207	=	t	;
// 	double	f5208	=	t	;
// 	double	f5209	=	t	;
// 	double	f5210	=	t	;
// 	double	f5211	=	t	;
// 	double	f5212	=	t	;
// 	double	f5213	=	t	;
// 	double	f5214	=	t	;
// 	double	f5215	=	t	;
// 	double	f5216	=	t	;
// 	double	f5217	=	t	;
// 	double	f5218	=	t	;
// 	double	f5219	=	t	;
// 	double	f5220	=	t	;
// 	double	f5221	=	t	;
// 	double	f5222	=	t	;
// 	double	f5223	=	t	;
// 	double	f5224	=	t	;
// 	double	f5225	=	t	;
// 	double	f5226	=	t	;
// 	double	f5227	=	t	;
// 	double	f5228	=	t	;
// 	double	f5229	=	t	;
// 	double	f5230	=	t	;
// 	double	f5231	=	t	;
// 	double	f5232	=	t	;
// 	double	f5233	=	t	;
// 	double	f5234	=	t	;
// 	double	f5235	=	t	;
// 	double	f5236	=	t	;
// 	double	f5237	=	t	;
// 	double	f5238	=	t	;
// 	double	f5239	=	t	;
// 	double	f5240	=	t	;
// 	double	f5241	=	t	;
// 	double	f5242	=	t	;
// 	double	f5243	=	t	;
// 	double	f5244	=	t	;
// 	double	f5245	=	t	;
// 	double	f5246	=	t	;
// 	double	f5247	=	t	;
// 	double	f5248	=	t	;
// 	double	f5249	=	t	;
// 	double	f5250	=	t	;
// 	double	f5251	=	t	;
// 	double	f5252	=	t	;
// 	double	f5253	=	t	;
// 	double	f5254	=	t	;
// 	double	f5255	=	t	;
// 	double	f5256	=	t	;
// 	double	f5257	=	t	;
// 	double	f5258	=	t	;
// 	double	f5259	=	t	;
// 	double	f5260	=	t	;
// 	double	f5261	=	t	;
// 	double	f5262	=	t	;
// 	double	f5263	=	t	;
// 	double	f5264	=	t	;
// 	double	f5265	=	t	;
// 	double	f5266	=	t	;
// 	double	f5267	=	t	;
// 	double	f5268	=	t	;
// 	double	f5269	=	t	;
// 	double	f5270	=	t	;
// 	double	f5271	=	t	;
// 	double	f5272	=	t	;
// 	double	f5273	=	t	;
// 	double	f5274	=	t	;
// 	double	f5275	=	t	;
// 	double	f5276	=	t	;
// 	double	f5277	=	t	;
// 	double	f5278	=	t	;
// 	double	f5279	=	t	;
// 	double	f5280	=	t	;
// 	double	f5281	=	t	;
// 	double	f5282	=	t	;
// 	double	f5283	=	t	;
// 	double	f5284	=	t	;
// 	double	f5285	=	t	;
// 	double	f5286	=	t	;
// 	double	f5287	=	t	;
// 	double	f5288	=	t	;
// 	double	f5289	=	t	;
// 	double	f5290	=	t	;
// 	double	f5291	=	t	;
// 	double	f5292	=	t	;
// 	double	f5293	=	t	;
// 	double	f5294	=	t	;
// 	double	f5295	=	t	;
// 	double	f5296	=	t	;
// 	double	f5297	=	t	;
// 	double	f5298	=	t	;
// 	double	f5299	=	t	;
// 	double	f5300	=	t	;
// 	double	f5301	=	t	;
// 	double	f5302	=	t	;
// 	double	f5303	=	t	;
// 	double	f5304	=	t	;
// 	double	f5305	=	t	;
// 	double	f5306	=	t	;
// 	double	f5307	=	t	;
// 	double	f5308	=	t	;
// 	double	f5309	=	t	;
// 	double	f5310	=	t	;
// 	double	f5311	=	t	;
// 	double	f5312	=	t	;
// 	double	f5313	=	t	;
// 	double	f5314	=	t	;
// 	double	f5315	=	t	;
// 	double	f5316	=	t	;
// 	double	f5317	=	t	;
// 	double	f5318	=	t	;
// 	double	f5319	=	t	;
// 	double	f5320	=	t	;
// 	double	f5321	=	t	;
// 	double	f5322	=	t	;
// 	double	f5323	=	t	;
// 	double	f5324	=	t	;
// 	double	f5325	=	t	;
// 	double	f5326	=	t	;
// 	double	f5327	=	t	;
// 	double	f5328	=	t	;
// 	double	f5329	=	t	;
// 	double	f5330	=	t	;
// 	double	f5331	=	t	;
// 	double	f5332	=	t	;
// 	double	f5333	=	t	;
// 	double	f5334	=	t	;
// 	double	f5335	=	t	;
// 	double	f5336	=	t	;
// 	double	f5337	=	t	;
// 	double	f5338	=	t	;
// 	double	f5339	=	t	;
// 	double	f5340	=	t	;
// 	double	f5341	=	t	;
// 	double	f5342	=	t	;
// 	double	f5343	=	t	;
// 	double	f5344	=	t	;
// 	double	f5345	=	t	;
// 	double	f5346	=	t	;
// 	double	f5347	=	t	;
// 	double	f5348	=	t	;
// 	double	f5349	=	t	;
// 	double	f5350	=	t	;
// 	double	f5351	=	t	;
// 	double	f5352	=	t	;
// 	double	f5353	=	t	;
// 	double	f5354	=	t	;
// 	double	f5355	=	t	;
// 	double	f5356	=	t	;
// 	double	f5357	=	t	;
// 	double	f5358	=	t	;
// 	double	f5359	=	t	;
// 	double	f5360	=	t	;
// 	double	f5361	=	t	;
// 	double	f5362	=	t	;
// 	double	f5363	=	t	;
// 	double	f5364	=	t	;
// 	double	f5365	=	t	;
// 	double	f5366	=	t	;
// 	double	f5367	=	t	;
// 	double	f5368	=	t	;
// 	double	f5369	=	t	;
// 	double	f5370	=	t	;
// 	double	f5371	=	t	;
// 	double	f5372	=	t	;
// 	double	f5373	=	t	;
// 	double	f5374	=	t	;
// 	double	f5375	=	t	;
// 	double	f5376	=	t	;
// 	double	f5377	=	t	;
// 	double	f5378	=	t	;
// 	double	f5379	=	t	;
// 	double	f5380	=	t	;
// 	double	f5381	=	t	;
// 	double	f5382	=	t	;
// 	double	f5383	=	t	;
// 	double	f5384	=	t	;
// 	double	f5385	=	t	;
// 	double	f5386	=	t	;
// 	double	f5387	=	t	;
// 	double	f5388	=	t	;
// 	double	f5389	=	t	;
// 	double	f5390	=	t	;
// 	double	f5391	=	t	;
// 	double	f5392	=	t	;
// 	double	f5393	=	t	;
// 	double	f5394	=	t	;
// 	double	f5395	=	t	;
// 	double	f5396	=	t	;
// 	double	f5397	=	t	;
// 	double	f5398	=	t	;
// 	double	f5399	=	t	;
// 	double	f5400	=	t	;
// 	double	f5401	=	t	;
// 	double	f5402	=	t	;
// 	double	f5403	=	t	;
// 	double	f5404	=	t	;
// 	double	f5405	=	t	;
// 	double	f5406	=	t	;
// 	double	f5407	=	t	;
// 	double	f5408	=	t	;
// 	double	f5409	=	t	;
// 	double	f5410	=	t	;
// 	double	f5411	=	t	;
// 	double	f5412	=	t	;
// 	double	f5413	=	t	;
// 	double	f5414	=	t	;
// 	double	f5415	=	t	;
// 	double	f5416	=	t	;
// 	double	f5417	=	t	;
// 	double	f5418	=	t	;
// 	double	f5419	=	t	;
// 	double	f5420	=	t	;
// 	double	f5421	=	t	;
// 	double	f5422	=	t	;
// 	double	f5423	=	t	;
// 	double	f5424	=	t	;
// 	double	f5425	=	t	;
// 	double	f5426	=	t	;
// 	double	f5427	=	t	;
// 	double	f5428	=	t	;
// 	double	f5429	=	t	;
// 	double	f5430	=	t	;
// 	double	f5431	=	t	;
// 	double	f5432	=	t	;
// 	double	f5433	=	t	;
// 	double	f5434	=	t	;
// 	double	f5435	=	t	;
// 	double	f5436	=	t	;
// 	double	f5437	=	t	;
// 	double	f5438	=	t	;
// 	double	f5439	=	t	;
// 	double	f5440	=	t	;
// 	double	f5441	=	t	;
// 	double	f5442	=	t	;
// 	double	f5443	=	t	;
// 	double	f5444	=	t	;
// 	double	f5445	=	t	;
// 	double	f5446	=	t	;
// 	double	f5447	=	t	;
// 	double	f5448	=	t	;
// 	double	f5449	=	t	;
// 	double	f5450	=	t	;
// 	double	f5451	=	t	;
// 	double	f5452	=	t	;
// 	double	f5453	=	t	;
// 	double	f5454	=	t	;
// 	double	f5455	=	t	;
// 	double	f5456	=	t	;
// 	double	f5457	=	t	;
// 	double	f5458	=	t	;
// 	double	f5459	=	t	;
// 	double	f5460	=	t	;
// 	double	f5461	=	t	;
// 	double	f5462	=	t	;
// 	double	f5463	=	t	;
// 	double	f5464	=	t	;
// 	double	f5465	=	t	;
// 	double	f5466	=	t	;
// 	double	f5467	=	t	;
// 	double	f5468	=	t	;
// 	double	f5469	=	t	;
// 	double	f5470	=	t	;
// 	double	f5471	=	t	;
// 	double	f5472	=	t	;
// 	double	f5473	=	t	;
// 	double	f5474	=	t	;
// 	double	f5475	=	t	;
// 	double	f5476	=	t	;
// 	double	f5477	=	t	;
// 	double	f5478	=	t	;
// 	double	f5479	=	t	;
// 	double	f5480	=	t	;
// 	double	f5481	=	t	;
// 	double	f5482	=	t	;
// 	double	f5483	=	t	;
// 	double	f5484	=	t	;
// 	double	f5485	=	t	;
// 	double	f5486	=	t	;
// 	double	f5487	=	t	;
// 	double	f5488	=	t	;
// 	double	f5489	=	t	;
// 	double	f5490	=	t	;
// 	double	f5491	=	t	;
// 	double	f5492	=	t	;
// 	double	f5493	=	t	;
// 	double	f5494	=	t	;
// 	double	f5495	=	t	;
// 	double	f5496	=	t	;
// 	double	f5497	=	t	;
// 	double	f5498	=	t	;
// 	double	f5499	=	t	;
// 	double	f5500	=	t	;
// 	double	f5501	=	t	;
// 	double	f5502	=	t	;
// 	double	f5503	=	t	;
// 	double	f5504	=	t	;
// 	double	f5505	=	t	;
// 	double	f5506	=	t	;
// 	double	f5507	=	t	;
// 	double	f5508	=	t	;
// 	double	f5509	=	t	;
// 	double	f5510	=	t	;
// 	double	f5511	=	t	;
// 	double	f5512	=	t	;
// 	double	f5513	=	t	;
// 	double	f5514	=	t	;
// 	double	f5515	=	t	;
// 	double	f5516	=	t	;
// 	double	f5517	=	t	;
// 	double	f5518	=	t	;
// 	double	f5519	=	t	;
// 	double	f5520	=	t	;
// 	double	f5521	=	t	;
// 	double	f5522	=	t	;
// 	double	f5523	=	t	;
// 	double	f5524	=	t	;
// 	double	f5525	=	t	;
// 	double	f5526	=	t	;
// 	double	f5527	=	t	;
// 	double	f5528	=	t	;
// 	double	f5529	=	t	;
// 	double	f5530	=	t	;
// 	double	f5531	=	t	;
// 	double	f5532	=	t	;
// 	double	f5533	=	t	;
// 	double	f5534	=	t	;
// 	double	f5535	=	t	;
// 	double	f5536	=	t	;
// 	double	f5537	=	t	;
// 	double	f5538	=	t	;
// 	double	f5539	=	t	;
// 	double	f5540	=	t	;
// 	double	f5541	=	t	;
// 	double	f5542	=	t	;
// 	double	f5543	=	t	;
// 	double	f5544	=	t	;
// 	double	f5545	=	t	;
// 	double	f5546	=	t	;
// 	double	f5547	=	t	;
// 	double	f5548	=	t	;
// 	double	f5549	=	t	;
// 	double	f5550	=	t	;
// 	double	f5551	=	t	;
// 	double	f5552	=	t	;
// 	double	f5553	=	t	;
// 	double	f5554	=	t	;
// 	double	f5555	=	t	;
// 	double	f5556	=	t	;
// 	double	f5557	=	t	;
// 	double	f5558	=	t	;
// 	double	f5559	=	t	;
// 	double	f5560	=	t	;
// 	double	f5561	=	t	;
// 	double	f5562	=	t	;
// 	double	f5563	=	t	;
// 	double	f5564	=	t	;
// 	double	f5565	=	t	;
// 	double	f5566	=	t	;
// 	double	f5567	=	t	;
// 	double	f5568	=	t	;
// 	double	f5569	=	t	;
// 	double	f5570	=	t	;
// 	double	f5571	=	t	;
// 	double	f5572	=	t	;
// 	double	f5573	=	t	;
// 	double	f5574	=	t	;
// 	double	f5575	=	t	;
// 	double	f5576	=	t	;
// 	double	f5577	=	t	;
// 	double	f5578	=	t	;
// 	double	f5579	=	t	;
// 	double	f5580	=	t	;
// 	double	f5581	=	t	;
// 	double	f5582	=	t	;
// 	double	f5583	=	t	;
// 	double	f5584	=	t	;
// 	double	f5585	=	t	;
// 	double	f5586	=	t	;
// 	double	f5587	=	t	;
// 	double	f5588	=	t	;
// 	double	f5589	=	t	;
// 	double	f5590	=	t	;
// 	double	f5591	=	t	;
// 	double	f5592	=	t	;
// 	double	f5593	=	t	;
// 	double	f5594	=	t	;
// 	double	f5595	=	t	;
// 	double	f5596	=	t	;
// 	double	f5597	=	t	;
// 	double	f5598	=	t	;
// 	double	f5599	=	t	;
// 	double	f5600	=	t	;
// 	double	f5601	=	t	;
// 	double	f5602	=	t	;
// 	double	f5603	=	t	;
// 	double	f5604	=	t	;
// 	double	f5605	=	t	;
// 	double	f5606	=	t	;
// 	double	f5607	=	t	;
// 	double	f5608	=	t	;
// 	double	f5609	=	t	;
// 	double	f5610	=	t	;
// 	double	f5611	=	t	;
// 	double	f5612	=	t	;
// 	double	f5613	=	t	;
// 	double	f5614	=	t	;
// 	double	f5615	=	t	;
// 	double	f5616	=	t	;
// 	double	f5617	=	t	;
// 	double	f5618	=	t	;
// 	double	f5619	=	t	;
// 	double	f5620	=	t	;
// 	double	f5621	=	t	;
// 	double	f5622	=	t	;
// 	double	f5623	=	t	;
// 	double	f5624	=	t	;
// 	double	f5625	=	t	;
// 	double	f5626	=	t	;
// 	double	f5627	=	t	;
// 	double	f5628	=	t	;
// 	double	f5629	=	t	;
// 	double	f5630	=	t	;
// 	double	f5631	=	t	;
// 	double	f5632	=	t	;
// 	double	f5633	=	t	;
// 	double	f5634	=	t	;
// 	double	f5635	=	t	;
// 	double	f5636	=	t	;
// 	double	f5637	=	t	;
// 	double	f5638	=	t	;
// 	double	f5639	=	t	;
// 	double	f5640	=	t	;
// 	double	f5641	=	t	;
// 	double	f5642	=	t	;
// 	double	f5643	=	t	;
// 	double	f5644	=	t	;
// 	double	f5645	=	t	;
// 	double	f5646	=	t	;
// 	double	f5647	=	t	;
// 	double	f5648	=	t	;
// 	double	f5649	=	t	;
// 	double	f5650	=	t	;
// 	double	f5651	=	t	;
// 	double	f5652	=	t	;
// 	double	f5653	=	t	;
// 	double	f5654	=	t	;
// 	double	f5655	=	t	;
// 	double	f5656	=	t	;
// 	double	f5657	=	t	;
// 	double	f5658	=	t	;
// 	double	f5659	=	t	;
// 	double	f5660	=	t	;
// 	double	f5661	=	t	;
// 	double	f5662	=	t	;
// 	double	f5663	=	t	;
// 	double	f5664	=	t	;
// 	double	f5665	=	t	;
// 	double	f5666	=	t	;
// 	double	f5667	=	t	;
// 	double	f5668	=	t	;
// 	double	f5669	=	t	;
// 	double	f5670	=	t	;
// 	double	f5671	=	t	;
// 	double	f5672	=	t	;
// 	double	f5673	=	t	;
// 	double	f5674	=	t	;
// 	double	f5675	=	t	;
// 	double	f5676	=	t	;
// 	double	f5677	=	t	;
// 	double	f5678	=	t	;
// 	double	f5679	=	t	;
// 	double	f5680	=	t	;
// 	double	f5681	=	t	;
// 	double	f5682	=	t	;
// 	double	f5683	=	t	;
// 	double	f5684	=	t	;
// 	double	f5685	=	t	;
// 	double	f5686	=	t	;
// 	double	f5687	=	t	;
// 	double	f5688	=	t	;
// 	double	f5689	=	t	;
// 	double	f5690	=	t	;
// 	double	f5691	=	t	;
// 	double	f5692	=	t	;
// 	double	f5693	=	t	;
// 	double	f5694	=	t	;
// 	double	f5695	=	t	;
// 	double	f5696	=	t	;
// 	double	f5697	=	t	;
// 	double	f5698	=	t	;
// 	double	f5699	=	t	;
// 	double	f5700	=	t	;
// 	double	f5701	=	t	;
// 	double	f5702	=	t	;
// 	double	f5703	=	t	;
// 	double	f5704	=	t	;
// 	double	f5705	=	t	;
// 	double	f5706	=	t	;
// 	double	f5707	=	t	;
// 	double	f5708	=	t	;
// 	double	f5709	=	t	;
// 	double	f5710	=	t	;
// 	double	f5711	=	t	;
// 	double	f5712	=	t	;
// 	double	f5713	=	t	;
// 	double	f5714	=	t	;
// 	double	f5715	=	t	;
// 	double	f5716	=	t	;
// 	double	f5717	=	t	;
// 	double	f5718	=	t	;
// 	double	f5719	=	t	;
// 	double	f5720	=	t	;
// 	double	f5721	=	t	;
// 	double	f5722	=	t	;
// 	double	f5723	=	t	;
// 	double	f5724	=	t	;
// 	double	f5725	=	t	;
// 	double	f5726	=	t	;
// 	double	f5727	=	t	;
// 	double	f5728	=	t	;
// 	double	f5729	=	t	;
// 	double	f5730	=	t	;
// 	double	f5731	=	t	;
// 	double	f5732	=	t	;
// 	double	f5733	=	t	;
// 	double	f5734	=	t	;
// 	double	f5735	=	t	;
// 	double	f5736	=	t	;
// 	double	f5737	=	t	;
// 	double	f5738	=	t	;
// 	double	f5739	=	t	;
// 	double	f5740	=	t	;
// 	double	f5741	=	t	;
// 	double	f5742	=	t	;
// 	double	f5743	=	t	;
// 	double	f5744	=	t	;
// 	double	f5745	=	t	;
// 	double	f5746	=	t	;
// 	double	f5747	=	t	;
// 	double	f5748	=	t	;
// 	double	f5749	=	t	;
// 	double	f5750	=	t	;
// 	double	f5751	=	t	;
// 	double	f5752	=	t	;
// 	double	f5753	=	t	;
// 	double	f5754	=	t	;
// 	double	f5755	=	t	;
// 	double	f5756	=	t	;
// 	double	f5757	=	t	;
// 	double	f5758	=	t	;
// 	double	f5759	=	t	;
// 	double	f5760	=	t	;
// 	double	f5761	=	t	;
// 	double	f5762	=	t	;
// 	double	f5763	=	t	;
// 	double	f5764	=	t	;
// 	double	f5765	=	t	;
// 	double	f5766	=	t	;
// 	double	f5767	=	t	;
// 	double	f5768	=	t	;
// 	double	f5769	=	t	;
// 	double	f5770	=	t	;
// 	double	f5771	=	t	;
// 	double	f5772	=	t	;
// 	double	f5773	=	t	;
// 	double	f5774	=	t	;
// 	double	f5775	=	t	;
// 	double	f5776	=	t	;
// 	double	f5777	=	t	;
// 	double	f5778	=	t	;
// 	double	f5779	=	t	;
// 	double	f5780	=	t	;
// 	double	f5781	=	t	;
// 	double	f5782	=	t	;
// 	double	f5783	=	t	;
// 	double	f5784	=	t	;
// 	double	f5785	=	t	;
// 	double	f5786	=	t	;
// 	double	f5787	=	t	;
// 	double	f5788	=	t	;
// 	double	f5789	=	t	;
// 	double	f5790	=	t	;
// 	double	f5791	=	t	;
// 	double	f5792	=	t	;
// 	double	f5793	=	t	;
// 	double	f5794	=	t	;
// 	double	f5795	=	t	;
// 	double	f5796	=	t	;
// 	double	f5797	=	t	;
// 	double	f5798	=	t	;
// 	double	f5799	=	t	;
// 	double	f5800	=	t	;
// 	double	f5801	=	t	;
// 	double	f5802	=	t	;
// 	double	f5803	=	t	;
// 	double	f5804	=	t	;
// 	double	f5805	=	t	;
// 	double	f5806	=	t	;
// 	double	f5807	=	t	;
// 	double	f5808	=	t	;
// 	double	f5809	=	t	;
// 	double	f5810	=	t	;
// 	double	f5811	=	t	;
// 	double	f5812	=	t	;
// 	double	f5813	=	t	;
// 	double	f5814	=	t	;
// 	double	f5815	=	t	;
// 	double	f5816	=	t	;
// 	double	f5817	=	t	;
// 	double	f5818	=	t	;
// 	double	f5819	=	t	;
// 	double	f5820	=	t	;
// 	double	f5821	=	t	;
// 	double	f5822	=	t	;
// 	double	f5823	=	t	;
// 	double	f5824	=	t	;
// 	double	f5825	=	t	;
// 	double	f5826	=	t	;
// 	double	f5827	=	t	;
// 	double	f5828	=	t	;
// 	double	f5829	=	t	;
// 	double	f5830	=	t	;
// 	double	f5831	=	t	;
// 	double	f5832	=	t	;
// 	double	f5833	=	t	;
// 	double	f5834	=	t	;
// 	double	f5835	=	t	;
// 	double	f5836	=	t	;
// 	double	f5837	=	t	;
// 	double	f5838	=	t	;
// 	double	f5839	=	t	;
// 	double	f5840	=	t	;
// 	double	f5841	=	t	;
// 	double	f5842	=	t	;
// 	double	f5843	=	t	;
// 	double	f5844	=	t	;
// 	double	f5845	=	t	;
// 	double	f5846	=	t	;
// 	double	f5847	=	t	;
// 	double	f5848	=	t	;
// 	double	f5849	=	t	;
// 	double	f5850	=	t	;
// 	double	f5851	=	t	;
// 	double	f5852	=	t	;
// 	double	f5853	=	t	;
// 	double	f5854	=	t	;
// 	double	f5855	=	t	;
// 	double	f5856	=	t	;
// 	double	f5857	=	t	;
// 	double	f5858	=	t	;
// 	double	f5859	=	t	;
// 	double	f5860	=	t	;
// 	double	f5861	=	t	;
// 	double	f5862	=	t	;
// 	double	f5863	=	t	;
// 	double	f5864	=	t	;
// 	double	f5865	=	t	;
// 	double	f5866	=	t	;
// 	double	f5867	=	t	;
// 	double	f5868	=	t	;
// 	double	f5869	=	t	;
// 	double	f5870	=	t	;
// 	double	f5871	=	t	;
// 	double	f5872	=	t	;
// 	double	f5873	=	t	;
// 	double	f5874	=	t	;
// 	double	f5875	=	t	;
// 	double	f5876	=	t	;
// 	double	f5877	=	t	;
// 	double	f5878	=	t	;
// 	double	f5879	=	t	;
// 	double	f5880	=	t	;
// 	double	f5881	=	t	;
// 	double	f5882	=	t	;
// 	double	f5883	=	t	;
// 	double	f5884	=	t	;
// 	double	f5885	=	t	;
// 	double	f5886	=	t	;
// 	double	f5887	=	t	;
// 	double	f5888	=	t	;
// 	double	f5889	=	t	;
// 	double	f5890	=	t	;
// 	double	f5891	=	t	;
// 	double	f5892	=	t	;
// 	double	f5893	=	t	;
// 	double	f5894	=	t	;
// 	double	f5895	=	t	;
// 	double	f5896	=	t	;
// 	double	f5897	=	t	;
// 	double	f5898	=	t	;
// 	double	f5899	=	t	;
// 	double	f5900	=	t	;
// 	double	f5901	=	t	;
// 	double	f5902	=	t	;
// 	double	f5903	=	t	;
// 	double	f5904	=	t	;
// 	double	f5905	=	t	;
// 	double	f5906	=	t	;
// 	double	f5907	=	t	;
// 	double	f5908	=	t	;
// 	double	f5909	=	t	;
// 	double	f5910	=	t	;
// 	double	f5911	=	t	;
// 	double	f5912	=	t	;
// 	double	f5913	=	t	;
// 	double	f5914	=	t	;
// 	double	f5915	=	t	;
// 	double	f5916	=	t	;
// 	double	f5917	=	t	;
// 	double	f5918	=	t	;
// 	double	f5919	=	t	;
// 	double	f5920	=	t	;
// 	double	f5921	=	t	;
// 	double	f5922	=	t	;
// 	double	f5923	=	t	;
// 	double	f5924	=	t	;
// 	double	f5925	=	t	;
// 	double	f5926	=	t	;
// 	double	f5927	=	t	;
// 	double	f5928	=	t	;
// 	double	f5929	=	t	;
// 	double	f5930	=	t	;
// 	double	f5931	=	t	;
// 	double	f5932	=	t	;
// 	double	f5933	=	t	;
// 	double	f5934	=	t	;
// 	double	f5935	=	t	;
// 	double	f5936	=	t	;
// 	double	f5937	=	t	;
// 	double	f5938	=	t	;
// 	double	f5939	=	t	;
// 	double	f5940	=	t	;
// 	double	f5941	=	t	;
// 	double	f5942	=	t	;
// 	double	f5943	=	t	;
// 	double	f5944	=	t	;
// 	double	f5945	=	t	;
// 	double	f5946	=	t	;
// 	double	f5947	=	t	;
// 	double	f5948	=	t	;
// 	double	f5949	=	t	;
// 	double	f5950	=	t	;
// 	double	f5951	=	t	;
// 	double	f5952	=	t	;
// 	double	f5953	=	t	;
// 	double	f5954	=	t	;
// 	double	f5955	=	t	;
// 	double	f5956	=	t	;
// 	double	f5957	=	t	;
// 	double	f5958	=	t	;
// 	double	f5959	=	t	;
// 	double	f5960	=	t	;
// 	double	f5961	=	t	;
// 	double	f5962	=	t	;
// 	double	f5963	=	t	;
// 	double	f5964	=	t	;
// 	double	f5965	=	t	;
// 	double	f5966	=	t	;
// 	double	f5967	=	t	;
// 	double	f5968	=	t	;
// 	double	f5969	=	t	;
// 	double	f5970	=	t	;
// 	double	f5971	=	t	;
// 	double	f5972	=	t	;
// 	double	f5973	=	t	;
// 	double	f5974	=	t	;
// 	double	f5975	=	t	;
// 	double	f5976	=	t	;
// 	double	f5977	=	t	;
// 	double	f5978	=	t	;
// 	double	f5979	=	t	;
// 	double	f5980	=	t	;
// 	double	f5981	=	t	;
// 	double	f5982	=	t	;
// 	double	f5983	=	t	;
// 	double	f5984	=	t	;
// 	double	f5985	=	t	;
// 	double	f5986	=	t	;
// 	double	f5987	=	t	;
// 	double	f5988	=	t	;
// 	double	f5989	=	t	;
// 	double	f5990	=	t	;
// 	double	f5991	=	t	;
// 	double	f5992	=	t	;
// 	double	f5993	=	t	;
// 	double	f5994	=	t	;
// 	double	f5995	=	t	;
// 	double	f5996	=	t	;
// 	double	f5997	=	t	;
// 	double	f5998	=	t	;
// 	double	f5999	=	t	;
// 	double	f6000	=	t	;
// 	double	f6001	=	t	;
// 	double	f6002	=	t	;
// 	double	f6003	=	t	;
// 	double	f6004	=	t	;
// 	double	f6005	=	t	;
// 	double	f6006	=	t	;
// 	double	f6007	=	t	;
// 	double	f6008	=	t	;
// 	double	f6009	=	t	;
// 	double	f6010	=	t	;
// 	double	f6011	=	t	;
// 	double	f6012	=	t	;
// 	double	f6013	=	t	;
// 	double	f6014	=	t	;
// 	double	f6015	=	t	;
// 	double	f6016	=	t	;
// 	double	f6017	=	t	;
// 	double	f6018	=	t	;
// 	double	f6019	=	t	;
// 	double	f6020	=	t	;
// 	double	f6021	=	t	;
// 	double	f6022	=	t	;
// 	double	f6023	=	t	;
// 	double	f6024	=	t	;
// 	double	f6025	=	t	;
// 	double	f6026	=	t	;
// 	double	f6027	=	t	;
// 	double	f6028	=	t	;
// 	double	f6029	=	t	;
// 	double	f6030	=	t	;
// 	double	f6031	=	t	;
// 	double	f6032	=	t	;
// 	double	f6033	=	t	;
// 	double	f6034	=	t	;
// 	double	f6035	=	t	;
// 	double	f6036	=	t	;
// 	double	f6037	=	t	;
// 	double	f6038	=	t	;
// 	double	f6039	=	t	;
// 	double	f6040	=	t	;
// 	double	f6041	=	t	;
// 	double	f6042	=	t	;
// 	double	f6043	=	t	;
// 	double	f6044	=	t	;
// 	double	f6045	=	t	;
// 	double	f6046	=	t	;
// 	double	f6047	=	t	;
// 	double	f6048	=	t	;
// 	double	f6049	=	t	;
// 	double	f6050	=	t	;
// 	double	f6051	=	t	;
// 	double	f6052	=	t	;
// 	double	f6053	=	t	;
// 	double	f6054	=	t	;
// 	double	f6055	=	t	;
// 	double	f6056	=	t	;
// 	double	f6057	=	t	;
// 	double	f6058	=	t	;
// 	double	f6059	=	t	;
// 	double	f6060	=	t	;
// 	double	f6061	=	t	;
// 	double	f6062	=	t	;
// 	double	f6063	=	t	;
// 	double	f6064	=	t	;
// 	double	f6065	=	t	;
// 	double	f6066	=	t	;
// 	double	f6067	=	t	;
// 	double	f6068	=	t	;
// 	double	f6069	=	t	;
// 	double	f6070	=	t	;
// 	double	f6071	=	t	;
// 	double	f6072	=	t	;
// 	double	f6073	=	t	;
// 	double	f6074	=	t	;
// 	double	f6075	=	t	;
// 	double	f6076	=	t	;
// 	double	f6077	=	t	;
// 	double	f6078	=	t	;
// 	double	f6079	=	t	;
// 	double	f6080	=	t	;
// 	double	f6081	=	t	;
// 	double	f6082	=	t	;
// 	double	f6083	=	t	;
// 	double	f6084	=	t	;
// 	double	f6085	=	t	;
// 	double	f6086	=	t	;
// 	double	f6087	=	t	;
// 	double	f6088	=	t	;
// 	double	f6089	=	t	;
// 	double	f6090	=	t	;
// 	double	f6091	=	t	;
// 	double	f6092	=	t	;
// 	double	f6093	=	t	;
// 	double	f6094	=	t	;
// 	double	f6095	=	t	;
// 	double	f6096	=	t	;
// 	double	f6097	=	t	;
// 	double	f6098	=	t	;
// 	double	f6099	=	t	;
// 	double	f6100	=	t	;
// 	double	f6101	=	t	;
// 	double	f6102	=	t	;
// 	double	f6103	=	t	;
// 	double	f6104	=	t	;
// 	double	f6105	=	t	;
// 	double	f6106	=	t	;
// 	double	f6107	=	t	;
// 	double	f6108	=	t	;
// 	double	f6109	=	t	;
// 	double	f6110	=	t	;
// 	double	f6111	=	t	;
// 	double	f6112	=	t	;
// 	double	f6113	=	t	;
// 	double	f6114	=	t	;
// 	double	f6115	=	t	;
// 	double	f6116	=	t	;
// 	double	f6117	=	t	;
// 	double	f6118	=	t	;
// 	double	f6119	=	t	;
// 	double	f6120	=	t	;
// 	double	f6121	=	t	;
// 	double	f6122	=	t	;
// 	double	f6123	=	t	;
// 	double	f6124	=	t	;
// 	double	f6125	=	t	;
// 	double	f6126	=	t	;
// 	double	f6127	=	t	;
// 	double	f6128	=	t	;
// 	double	f6129	=	t	;
// 	double	f6130	=	t	;
// 	double	f6131	=	t	;
// 	double	f6132	=	t	;
// 	double	f6133	=	t	;
// 	double	f6134	=	t	;
// 	double	f6135	=	t	;
// 	double	f6136	=	t	;
// 	double	f6137	=	t	;
// 	double	f6138	=	t	;
// 	double	f6139	=	t	;
// 	double	f6140	=	t	;
// 	double	f6141	=	t	;
// 	double	f6142	=	t	;
// 	double	f6143	=	t	;
// 	double	f6144	=	t	;
// 	double	f6145	=	t	;
// 	double	f6146	=	t	;
// 	double	f6147	=	t	;
// 	double	f6148	=	t	;
// 	double	f6149	=	t	;
// 	double	f6150	=	t	;
// 	double	f6151	=	t	;
// 	double	f6152	=	t	;
// 	double	f6153	=	t	;
// 	double	f6154	=	t	;
// 	double	f6155	=	t	;
// 	double	f6156	=	t	;
// 	double	f6157	=	t	;
// 	double	f6158	=	t	;
// 	double	f6159	=	t	;
// 	double	f6160	=	t	;
// 	double	f6161	=	t	;
// 	double	f6162	=	t	;
// 	double	f6163	=	t	;
// 	double	f6164	=	t	;
// 	double	f6165	=	t	;
// 	double	f6166	=	t	;
// 	double	f6167	=	t	;
// 	double	f6168	=	t	;
// 	double	f6169	=	t	;
// 	double	f6170	=	t	;
// 	double	f6171	=	t	;
// 	double	f6172	=	t	;
// 	double	f6173	=	t	;
// 	double	f6174	=	t	;
// 	double	f6175	=	t	;
// 	double	f6176	=	t	;
// 	double	f6177	=	t	;
// 	double	f6178	=	t	;
// 	double	f6179	=	t	;
// 	double	f6180	=	t	;
// 	double	f6181	=	t	;
// 	double	f6182	=	t	;
// 	double	f6183	=	t	;
// 	double	f6184	=	t	;
// 	double	f6185	=	t	;
// 	double	f6186	=	t	;
// 	double	f6187	=	t	;
// 	double	f6188	=	t	;
// 	double	f6189	=	t	;
// 	double	f6190	=	t	;
// 	double	f6191	=	t	;
// 	double	f6192	=	t	;
// 	double	f6193	=	t	;
// 	double	f6194	=	t	;
// 	double	f6195	=	t	;
// 	double	f6196	=	t	;
// 	double	f6197	=	t	;
// 	double	f6198	=	t	;
// 	double	f6199	=	t	;
// 	double	f6200	=	t	;
// 	double	f6201	=	t	;
// 	double	f6202	=	t	;
// 	double	f6203	=	t	;
// 	double	f6204	=	t	;
// 	double	f6205	=	t	;
// 	double	f6206	=	t	;
// 	double	f6207	=	t	;
// 	double	f6208	=	t	;
// 	double	f6209	=	t	;
// 	double	f6210	=	t	;
// 	double	f6211	=	t	;
// 	double	f6212	=	t	;
// 	double	f6213	=	t	;
// 	double	f6214	=	t	;
// 	double	f6215	=	t	;
// 	double	f6216	=	t	;
// 	double	f6217	=	t	;
// 	double	f6218	=	t	;
// 	double	f6219	=	t	;
// 	double	f6220	=	t	;
// 	double	f6221	=	t	;
// 	double	f6222	=	t	;
// 	double	f6223	=	t	;
// 	double	f6224	=	t	;
// 	double	f6225	=	t	;
// 	double	f6226	=	t	;
// 	double	f6227	=	t	;
// 	double	f6228	=	t	;
// 	double	f6229	=	t	;
// 	double	f6230	=	t	;
// 	double	f6231	=	t	;
// 	double	f6232	=	t	;
// 	double	f6233	=	t	;
// 	double	f6234	=	t	;
// 	double	f6235	=	t	;
// 	double	f6236	=	t	;
// 	double	f6237	=	t	;
// 	double	f6238	=	t	;
// 	double	f6239	=	t	;
// 	double	f6240	=	t	;
// 	double	f6241	=	t	;
// 	double	f6242	=	t	;
// 	double	f6243	=	t	;
// 	double	f6244	=	t	;
// 	double	f6245	=	t	;
// 	double	f6246	=	t	;
// 	double	f6247	=	t	;
// 	double	f6248	=	t	;
// 	double	f6249	=	t	;
// 	double	f6250	=	t	;
// 	double	f6251	=	t	;
// 	double	f6252	=	t	;
// 	double	f6253	=	t	;
// 	double	f6254	=	t	;
// 	double	f6255	=	t	;
// 	double	f6256	=	t	;
// 	double	f6257	=	t	;
// 	double	f6258	=	t	;
// 	double	f6259	=	t	;
// 	double	f6260	=	t	;
// 	double	f6261	=	t	;
// 	double	f6262	=	t	;
// 	double	f6263	=	t	;
// 	double	f6264	=	t	;
// 	double	f6265	=	t	;
// 	double	f6266	=	t	;
// 	double	f6267	=	t	;
// 	double	f6268	=	t	;
// 	double	f6269	=	t	;
// 	double	f6270	=	t	;
// 	double	f6271	=	t	;
// 	double	f6272	=	t	;
// 	double	f6273	=	t	;
// 	double	f6274	=	t	;
// 	double	f6275	=	t	;
// 	double	f6276	=	t	;
// 	double	f6277	=	t	;
// 	double	f6278	=	t	;
// 	double	f6279	=	t	;
// 	double	f6280	=	t	;
// 	double	f6281	=	t	;
// 	double	f6282	=	t	;
// 	double	f6283	=	t	;
// 	double	f6284	=	t	;
// 	double	f6285	=	t	;
// 	double	f6286	=	t	;
// 	double	f6287	=	t	;
// 	double	f6288	=	t	;
// 	double	f6289	=	t	;
// 	double	f6290	=	t	;
// 	double	f6291	=	t	;
// 	double	f6292	=	t	;
// 	double	f6293	=	t	;
// 	double	f6294	=	t	;
// 	double	f6295	=	t	;
// 	double	f6296	=	t	;
// 	double	f6297	=	t	;
// 	double	f6298	=	t	;
// 	double	f6299	=	t	;
// 	double	f6300	=	t	;
// 	double	f6301	=	t	;
// 	double	f6302	=	t	;
// 	double	f6303	=	t	;
// 	double	f6304	=	t	;
// 	double	f6305	=	t	;
// 	double	f6306	=	t	;
// 	double	f6307	=	t	;
// 	double	f6308	=	t	;
// 	double	f6309	=	t	;
// 	double	f6310	=	t	;
// 	double	f6311	=	t	;
// 	double	f6312	=	t	;
// 	double	f6313	=	t	;
// 	double	f6314	=	t	;
// 	double	f6315	=	t	;
// 	double	f6316	=	t	;
// 	double	f6317	=	t	;
// 	double	f6318	=	t	;
// 	double	f6319	=	t	;
// 	double	f6320	=	t	;
// 	double	f6321	=	t	;
// 	double	f6322	=	t	;
// 	double	f6323	=	t	;
// 	double	f6324	=	t	;
// 	double	f6325	=	t	;
// 	double	f6326	=	t	;
// 	double	f6327	=	t	;
// 	double	f6328	=	t	;
// 	double	f6329	=	t	;
// 	double	f6330	=	t	;
// 	double	f6331	=	t	;
// 	double	f6332	=	t	;
// 	double	f6333	=	t	;
// 	double	f6334	=	t	;
// 	double	f6335	=	t	;
// 	double	f6336	=	t	;
// 	double	f6337	=	t	;
// 	double	f6338	=	t	;
// 	double	f6339	=	t	;
// 	double	f6340	=	t	;
// 	double	f6341	=	t	;
// 	double	f6342	=	t	;
// 	double	f6343	=	t	;
// 	double	f6344	=	t	;
// 	double	f6345	=	t	;
// 	double	f6346	=	t	;
// 	double	f6347	=	t	;
// 	double	f6348	=	t	;
// 	double	f6349	=	t	;
// 	double	f6350	=	t	;
// 	double	f6351	=	t	;
// 	double	f6352	=	t	;
// 	double	f6353	=	t	;
// 	double	f6354	=	t	;
// 	double	f6355	=	t	;
// 	double	f6356	=	t	;
// 	double	f6357	=	t	;
// 	double	f6358	=	t	;
// 	double	f6359	=	t	;
// 	double	f6360	=	t	;
// 	double	f6361	=	t	;
// 	double	f6362	=	t	;
// 	double	f6363	=	t	;
// 	double	f6364	=	t	;
// 	double	f6365	=	t	;
// 	double	f6366	=	t	;
// 	double	f6367	=	t	;
// 	double	f6368	=	t	;
// 	double	f6369	=	t	;
// 	double	f6370	=	t	;
// 	double	f6371	=	t	;
// 	double	f6372	=	t	;
// 	double	f6373	=	t	;
// 	double	f6374	=	t	;
// 	double	f6375	=	t	;
// 	double	f6376	=	t	;
// 	double	f6377	=	t	;
// 	double	f6378	=	t	;
// 	double	f6379	=	t	;
// 	double	f6380	=	t	;
// 	double	f6381	=	t	;
// 	double	f6382	=	t	;
// 	double	f6383	=	t	;
// 	double	f6384	=	t	;
// 	double	f6385	=	t	;
// 	double	f6386	=	t	;
// 	double	f6387	=	t	;
// 	double	f6388	=	t	;
// 	double	f6389	=	t	;
// 	double	f6390	=	t	;
// 	double	f6391	=	t	;
// 	double	f6392	=	t	;
// 	double	f6393	=	t	;
// 	double	f6394	=	t	;
// 	double	f6395	=	t	;
// 	double	f6396	=	t	;
// 	double	f6397	=	t	;
// 	double	f6398	=	t	;
// 	double	f6399	=	t	;
// 	double	f6400	=	t	;
// 	double	f6401	=	t	;
// 	double	f6402	=	t	;
// 	double	f6403	=	t	;
// 	double	f6404	=	t	;
// 	double	f6405	=	t	;
// 	double	f6406	=	t	;
// 	double	f6407	=	t	;
// 	double	f6408	=	t	;
// 	double	f6409	=	t	;
// 	double	f6410	=	t	;
// 	double	f6411	=	t	;
// 	double	f6412	=	t	;
// 	double	f6413	=	t	;
// 	double	f6414	=	t	;
// 	double	f6415	=	t	;
// 	double	f6416	=	t	;
// 	double	f6417	=	t	;
// 	double	f6418	=	t	;
// 	double	f6419	=	t	;
// 	double	f6420	=	t	;
// 	double	f6421	=	t	;
// 	double	f6422	=	t	;
// 	double	f6423	=	t	;
// 	double	f6424	=	t	;
// 	double	f6425	=	t	;
// 	double	f6426	=	t	;
// 	double	f6427	=	t	;
// 	double	f6428	=	t	;
// 	double	f6429	=	t	;
// 	double	f6430	=	t	;
// 	double	f6431	=	t	;
// 	double	f6432	=	t	;
// 	double	f6433	=	t	;
// 	double	f6434	=	t	;
// 	double	f6435	=	t	;
// 	double	f6436	=	t	;
// 	double	f6437	=	t	;
// 	double	f6438	=	t	;
// 	double	f6439	=	t	;
// 	double	f6440	=	t	;
// 	double	f6441	=	t	;
// 	double	f6442	=	t	;
// 	double	f6443	=	t	;
// 	double	f6444	=	t	;
// 	double	f6445	=	t	;
// 	double	f6446	=	t	;
// 	double	f6447	=	t	;
// 	double	f6448	=	t	;
// 	double	f6449	=	t	;
// 	double	f6450	=	t	;
// 	double	f6451	=	t	;
// 	double	f6452	=	t	;
// 	double	f6453	=	t	;
// 	double	f6454	=	t	;
// 	double	f6455	=	t	;
// 	double	f6456	=	t	;
// 	double	f6457	=	t	;
// 	double	f6458	=	t	;
// 	double	f6459	=	t	;
// 	double	f6460	=	t	;
// 	double	f6461	=	t	;
// 	double	f6462	=	t	;
// 	double	f6463	=	t	;
// 	double	f6464	=	t	;
// 	double	f6465	=	t	;
// 	double	f6466	=	t	;
// 	double	f6467	=	t	;
// 	double	f6468	=	t	;
// 	double	f6469	=	t	;
// 	double	f6470	=	t	;
// 	double	f6471	=	t	;
// 	double	f6472	=	t	;
// 	double	f6473	=	t	;
// 	double	f6474	=	t	;
// 	double	f6475	=	t	;
// 	double	f6476	=	t	;
// 	double	f6477	=	t	;
// 	double	f6478	=	t	;
// 	double	f6479	=	t	;
// 	double	f6480	=	t	;
// 	double	f6481	=	t	;
// 	double	f6482	=	t	;
// 	double	f6483	=	t	;
// 	double	f6484	=	t	;
// 	double	f6485	=	t	;
// 	double	f6486	=	t	;
// 	double	f6487	=	t	;
// 	double	f6488	=	t	;
// 	double	f6489	=	t	;
// 	double	f6490	=	t	;
// 	double	f6491	=	t	;
// 	double	f6492	=	t	;
// 	double	f6493	=	t	;
// 	double	f6494	=	t	;
// 	double	f6495	=	t	;
// 	double	f6496	=	t	;
// 	double	f6497	=	t	;
// 	double	f6498	=	t	;
// 	double	f6499	=	t	;
// 	double	f6500	=	t	;
// 	double	f6501	=	t	;
// 	double	f6502	=	t	;
// 	double	f6503	=	t	;
// 	double	f6504	=	t	;
// 	double	f6505	=	t	;
// 	double	f6506	=	t	;
// 	double	f6507	=	t	;
// 	double	f6508	=	t	;
// 	double	f6509	=	t	;
// 	double	f6510	=	t	;
// 	double	f6511	=	t	;
// 	double	f6512	=	t	;
// 	double	f6513	=	t	;
// 	double	f6514	=	t	;
// 	double	f6515	=	t	;
// 	double	f6516	=	t	;
// 	double	f6517	=	t	;
// 	double	f6518	=	t	;
// 	double	f6519	=	t	;
// 	double	f6520	=	t	;
// 	double	f6521	=	t	;
// 	double	f6522	=	t	;
// 	double	f6523	=	t	;
// 	double	f6524	=	t	;
// 	double	f6525	=	t	;
// 	double	f6526	=	t	;
// 	double	f6527	=	t	;
// 	double	f6528	=	t	;
// 	double	f6529	=	t	;
// 	double	f6530	=	t	;
// 	double	f6531	=	t	;
// 	double	f6532	=	t	;
// 	double	f6533	=	t	;
// 	double	f6534	=	t	;
// 	double	f6535	=	t	;
// 	double	f6536	=	t	;
// 	double	f6537	=	t	;
// 	double	f6538	=	t	;
// 	double	f6539	=	t	;
// 	double	f6540	=	t	;
// 	double	f6541	=	t	;
// 	double	f6542	=	t	;
// 	double	f6543	=	t	;
// 	double	f6544	=	t	;
// 	double	f6545	=	t	;
// 	double	f6546	=	t	;
// 	double	f6547	=	t	;
// 	double	f6548	=	t	;
// 	double	f6549	=	t	;
// 	double	f6550	=	t	;
// 	double	f6551	=	t	;
// 	double	f6552	=	t	;
// 	double	f6553	=	t	;
// 	double	f6554	=	t	;
// 	double	f6555	=	t	;
// 	double	f6556	=	t	;
// 	double	f6557	=	t	;
// 	double	f6558	=	t	;
// 	double	f6559	=	t	;
// 	double	f6560	=	t	;
// 	double	f6561	=	t	;
// 	double	f6562	=	t	;
// 	double	f6563	=	t	;
// 	double	f6564	=	t	;
// 	double	f6565	=	t	;
// 	double	f6566	=	t	;
// 	double	f6567	=	t	;
// 	double	f6568	=	t	;
// 	double	f6569	=	t	;
// 	double	f6570	=	t	;
// 	double	f6571	=	t	;
// 	double	f6572	=	t	;
// 	double	f6573	=	t	;
// 	double	f6574	=	t	;
// 	double	f6575	=	t	;
// 	double	f6576	=	t	;
// 	double	f6577	=	t	;
// 	double	f6578	=	t	;
// 	double	f6579	=	t	;
// 	double	f6580	=	t	;
// 	double	f6581	=	t	;
// 	double	f6582	=	t	;
// 	double	f6583	=	t	;
// 	double	f6584	=	t	;
// 	double	f6585	=	t	;
// 	double	f6586	=	t	;
// 	double	f6587	=	t	;
// 	double	f6588	=	t	;
// 	double	f6589	=	t	;
// 	double	f6590	=	t	;
// 	double	f6591	=	t	;
// 	double	f6592	=	t	;
// 	double	f6593	=	t	;
// 	double	f6594	=	t	;
// 	double	f6595	=	t	;
// 	double	f6596	=	t	;
// 	double	f6597	=	t	;
// 	double	f6598	=	t	;
// 	double	f6599	=	t	;
// 	double	f6600	=	t	;
// 	double	f6601	=	t	;
// 	double	f6602	=	t	;
// 	double	f6603	=	t	;
// 	double	f6604	=	t	;
// 	double	f6605	=	t	;
// 	double	f6606	=	t	;
// 	double	f6607	=	t	;
// 	double	f6608	=	t	;
// 	double	f6609	=	t	;
// 	double	f6610	=	t	;
// 	double	f6611	=	t	;
// 	double	f6612	=	t	;
// 	double	f6613	=	t	;
// 	double	f6614	=	t	;
// 	double	f6615	=	t	;
// 	double	f6616	=	t	;
// 	double	f6617	=	t	;
// 	double	f6618	=	t	;
// 	double	f6619	=	t	;
// 	double	f6620	=	t	;
// 	double	f6621	=	t	;
// 	double	f6622	=	t	;
// 	double	f6623	=	t	;
// 	double	f6624	=	t	;
// 	double	f6625	=	t	;
// 	double	f6626	=	t	;
// 	double	f6627	=	t	;
// 	double	f6628	=	t	;
// 	double	f6629	=	t	;
// 	double	f6630	=	t	;
// 	double	f6631	=	t	;
// 	double	f6632	=	t	;
// 	double	f6633	=	t	;
// 	double	f6634	=	t	;
// 	double	f6635	=	t	;
// 	double	f6636	=	t	;
// 	double	f6637	=	t	;
// 	double	f6638	=	t	;
// 	double	f6639	=	t	;
// 	double	f6640	=	t	;
// 	double	f6641	=	t	;
// 	double	f6642	=	t	;
// 	double	f6643	=	t	;
// 	double	f6644	=	t	;
// 	double	f6645	=	t	;
// 	double	f6646	=	t	;
// 	double	f6647	=	t	;
// 	double	f6648	=	t	;
// 	double	f6649	=	t	;
// 	double	f6650	=	t	;
// 	double	f6651	=	t	;
// 	double	f6652	=	t	;
// 	double	f6653	=	t	;
// 	double	f6654	=	t	;
// 	double	f6655	=	t	;
// 	double	f6656	=	t	;
// 	double	f6657	=	t	;
// 	double	f6658	=	t	;
// 	double	f6659	=	t	;
// 	double	f6660	=	t	;
// 	double	f6661	=	t	;
// 	double	f6662	=	t	;
// 	double	f6663	=	t	;
// 	double	f6664	=	t	;
// 	double	f6665	=	t	;
// 	double	f6666	=	t	;
// 	double	f6667	=	t	;
// 	double	f6668	=	t	;
// 	double	f6669	=	t	;
// 	double	f6670	=	t	;
// 	double	f6671	=	t	;
// 	double	f6672	=	t	;
// 	double	f6673	=	t	;
// 	double	f6674	=	t	;
// 	double	f6675	=	t	;
// 	double	f6676	=	t	;
// 	double	f6677	=	t	;
// 	double	f6678	=	t	;
// 	double	f6679	=	t	;
// 	double	f6680	=	t	;
// 	double	f6681	=	t	;
// 	double	f6682	=	t	;
// 	double	f6683	=	t	;
// 	double	f6684	=	t	;
// 	double	f6685	=	t	;
// 	double	f6686	=	t	;
// 	double	f6687	=	t	;
// 	double	f6688	=	t	;
// 	double	f6689	=	t	;
// 	double	f6690	=	t	;
// 	double	f6691	=	t	;
// 	double	f6692	=	t	;
// 	double	f6693	=	t	;
// 	double	f6694	=	t	;
// 	double	f6695	=	t	;
// 	double	f6696	=	t	;
// 	double	f6697	=	t	;
// 	double	f6698	=	t	;
// 	double	f6699	=	t	;
// 	double	f6700	=	t	;
// 	double	f6701	=	t	;
// 	double	f6702	=	t	;
// 	double	f6703	=	t	;
// 	double	f6704	=	t	;
// 	double	f6705	=	t	;
// 	double	f6706	=	t	;
// 	double	f6707	=	t	;
// 	double	f6708	=	t	;
// 	double	f6709	=	t	;
// 	double	f6710	=	t	;
// 	double	f6711	=	t	;
// 	double	f6712	=	t	;
// 	double	f6713	=	t	;
// 	double	f6714	=	t	;
// 	double	f6715	=	t	;
// 	double	f6716	=	t	;
// 	double	f6717	=	t	;
// 	double	f6718	=	t	;
// 	double	f6719	=	t	;
// 	double	f6720	=	t	;
// 	double	f6721	=	t	;
// 	double	f6722	=	t	;
// 	double	f6723	=	t	;
// 	double	f6724	=	t	;
// 	double	f6725	=	t	;
// 	double	f6726	=	t	;
// 	double	f6727	=	t	;
// 	double	f6728	=	t	;
// 	double	f6729	=	t	;
// 	double	f6730	=	t	;
// 	double	f6731	=	t	;
// 	double	f6732	=	t	;
// 	double	f6733	=	t	;
// 	double	f6734	=	t	;
// 	double	f6735	=	t	;
// 	double	f6736	=	t	;
// 	double	f6737	=	t	;
// 	double	f6738	=	t	;
// 	double	f6739	=	t	;
// 	double	f6740	=	t	;
// 	double	f6741	=	t	;
// 	double	f6742	=	t	;
// 	double	f6743	=	t	;
// 	double	f6744	=	t	;
// 	double	f6745	=	t	;
// 	double	f6746	=	t	;
// 	double	f6747	=	t	;
// 	double	f6748	=	t	;
// 	double	f6749	=	t	;
// 	double	f6750	=	t	;
// 	double	f6751	=	t	;
// 	double	f6752	=	t	;
// 	double	f6753	=	t	;
// 	double	f6754	=	t	;
// 	double	f6755	=	t	;
// 	double	f6756	=	t	;
// 	double	f6757	=	t	;
// 	double	f6758	=	t	;
// 	double	f6759	=	t	;
// 	double	f6760	=	t	;
// 	double	f6761	=	t	;
// 	double	f6762	=	t	;
// 	double	f6763	=	t	;
// 	double	f6764	=	t	;
// 	double	f6765	=	t	;
// 	double	f6766	=	t	;
// 	double	f6767	=	t	;
// 	double	f6768	=	t	;
// 	double	f6769	=	t	;
// 	double	f6770	=	t	;
// 	double	f6771	=	t	;
// 	double	f6772	=	t	;
// 	double	f6773	=	t	;
// 	double	f6774	=	t	;
// 	double	f6775	=	t	;
// 	double	f6776	=	t	;
// 	double	f6777	=	t	;
// 	double	f6778	=	t	;
// 	double	f6779	=	t	;
// 	double	f6780	=	t	;
// 	double	f6781	=	t	;
// 	double	f6782	=	t	;
// 	double	f6783	=	t	;
// 	double	f6784	=	t	;
// 	double	f6785	=	t	;
// 	double	f6786	=	t	;
// 	double	f6787	=	t	;
// 	double	f6788	=	t	;
// 	double	f6789	=	t	;
// 	double	f6790	=	t	;
// 	double	f6791	=	t	;
// 	double	f6792	=	t	;
// 	double	f6793	=	t	;
// 	double	f6794	=	t	;
// 	double	f6795	=	t	;
// 	double	f6796	=	t	;
// 	double	f6797	=	t	;
// 	double	f6798	=	t	;
// 	double	f6799	=	t	;
// 	double	f6800	=	t	;
// 	double	f6801	=	t	;
// 	double	f6802	=	t	;
// 	double	f6803	=	t	;
// 	double	f6804	=	t	;
// 	double	f6805	=	t	;
// 	double	f6806	=	t	;
// 	double	f6807	=	t	;
// 	double	f6808	=	t	;
// 	double	f6809	=	t	;
// 	double	f6810	=	t	;
// 	double	f6811	=	t	;
// 	double	f6812	=	t	;
// 	double	f6813	=	t	;
// 	double	f6814	=	t	;
// 	double	f6815	=	t	;
// 	double	f6816	=	t	;
// 	double	f6817	=	t	;
// 	double	f6818	=	t	;
// 	double	f6819	=	t	;
// 	double	f6820	=	t	;
// 	double	f6821	=	t	;
// 	double	f6822	=	t	;
// 	double	f6823	=	t	;
// 	double	f6824	=	t	;
// 	double	f6825	=	t	;
// 	double	f6826	=	t	;
// 	double	f6827	=	t	;
// 	double	f6828	=	t	;
// 	double	f6829	=	t	;
// 	double	f6830	=	t	;
// 	double	f6831	=	t	;
// 	double	f6832	=	t	;
// 	double	f6833	=	t	;
// 	double	f6834	=	t	;
// 	double	f6835	=	t	;
// 	double	f6836	=	t	;
// 	double	f6837	=	t	;
// 	double	f6838	=	t	;
// 	double	f6839	=	t	;
// 	double	f6840	=	t	;
// 	double	f6841	=	t	;
// 	double	f6842	=	t	;
// 	double	f6843	=	t	;
// 	double	f6844	=	t	;
// 	double	f6845	=	t	;
// 	double	f6846	=	t	;
// 	double	f6847	=	t	;
// 	double	f6848	=	t	;
// 	double	f6849	=	t	;
// 	double	f6850	=	t	;
// 	double	f6851	=	t	;
// 	double	f6852	=	t	;
// 	double	f6853	=	t	;
// 	double	f6854	=	t	;
// 	double	f6855	=	t	;
// 	double	f6856	=	t	;
// 	double	f6857	=	t	;
// 	double	f6858	=	t	;
// 	double	f6859	=	t	;
// 	double	f6860	=	t	;
// 	double	f6861	=	t	;
// 	double	f6862	=	t	;
// 	double	f6863	=	t	;
// 	double	f6864	=	t	;
// 	double	f6865	=	t	;
// 	double	f6866	=	t	;
// 	double	f6867	=	t	;
// 	double	f6868	=	t	;
// 	double	f6869	=	t	;
// 	double	f6870	=	t	;
// 	double	f6871	=	t	;
// 	double	f6872	=	t	;
// 	double	f6873	=	t	;
// 	double	f6874	=	t	;
// 	double	f6875	=	t	;
// 	double	f6876	=	t	;
// 	double	f6877	=	t	;
// 	double	f6878	=	t	;
// 	double	f6879	=	t	;
// 	double	f6880	=	t	;
// 	double	f6881	=	t	;
// 	double	f6882	=	t	;
// 	double	f6883	=	t	;
// 	double	f6884	=	t	;
// 	double	f6885	=	t	;
// 	double	f6886	=	t	;
// 	double	f6887	=	t	;
// 	double	f6888	=	t	;
// 	double	f6889	=	t	;
// 	double	f6890	=	t	;
// 	double	f6891	=	t	;
// 	double	f6892	=	t	;
// 	double	f6893	=	t	;
// 	double	f6894	=	t	;
// 	double	f6895	=	t	;
// 	double	f6896	=	t	;
// 	double	f6897	=	t	;
// 	double	f6898	=	t	;
// 	double	f6899	=	t	;
// 	double	f6900	=	t	;
// 	double	f6901	=	t	;
// 	double	f6902	=	t	;
// 	double	f6903	=	t	;
// 	double	f6904	=	t	;
// 	double	f6905	=	t	;
// 	double	f6906	=	t	;
// 	double	f6907	=	t	;
// 	double	f6908	=	t	;
// 	double	f6909	=	t	;
// 	double	f6910	=	t	;
// 	double	f6911	=	t	;
// 	double	f6912	=	t	;
// 	double	f6913	=	t	;
// 	double	f6914	=	t	;
// 	double	f6915	=	t	;
// 	double	f6916	=	t	;
// 	double	f6917	=	t	;
// 	double	f6918	=	t	;
// 	double	f6919	=	t	;
// 	double	f6920	=	t	;
// 	double	f6921	=	t	;
// 	double	f6922	=	t	;
// 	double	f6923	=	t	;
// 	double	f6924	=	t	;
// 	double	f6925	=	t	;
// 	double	f6926	=	t	;
// 	double	f6927	=	t	;
// 	double	f6928	=	t	;
// 	double	f6929	=	t	;
// 	double	f6930	=	t	;
// 	double	f6931	=	t	;
// 	double	f6932	=	t	;
// 	double	f6933	=	t	;
// 	double	f6934	=	t	;
// 	double	f6935	=	t	;
// 	double	f6936	=	t	;
// 	double	f6937	=	t	;
// 	double	f6938	=	t	;
// 	double	f6939	=	t	;
// 	double	f6940	=	t	;
// 	double	f6941	=	t	;
// 	double	f6942	=	t	;
// 	double	f6943	=	t	;
// 	double	f6944	=	t	;
// 	double	f6945	=	t	;
// 	double	f6946	=	t	;
// 	double	f6947	=	t	;
// 	double	f6948	=	t	;
// 	double	f6949	=	t	;
// 	double	f6950	=	t	;
// 	double	f6951	=	t	;
// 	double	f6952	=	t	;
// 	double	f6953	=	t	;
// 	double	f6954	=	t	;
// 	double	f6955	=	t	;
// 	double	f6956	=	t	;
// 	double	f6957	=	t	;
// 	double	f6958	=	t	;
// 	double	f6959	=	t	;
// 	double	f6960	=	t	;
// 	double	f6961	=	t	;
// 	double	f6962	=	t	;
// 	double	f6963	=	t	;
// 	double	f6964	=	t	;
// 	double	f6965	=	t	;
// 	double	f6966	=	t	;
// 	double	f6967	=	t	;
// 	double	f6968	=	t	;
// 	double	f6969	=	t	;
// 	double	f6970	=	t	;
// 	double	f6971	=	t	;
// 	double	f6972	=	t	;
// 	double	f6973	=	t	;
// 	double	f6974	=	t	;
// 	double	f6975	=	t	;
// 	double	f6976	=	t	;
// 	double	f6977	=	t	;
// 	double	f6978	=	t	;
// 	double	f6979	=	t	;
// 	double	f6980	=	t	;
// 	double	f6981	=	t	;
// 	double	f6982	=	t	;
// 	double	f6983	=	t	;
// 	double	f6984	=	t	;
// 	double	f6985	=	t	;
// 	double	f6986	=	t	;
// 	double	f6987	=	t	;
// 	double	f6988	=	t	;
// 	double	f6989	=	t	;
// 	double	f6990	=	t	;
// 	double	f6991	=	t	;
// 	double	f6992	=	t	;
// 	double	f6993	=	t	;
// 	double	f6994	=	t	;
// 	double	f6995	=	t	;
// 	double	f6996	=	t	;
// 	double	f6997	=	t	;
// 	double	f6998	=	t	;
// 	double	f6999	=	t	;
// 	double	f7000	=	t	;
// 	double	f7001	=	t	;
// 	double	f7002	=	t	;
// 	double	f7003	=	t	;
// 	double	f7004	=	t	;
// 	double	f7005	=	t	;
// 	double	f7006	=	t	;
// 	double	f7007	=	t	;
// 	double	f7008	=	t	;
// 	double	f7009	=	t	;
// 	double	f7010	=	t	;
// 	double	f7011	=	t	;
// 	double	f7012	=	t	;
// 	double	f7013	=	t	;
// 	double	f7014	=	t	;
// 	double	f7015	=	t	;
// 	double	f7016	=	t	;
// 	double	f7017	=	t	;
// 	double	f7018	=	t	;
// 	double	f7019	=	t	;
// 	double	f7020	=	t	;
// 	double	f7021	=	t	;
// 	double	f7022	=	t	;
// 	double	f7023	=	t	;
// 	double	f7024	=	t	;
// 	double	f7025	=	t	;
// 	double	f7026	=	t	;
// 	double	f7027	=	t	;
// 	double	f7028	=	t	;
// 	double	f7029	=	t	;
// 	double	f7030	=	t	;
// 	double	f7031	=	t	;
// 	double	f7032	=	t	;
// 	double	f7033	=	t	;
// 	double	f7034	=	t	;
// 	double	f7035	=	t	;
// 	double	f7036	=	t	;
// 	double	f7037	=	t	;
// 	double	f7038	=	t	;
// 	double	f7039	=	t	;
// 	double	f7040	=	t	;
// 	double	f7041	=	t	;
// 	double	f7042	=	t	;
// 	double	f7043	=	t	;
// 	double	f7044	=	t	;
// 	double	f7045	=	t	;
// 	double	f7046	=	t	;
// 	double	f7047	=	t	;
// 	double	f7048	=	t	;
// 	double	f7049	=	t	;
// 	double	f7050	=	t	;
// 	double	f7051	=	t	;
// 	double	f7052	=	t	;
// 	double	f7053	=	t	;
// 	double	f7054	=	t	;
// 	double	f7055	=	t	;
// 	double	f7056	=	t	;
// 	double	f7057	=	t	;
// 	double	f7058	=	t	;
// 	double	f7059	=	t	;
// 	double	f7060	=	t	;
// 	double	f7061	=	t	;
// 	double	f7062	=	t	;
// 	double	f7063	=	t	;
// 	double	f7064	=	t	;
// 	double	f7065	=	t	;
// 	double	f7066	=	t	;
// 	double	f7067	=	t	;
// 	double	f7068	=	t	;
// 	double	f7069	=	t	;
// 	double	f7070	=	t	;
// 	double	f7071	=	t	;
// 	double	f7072	=	t	;
// 	double	f7073	=	t	;
// 	double	f7074	=	t	;
// 	double	f7075	=	t	;
// 	double	f7076	=	t	;
// 	double	f7077	=	t	;
// 	double	f7078	=	t	;
// 	double	f7079	=	t	;
// 	double	f7080	=	t	;
// 	double	f7081	=	t	;
// 	double	f7082	=	t	;
// 	double	f7083	=	t	;
// 	double	f7084	=	t	;
// 	double	f7085	=	t	;
// 	double	f7086	=	t	;
// 	double	f7087	=	t	;
// 	double	f7088	=	t	;
// 	double	f7089	=	t	;
// 	double	f7090	=	t	;
// 	double	f7091	=	t	;
// 	double	f7092	=	t	;
// 	double	f7093	=	t	;
// 	double	f7094	=	t	;
// 	double	f7095	=	t	;
// 	double	f7096	=	t	;
// 	double	f7097	=	t	;
// 	double	f7098	=	t	;
// 	double	f7099	=	t	;
// 	double	f7100	=	t	;
// 	double	f7101	=	t	;
// 	double	f7102	=	t	;
// 	double	f7103	=	t	;
// 	double	f7104	=	t	;
// 	double	f7105	=	t	;
// 	double	f7106	=	t	;
// 	double	f7107	=	t	;
// 	double	f7108	=	t	;
// 	double	f7109	=	t	;
// 	double	f7110	=	t	;
// 	double	f7111	=	t	;
// 	double	f7112	=	t	;
// 	double	f7113	=	t	;
// 	double	f7114	=	t	;
// 	double	f7115	=	t	;
// 	double	f7116	=	t	;
// 	double	f7117	=	t	;
// 	double	f7118	=	t	;
// 	double	f7119	=	t	;
// 	double	f7120	=	t	;
// 	double	f7121	=	t	;
// 	double	f7122	=	t	;
// 	double	f7123	=	t	;
// 	double	f7124	=	t	;
// 	double	f7125	=	t	;
// 	double	f7126	=	t	;
// 	double	f7127	=	t	;
// 	double	f7128	=	t	;
// 	double	f7129	=	t	;
// 	double	f7130	=	t	;
// 	double	f7131	=	t	;
// 	double	f7132	=	t	;
// 	double	f7133	=	t	;
// 	double	f7134	=	t	;
// 	double	f7135	=	t	;
// 	double	f7136	=	t	;
// 	double	f7137	=	t	;
// 	double	f7138	=	t	;
// 	double	f7139	=	t	;
// 	double	f7140	=	t	;
// 	double	f7141	=	t	;
// 	double	f7142	=	t	;
// 	double	f7143	=	t	;
// 	double	f7144	=	t	;
// 	double	f7145	=	t	;
// 	double	f7146	=	t	;
// 	double	f7147	=	t	;
// 	double	f7148	=	t	;
// 	double	f7149	=	t	;
// 	double	f7150	=	t	;
// 	double	f7151	=	t	;
// 	double	f7152	=	t	;
// 	double	f7153	=	t	;
// 	double	f7154	=	t	;
// 	double	f7155	=	t	;
// 	double	f7156	=	t	;
// 	double	f7157	=	t	;
// 	double	f7158	=	t	;
// 	double	f7159	=	t	;
// 	double	f7160	=	t	;
// 	double	f7161	=	t	;
// 	double	f7162	=	t	;
// 	double	f7163	=	t	;
// 	double	f7164	=	t	;
// 	double	f7165	=	t	;
// 	double	f7166	=	t	;
// 	double	f7167	=	t	;
// 	double	f7168	=	t	;
// 	double	f7169	=	t	;
// 	double	f7170	=	t	;
// 	double	f7171	=	t	;
// 	double	f7172	=	t	;
// 	double	f7173	=	t	;
// 	double	f7174	=	t	;
// 	double	f7175	=	t	;
// 	double	f7176	=	t	;
// 	double	f7177	=	t	;
// 	double	f7178	=	t	;
// 	double	f7179	=	t	;
// 	double	f7180	=	t	;
// 	double	f7181	=	t	;
// 	double	f7182	=	t	;
// 	double	f7183	=	t	;
// 	double	f7184	=	t	;
// 	double	f7185	=	t	;
// 	double	f7186	=	t	;
// 	double	f7187	=	t	;
// 	double	f7188	=	t	;
// 	double	f7189	=	t	;
// 	double	f7190	=	t	;
// 	double	f7191	=	t	;
// 	double	f7192	=	t	;
// 	double	f7193	=	t	;
// 	double	f7194	=	t	;
// 	double	f7195	=	t	;
// 	double	f7196	=	t	;
// 	double	f7197	=	t	;
// 	double	f7198	=	t	;
// 	double	f7199	=	t	;
// 	double	f7200	=	t	;
// 	double	f7201	=	t	;
// 	double	f7202	=	t	;
// 	double	f7203	=	t	;
// 	double	f7204	=	t	;
// 	double	f7205	=	t	;
// 	double	f7206	=	t	;
// 	double	f7207	=	t	;
// 	double	f7208	=	t	;
// 	double	f7209	=	t	;
// 	double	f7210	=	t	;
// 	double	f7211	=	t	;
// 	double	f7212	=	t	;
// 	double	f7213	=	t	;
// 	double	f7214	=	t	;
// 	double	f7215	=	t	;
// 	double	f7216	=	t	;
// 	double	f7217	=	t	;
// 	double	f7218	=	t	;
// 	double	f7219	=	t	;
// 	double	f7220	=	t	;
// 	double	f7221	=	t	;
// 	double	f7222	=	t	;
// 	double	f7223	=	t	;
// 	double	f7224	=	t	;
// 	double	f7225	=	t	;
// 	double	f7226	=	t	;
// 	double	f7227	=	t	;
// 	double	f7228	=	t	;
// 	double	f7229	=	t	;
// 	double	f7230	=	t	;
// 	double	f7231	=	t	;
// 	double	f7232	=	t	;
// 	double	f7233	=	t	;
// 	double	f7234	=	t	;
// 	double	f7235	=	t	;
// 	double	f7236	=	t	;
// 	double	f7237	=	t	;
// 	double	f7238	=	t	;
// 	double	f7239	=	t	;
// 	double	f7240	=	t	;
// 	double	f7241	=	t	;
// 	double	f7242	=	t	;
// 	double	f7243	=	t	;
// 	double	f7244	=	t	;
// 	double	f7245	=	t	;
// 	double	f7246	=	t	;
// 	double	f7247	=	t	;
// 	double	f7248	=	t	;
// 	double	f7249	=	t	;
// 	double	f7250	=	t	;
// 	double	f7251	=	t	;
// 	double	f7252	=	t	;
// 	double	f7253	=	t	;
// 	double	f7254	=	t	;
// 	double	f7255	=	t	;
// 	double	f7256	=	t	;
// 	double	f7257	=	t	;
// 	double	f7258	=	t	;
// 	double	f7259	=	t	;
// 	double	f7260	=	t	;
// 	double	f7261	=	t	;
// 	double	f7262	=	t	;
// 	double	f7263	=	t	;
// 	double	f7264	=	t	;
// 	double	f7265	=	t	;
// 	double	f7266	=	t	;
// 	double	f7267	=	t	;
// 	double	f7268	=	t	;
// 	double	f7269	=	t	;
// 	double	f7270	=	t	;
// 	double	f7271	=	t	;
// 	double	f7272	=	t	;
// 	double	f7273	=	t	;
// 	double	f7274	=	t	;
// 	double	f7275	=	t	;
// 	double	f7276	=	t	;
// 	double	f7277	=	t	;
// 	double	f7278	=	t	;
// 	double	f7279	=	t	;
// 	double	f7280	=	t	;
// 	double	f7281	=	t	;
// 	double	f7282	=	t	;
// 	double	f7283	=	t	;
// 	double	f7284	=	t	;
// 	double	f7285	=	t	;
// 	double	f7286	=	t	;
// 	double	f7287	=	t	;
// 	double	f7288	=	t	;
// 	double	f7289	=	t	;
// 	double	f7290	=	t	;
// 	double	f7291	=	t	;
// 	double	f7292	=	t	;
// 	double	f7293	=	t	;
// 	double	f7294	=	t	;
// 	double	f7295	=	t	;
// 	double	f7296	=	t	;
// 	double	f7297	=	t	;
// 	double	f7298	=	t	;
// 	double	f7299	=	t	;
// 	double	f7300	=	t	;
// 	double	f7301	=	t	;
// 	double	f7302	=	t	;
// 	double	f7303	=	t	;
// 	double	f7304	=	t	;
// 	double	f7305	=	t	;
// 	double	f7306	=	t	;
// 	double	f7307	=	t	;
// 	double	f7308	=	t	;
// 	double	f7309	=	t	;
// 	double	f7310	=	t	;
// 	double	f7311	=	t	;
// 	double	f7312	=	t	;
// 	double	f7313	=	t	;
// 	double	f7314	=	t	;
// 	double	f7315	=	t	;
// 	double	f7316	=	t	;
// 	double	f7317	=	t	;
// 	double	f7318	=	t	;
// 	double	f7319	=	t	;
// 	double	f7320	=	t	;
// 	double	f7321	=	t	;
// 	double	f7322	=	t	;
// 	double	f7323	=	t	;
// 	double	f7324	=	t	;
// 	double	f7325	=	t	;
// 	double	f7326	=	t	;
// 	double	f7327	=	t	;
// 	double	f7328	=	t	;
// 	double	f7329	=	t	;
// 	double	f7330	=	t	;
// 	double	f7331	=	t	;
// 	double	f7332	=	t	;
// 	double	f7333	=	t	;
// 	double	f7334	=	t	;
// 	double	f7335	=	t	;
// 	double	f7336	=	t	;
// 	double	f7337	=	t	;
// 	double	f7338	=	t	;
// 	double	f7339	=	t	;
// 	double	f7340	=	t	;
// 	double	f7341	=	t	;
// 	double	f7342	=	t	;
// 	double	f7343	=	t	;
// 	double	f7344	=	t	;
// 	double	f7345	=	t	;
// 	double	f7346	=	t	;
// 	double	f7347	=	t	;
// 	double	f7348	=	t	;
// 	double	f7349	=	t	;
// 	double	f7350	=	t	;
// 	double	f7351	=	t	;
// 	double	f7352	=	t	;
// 	double	f7353	=	t	;
// 	double	f7354	=	t	;
// 	double	f7355	=	t	;
// 	double	f7356	=	t	;
// 	double	f7357	=	t	;
// 	double	f7358	=	t	;
// 	double	f7359	=	t	;
// 	double	f7360	=	t	;
// 	double	f7361	=	t	;
// 	double	f7362	=	t	;
// 	double	f7363	=	t	;
// 	double	f7364	=	t	;
// 	double	f7365	=	t	;
// 	double	f7366	=	t	;
// 	double	f7367	=	t	;
// 	double	f7368	=	t	;
// 	double	f7369	=	t	;
// 	double	f7370	=	t	;
// 	double	f7371	=	t	;
// 	double	f7372	=	t	;
// 	double	f7373	=	t	;
// 	double	f7374	=	t	;
// 	double	f7375	=	t	;
// 	double	f7376	=	t	;
// 	double	f7377	=	t	;
// 	double	f7378	=	t	;
// 	double	f7379	=	t	;
// 	double	f7380	=	t	;
// 	double	f7381	=	t	;
// 	double	f7382	=	t	;
// 	double	f7383	=	t	;
// 	double	f7384	=	t	;
// 	double	f7385	=	t	;
// 	double	f7386	=	t	;
// 	double	f7387	=	t	;
// 	double	f7388	=	t	;
// 	double	f7389	=	t	;
// 	double	f7390	=	t	;
// 	double	f7391	=	t	;
// 	double	f7392	=	t	;
// 	double	f7393	=	t	;
// 	double	f7394	=	t	;
// 	double	f7395	=	t	;
// 	double	f7396	=	t	;
// 	double	f7397	=	t	;
// 	double	f7398	=	t	;
// 	double	f7399	=	t	;
// 	double	f7400	=	t	;
// 	double	f7401	=	t	;
// 	double	f7402	=	t	;
// 	double	f7403	=	t	;
// 	double	f7404	=	t	;
// 	double	f7405	=	t	;
// 	double	f7406	=	t	;
// 	double	f7407	=	t	;
// 	double	f7408	=	t	;
// 	double	f7409	=	t	;
// 	double	f7410	=	t	;
// 	double	f7411	=	t	;
// 	double	f7412	=	t	;
// 	double	f7413	=	t	;
// 	double	f7414	=	t	;
// 	double	f7415	=	t	;
// 	double	f7416	=	t	;
// 	double	f7417	=	t	;
// 	double	f7418	=	t	;
// 	double	f7419	=	t	;
// 	double	f7420	=	t	;
// 	double	f7421	=	t	;
// 	double	f7422	=	t	;
// 	double	f7423	=	t	;
// 	double	f7424	=	t	;
// 	double	f7425	=	t	;
// 	double	f7426	=	t	;
// 	double	f7427	=	t	;
// 	double	f7428	=	t	;
// 	double	f7429	=	t	;
// 	double	f7430	=	t	;
// 	double	f7431	=	t	;
// 	double	f7432	=	t	;
// 	double	f7433	=	t	;
// 	double	f7434	=	t	;
// 	double	f7435	=	t	;
// 	double	f7436	=	t	;
// 	double	f7437	=	t	;
// 	double	f7438	=	t	;
// 	double	f7439	=	t	;
// 	double	f7440	=	t	;
// 	double	f7441	=	t	;
// 	double	f7442	=	t	;
// 	double	f7443	=	t	;
// 	double	f7444	=	t	;
// 	double	f7445	=	t	;
// 	double	f7446	=	t	;
// 	double	f7447	=	t	;
// 	double	f7448	=	t	;
// 	double	f7449	=	t	;
// 	double	f7450	=	t	;
// 	double	f7451	=	t	;
// 	double	f7452	=	t	;
// 	double	f7453	=	t	;
// 	double	f7454	=	t	;
// 	double	f7455	=	t	;
// 	double	f7456	=	t	;
// 	double	f7457	=	t	;
// 	double	f7458	=	t	;
// 	double	f7459	=	t	;
// 	double	f7460	=	t	;
// 	double	f7461	=	t	;
// 	double	f7462	=	t	;
// 	double	f7463	=	t	;
// 	double	f7464	=	t	;
// 	double	f7465	=	t	;
// 	double	f7466	=	t	;
// 	double	f7467	=	t	;
// 	double	f7468	=	t	;
// 	double	f7469	=	t	;
// 	double	f7470	=	t	;
// 	double	f7471	=	t	;
// 	double	f7472	=	t	;
// 	double	f7473	=	t	;
// 	double	f7474	=	t	;
// 	double	f7475	=	t	;
// 	double	f7476	=	t	;
// 	double	f7477	=	t	;
// 	double	f7478	=	t	;
// 	double	f7479	=	t	;
// 	double	f7480	=	t	;
// 	double	f7481	=	t	;
// 	double	f7482	=	t	;
// 	double	f7483	=	t	;
// 	double	f7484	=	t	;
// 	double	f7485	=	t	;
// 	double	f7486	=	t	;
// 	double	f7487	=	t	;
// 	double	f7488	=	t	;
// 	double	f7489	=	t	;
// 	double	f7490	=	t	;
// 	double	f7491	=	t	;
// 	double	f7492	=	t	;
// 	double	f7493	=	t	;
// 	double	f7494	=	t	;
// 	double	f7495	=	t	;
// 	double	f7496	=	t	;
// 	double	f7497	=	t	;
// 	double	f7498	=	t	;
// 	double	f7499	=	t	;
// 	double	f7500	=	t	;
// 	double	f7501	=	t	;
// 	double	f7502	=	t	;
// 	double	f7503	=	t	;
// 	double	f7504	=	t	;
// 	double	f7505	=	t	;
// 	double	f7506	=	t	;
// 	double	f7507	=	t	;
// 	double	f7508	=	t	;
// 	double	f7509	=	t	;
// 	double	f7510	=	t	;
// 	double	f7511	=	t	;
// 	double	f7512	=	t	;
// 	double	f7513	=	t	;
// 	double	f7514	=	t	;
// 	double	f7515	=	t	;
// 	double	f7516	=	t	;
// 	double	f7517	=	t	;
// 	double	f7518	=	t	;
// 	double	f7519	=	t	;
// 	double	f7520	=	t	;
// 	double	f7521	=	t	;
// 	double	f7522	=	t	;
// 	double	f7523	=	t	;
// 	double	f7524	=	t	;
// 	double	f7525	=	t	;
// 	double	f7526	=	t	;
// 	double	f7527	=	t	;
// 	double	f7528	=	t	;
// 	double	f7529	=	t	;
// 	double	f7530	=	t	;
// 	double	f7531	=	t	;
// 	double	f7532	=	t	;
// 	double	f7533	=	t	;
// 	double	f7534	=	t	;
// 	double	f7535	=	t	;
// 	double	f7536	=	t	;
// 	double	f7537	=	t	;
// 	double	f7538	=	t	;
// 	double	f7539	=	t	;
// 	double	f7540	=	t	;
// 	double	f7541	=	t	;
// 	double	f7542	=	t	;
// 	double	f7543	=	t	;
// 	double	f7544	=	t	;
// 	double	f7545	=	t	;
// 	double	f7546	=	t	;
// 	double	f7547	=	t	;
// 	double	f7548	=	t	;
// 	double	f7549	=	t	;
// 	double	f7550	=	t	;
// 	double	f7551	=	t	;
// 	double	f7552	=	t	;
// 	double	f7553	=	t	;
// 	double	f7554	=	t	;
// 	double	f7555	=	t	;
// 	double	f7556	=	t	;
// 	double	f7557	=	t	;
// 	double	f7558	=	t	;
// 	double	f7559	=	t	;
// 	double	f7560	=	t	;
// 	double	f7561	=	t	;
// 	double	f7562	=	t	;
// 	double	f7563	=	t	;
// 	double	f7564	=	t	;
// 	double	f7565	=	t	;
// 	double	f7566	=	t	;
// 	double	f7567	=	t	;
// 	double	f7568	=	t	;
// 	double	f7569	=	t	;
// 	double	f7570	=	t	;
// 	double	f7571	=	t	;
// 	double	f7572	=	t	;
// 	double	f7573	=	t	;
// 	double	f7574	=	t	;
// 	double	f7575	=	t	;
// 	double	f7576	=	t	;
// 	double	f7577	=	t	;
// 	double	f7578	=	t	;
// 	double	f7579	=	t	;
// 	double	f7580	=	t	;
// 	double	f7581	=	t	;
// 	double	f7582	=	t	;
// 	double	f7583	=	t	;
// 	double	f7584	=	t	;
// 	double	f7585	=	t	;
// 	double	f7586	=	t	;
// 	double	f7587	=	t	;
// 	double	f7588	=	t	;
// 	double	f7589	=	t	;
// 	double	f7590	=	t	;
// 	double	f7591	=	t	;
// 	double	f7592	=	t	;
// 	double	f7593	=	t	;
// 	double	f7594	=	t	;
// 	double	f7595	=	t	;
// 	double	f7596	=	t	;
// 	double	f7597	=	t	;
// 	double	f7598	=	t	;
// 	double	f7599	=	t	;
// 	double	f7600	=	t	;
// 	double	f7601	=	t	;
// 	double	f7602	=	t	;
// 	double	f7603	=	t	;
// 	double	f7604	=	t	;
// 	double	f7605	=	t	;
// 	double	f7606	=	t	;
// 	double	f7607	=	t	;
// 	double	f7608	=	t	;
// 	double	f7609	=	t	;
// 	double	f7610	=	t	;
// 	double	f7611	=	t	;
// 	double	f7612	=	t	;
// 	double	f7613	=	t	;
// 	double	f7614	=	t	;
// 	double	f7615	=	t	;
// 	double	f7616	=	t	;
// 	double	f7617	=	t	;
// 	double	f7618	=	t	;
// 	double	f7619	=	t	;
// 	double	f7620	=	t	;
// 	double	f7621	=	t	;
// 	double	f7622	=	t	;
// 	double	f7623	=	t	;
// 	double	f7624	=	t	;
// 	double	f7625	=	t	;
// 	double	f7626	=	t	;
// 	double	f7627	=	t	;
// 	double	f7628	=	t	;
// 	double	f7629	=	t	;
// 	double	f7630	=	t	;
// 	double	f7631	=	t	;
// 	double	f7632	=	t	;
// 	double	f7633	=	t	;
// 	double	f7634	=	t	;
// 	double	f7635	=	t	;
// 	double	f7636	=	t	;
// 	double	f7637	=	t	;
// 	double	f7638	=	t	;
// 	double	f7639	=	t	;
// 	double	f7640	=	t	;
// 	double	f7641	=	t	;
// 	double	f7642	=	t	;
// 	double	f7643	=	t	;
// 	double	f7644	=	t	;
// 	double	f7645	=	t	;
// 	double	f7646	=	t	;
// 	double	f7647	=	t	;
// 	double	f7648	=	t	;
// 	double	f7649	=	t	;
// 	double	f7650	=	t	;
// 	double	f7651	=	t	;
// 	double	f7652	=	t	;
// 	double	f7653	=	t	;
// 	double	f7654	=	t	;
// 	double	f7655	=	t	;
// 	double	f7656	=	t	;
// 	double	f7657	=	t	;
// 	double	f7658	=	t	;
// 	double	f7659	=	t	;
// 	double	f7660	=	t	;
// 	double	f7661	=	t	;
// 	double	f7662	=	t	;
// 	double	f7663	=	t	;
// 	double	f7664	=	t	;
// 	double	f7665	=	t	;
// 	double	f7666	=	t	;
// 	double	f7667	=	t	;
// 	double	f7668	=	t	;
// 	double	f7669	=	t	;
// 	double	f7670	=	t	;
// 	double	f7671	=	t	;
// 	double	f7672	=	t	;
// 	double	f7673	=	t	;
// 	double	f7674	=	t	;
// 	double	f7675	=	t	;
// 	double	f7676	=	t	;
// 	double	f7677	=	t	;
// 	double	f7678	=	t	;
// 	double	f7679	=	t	;
// 	double	f7680	=	t	;
// 	double	f7681	=	t	;
// 	double	f7682	=	t	;
// 	double	f7683	=	t	;
// 	double	f7684	=	t	;
// 	double	f7685	=	t	;
// 	double	f7686	=	t	;
// 	double	f7687	=	t	;
// 	double	f7688	=	t	;
// 	double	f7689	=	t	;
// 	double	f7690	=	t	;
// 	double	f7691	=	t	;
// 	double	f7692	=	t	;
// 	double	f7693	=	t	;
// 	double	f7694	=	t	;
// 	double	f7695	=	t	;
// 	double	f7696	=	t	;
// 	double	f7697	=	t	;
// 	double	f7698	=	t	;
// 	double	f7699	=	t	;
// 	double	f7700	=	t	;
// 	double	f7701	=	t	;
// 	double	f7702	=	t	;
// 	double	f7703	=	t	;
// 	double	f7704	=	t	;
// 	double	f7705	=	t	;
// 	double	f7706	=	t	;
// 	double	f7707	=	t	;
// 	double	f7708	=	t	;
// 	double	f7709	=	t	;
// 	double	f7710	=	t	;
// 	double	f7711	=	t	;
// 	double	f7712	=	t	;
// 	double	f7713	=	t	;
// 	double	f7714	=	t	;
// 	double	f7715	=	t	;
// 	double	f7716	=	t	;
// 	double	f7717	=	t	;
// 	double	f7718	=	t	;
// 	double	f7719	=	t	;
// 	double	f7720	=	t	;
// 	double	f7721	=	t	;
// 	double	f7722	=	t	;
// 	double	f7723	=	t	;
// 	double	f7724	=	t	;
// 	double	f7725	=	t	;
// 	double	f7726	=	t	;
// 	double	f7727	=	t	;
// 	double	f7728	=	t	;
// 	double	f7729	=	t	;
// 	double	f7730	=	t	;
// 	double	f7731	=	t	;
// 	double	f7732	=	t	;
// 	double	f7733	=	t	;
// 	double	f7734	=	t	;
// 	double	f7735	=	t	;
// 	double	f7736	=	t	;
// 	double	f7737	=	t	;
// 	double	f7738	=	t	;
// 	double	f7739	=	t	;
// 	double	f7740	=	t	;
// 	double	f7741	=	t	;
// 	double	f7742	=	t	;
// 	double	f7743	=	t	;
// 	double	f7744	=	t	;
// 	double	f7745	=	t	;
// 	double	f7746	=	t	;
// 	double	f7747	=	t	;
// 	double	f7748	=	t	;
// 	double	f7749	=	t	;
// 	double	f7750	=	t	;
// 	double	f7751	=	t	;
// 	double	f7752	=	t	;
// 	double	f7753	=	t	;
// 	double	f7754	=	t	;
// 	double	f7755	=	t	;
// 	double	f7756	=	t	;
// 	double	f7757	=	t	;
// 	double	f7758	=	t	;
// 	double	f7759	=	t	;
// 	double	f7760	=	t	;
// 	double	f7761	=	t	;
// 	double	f7762	=	t	;
// 	double	f7763	=	t	;
// 	double	f7764	=	t	;
// 	double	f7765	=	t	;
// 	double	f7766	=	t	;
// 	double	f7767	=	t	;
// 	double	f7768	=	t	;
// 	double	f7769	=	t	;
// 	double	f7770	=	t	;
// 	double	f7771	=	t	;
// 	double	f7772	=	t	;
// 	double	f7773	=	t	;
// 	double	f7774	=	t	;
// 	double	f7775	=	t	;
// 	double	f7776	=	t	;
// 	double	f7777	=	t	;
// 	double	f7778	=	t	;
// 	double	f7779	=	t	;
// 	double	f7780	=	t	;
// 	double	f7781	=	t	;
// 	double	f7782	=	t	;
// 	double	f7783	=	t	;
// 	double	f7784	=	t	;
// 	double	f7785	=	t	;
// 	double	f7786	=	t	;
// 	double	f7787	=	t	;
// 	double	f7788	=	t	;
// 	double	f7789	=	t	;
// 	double	f7790	=	t	;
// 	double	f7791	=	t	;
// 	double	f7792	=	t	;
// 	double	f7793	=	t	;
// 	double	f7794	=	t	;
// 	double	f7795	=	t	;
// 	double	f7796	=	t	;
// 	double	f7797	=	t	;
// 	double	f7798	=	t	;
// 	double	f7799	=	t	;
// 	double	f7800	=	t	;
// 	double	f7801	=	t	;
// 	double	f7802	=	t	;
// 	double	f7803	=	t	;
// 	double	f7804	=	t	;
// 	double	f7805	=	t	;
// 	double	f7806	=	t	;
// 	double	f7807	=	t	;
// 	double	f7808	=	t	;
// 	double	f7809	=	t	;
// 	double	f7810	=	t	;
// 	double	f7811	=	t	;
// 	double	f7812	=	t	;
// 	double	f7813	=	t	;
// 	double	f7814	=	t	;
// 	double	f7815	=	t	;
// 	double	f7816	=	t	;
// 	double	f7817	=	t	;
// 	double	f7818	=	t	;
// 	double	f7819	=	t	;
// 	double	f7820	=	t	;
// 	double	f7821	=	t	;
// 	double	f7822	=	t	;
// 	double	f7823	=	t	;
// 	double	f7824	=	t	;
// 	double	f7825	=	t	;
// 	double	f7826	=	t	;
// 	double	f7827	=	t	;
// 	double	f7828	=	t	;
// 	double	f7829	=	t	;
// 	double	f7830	=	t	;
// 	double	f7831	=	t	;
// 	double	f7832	=	t	;
// 	double	f7833	=	t	;
// 	double	f7834	=	t	;
// 	double	f7835	=	t	;
// 	double	f7836	=	t	;
// 	double	f7837	=	t	;
// 	double	f7838	=	t	;
// 	double	f7839	=	t	;
// 	double	f7840	=	t	;
// 	double	f7841	=	t	;
// 	double	f7842	=	t	;
// 	double	f7843	=	t	;
// 	double	f7844	=	t	;
// 	double	f7845	=	t	;
// 	double	f7846	=	t	;
// 	double	f7847	=	t	;
// 	double	f7848	=	t	;
// 	double	f7849	=	t	;
// 	double	f7850	=	t	;
// 	double	f7851	=	t	;
// 	double	f7852	=	t	;
// 	double	f7853	=	t	;
// 	double	f7854	=	t	;
// 	double	f7855	=	t	;
// 	double	f7856	=	t	;
// 	double	f7857	=	t	;
// 	double	f7858	=	t	;
// 	double	f7859	=	t	;
// 	double	f7860	=	t	;
// 	double	f7861	=	t	;
// 	double	f7862	=	t	;
// 	double	f7863	=	t	;
// 	double	f7864	=	t	;
// 	double	f7865	=	t	;
// 	double	f7866	=	t	;
// 	double	f7867	=	t	;
// 	double	f7868	=	t	;
// 	double	f7869	=	t	;
// 	double	f7870	=	t	;
// 	double	f7871	=	t	;
// 	double	f7872	=	t	;
// 	double	f7873	=	t	;
// 	double	f7874	=	t	;
// 	double	f7875	=	t	;
// 	double	f7876	=	t	;
// 	double	f7877	=	t	;
// 	double	f7878	=	t	;
// 	double	f7879	=	t	;
// 	double	f7880	=	t	;
// 	double	f7881	=	t	;
// 	double	f7882	=	t	;
// 	double	f7883	=	t	;
// 	double	f7884	=	t	;
// 	double	f7885	=	t	;
// 	double	f7886	=	t	;
// 	double	f7887	=	t	;
// 	double	f7888	=	t	;
// 	double	f7889	=	t	;
// 	double	f7890	=	t	;
// 	double	f7891	=	t	;
// 	double	f7892	=	t	;
// 	double	f7893	=	t	;
// 	double	f7894	=	t	;
// 	double	f7895	=	t	;
// 	double	f7896	=	t	;
// 	double	f7897	=	t	;
// 	double	f7898	=	t	;
// 	double	f7899	=	t	;
// 	double	f7900	=	t	;
// 	double	f7901	=	t	;
// 	double	f7902	=	t	;
// 	double	f7903	=	t	;
// 	double	f7904	=	t	;
// 	double	f7905	=	t	;
// 	double	f7906	=	t	;
// 	double	f7907	=	t	;
// 	double	f7908	=	t	;
// 	double	f7909	=	t	;
// 	double	f7910	=	t	;
// 	double	f7911	=	t	;
// 	double	f7912	=	t	;
// 	double	f7913	=	t	;
// 	double	f7914	=	t	;
// 	double	f7915	=	t	;
// 	double	f7916	=	t	;
// 	double	f7917	=	t	;
// 	double	f7918	=	t	;
// 	double	f7919	=	t	;
// 	double	f7920	=	t	;
// 	double	f7921	=	t	;
// 	double	f7922	=	t	;
// 	double	f7923	=	t	;
// 	double	f7924	=	t	;
// 	double	f7925	=	t	;
// 	double	f7926	=	t	;
// 	double	f7927	=	t	;
// 	double	f7928	=	t	;
// 	double	f7929	=	t	;
// 	double	f7930	=	t	;
// 	double	f7931	=	t	;
// 	double	f7932	=	t	;
// 	double	f7933	=	t	;
// 	double	f7934	=	t	;
// 	double	f7935	=	t	;
// 	double	f7936	=	t	;
// 	double	f7937	=	t	;
// 	double	f7938	=	t	;
// 	double	f7939	=	t	;
// 	double	f7940	=	t	;
// 	double	f7941	=	t	;
// 	double	f7942	=	t	;
// 	double	f7943	=	t	;
// 	double	f7944	=	t	;
// 	double	f7945	=	t	;
// 	double	f7946	=	t	;
// 	double	f7947	=	t	;
// 	double	f7948	=	t	;
// 	double	f7949	=	t	;
// 	double	f7950	=	t	;
// 	double	f7951	=	t	;
// 	double	f7952	=	t	;
// 	double	f7953	=	t	;
// 	double	f7954	=	t	;
// 	double	f7955	=	t	;
// 	double	f7956	=	t	;
// 	double	f7957	=	t	;
// 	double	f7958	=	t	;
// 	double	f7959	=	t	;
// 	double	f7960	=	t	;
// 	double	f7961	=	t	;
// 	double	f7962	=	t	;
// 	double	f7963	=	t	;
// 	double	f7964	=	t	;
// 	double	f7965	=	t	;
// 	double	f7966	=	t	;
// 	double	f7967	=	t	;
// 	double	f7968	=	t	;
// 	double	f7969	=	t	;
// 	double	f7970	=	t	;
// 	double	f7971	=	t	;
// 	double	f7972	=	t	;
// 	double	f7973	=	t	;
// 	double	f7974	=	t	;
// 	double	f7975	=	t	;
// 	double	f7976	=	t	;
// 	double	f7977	=	t	;
// 	double	f7978	=	t	;
// 	double	f7979	=	t	;
// 	double	f7980	=	t	;
// 	double	f7981	=	t	;
// 	double	f7982	=	t	;
// 	double	f7983	=	t	;
// 	double	f7984	=	t	;
// 	double	f7985	=	t	;
// 	double	f7986	=	t	;
// 	double	f7987	=	t	;
// 	double	f7988	=	t	;
// 	double	f7989	=	t	;
// 	double	f7990	=	t	;
// 	double	f7991	=	t	;
// 	double	f7992	=	t	;
// 	double	f7993	=	t	;
// 	double	f7994	=	t	;
// 	double	f7995	=	t	;
// 	double	f7996	=	t	;
// 	double	f7997	=	t	;
// 	double	f7998	=	t	;
// 	double	f7999	=	t	;
// 	double	f8000	=	t	;
// 	double	f8001	=	t	;
// 	double	f8002	=	t	;
// 	double	f8003	=	t	;
// 	double	f8004	=	t	;
// 	double	f8005	=	t	;
// 	double	f8006	=	t	;
// 	double	f8007	=	t	;
// 	double	f8008	=	t	;
// 	double	f8009	=	t	;
// 	double	f8010	=	t	;
// 	double	f8011	=	t	;
// 	double	f8012	=	t	;
// 	double	f8013	=	t	;
// 	double	f8014	=	t	;
// 	double	f8015	=	t	;
// 	double	f8016	=	t	;
// 	double	f8017	=	t	;
// 	double	f8018	=	t	;
// 	double	f8019	=	t	;
// 	double	f8020	=	t	;
// 	double	f8021	=	t	;
// 	double	f8022	=	t	;
// 	double	f8023	=	t	;
// 	double	f8024	=	t	;
// 	double	f8025	=	t	;
// 	double	f8026	=	t	;
// 	double	f8027	=	t	;
// 	double	f8028	=	t	;
// 	double	f8029	=	t	;
// 	double	f8030	=	t	;
// 	double	f8031	=	t	;
// 	double	f8032	=	t	;
// 	double	f8033	=	t	;
// 	double	f8034	=	t	;
// 	double	f8035	=	t	;
// 	double	f8036	=	t	;
// 	double	f8037	=	t	;
// 	double	f8038	=	t	;
// 	double	f8039	=	t	;
// 	double	f8040	=	t	;
// 	double	f8041	=	t	;
// 	double	f8042	=	t	;
// 	double	f8043	=	t	;
// 	double	f8044	=	t	;
// 	double	f8045	=	t	;
// 	double	f8046	=	t	;
// 	double	f8047	=	t	;
// 	double	f8048	=	t	;
// 	double	f8049	=	t	;
// 	double	f8050	=	t	;
// 	double	f8051	=	t	;
// 	double	f8052	=	t	;
// 	double	f8053	=	t	;
// 	double	f8054	=	t	;
// 	double	f8055	=	t	;
// 	double	f8056	=	t	;
// 	double	f8057	=	t	;
// 	double	f8058	=	t	;
// 	double	f8059	=	t	;
// 	double	f8060	=	t	;
// 	double	f8061	=	t	;
// 	double	f8062	=	t	;
// 	double	f8063	=	t	;
// 	double	f8064	=	t	;
// 	double	f8065	=	t	;
// 	double	f8066	=	t	;
// 	double	f8067	=	t	;
// 	double	f8068	=	t	;
// 	double	f8069	=	t	;
// 	double	f8070	=	t	;
// 	double	f8071	=	t	;
// 	double	f8072	=	t	;
// 	double	f8073	=	t	;
// 	double	f8074	=	t	;
// 	double	f8075	=	t	;
// 	double	f8076	=	t	;
// 	double	f8077	=	t	;
// 	double	f8078	=	t	;
// 	double	f8079	=	t	;
// 	double	f8080	=	t	;
// 	double	f8081	=	t	;
// 	double	f8082	=	t	;
// 	double	f8083	=	t	;
// 	double	f8084	=	t	;
// 	double	f8085	=	t	;
// 	double	f8086	=	t	;
// 	double	f8087	=	t	;
// 	double	f8088	=	t	;
// 	double	f8089	=	t	;
// 	double	f8090	=	t	;
// 	double	f8091	=	t	;
// 	double	f8092	=	t	;
// 	double	f8093	=	t	;
// 	double	f8094	=	t	;
// 	double	f8095	=	t	;
// 	double	f8096	=	t	;
// 	double	f8097	=	t	;
// 	double	f8098	=	t	;
// 	double	f8099	=	t	;
// 	double	f8100	=	t	;
// 	double	f8101	=	t	;
// 	double	f8102	=	t	;
// 	double	f8103	=	t	;
// 	double	f8104	=	t	;
// 	double	f8105	=	t	;
// 	double	f8106	=	t	;
// 	double	f8107	=	t	;
// 	double	f8108	=	t	;
// 	double	f8109	=	t	;
// 	double	f8110	=	t	;
// 	double	f8111	=	t	;
// 	double	f8112	=	t	;
// 	double	f8113	=	t	;
// 	double	f8114	=	t	;
// 	double	f8115	=	t	;
// 	double	f8116	=	t	;
// 	double	f8117	=	t	;
// 	double	f8118	=	t	;
// 	double	f8119	=	t	;
// 	double	f8120	=	t	;
// 	double	f8121	=	t	;
// 	double	f8122	=	t	;
// 	double	f8123	=	t	;
// 	double	f8124	=	t	;
// 	double	f8125	=	t	;
// 	double	f8126	=	t	;
// 	double	f8127	=	t	;
// 	double	f8128	=	t	;
// 	double	f8129	=	t	;
// 	double	f8130	=	t	;
// 	double	f8131	=	t	;
// 	double	f8132	=	t	;
// 	double	f8133	=	t	;
// 	double	f8134	=	t	;
// 	double	f8135	=	t	;
// 	double	f8136	=	t	;
// 	double	f8137	=	t	;
// 	double	f8138	=	t	;
// 	double	f8139	=	t	;
// 	double	f8140	=	t	;
// 	double	f8141	=	t	;
// 	double	f8142	=	t	;
// 	double	f8143	=	t	;
// 	double	f8144	=	t	;
// 	double	f8145	=	t	;
// 	double	f8146	=	t	;
// 	double	f8147	=	t	;
// 	double	f8148	=	t	;
// 	double	f8149	=	t	;
// 	double	f8150	=	t	;
// 	double	f8151	=	t	;
// 	double	f8152	=	t	;
// 	double	f8153	=	t	;
// 	double	f8154	=	t	;
// 	double	f8155	=	t	;
// 	double	f8156	=	t	;
// 	double	f8157	=	t	;
// 	double	f8158	=	t	;
// 	double	f8159	=	t	;
// 	double	f8160	=	t	;
// 	double	f8161	=	t	;
// 	double	f8162	=	t	;
// 	double	f8163	=	t	;
// 	double	f8164	=	t	;
// 	double	f8165	=	t	;
// 	double	f8166	=	t	;
// 	double	f8167	=	t	;
// 	double	f8168	=	t	;
// 	double	f8169	=	t	;
// 	double	f8170	=	t	;
// 	double	f8171	=	t	;
// 	double	f8172	=	t	;
// 	double	f8173	=	t	;
// 	double	f8174	=	t	;
// 	double	f8175	=	t	;
// 	double	f8176	=	t	;
// 	double	f8177	=	t	;
// 	double	f8178	=	t	;
// 	double	f8179	=	t	;
// 	double	f8180	=	t	;
// 	double	f8181	=	t	;
// 	double	f8182	=	t	;
// 	double	f8183	=	t	;
// 	double	f8184	=	t	;
// 	double	f8185	=	t	;
// 	double	f8186	=	t	;
// 	double	f8187	=	t	;
// 	double	f8188	=	t	;
// 	double	f8189	=	t	;
// 	double	f8190	=	t	;
// 	double	f8191	=	t	;
// 	double	f8192	=	t	;
// 	double	f8193	=	t	;
// 	double	f8194	=	t	;
// 	double	f8195	=	t	;
// 	double	f8196	=	t	;
// 	double	f8197	=	t	;
// 	double	f8198	=	t	;
// 	double	f8199	=	t	;
// 	double	f8200	=	t	;
// 	double	f8201	=	t	;
// 	double	f8202	=	t	;
// 	double	f8203	=	t	;
// 	double	f8204	=	t	;
// 	double	f8205	=	t	;
// 	double	f8206	=	t	;
// 	double	f8207	=	t	;
// 	double	f8208	=	t	;
// 	double	f8209	=	t	;
// 	double	f8210	=	t	;
// 	double	f8211	=	t	;
// 	double	f8212	=	t	;
// 	double	f8213	=	t	;
// 	double	f8214	=	t	;
// 	double	f8215	=	t	;
// 	double	f8216	=	t	;
// 	double	f8217	=	t	;
// 	double	f8218	=	t	;
// 	double	f8219	=	t	;
// 	double	f8220	=	t	;
// 	double	f8221	=	t	;
// 	double	f8222	=	t	;
// 	double	f8223	=	t	;
// 	double	f8224	=	t	;
// 	double	f8225	=	t	;
// 	double	f8226	=	t	;
// 	double	f8227	=	t	;
// 	double	f8228	=	t	;
// 	double	f8229	=	t	;
// 	double	f8230	=	t	;
// 	double	f8231	=	t	;
// 	double	f8232	=	t	;
// 	double	f8233	=	t	;
// 	double	f8234	=	t	;
// 	double	f8235	=	t	;
// 	double	f8236	=	t	;
// 	double	f8237	=	t	;
// 	double	f8238	=	t	;
// 	double	f8239	=	t	;
// 	double	f8240	=	t	;
// 	double	f8241	=	t	;
// 	double	f8242	=	t	;
// 	double	f8243	=	t	;
// 	double	f8244	=	t	;
// 	double	f8245	=	t	;
// 	double	f8246	=	t	;
// 	double	f8247	=	t	;
// 	double	f8248	=	t	;
// 	double	f8249	=	t	;
// 	double	f8250	=	t	;
// 	double	f8251	=	t	;
// 	double	f8252	=	t	;
// 	double	f8253	=	t	;
// 	double	f8254	=	t	;
// 	double	f8255	=	t	;
// 	double	f8256	=	t	;
// 	double	f8257	=	t	;
// 	double	f8258	=	t	;
// 	double	f8259	=	t	;
// 	double	f8260	=	t	;
// 	double	f8261	=	t	;
// 	double	f8262	=	t	;
// 	double	f8263	=	t	;
// 	double	f8264	=	t	;
// 	double	f8265	=	t	;
// 	double	f8266	=	t	;
// 	double	f8267	=	t	;
// 	double	f8268	=	t	;
// 	double	f8269	=	t	;
// 	double	f8270	=	t	;
// 	double	f8271	=	t	;
// 	double	f8272	=	t	;
// 	double	f8273	=	t	;
// 	double	f8274	=	t	;
// 	double	f8275	=	t	;
// 	double	f8276	=	t	;
// 	double	f8277	=	t	;
// 	double	f8278	=	t	;
// 	double	f8279	=	t	;
// 	double	f8280	=	t	;
// 	double	f8281	=	t	;
// 	double	f8282	=	t	;
// 	double	f8283	=	t	;
// 	double	f8284	=	t	;
// 	double	f8285	=	t	;
// 	double	f8286	=	t	;
// 	double	f8287	=	t	;
// 	double	f8288	=	t	;
// 	double	f8289	=	t	;
// 	double	f8290	=	t	;
// 	double	f8291	=	t	;
// 	double	f8292	=	t	;
// 	double	f8293	=	t	;
// 	double	f8294	=	t	;
// 	double	f8295	=	t	;
// 	double	f8296	=	t	;
// 	double	f8297	=	t	;
// 	double	f8298	=	t	;
// 	double	f8299	=	t	;
// 	double	f8300	=	t	;
// 	double	f8301	=	t	;
// 	double	f8302	=	t	;
// 	double	f8303	=	t	;
// 	double	f8304	=	t	;
// 	double	f8305	=	t	;
// 	double	f8306	=	t	;
// 	double	f8307	=	t	;
// 	double	f8308	=	t	;
// 	double	f8309	=	t	;
// 	double	f8310	=	t	;
// 	double	f8311	=	t	;
// 	double	f8312	=	t	;
// 	double	f8313	=	t	;
// 	double	f8314	=	t	;
// 	double	f8315	=	t	;
// 	double	f8316	=	t	;
// 	double	f8317	=	t	;
// 	double	f8318	=	t	;
// 	double	f8319	=	t	;
// 	double	f8320	=	t	;
// 	double	f8321	=	t	;
// 	double	f8322	=	t	;
// 	double	f8323	=	t	;
// 	double	f8324	=	t	;
// 	double	f8325	=	t	;
// 	double	f8326	=	t	;
// 	double	f8327	=	t	;
// 	double	f8328	=	t	;
// 	double	f8329	=	t	;
// 	double	f8330	=	t	;
// 	double	f8331	=	t	;
// 	double	f8332	=	t	;
// 	double	f8333	=	t	;
// 	double	f8334	=	t	;
// 	double	f8335	=	t	;
// 	double	f8336	=	t	;
// 	double	f8337	=	t	;
// 	double	f8338	=	t	;
// 	double	f8339	=	t	;
// 	double	f8340	=	t	;
// 	double	f8341	=	t	;
// 	double	f8342	=	t	;
// 	double	f8343	=	t	;
// 	double	f8344	=	t	;
// 	double	f8345	=	t	;
// 	double	f8346	=	t	;
// 	double	f8347	=	t	;
// 	double	f8348	=	t	;
// 	double	f8349	=	t	;
// 	double	f8350	=	t	;
// 	double	f8351	=	t	;
// 	double	f8352	=	t	;
// 	double	f8353	=	t	;
// 	double	f8354	=	t	;
// 	double	f8355	=	t	;
// 	double	f8356	=	t	;
// 	double	f8357	=	t	;
// 	double	f8358	=	t	;
// 	double	f8359	=	t	;
// 	double	f8360	=	t	;
// 	double	f8361	=	t	;
// 	double	f8362	=	t	;
// 	double	f8363	=	t	;
// 	double	f8364	=	t	;
// 	double	f8365	=	t	;
// 	double	f8366	=	t	;
// 	double	f8367	=	t	;
// 	double	f8368	=	t	;
// 	double	f8369	=	t	;
// 	double	f8370	=	t	;
// 	double	f8371	=	t	;
// 	double	f8372	=	t	;
// 	double	f8373	=	t	;
// 	double	f8374	=	t	;
// 	double	f8375	=	t	;
// 	double	f8376	=	t	;
// 	double	f8377	=	t	;
// 	double	f8378	=	t	;
// 	double	f8379	=	t	;
// 	double	f8380	=	t	;
// 	double	f8381	=	t	;
// 	double	f8382	=	t	;
// 	double	f8383	=	t	;
// 	double	f8384	=	t	;
// 	double	f8385	=	t	;
// 	double	f8386	=	t	;
// 	double	f8387	=	t	;
// 	double	f8388	=	t	;
// 	double	f8389	=	t	;
// 	double	f8390	=	t	;
// 	double	f8391	=	t	;
// 	double	f8392	=	t	;
// 	double	f8393	=	t	;
// 	double	f8394	=	t	;
// 	double	f8395	=	t	;
// 	double	f8396	=	t	;
// 	double	f8397	=	t	;
// 	double	f8398	=	t	;
// 	double	f8399	=	t	;
// 	double	f8400	=	t	;
// 	double	f8401	=	t	;
// 	double	f8402	=	t	;
// 	double	f8403	=	t	;
// 	double	f8404	=	t	;
// 	double	f8405	=	t	;
// 	double	f8406	=	t	;
// 	double	f8407	=	t	;
// 	double	f8408	=	t	;
// 	double	f8409	=	t	;
// 	double	f8410	=	t	;
// 	double	f8411	=	t	;
// 	double	f8412	=	t	;
// 	double	f8413	=	t	;
// 	double	f8414	=	t	;
// 	double	f8415	=	t	;
// 	double	f8416	=	t	;
// 	double	f8417	=	t	;
// 	double	f8418	=	t	;
// 	double	f8419	=	t	;
// 	double	f8420	=	t	;
// 	double	f8421	=	t	;
// 	double	f8422	=	t	;
// 	double	f8423	=	t	;
// 	double	f8424	=	t	;
// 	double	f8425	=	t	;
// 	double	f8426	=	t	;
// 	double	f8427	=	t	;
// 	double	f8428	=	t	;
// 	double	f8429	=	t	;
// 	double	f8430	=	t	;
// 	double	f8431	=	t	;
// 	double	f8432	=	t	;
// 	double	f8433	=	t	;
// 	double	f8434	=	t	;
// 	double	f8435	=	t	;
// 	double	f8436	=	t	;
// 	double	f8437	=	t	;
// 	double	f8438	=	t	;
// 	double	f8439	=	t	;
// 	double	f8440	=	t	;
// 	double	f8441	=	t	;
// 	double	f8442	=	t	;
// 	double	f8443	=	t	;
// 	double	f8444	=	t	;
// 	double	f8445	=	t	;
// 	double	f8446	=	t	;
// 	double	f8447	=	t	;
// 	double	f8448	=	t	;
// 	double	f8449	=	t	;
// 	double	f8450	=	t	;
// 	double	f8451	=	t	;
// 	double	f8452	=	t	;
// 	double	f8453	=	t	;
// 	double	f8454	=	t	;
// 	double	f8455	=	t	;
// 	double	f8456	=	t	;
// 	double	f8457	=	t	;
// 	double	f8458	=	t	;
// 	double	f8459	=	t	;
// 	double	f8460	=	t	;
// 	double	f8461	=	t	;
// 	double	f8462	=	t	;
// 	double	f8463	=	t	;
// 	double	f8464	=	t	;
// 	double	f8465	=	t	;
// 	double	f8466	=	t	;
// 	double	f8467	=	t	;
// 	double	f8468	=	t	;
// 	double	f8469	=	t	;
// 	double	f8470	=	t	;
// 	double	f8471	=	t	;
// 	double	f8472	=	t	;
// 	double	f8473	=	t	;
// 	double	f8474	=	t	;
// 	double	f8475	=	t	;
// 	double	f8476	=	t	;
// 	double	f8477	=	t	;
// 	double	f8478	=	t	;
// 	double	f8479	=	t	;
// 	double	f8480	=	t	;
// 	double	f8481	=	t	;
// 	double	f8482	=	t	;
// 	double	f8483	=	t	;
// 	double	f8484	=	t	;
// 	double	f8485	=	t	;
// 	double	f8486	=	t	;
// 	double	f8487	=	t	;
// 	double	f8488	=	t	;
// 	double	f8489	=	t	;
// 	double	f8490	=	t	;
// 	double	f8491	=	t	;
// 	double	f8492	=	t	;
// 	double	f8493	=	t	;
// 	double	f8494	=	t	;
// 	double	f8495	=	t	;
// 	double	f8496	=	t	;
// 	double	f8497	=	t	;
// 	double	f8498	=	t	;
// 	double	f8499	=	t	;
// 	double	f8500	=	t	;
// 	double	f8501	=	t	;
// 	double	f8502	=	t	;
// 	double	f8503	=	t	;
// 	double	f8504	=	t	;
// 	double	f8505	=	t	;
// 	double	f8506	=	t	;
// 	double	f8507	=	t	;
// 	double	f8508	=	t	;
// 	double	f8509	=	t	;
// 	double	f8510	=	t	;
// 	double	f8511	=	t	;
// 	double	f8512	=	t	;
// 	double	f8513	=	t	;
// 	double	f8514	=	t	;
// 	double	f8515	=	t	;
// 	double	f8516	=	t	;
// 	double	f8517	=	t	;
// 	double	f8518	=	t	;
// 	double	f8519	=	t	;
// 	double	f8520	=	t	;
// 	double	f8521	=	t	;
// 	double	f8522	=	t	;
// 	double	f8523	=	t	;
// 	double	f8524	=	t	;
// 	double	f8525	=	t	;
// 	double	f8526	=	t	;
// 	double	f8527	=	t	;
// 	double	f8528	=	t	;
// 	double	f8529	=	t	;
// 	double	f8530	=	t	;
// 	double	f8531	=	t	;
// 	double	f8532	=	t	;
// 	double	f8533	=	t	;
// 	double	f8534	=	t	;
// 	double	f8535	=	t	;
// 	double	f8536	=	t	;
// 	double	f8537	=	t	;
// 	double	f8538	=	t	;
// 	double	f8539	=	t	;
// 	double	f8540	=	t	;
// 	double	f8541	=	t	;
// 	double	f8542	=	t	;
// 	double	f8543	=	t	;
// 	double	f8544	=	t	;
// 	double	f8545	=	t	;
// 	double	f8546	=	t	;
// 	double	f8547	=	t	;
// 	double	f8548	=	t	;
// 	double	f8549	=	t	;
// 	double	f8550	=	t	;
// 	double	f8551	=	t	;
// 	double	f8552	=	t	;
// 	double	f8553	=	t	;
// 	double	f8554	=	t	;
// 	double	f8555	=	t	;
// 	double	f8556	=	t	;
// 	double	f8557	=	t	;
// 	double	f8558	=	t	;
// 	double	f8559	=	t	;
// 	double	f8560	=	t	;
// 	double	f8561	=	t	;
// 	double	f8562	=	t	;
// 	double	f8563	=	t	;
// 	double	f8564	=	t	;
// 	double	f8565	=	t	;
// 	double	f8566	=	t	;
// 	double	f8567	=	t	;
// 	double	f8568	=	t	;
// 	double	f8569	=	t	;
// 	double	f8570	=	t	;
// 	double	f8571	=	t	;
// 	double	f8572	=	t	;
// 	double	f8573	=	t	;
// 	double	f8574	=	t	;
// 	double	f8575	=	t	;
// 	double	f8576	=	t	;
// 	double	f8577	=	t	;
// 	double	f8578	=	t	;
// 	double	f8579	=	t	;
// 	double	f8580	=	t	;
// 	double	f8581	=	t	;
// 	double	f8582	=	t	;
// 	double	f8583	=	t	;
// 	double	f8584	=	t	;
// 	double	f8585	=	t	;
// 	double	f8586	=	t	;
// 	double	f8587	=	t	;
// 	double	f8588	=	t	;
// 	double	f8589	=	t	;
// 	double	f8590	=	t	;
// 	double	f8591	=	t	;
// 	double	f8592	=	t	;
// 	double	f8593	=	t	;
// 	double	f8594	=	t	;
// 	double	f8595	=	t	;
// 	double	f8596	=	t	;
// 	double	f8597	=	t	;
// 	double	f8598	=	t	;
// 	double	f8599	=	t	;
// 	double	f8600	=	t	;
// 	double	f8601	=	t	;
// 	double	f8602	=	t	;
// 	double	f8603	=	t	;
// 	double	f8604	=	t	;
// 	double	f8605	=	t	;
// 	double	f8606	=	t	;
// 	double	f8607	=	t	;
// 	double	f8608	=	t	;
// 	double	f8609	=	t	;
// 	double	f8610	=	t	;
// 	double	f8611	=	t	;
// 	double	f8612	=	t	;
// 	double	f8613	=	t	;
// 	double	f8614	=	t	;
// 	double	f8615	=	t	;
// 	double	f8616	=	t	;
// 	double	f8617	=	t	;
// 	double	f8618	=	t	;
// 	double	f8619	=	t	;
// 	double	f8620	=	t	;
// 	double	f8621	=	t	;
// 	double	f8622	=	t	;
// 	double	f8623	=	t	;
// 	double	f8624	=	t	;
// 	double	f8625	=	t	;
// 	double	f8626	=	t	;
// 	double	f8627	=	t	;
// 	double	f8628	=	t	;
// 	double	f8629	=	t	;
// 	double	f8630	=	t	;
// 	double	f8631	=	t	;
// 	double	f8632	=	t	;
// 	double	f8633	=	t	;
// 	double	f8634	=	t	;
// 	double	f8635	=	t	;
// 	double	f8636	=	t	;
// 	double	f8637	=	t	;
// 	double	f8638	=	t	;
// 	double	f8639	=	t	;
// 	double	f8640	=	t	;
// 	double	f8641	=	t	;
// 	double	f8642	=	t	;
// 	double	f8643	=	t	;
// 	double	f8644	=	t	;
// 	double	f8645	=	t	;
// 	double	f8646	=	t	;
// 	double	f8647	=	t	;
// 	double	f8648	=	t	;
// 	double	f8649	=	t	;
// 	double	f8650	=	t	;
// 	double	f8651	=	t	;
// 	double	f8652	=	t	;
// 	double	f8653	=	t	;
// 	double	f8654	=	t	;
// 	double	f8655	=	t	;
// 	double	f8656	=	t	;
// 	double	f8657	=	t	;
// 	double	f8658	=	t	;
// 	double	f8659	=	t	;
// 	double	f8660	=	t	;
// 	double	f8661	=	t	;
// 	double	f8662	=	t	;
// 	double	f8663	=	t	;
// 	double	f8664	=	t	;
// 	double	f8665	=	t	;
// 	double	f8666	=	t	;
// 	double	f8667	=	t	;
// 	double	f8668	=	t	;
// 	double	f8669	=	t	;
// 	double	f8670	=	t	;
// 	double	f8671	=	t	;
// 	double	f8672	=	t	;
// 	double	f8673	=	t	;
// 	double	f8674	=	t	;
// 	double	f8675	=	t	;
// 	double	f8676	=	t	;
// 	double	f8677	=	t	;
// 	double	f8678	=	t	;
// 	double	f8679	=	t	;
// 	double	f8680	=	t	;
// 	double	f8681	=	t	;
// 	double	f8682	=	t	;
// 	double	f8683	=	t	;
// 	double	f8684	=	t	;
// 	double	f8685	=	t	;
// 	double	f8686	=	t	;
// 	double	f8687	=	t	;
// 	double	f8688	=	t	;
// 	double	f8689	=	t	;
// 	double	f8690	=	t	;
// 	double	f8691	=	t	;
// 	double	f8692	=	t	;
// 	double	f8693	=	t	;
// 	double	f8694	=	t	;
// 	double	f8695	=	t	;
// 	double	f8696	=	t	;
// 	double	f8697	=	t	;
// 	double	f8698	=	t	;
// 	double	f8699	=	t	;
// 	double	f8700	=	t	;
// 	double	f8701	=	t	;
// 	double	f8702	=	t	;
// 	double	f8703	=	t	;
// 	double	f8704	=	t	;
// 	double	f8705	=	t	;
// 	double	f8706	=	t	;
// 	double	f8707	=	t	;
// 	double	f8708	=	t	;
// 	double	f8709	=	t	;
// 	double	f8710	=	t	;
// 	double	f8711	=	t	;
// 	double	f8712	=	t	;
// 	double	f8713	=	t	;
// 	double	f8714	=	t	;
// 	double	f8715	=	t	;
// 	double	f8716	=	t	;
// 	double	f8717	=	t	;
// 	double	f8718	=	t	;
// 	double	f8719	=	t	;
// 	double	f8720	=	t	;
// 	double	f8721	=	t	;
// 	double	f8722	=	t	;
// 	double	f8723	=	t	;
// 	double	f8724	=	t	;
// 	double	f8725	=	t	;
// 	double	f8726	=	t	;
// 	double	f8727	=	t	;
// 	double	f8728	=	t	;
// 	double	f8729	=	t	;
// 	double	f8730	=	t	;
// 	double	f8731	=	t	;
// 	double	f8732	=	t	;
// 	double	f8733	=	t	;
// 	double	f8734	=	t	;
// 	double	f8735	=	t	;
// 	double	f8736	=	t	;
// 	double	f8737	=	t	;
// 	double	f8738	=	t	;
// 	double	f8739	=	t	;
// 	double	f8740	=	t	;
// 	double	f8741	=	t	;
// 	double	f8742	=	t	;
// 	double	f8743	=	t	;
// 	double	f8744	=	t	;
// 	double	f8745	=	t	;
// 	double	f8746	=	t	;
// 	double	f8747	=	t	;
// 	double	f8748	=	t	;
// 	double	f8749	=	t	;
// 	double	f8750	=	t	;
// 	double	f8751	=	t	;
// 	double	f8752	=	t	;
// 	double	f8753	=	t	;
// 	double	f8754	=	t	;
// 	double	f8755	=	t	;
// 	double	f8756	=	t	;
// 	double	f8757	=	t	;
// 	double	f8758	=	t	;
// 	double	f8759	=	t	;
// 	double	f8760	=	t	;
// 	double	f8761	=	t	;
// 	double	f8762	=	t	;
// 	double	f8763	=	t	;
// 	double	f8764	=	t	;
// 	double	f8765	=	t	;
// 	double	f8766	=	t	;
// 	double	f8767	=	t	;
// 	double	f8768	=	t	;
// 	double	f8769	=	t	;
// 	double	f8770	=	t	;
// 	double	f8771	=	t	;
// 	double	f8772	=	t	;
// 	double	f8773	=	t	;
// 	double	f8774	=	t	;
// 	double	f8775	=	t	;
// 	double	f8776	=	t	;
// 	double	f8777	=	t	;
// 	double	f8778	=	t	;
// 	double	f8779	=	t	;
// 	double	f8780	=	t	;
// 	double	f8781	=	t	;
// 	double	f8782	=	t	;
// 	double	f8783	=	t	;
// 	double	f8784	=	t	;
// 	double	f8785	=	t	;
// 	double	f8786	=	t	;
// 	double	f8787	=	t	;
// 	double	f8788	=	t	;
// 	double	f8789	=	t	;
// 	double	f8790	=	t	;
// 	double	f8791	=	t	;
// 	double	f8792	=	t	;
// 	double	f8793	=	t	;
// 	double	f8794	=	t	;
// 	double	f8795	=	t	;
// 	double	f8796	=	t	;
// 	double	f8797	=	t	;
// 	double	f8798	=	t	;
// 	double	f8799	=	t	;
// 	double	f8800	=	t	;
// 	double	f8801	=	t	;
// 	double	f8802	=	t	;
// 	double	f8803	=	t	;
// 	double	f8804	=	t	;
// 	double	f8805	=	t	;
// 	double	f8806	=	t	;
// 	double	f8807	=	t	;
// 	double	f8808	=	t	;
// 	double	f8809	=	t	;
// 	double	f8810	=	t	;
// 	double	f8811	=	t	;
// 	double	f8812	=	t	;
// 	double	f8813	=	t	;
// 	double	f8814	=	t	;
// 	double	f8815	=	t	;
// 	double	f8816	=	t	;
// 	double	f8817	=	t	;
// 	double	f8818	=	t	;
// 	double	f8819	=	t	;
// 	double	f8820	=	t	;
// 	double	f8821	=	t	;
// 	double	f8822	=	t	;
// 	double	f8823	=	t	;
// 	double	f8824	=	t	;
// 	double	f8825	=	t	;
// 	double	f8826	=	t	;
// 	double	f8827	=	t	;
// 	double	f8828	=	t	;
// 	double	f8829	=	t	;
// 	double	f8830	=	t	;
// 	double	f8831	=	t	;
// 	double	f8832	=	t	;
// 	double	f8833	=	t	;
// 	double	f8834	=	t	;
// 	double	f8835	=	t	;
// 	double	f8836	=	t	;
// 	double	f8837	=	t	;
// 	double	f8838	=	t	;
// 	double	f8839	=	t	;
// 	double	f8840	=	t	;
// 	double	f8841	=	t	;
// 	double	f8842	=	t	;
// 	double	f8843	=	t	;
// 	double	f8844	=	t	;
// 	double	f8845	=	t	;
// 	double	f8846	=	t	;
// 	double	f8847	=	t	;
// 	double	f8848	=	t	;
// 	double	f8849	=	t	;
// 	double	f8850	=	t	;
// 	double	f8851	=	t	;
// 	double	f8852	=	t	;
// 	double	f8853	=	t	;
// 	double	f8854	=	t	;
// 	double	f8855	=	t	;
// 	double	f8856	=	t	;
// 	double	f8857	=	t	;
// 	double	f8858	=	t	;
// 	double	f8859	=	t	;
// 	double	f8860	=	t	;
// 	double	f8861	=	t	;
// 	double	f8862	=	t	;
// 	double	f8863	=	t	;
// 	double	f8864	=	t	;
// 	double	f8865	=	t	;
// 	double	f8866	=	t	;
// 	double	f8867	=	t	;
// 	double	f8868	=	t	;
// 	double	f8869	=	t	;
// 	double	f8870	=	t	;
// 	double	f8871	=	t	;
// 	double	f8872	=	t	;
// 	double	f8873	=	t	;
// 	double	f8874	=	t	;
// 	double	f8875	=	t	;
// 	double	f8876	=	t	;
// 	double	f8877	=	t	;
// 	double	f8878	=	t	;
// 	double	f8879	=	t	;
// 	double	f8880	=	t	;
// 	double	f8881	=	t	;
// 	double	f8882	=	t	;
// 	double	f8883	=	t	;
// 	double	f8884	=	t	;
// 	double	f8885	=	t	;
// 	double	f8886	=	t	;
// 	double	f8887	=	t	;
// 	double	f8888	=	t	;
// 	double	f8889	=	t	;
// 	double	f8890	=	t	;
// 	double	f8891	=	t	;
// 	double	f8892	=	t	;
// 	double	f8893	=	t	;
// 	double	f8894	=	t	;
// 	double	f8895	=	t	;
// 	double	f8896	=	t	;
// 	double	f8897	=	t	;
// 	double	f8898	=	t	;
// 	double	f8899	=	t	;
// 	double	f8900	=	t	;
// 	double	f8901	=	t	;
// 	double	f8902	=	t	;
// 	double	f8903	=	t	;
// 	double	f8904	=	t	;
// 	double	f8905	=	t	;
// 	double	f8906	=	t	;
// 	double	f8907	=	t	;
// 	double	f8908	=	t	;
// 	double	f8909	=	t	;
// 	double	f8910	=	t	;
// 	double	f8911	=	t	;
// 	double	f8912	=	t	;
// 	double	f8913	=	t	;
// 	double	f8914	=	t	;
// 	double	f8915	=	t	;
// 	double	f8916	=	t	;
// 	double	f8917	=	t	;
// 	double	f8918	=	t	;
// 	double	f8919	=	t	;
// 	double	f8920	=	t	;
// 	double	f8921	=	t	;
// 	double	f8922	=	t	;
// 	double	f8923	=	t	;
// 	double	f8924	=	t	;
// 	double	f8925	=	t	;
// 	double	f8926	=	t	;
// 	double	f8927	=	t	;
// 	double	f8928	=	t	;
// 	double	f8929	=	t	;
// 	double	f8930	=	t	;
// 	double	f8931	=	t	;
// 	double	f8932	=	t	;
// 	double	f8933	=	t	;
// 	double	f8934	=	t	;
// 	double	f8935	=	t	;
// 	double	f8936	=	t	;
// 	double	f8937	=	t	;
// 	double	f8938	=	t	;
// 	double	f8939	=	t	;
// 	double	f8940	=	t	;
// 	double	f8941	=	t	;
// 	double	f8942	=	t	;
// 	double	f8943	=	t	;
// 	double	f8944	=	t	;
// 	double	f8945	=	t	;
// 	double	f8946	=	t	;
// 	double	f8947	=	t	;
// 	double	f8948	=	t	;
// 	double	f8949	=	t	;
// 	double	f8950	=	t	;
// 	double	f8951	=	t	;
// 	double	f8952	=	t	;
// 	double	f8953	=	t	;
// 	double	f8954	=	t	;
// 	double	f8955	=	t	;
// 	double	f8956	=	t	;
// 	double	f8957	=	t	;
// 	double	f8958	=	t	;
// 	double	f8959	=	t	;
// 	double	f8960	=	t	;
// 	double	f8961	=	t	;
// 	double	f8962	=	t	;
// 	double	f8963	=	t	;
// 	double	f8964	=	t	;
// 	double	f8965	=	t	;
// 	double	f8966	=	t	;
// 	double	f8967	=	t	;
// 	double	f8968	=	t	;
// 	double	f8969	=	t	;
// 	double	f8970	=	t	;
// 	double	f8971	=	t	;
// 	double	f8972	=	t	;
// 	double	f8973	=	t	;
// 	double	f8974	=	t	;
// 	double	f8975	=	t	;
// 	double	f8976	=	t	;
// 	double	f8977	=	t	;
// 	double	f8978	=	t	;
// 	double	f8979	=	t	;
// 	double	f8980	=	t	;
// 	double	f8981	=	t	;
// 	double	f8982	=	t	;
// 	double	f8983	=	t	;
// 	double	f8984	=	t	;
// 	double	f8985	=	t	;
// 	double	f8986	=	t	;
// 	double	f8987	=	t	;
// 	double	f8988	=	t	;
// 	double	f8989	=	t	;
// 	double	f8990	=	t	;
// 	double	f8991	=	t	;
// 	double	f8992	=	t	;
// 	double	f8993	=	t	;
// 	double	f8994	=	t	;
// 	double	f8995	=	t	;
// 	double	f8996	=	t	;
// 	double	f8997	=	t	;
// 	double	f8998	=	t	;
// 	double	f8999	=	t	;
// 	double	f9000	=	t	;
// 	double	f9001	=	t	;
// 	double	f9002	=	t	;
// 	double	f9003	=	t	;
// 	double	f9004	=	t	;
// 	double	f9005	=	t	;
// 	double	f9006	=	t	;
// 	double	f9007	=	t	;
// 	double	f9008	=	t	;
// 	double	f9009	=	t	;
// 	double	f9010	=	t	;
// 	double	f9011	=	t	;
// 	double	f9012	=	t	;
// 	double	f9013	=	t	;
// 	double	f9014	=	t	;
// 	double	f9015	=	t	;
// 	double	f9016	=	t	;
// 	double	f9017	=	t	;
// 	double	f9018	=	t	;
// 	double	f9019	=	t	;
// 	double	f9020	=	t	;
// 	double	f9021	=	t	;
// 	double	f9022	=	t	;
// 	double	f9023	=	t	;
// 	double	f9024	=	t	;
// 	double	f9025	=	t	;
// 	double	f9026	=	t	;
// 	double	f9027	=	t	;
// 	double	f9028	=	t	;
// 	double	f9029	=	t	;
// 	double	f9030	=	t	;
// 	double	f9031	=	t	;
// 	double	f9032	=	t	;
// 	double	f9033	=	t	;
// 	double	f9034	=	t	;
// 	double	f9035	=	t	;
// 	double	f9036	=	t	;
// 	double	f9037	=	t	;
// 	double	f9038	=	t	;
// 	double	f9039	=	t	;
// 	double	f9040	=	t	;
// 	double	f9041	=	t	;
// 	double	f9042	=	t	;
// 	double	f9043	=	t	;
// 	double	f9044	=	t	;
// 	double	f9045	=	t	;
// 	double	f9046	=	t	;
// 	double	f9047	=	t	;
// 	double	f9048	=	t	;
// 	double	f9049	=	t	;
// 	double	f9050	=	t	;
// 	double	f9051	=	t	;
// 	double	f9052	=	t	;
// 	double	f9053	=	t	;
// 	double	f9054	=	t	;
// 	double	f9055	=	t	;
// 	double	f9056	=	t	;
// 	double	f9057	=	t	;
// 	double	f9058	=	t	;
// 	double	f9059	=	t	;
// 	double	f9060	=	t	;
// 	double	f9061	=	t	;
// 	double	f9062	=	t	;
// 	double	f9063	=	t	;
// 	double	f9064	=	t	;
// 	double	f9065	=	t	;
// 	double	f9066	=	t	;
// 	double	f9067	=	t	;
// 	double	f9068	=	t	;
// 	double	f9069	=	t	;
// 	double	f9070	=	t	;
// 	double	f9071	=	t	;
// 	double	f9072	=	t	;
// 	double	f9073	=	t	;
// 	double	f9074	=	t	;
// 	double	f9075	=	t	;
// 	double	f9076	=	t	;
// 	double	f9077	=	t	;
// 	double	f9078	=	t	;
// 	double	f9079	=	t	;
// 	double	f9080	=	t	;
// 	double	f9081	=	t	;
// 	double	f9082	=	t	;
// 	double	f9083	=	t	;
// 	double	f9084	=	t	;
// 	double	f9085	=	t	;
// 	double	f9086	=	t	;
// 	double	f9087	=	t	;
// 	double	f9088	=	t	;
// 	double	f9089	=	t	;
// 	double	f9090	=	t	;
// 	double	f9091	=	t	;
// 	double	f9092	=	t	;
// 	double	f9093	=	t	;
// 	double	f9094	=	t	;
// 	double	f9095	=	t	;
// 	double	f9096	=	t	;
// 	double	f9097	=	t	;
// 	double	f9098	=	t	;
// 	double	f9099	=	t	;
// 	double	f9100	=	t	;
// 	double	f9101	=	t	;
// 	double	f9102	=	t	;
// 	double	f9103	=	t	;
// 	double	f9104	=	t	;
// 	double	f9105	=	t	;
// 	double	f9106	=	t	;
// 	double	f9107	=	t	;
// 	double	f9108	=	t	;
// 	double	f9109	=	t	;
// 	double	f9110	=	t	;
// 	double	f9111	=	t	;
// 	double	f9112	=	t	;
// 	double	f9113	=	t	;
// 	double	f9114	=	t	;
// 	double	f9115	=	t	;
// 	double	f9116	=	t	;
// 	double	f9117	=	t	;
// 	double	f9118	=	t	;
// 	double	f9119	=	t	;
// 	double	f9120	=	t	;
// 	double	f9121	=	t	;
// 	double	f9122	=	t	;
// 	double	f9123	=	t	;
// 	double	f9124	=	t	;
// 	double	f9125	=	t	;
// 	double	f9126	=	t	;
// 	double	f9127	=	t	;
// 	double	f9128	=	t	;
// 	double	f9129	=	t	;
// 	double	f9130	=	t	;
// 	double	f9131	=	t	;
// 	double	f9132	=	t	;
// 	double	f9133	=	t	;
// 	double	f9134	=	t	;
// 	double	f9135	=	t	;
// 	double	f9136	=	t	;
// 	double	f9137	=	t	;
// 	double	f9138	=	t	;
// 	double	f9139	=	t	;
// 	double	f9140	=	t	;
// 	double	f9141	=	t	;
// 	double	f9142	=	t	;
// 	double	f9143	=	t	;
// 	double	f9144	=	t	;
// 	double	f9145	=	t	;
// 	double	f9146	=	t	;
// 	double	f9147	=	t	;
// 	double	f9148	=	t	;
// 	double	f9149	=	t	;
// 	double	f9150	=	t	;
// 	double	f9151	=	t	;
// 	double	f9152	=	t	;
// 	double	f9153	=	t	;
// 	double	f9154	=	t	;
// 	double	f9155	=	t	;
// 	double	f9156	=	t	;
// 	double	f9157	=	t	;
// 	double	f9158	=	t	;
// 	double	f9159	=	t	;
// 	double	f9160	=	t	;
// 	double	f9161	=	t	;
// 	double	f9162	=	t	;
// 	double	f9163	=	t	;
// 	double	f9164	=	t	;
// 	double	f9165	=	t	;
// 	double	f9166	=	t	;
// 	double	f9167	=	t	;
// 	double	f9168	=	t	;
// 	double	f9169	=	t	;
// 	double	f9170	=	t	;
// 	double	f9171	=	t	;
// 	double	f9172	=	t	;
// 	double	f9173	=	t	;
// 	double	f9174	=	t	;
// 	double	f9175	=	t	;
// 	double	f9176	=	t	;
// 	double	f9177	=	t	;
// 	double	f9178	=	t	;
// 	double	f9179	=	t	;
// 	double	f9180	=	t	;
// 	double	f9181	=	t	;
// 	double	f9182	=	t	;
// 	double	f9183	=	t	;
// 	double	f9184	=	t	;
// 	double	f9185	=	t	;
// 	double	f9186	=	t	;
// 	double	f9187	=	t	;
// 	double	f9188	=	t	;
// 	double	f9189	=	t	;
// 	double	f9190	=	t	;
// 	double	f9191	=	t	;
// 	double	f9192	=	t	;
// 	double	f9193	=	t	;
// 	double	f9194	=	t	;
// 	double	f9195	=	t	;
// 	double	f9196	=	t	;
// 	double	f9197	=	t	;
// 	double	f9198	=	t	;
// 	double	f9199	=	t	;
// 	double	f9200	=	t	;
// 	double	f9201	=	t	;
// 	double	f9202	=	t	;
// 	double	f9203	=	t	;
// 	double	f9204	=	t	;
// 	double	f9205	=	t	;
// 	double	f9206	=	t	;
// 	double	f9207	=	t	;
// 	double	f9208	=	t	;
// 	double	f9209	=	t	;
// 	double	f9210	=	t	;
// 	double	f9211	=	t	;
// 	double	f9212	=	t	;
// 	double	f9213	=	t	;
// 	double	f9214	=	t	;
// 	double	f9215	=	t	;
// 	double	f9216	=	t	;
// 	double	f9217	=	t	;
// 	double	f9218	=	t	;
// 	double	f9219	=	t	;
// 	double	f9220	=	t	;
// 	double	f9221	=	t	;
// 	double	f9222	=	t	;
// 	double	f9223	=	t	;
// 	double	f9224	=	t	;
// 	double	f9225	=	t	;
// 	double	f9226	=	t	;
// 	double	f9227	=	t	;
// 	double	f9228	=	t	;
// 	double	f9229	=	t	;
// 	double	f9230	=	t	;
// 	double	f9231	=	t	;
// 	double	f9232	=	t	;
// 	double	f9233	=	t	;
// 	double	f9234	=	t	;
// 	double	f9235	=	t	;
// 	double	f9236	=	t	;
// 	double	f9237	=	t	;
// 	double	f9238	=	t	;
// 	double	f9239	=	t	;
// 	double	f9240	=	t	;
// 	double	f9241	=	t	;
// 	double	f9242	=	t	;
// 	double	f9243	=	t	;
// 	double	f9244	=	t	;
// 	double	f9245	=	t	;
// 	double	f9246	=	t	;
// 	double	f9247	=	t	;
// 	double	f9248	=	t	;
// 	double	f9249	=	t	;
// 	double	f9250	=	t	;
// 	double	f9251	=	t	;
// 	double	f9252	=	t	;
// 	double	f9253	=	t	;
// 	double	f9254	=	t	;
// 	double	f9255	=	t	;
// 	double	f9256	=	t	;
// 	double	f9257	=	t	;
// 	double	f9258	=	t	;
// 	double	f9259	=	t	;
// 	double	f9260	=	t	;
// 	double	f9261	=	t	;
// 	double	f9262	=	t	;
// 	double	f9263	=	t	;
// 	double	f9264	=	t	;
// 	double	f9265	=	t	;
// 	double	f9266	=	t	;
// 	double	f9267	=	t	;
// 	double	f9268	=	t	;
// 	double	f9269	=	t	;
// 	double	f9270	=	t	;
// 	double	f9271	=	t	;
// 	double	f9272	=	t	;
// 	double	f9273	=	t	;
// 	double	f9274	=	t	;
// 	double	f9275	=	t	;
// 	double	f9276	=	t	;
// 	double	f9277	=	t	;
// 	double	f9278	=	t	;
// 	double	f9279	=	t	;
// 	double	f9280	=	t	;
// 	double	f9281	=	t	;
// 	double	f9282	=	t	;
// 	double	f9283	=	t	;
// 	double	f9284	=	t	;
// 	double	f9285	=	t	;
// 	double	f9286	=	t	;
// 	double	f9287	=	t	;
// 	double	f9288	=	t	;
// 	double	f9289	=	t	;
// 	double	f9290	=	t	;
// 	double	f9291	=	t	;
// 	double	f9292	=	t	;
// 	double	f9293	=	t	;
// 	double	f9294	=	t	;
// 	double	f9295	=	t	;
// 	double	f9296	=	t	;
// 	double	f9297	=	t	;
// 	double	f9298	=	t	;
// 	double	f9299	=	t	;
// 	double	f9300	=	t	;
// 	double	f9301	=	t	;
// 	double	f9302	=	t	;
// 	double	f9303	=	t	;
// 	double	f9304	=	t	;
// 	double	f9305	=	t	;
// 	double	f9306	=	t	;
// 	double	f9307	=	t	;
// 	double	f9308	=	t	;
// 	double	f9309	=	t	;
// 	double	f9310	=	t	;
// 	double	f9311	=	t	;
// 	double	f9312	=	t	;
// 	double	f9313	=	t	;
// 	double	f9314	=	t	;
// 	double	f9315	=	t	;
// 	double	f9316	=	t	;
// 	double	f9317	=	t	;
// 	double	f9318	=	t	;
// 	double	f9319	=	t	;
// 	double	f9320	=	t	;
// 	double	f9321	=	t	;
// 	double	f9322	=	t	;
// 	double	f9323	=	t	;
// 	double	f9324	=	t	;
// 	double	f9325	=	t	;
// 	double	f9326	=	t	;
// 	double	f9327	=	t	;
// 	double	f9328	=	t	;
// 	double	f9329	=	t	;
// 	double	f9330	=	t	;
// 	double	f9331	=	t	;
// 	double	f9332	=	t	;
// 	double	f9333	=	t	;
// 	double	f9334	=	t	;
// 	double	f9335	=	t	;
// 	double	f9336	=	t	;
// 	double	f9337	=	t	;
// 	double	f9338	=	t	;
// 	double	f9339	=	t	;
// 	double	f9340	=	t	;
// 	double	f9341	=	t	;
// 	double	f9342	=	t	;
// 	double	f9343	=	t	;
// 	double	f9344	=	t	;
// 	double	f9345	=	t	;
// 	double	f9346	=	t	;
// 	double	f9347	=	t	;
// 	double	f9348	=	t	;
// 	double	f9349	=	t	;
// 	double	f9350	=	t	;
// 	double	f9351	=	t	;
// 	double	f9352	=	t	;
// 	double	f9353	=	t	;
// 	double	f9354	=	t	;
// 	double	f9355	=	t	;
// 	double	f9356	=	t	;
// 	double	f9357	=	t	;
// 	double	f9358	=	t	;
// 	double	f9359	=	t	;
// 	double	f9360	=	t	;
// 	double	f9361	=	t	;
// 	double	f9362	=	t	;
// 	double	f9363	=	t	;
// 	double	f9364	=	t	;
// 	double	f9365	=	t	;
// 	double	f9366	=	t	;
// 	double	f9367	=	t	;
// 	double	f9368	=	t	;
// 	double	f9369	=	t	;
// 	double	f9370	=	t	;
// 	double	f9371	=	t	;
// 	double	f9372	=	t	;
// 	double	f9373	=	t	;
// 	double	f9374	=	t	;
// 	double	f9375	=	t	;
// 	double	f9376	=	t	;
// 	double	f9377	=	t	;
// 	double	f9378	=	t	;
// 	double	f9379	=	t	;
// 	double	f9380	=	t	;
// 	double	f9381	=	t	;
// 	double	f9382	=	t	;
// 	double	f9383	=	t	;
// 	double	f9384	=	t	;
// 	double	f9385	=	t	;
// 	double	f9386	=	t	;
// 	double	f9387	=	t	;
// 	double	f9388	=	t	;
// 	double	f9389	=	t	;
// 	double	f9390	=	t	;
// 	double	f9391	=	t	;
// 	double	f9392	=	t	;
// 	double	f9393	=	t	;
// 	double	f9394	=	t	;
// 	double	f9395	=	t	;
// 	double	f9396	=	t	;
// 	double	f9397	=	t	;
// 	double	f9398	=	t	;
// 	double	f9399	=	t	;
// 	double	f9400	=	t	;
// 	double	f9401	=	t	;
// 	double	f9402	=	t	;
// 	double	f9403	=	t	;
// 	double	f9404	=	t	;
// 	double	f9405	=	t	;
// 	double	f9406	=	t	;
// 	double	f9407	=	t	;
// 	double	f9408	=	t	;
// 	double	f9409	=	t	;
// 	double	f9410	=	t	;
// 	double	f9411	=	t	;
// 	double	f9412	=	t	;
// 	double	f9413	=	t	;
// 	double	f9414	=	t	;
// 	double	f9415	=	t	;
// 	double	f9416	=	t	;
// 	double	f9417	=	t	;
// 	double	f9418	=	t	;
// 	double	f9419	=	t	;
// 	double	f9420	=	t	;
// 	double	f9421	=	t	;
// 	double	f9422	=	t	;
// 	double	f9423	=	t	;
// 	double	f9424	=	t	;
// 	double	f9425	=	t	;
// 	double	f9426	=	t	;
// 	double	f9427	=	t	;
// 	double	f9428	=	t	;
// 	double	f9429	=	t	;
// 	double	f9430	=	t	;
// 	double	f9431	=	t	;
// 	double	f9432	=	t	;
// 	double	f9433	=	t	;
// 	double	f9434	=	t	;
// 	double	f9435	=	t	;
// 	double	f9436	=	t	;
// 	double	f9437	=	t	;
// 	double	f9438	=	t	;
// 	double	f9439	=	t	;
// 	double	f9440	=	t	;
// 	double	f9441	=	t	;
// 	double	f9442	=	t	;
// 	double	f9443	=	t	;
// 	double	f9444	=	t	;
// 	double	f9445	=	t	;
// 	double	f9446	=	t	;
// 	double	f9447	=	t	;
// 	double	f9448	=	t	;
// 	double	f9449	=	t	;
// 	double	f9450	=	t	;
// 	double	f9451	=	t	;
// 	double	f9452	=	t	;
// 	double	f9453	=	t	;
// 	double	f9454	=	t	;
// 	double	f9455	=	t	;
// 	double	f9456	=	t	;
// 	double	f9457	=	t	;
// 	double	f9458	=	t	;
// 	double	f9459	=	t	;
// 	double	f9460	=	t	;
// 	double	f9461	=	t	;
// 	double	f9462	=	t	;
// 	double	f9463	=	t	;
// 	double	f9464	=	t	;
// 	double	f9465	=	t	;
// 	double	f9466	=	t	;
// 	double	f9467	=	t	;
// 	double	f9468	=	t	;
// 	double	f9469	=	t	;
// 	double	f9470	=	t	;
// 	double	f9471	=	t	;
// 	double	f9472	=	t	;
// 	double	f9473	=	t	;
// 	double	f9474	=	t	;
// 	double	f9475	=	t	;
// 	double	f9476	=	t	;
// 	double	f9477	=	t	;
// 	double	f9478	=	t	;
// 	double	f9479	=	t	;
// 	double	f9480	=	t	;
// 	double	f9481	=	t	;
// 	double	f9482	=	t	;
// 	double	f9483	=	t	;
// 	double	f9484	=	t	;
// 	double	f9485	=	t	;
// 	double	f9486	=	t	;
// 	double	f9487	=	t	;
// 	double	f9488	=	t	;
// 	double	f9489	=	t	;
// 	double	f9490	=	t	;
// 	double	f9491	=	t	;
// 	double	f9492	=	t	;
// 	double	f9493	=	t	;
// 	double	f9494	=	t	;
// 	double	f9495	=	t	;
// 	double	f9496	=	t	;
// 	double	f9497	=	t	;
// 	double	f9498	=	t	;
// 	double	f9499	=	t	;
// 	double	f9500	=	t	;
// 	double	f9501	=	t	;
// 	double	f9502	=	t	;
// 	double	f9503	=	t	;
// 	double	f9504	=	t	;
// 	double	f9505	=	t	;
// 	double	f9506	=	t	;
// 	double	f9507	=	t	;
// 	double	f9508	=	t	;
// 	double	f9509	=	t	;
// 	double	f9510	=	t	;
// 	double	f9511	=	t	;
// 	double	f9512	=	t	;
// 	double	f9513	=	t	;
// 	double	f9514	=	t	;
// 	double	f9515	=	t	;
// 	double	f9516	=	t	;
// 	double	f9517	=	t	;
// 	double	f9518	=	t	;
// 	double	f9519	=	t	;
// 	double	f9520	=	t	;
// 	double	f9521	=	t	;
// 	double	f9522	=	t	;
// 	double	f9523	=	t	;
// 	double	f9524	=	t	;
// 	double	f9525	=	t	;
// 	double	f9526	=	t	;
// 	double	f9527	=	t	;
// 	double	f9528	=	t	;
// 	double	f9529	=	t	;
// 	double	f9530	=	t	;
// 	double	f9531	=	t	;
// 	double	f9532	=	t	;
// 	double	f9533	=	t	;
// 	double	f9534	=	t	;
// 	double	f9535	=	t	;
// 	double	f9536	=	t	;
// 	double	f9537	=	t	;
// 	double	f9538	=	t	;
// 	double	f9539	=	t	;
// 	double	f9540	=	t	;
// 	double	f9541	=	t	;
// 	double	f9542	=	t	;
// 	double	f9543	=	t	;
// 	double	f9544	=	t	;
// 	double	f9545	=	t	;
// 	double	f9546	=	t	;
// 	double	f9547	=	t	;
// 	double	f9548	=	t	;
// 	double	f9549	=	t	;
// 	double	f9550	=	t	;
// 	double	f9551	=	t	;
// 	double	f9552	=	t	;
// 	double	f9553	=	t	;
// 	double	f9554	=	t	;
// 	double	f9555	=	t	;
// 	double	f9556	=	t	;
// 	double	f9557	=	t	;
// 	double	f9558	=	t	;
// 	double	f9559	=	t	;
// 	double	f9560	=	t	;
// 	double	f9561	=	t	;
// 	double	f9562	=	t	;
// 	double	f9563	=	t	;
// 	double	f9564	=	t	;
// 	double	f9565	=	t	;
// 	double	f9566	=	t	;
// 	double	f9567	=	t	;
// 	double	f9568	=	t	;
// 	double	f9569	=	t	;
// 	double	f9570	=	t	;
// 	double	f9571	=	t	;
// 	double	f9572	=	t	;
// 	double	f9573	=	t	;
// 	double	f9574	=	t	;
// 	double	f9575	=	t	;
// 	double	f9576	=	t	;
// 	double	f9577	=	t	;
// 	double	f9578	=	t	;
// 	double	f9579	=	t	;
// 	double	f9580	=	t	;
// 	double	f9581	=	t	;
// 	double	f9582	=	t	;
// 	double	f9583	=	t	;
// 	double	f9584	=	t	;
// 	double	f9585	=	t	;
// 	double	f9586	=	t	;
// 	double	f9587	=	t	;
// 	double	f9588	=	t	;
// 	double	f9589	=	t	;
// 	double	f9590	=	t	;
// 	double	f9591	=	t	;
// 	double	f9592	=	t	;
// 	double	f9593	=	t	;
// 	double	f9594	=	t	;
// 	double	f9595	=	t	;
// 	double	f9596	=	t	;
// 	double	f9597	=	t	;
// 	double	f9598	=	t	;
// 	double	f9599	=	t	;
// 	double	f9600	=	t	;
// 	double	f9601	=	t	;
// 	double	f9602	=	t	;
// 	double	f9603	=	t	;
// 	double	f9604	=	t	;
// 	double	f9605	=	t	;
// 	double	f9606	=	t	;
// 	double	f9607	=	t	;
// 	double	f9608	=	t	;
// 	double	f9609	=	t	;
// 	double	f9610	=	t	;
// 	double	f9611	=	t	;
// 	double	f9612	=	t	;
// 	double	f9613	=	t	;
// 	double	f9614	=	t	;
// 	double	f9615	=	t	;
// 	double	f9616	=	t	;
// 	double	f9617	=	t	;
// 	double	f9618	=	t	;
// 	double	f9619	=	t	;
// 	double	f9620	=	t	;
// 	double	f9621	=	t	;
// 	double	f9622	=	t	;
// 	double	f9623	=	t	;
// 	double	f9624	=	t	;
// 	double	f9625	=	t	;
// 	double	f9626	=	t	;
// 	double	f9627	=	t	;
// 	double	f9628	=	t	;
// 	double	f9629	=	t	;
// 	double	f9630	=	t	;
// 	double	f9631	=	t	;
// 	double	f9632	=	t	;
// 	double	f9633	=	t	;
// 	double	f9634	=	t	;
// 	double	f9635	=	t	;
// 	double	f9636	=	t	;
// 	double	f9637	=	t	;
// 	double	f9638	=	t	;
// 	double	f9639	=	t	;
// 	double	f9640	=	t	;
// 	double	f9641	=	t	;
// 	double	f9642	=	t	;
// 	double	f9643	=	t	;
// 	double	f9644	=	t	;
// 	double	f9645	=	t	;
// 	double	f9646	=	t	;
// 	double	f9647	=	t	;
// 	double	f9648	=	t	;
// 	double	f9649	=	t	;
// 	double	f9650	=	t	;
// 	double	f9651	=	t	;
// 	double	f9652	=	t	;
// 	double	f9653	=	t	;
// 	double	f9654	=	t	;
// 	double	f9655	=	t	;
// 	double	f9656	=	t	;
// 	double	f9657	=	t	;
// 	double	f9658	=	t	;
// 	double	f9659	=	t	;
// 	double	f9660	=	t	;
// 	double	f9661	=	t	;
// 	double	f9662	=	t	;
// 	double	f9663	=	t	;
// 	double	f9664	=	t	;
// 	double	f9665	=	t	;
// 	double	f9666	=	t	;
// 	double	f9667	=	t	;
// 	double	f9668	=	t	;
// 	double	f9669	=	t	;
// 	double	f9670	=	t	;
// 	double	f9671	=	t	;
// 	double	f9672	=	t	;
// 	double	f9673	=	t	;
// 	double	f9674	=	t	;
// 	double	f9675	=	t	;
// 	double	f9676	=	t	;
// 	double	f9677	=	t	;
// 	double	f9678	=	t	;
// 	double	f9679	=	t	;
// 	double	f9680	=	t	;
// 	double	f9681	=	t	;
// 	double	f9682	=	t	;
// 	double	f9683	=	t	;
// 	double	f9684	=	t	;
// 	double	f9685	=	t	;
// 	double	f9686	=	t	;
// 	double	f9687	=	t	;
// 	double	f9688	=	t	;
// 	double	f9689	=	t	;
// 	double	f9690	=	t	;
// 	double	f9691	=	t	;
// 	double	f9692	=	t	;
// 	double	f9693	=	t	;
// 	double	f9694	=	t	;
// 	double	f9695	=	t	;
// 	double	f9696	=	t	;
// 	double	f9697	=	t	;
// 	double	f9698	=	t	;
// 	double	f9699	=	t	;
// 	double	f9700	=	t	;
// 	double	f9701	=	t	;
// 	double	f9702	=	t	;
// 	double	f9703	=	t	;
// 	double	f9704	=	t	;
// 	double	f9705	=	t	;
// 	double	f9706	=	t	;
// 	double	f9707	=	t	;
// 	double	f9708	=	t	;
// 	double	f9709	=	t	;
// 	double	f9710	=	t	;
// 	double	f9711	=	t	;
// 	double	f9712	=	t	;
// 	double	f9713	=	t	;
// 	double	f9714	=	t	;
// 	double	f9715	=	t	;
// 	double	f9716	=	t	;
// 	double	f9717	=	t	;
// 	double	f9718	=	t	;
// 	double	f9719	=	t	;
// 	double	f9720	=	t	;
// 	double	f9721	=	t	;
// 	double	f9722	=	t	;
// 	double	f9723	=	t	;
// 	double	f9724	=	t	;
// 	double	f9725	=	t	;
// 	double	f9726	=	t	;
// 	double	f9727	=	t	;
// 	double	f9728	=	t	;
// 	double	f9729	=	t	;
// 	double	f9730	=	t	;
// 	double	f9731	=	t	;
// 	double	f9732	=	t	;
// 	double	f9733	=	t	;
// 	double	f9734	=	t	;
// 	double	f9735	=	t	;
// 	double	f9736	=	t	;
// 	double	f9737	=	t	;
// 	double	f9738	=	t	;
// 	double	f9739	=	t	;
// 	double	f9740	=	t	;
// 	double	f9741	=	t	;
// 	double	f9742	=	t	;
// 	double	f9743	=	t	;
// 	double	f9744	=	t	;
// 	double	f9745	=	t	;
// 	double	f9746	=	t	;
// 	double	f9747	=	t	;
// 	double	f9748	=	t	;
// 	double	f9749	=	t	;
// 	double	f9750	=	t	;
// 	double	f9751	=	t	;
// 	double	f9752	=	t	;
// 	double	f9753	=	t	;
// 	double	f9754	=	t	;
// 	double	f9755	=	t	;
// 	double	f9756	=	t	;
// 	double	f9757	=	t	;
// 	double	f9758	=	t	;
// 	double	f9759	=	t	;
// 	double	f9760	=	t	;
// 	double	f9761	=	t	;
// 	double	f9762	=	t	;
// 	double	f9763	=	t	;
// 	double	f9764	=	t	;
// 	double	f9765	=	t	;
// 	double	f9766	=	t	;
// 	double	f9767	=	t	;
// 	double	f9768	=	t	;
// 	double	f9769	=	t	;
// 	double	f9770	=	t	;
// 	double	f9771	=	t	;
// 	double	f9772	=	t	;
// 	double	f9773	=	t	;
// 	double	f9774	=	t	;
// 	double	f9775	=	t	;
// 	double	f9776	=	t	;
// 	double	f9777	=	t	;
// 	double	f9778	=	t	;
// 	double	f9779	=	t	;
// 	double	f9780	=	t	;
// 	double	f9781	=	t	;
// 	double	f9782	=	t	;
// 	double	f9783	=	t	;
// 	double	f9784	=	t	;
// 	double	f9785	=	t	;
// 	double	f9786	=	t	;
// 	double	f9787	=	t	;
// 	double	f9788	=	t	;
// 	double	f9789	=	t	;
// 	double	f9790	=	t	;
// 	double	f9791	=	t	;
// 	double	f9792	=	t	;
// 	double	f9793	=	t	;
// 	double	f9794	=	t	;
// 	double	f9795	=	t	;
// 	double	f9796	=	t	;
// 	double	f9797	=	t	;
// 	double	f9798	=	t	;
// 	double	f9799	=	t	;
// 	double	f9800	=	t	;
// 	double	f9801	=	t	;
// 	double	f9802	=	t	;
// 	double	f9803	=	t	;
// 	double	f9804	=	t	;
// 	double	f9805	=	t	;
// 	double	f9806	=	t	;
// 	double	f9807	=	t	;
// 	double	f9808	=	t	;
// 	double	f9809	=	t	;
// 	double	f9810	=	t	;
// 	double	f9811	=	t	;
// 	double	f9812	=	t	;
// 	double	f9813	=	t	;
// 	double	f9814	=	t	;
// 	double	f9815	=	t	;
// 	double	f9816	=	t	;
// 	double	f9817	=	t	;
// 	double	f9818	=	t	;
// 	double	f9819	=	t	;
// 	double	f9820	=	t	;
// 	double	f9821	=	t	;
// 	double	f9822	=	t	;
// 	double	f9823	=	t	;
// 	double	f9824	=	t	;
// 	double	f9825	=	t	;
// 	double	f9826	=	t	;
// 	double	f9827	=	t	;
// 	double	f9828	=	t	;
// 	double	f9829	=	t	;
// 	double	f9830	=	t	;
// 	double	f9831	=	t	;
// 	double	f9832	=	t	;
// 	double	f9833	=	t	;
// 	double	f9834	=	t	;
// 	double	f9835	=	t	;
// 	double	f9836	=	t	;
// 	double	f9837	=	t	;
// 	double	f9838	=	t	;
// 	double	f9839	=	t	;
// 	double	f9840	=	t	;
// 	double	f9841	=	t	;
// 	double	f9842	=	t	;
// 	double	f9843	=	t	;
// 	double	f9844	=	t	;
// 	double	f9845	=	t	;
// 	double	f9846	=	t	;
// 	double	f9847	=	t	;
// 	double	f9848	=	t	;
// 	double	f9849	=	t	;
// 	double	f9850	=	t	;
// 	double	f9851	=	t	;
// 	double	f9852	=	t	;
// 	double	f9853	=	t	;
// 	double	f9854	=	t	;
// 	double	f9855	=	t	;
// 	double	f9856	=	t	;
// 	double	f9857	=	t	;
// 	double	f9858	=	t	;
// 	double	f9859	=	t	;
// 	double	f9860	=	t	;
// 	double	f9861	=	t	;
// 	double	f9862	=	t	;
// 	double	f9863	=	t	;
// 	double	f9864	=	t	;
// 	double	f9865	=	t	;
// 	double	f9866	=	t	;
// 	double	f9867	=	t	;
// 	double	f9868	=	t	;
// 	double	f9869	=	t	;
// 	double	f9870	=	t	;
// 	double	f9871	=	t	;
// 	double	f9872	=	t	;
// 	double	f9873	=	t	;
// 	double	f9874	=	t	;
// 	double	f9875	=	t	;
// 	double	f9876	=	t	;
// 	double	f9877	=	t	;
// 	double	f9878	=	t	;
// 	double	f9879	=	t	;
// 	double	f9880	=	t	;
// 	double	f9881	=	t	;
// 	double	f9882	=	t	;
// 	double	f9883	=	t	;
// 	double	f9884	=	t	;
// 	double	f9885	=	t	;
// 	double	f9886	=	t	;
// 	double	f9887	=	t	;
// 	double	f9888	=	t	;
// 	double	f9889	=	t	;
// 	double	f9890	=	t	;
// 	double	f9891	=	t	;
// 	double	f9892	=	t	;
// 	double	f9893	=	t	;
// 	double	f9894	=	t	;
// 	double	f9895	=	t	;
// 	double	f9896	=	t	;
// 	double	f9897	=	t	;
// 	double	f9898	=	t	;
// 	double	f9899	=	t	;
// 	double	f9900	=	t	;
// 	double	f9901	=	t	;
// 	double	f9902	=	t	;
// 	double	f9903	=	t	;
// 	double	f9904	=	t	;
// 	double	f9905	=	t	;
// 	double	f9906	=	t	;
// 	double	f9907	=	t	;
// 	double	f9908	=	t	;
// 	double	f9909	=	t	;
// 	double	f9910	=	t	;
// 	double	f9911	=	t	;
// 	double	f9912	=	t	;
// 	double	f9913	=	t	;
// 	double	f9914	=	t	;
// 	double	f9915	=	t	;
// 	double	f9916	=	t	;
// 	double	f9917	=	t	;
// 	double	f9918	=	t	;
// 	double	f9919	=	t	;
// 	double	f9920	=	t	;
// 	double	f9921	=	t	;
// 	double	f9922	=	t	;
// 	double	f9923	=	t	;
// 	double	f9924	=	t	;
// 	double	f9925	=	t	;
// 	double	f9926	=	t	;
// 	double	f9927	=	t	;
// 	double	f9928	=	t	;
// 	double	f9929	=	t	;
// 	double	f9930	=	t	;
// 	double	f9931	=	t	;
// 	double	f9932	=	t	;
// 	double	f9933	=	t	;
// 	double	f9934	=	t	;
// 	double	f9935	=	t	;
// 	double	f9936	=	t	;
// 	double	f9937	=	t	;
// 	double	f9938	=	t	;
// 	double	f9939	=	t	;
// 	double	f9940	=	t	;
// 	double	f9941	=	t	;
// 	double	f9942	=	t	;
// 	double	f9943	=	t	;
// 	double	f9944	=	t	;
// 	double	f9945	=	t	;
// 	double	f9946	=	t	;
// 	double	f9947	=	t	;
// 	double	f9948	=	t	;
// 	double	f9949	=	t	;
// 	double	f9950	=	t	;
// 	double	f9951	=	t	;
// 	double	f9952	=	t	;
// 	double	f9953	=	t	;
// 	double	f9954	=	t	;
// 	double	f9955	=	t	;
// 	double	f9956	=	t	;
// 	double	f9957	=	t	;
// 	double	f9958	=	t	;
// 	double	f9959	=	t	;
// 	double	f9960	=	t	;
// 	double	f9961	=	t	;
// 	double	f9962	=	t	;
// 	double	f9963	=	t	;
// 	double	f9964	=	t	;
// 	double	f9965	=	t	;
// 	double	f9966	=	t	;
// 	double	f9967	=	t	;
// 	double	f9968	=	t	;
// 	double	f9969	=	t	;
// 	double	f9970	=	t	;
// 	double	f9971	=	t	;
// 	double	f9972	=	t	;
// 	double	f9973	=	t	;
// 	double	f9974	=	t	;
// 	double	f9975	=	t	;
// 	double	f9976	=	t	;
// 	double	f9977	=	t	;
// 	double	f9978	=	t	;
// 	double	f9979	=	t	;
// 	double	f9980	=	t	;
// 	double	f9981	=	t	;
// 	double	f9982	=	t	;
// 	double	f9983	=	t	;
// 	double	f9984	=	t	;
// 	double	f9985	=	t	;
// 	double	f9986	=	t	;
// 	double	f9987	=	t	;
// 	double	f9988	=	t	;
// 	double	f9989	=	t	;
// 	double	f9990	=	t	;
// 	double	f9991	=	t	;
// 	double	f9992	=	t	;
// 	double	f9993	=	t	;
// 	double	f9994	=	t	;
// 	double	f9995	=	t	;
// 	double	f9996	=	t	;
// 	double	f9997	=	t	;
// 	double	f9998	=	t	;
// 	double	f9999	=	t	;
// 	double	f10000	=	t	;


}

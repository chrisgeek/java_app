/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquadike;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author CHRISGEEK
 */
public class TidalVelocity extends JPanel{        
    Pattern regEx;
    private JPanel tidalVelPanel;
    private Font answerFont;
    private JFrame frame;
    private JScrollPane sp;
     TitledBorder titledBorder =  BorderFactory.createTitledBorder(null,
                "<html><b>TIDAL VELOCITY</b></html>", TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    MigLayout ml;
    public TidalVelocity(JFrame frame,JScrollPane sp){
        this.frame = frame;
        this.sp = sp;
        initTidalVelComponents();
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", 
                javax.swing.border.TitledBorder.CENTER,javax.swing.border.TitledBorder.DEFAULT_POSITION));     
    }
    
        private void initTidalVelComponents(){
        ml = new MigLayout();
        border_title = "TIDAL LEVEL";
        answerFont = new Font("Tahoma", Font.BOLD, 12);
        tidalVelPanel = new JPanel();
        tidalVelPanel.setLayout(ml);
        tidalVelPanel.setBorder(titledBorder);
        um_label = new JLabel("<html><b>um value</b></html>");
        ua_label = new JLabel("<html><b>ua value</b></html>");
        rp_label = new JLabel("<html><b>rp value</b></html>");
        tv_label = new JLabel("<html><b>t value</b></html>");
        vt_label = new JLabel("<html><b>VT :</b></html>");
        vt_label.setForeground(Color.BLUE);
        vt_label.setFont(answerFont);
        vt_answer = new JLabel("");        
        vt_answer.setForeground(Color.BLUE);
        vt_answer.setFont(answerFont);
        um_field = new JTextField(5);
        ua_field = new JTextField(5);
        tv_field = new JTextField(5);
        rp_field = new JTextField(5);
        calc_tidalVelocity = new JButton("CALCULATE");
        calc_tidalVelocity.addActionListener(new CalculateTidalVelocity());
        back = new JButton("GO BACK");
        back.addActionListener((ActionEvent ae) -> {
            removeTidalVelPanel();
        });
        condition = new JLabel();
        condition.setForeground(Color.blue);
        //Add components to panel
        tidalVelPanel.add(um_label,"gap unrelated");
        tidalVelPanel.add(um_field,"wrap");
        tidalVelPanel.add(ua_label,"gap unrelated");
        tidalVelPanel.add(ua_field,"wrap");
        tidalVelPanel.add(tv_label,"gap unrelated");
        tidalVelPanel.add(tv_field,"wrap");
        tidalVelPanel.add(rp_label,"gap unrelated");
        tidalVelPanel.add(rp_field,"wrap");
        tidalVelPanel.add(vt_label,"gap unrelated");
        tidalVelPanel.add(vt_answer,"wrap");
        tidalVelPanel.add(condition,"growx,wrap");
        tidalVelPanel.add(calc_tidalVelocity);
        tidalVelPanel.add(back,"wrap");
        vt_label.setVisible(false);
        vt_answer.setVisible(false);
      
        add(tidalVelPanel,BorderLayout.CENTER);
        // setVisible(true);
        regEx = Pattern.compile("[0-9]*\\.?[0-9]*"); //[^\\s]
    }
    //Actionlistener for calctidalvel button
    protected class CalculateTidalVelocity implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae) {
            getTidalVelFieldvalues();            
        }
    }
    //Remove panels when back button is clicked
        public void removeTidalVelPanel(){
            setVisible(false);
            frame.remove(this);
            sp.setVisible(true);
            frame.add(sp,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();
        }
    // get field values
    public void getTidalVelFieldvalues(){
        get_um_field = um_field.getText();
        get_ua_field = ua_field.getText();
        get_rp_field = rp_field.getText();
        get_tv_field = tv_field.getText();
        try{
                 um = Double.parseDouble(get_um_field);
                 ua = Double.parseDouble(get_ua_field);
                 tv = Double.parseDouble(get_tv_field);
                 rp = Double.parseDouble(get_rp_field);
                 vt= um + ua * Math.cos((Math.PI*tv)/rp);
                 //vt = (h + ((tp/2)*Math.sin((Math.PI*t)/rp)));            
                if (vt > 0){
                    condition.setText("<html><b>FLOOD CONDITION</b></html>");
                       }
                else if(vt == 0){
                    condition.setText("<html><b>STATIC CONDITION</b></html>");
                       }
                else{
                    condition.setText("<html><b>EBB CONDITION</b></html>");
                       }
            vt_label.setVisible(true);
            vt_answer.setVisible(true);
            vt_answer.setText(String.valueOf(vt));
        }        
            catch(NumberFormatException | NullPointerException nfe){
                 JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
            }
//        if(((regEx.matcher(get_um_field = um_field.getText()).matches()) &&(!get_um_field.equalsIgnoreCase("")))&&
//           ((regEx.matcher(get_ua_field = ua_field.getText()).matches()) &&(!get_ua_field.equalsIgnoreCase("")))&&
//            ((regEx.matcher(get_rp_field = rp_field.getText()).matches()) &&(!get_rp_field.equalsIgnoreCase("")))&&
//             ((regEx.matcher(get_tv_field = tv_field.getText()).matches()) &&(!get_tv_field.equalsIgnoreCase("")))
//                ){
//                 um = Double.parseDouble(get_um_field);
//                 ua = Double.parseDouble(get_ua_field);
//                 tv = Double.parseDouble(get_tv_field);
//                 rp = Double.parseDouble(get_rp_field);
//                 vt= um + ua * Math.cos((Math.PI*tv)/rp);
//                 //vt = (h + ((tp/2)*Math.sin((Math.PI*t)/rp)));            
//                if (vt > 0){
//                    condition.setText("FLOOD CONDITION");
//                       }
//                else if(vt == 0){
//                    condition.setText("STATIC CONDITION");
//                       }
//                else{
//                    condition.setText("EBB CONDITION");
//                       }
//            vt_label.setVisible(true);
//            vt_answer.setVisible(true);
//            vt_answer.setText(String.valueOf(vt));
//        }
//        else{
//             JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
//        }
    }
    
    
    private JTextField um_field,ua_field,rp_field,tv_field;
    private JLabel um_label,ua_label,rp_label,tv_label,vt_label,vt_answer,condition;
    private JButton calc_tidalVelocity,back;
    private String get_um_field,get_rp_field,get_tv_field,get_ua_field,border_title;
    private double um,ua,rp,tv,vt;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 *author chrisgeeq@gmail.com
 * and open the template in the editor.
 */
package aquadike;

import com.placeholder.PlaceHolder;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author chrisgeeq@gmail.com
 */
public class BarChart extends JPanel{
 
    
     public BarChart(JFrame frame , JScrollPane sp){
        this.frame = frame;
        this.sp = sp;
         initComponents();
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", 
                javax.swing.border.TitledBorder.CENTER,javax.swing.border.TitledBorder.DEFAULT_POSITION)); 
    }
     
    private void initComponents(){
        ml = new MigLayout();
        back2Fields = new JButton("<html><b>BACK</b></html>");
        back2Fields.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            panel1.setVisible(true);
            panel2.setVisible(false);
            panel2.removeAll();
            remove(panel2);
            add(panel1,BorderLayout.CENTER);
            setVisible(true);
            frame.remove(scrollPane);
            frame.add(this);
            frame.repaint();
            frame.revalidate();
        });
        view_chart = new JButton("<html><b>VIEW CHART</b></html>");
        backFromChart = new JButton("<html><b>BACK</b></html>");
        
        backFromChart.setSize(18, 12);
        backFromChart.addActionListener((ActionEvent ae ) -> {closeChart();});
        back2Home = new JButton("<html><b>MAIN MENU</b></html>");
        back2Home.addActionListener((ActionEvent ae ) -> {
            setVisible(false);
            frame.remove(this);
            sp.setVisible(true);
            frame.add(sp);
            frame.repaint();
        });
        submit = new JButton("<html><b>SUBMIT</b></html>");
        submit2 = new JButton("<html><b>SUBMIT</b></html>");
        view_chart = new JButton("<html><b>VIEW CHART</b></html>");
        num_of_dataset_label =  new JLabel("<html><b>NUMBER OF DATASET</b></html>");
        title_label = new JLabel("<html><b>TITLE</b></html>");
        category_label = new JLabel("<html><b>CATEGORY</b></html>");
        x_axis_label = new JLabel("<html><b>X-AXIS</b></html>");
        y_axis_label = new JLabel("<html><b>Y-AXIS</b></html>");
        num_of_dataset_field = new JTextField(5);
        title_field = new JTextField(10);
        category_field = new JTextField(10);
        x_axis_field = new JTextField(10);
        y_axis_field = new JTextField(10);
        placeHolder2 = new PlaceHolder(title_field, "chart title");
        placeHolder2 = new PlaceHolder(category_field, "e.g Velocity");
        placeHolder2 = new PlaceHolder(x_axis_field, "x parameter");
        placeHolder2 = new PlaceHolder(y_axis_field, "y parameter");
        panel2 = new JPanel();
        panel2.setBorder(titledBorder);
        panel2.setLayout(ml);
        panel1 = new JPanel();
        panel1.setBorder(titledBorder);
        panel1.setLayout(ml);
         submit.addActionListener(submit_listener);
         view_chart.addActionListener((ActionEvent ae ) -> {drawChart();});
//add components to panel1
        
        
        panel1.add(title_label,"gap unrelated");
        panel1.add(title_field,"wrap");
        panel1.add(x_axis_label,"gap unrelated");
        panel1.add(x_axis_field,"wrap");
        panel1.add(y_axis_label,"gap unrelated");
        panel1.add(y_axis_field,"wrap");
        panel1.add(num_of_dataset_label,"gap unrelated");
        panel1.add(num_of_dataset_field,"wrap");
        panel1.add(back2Home,"span,growx,wrap");
        panel1.add(submit,"growx ,span");
        panel1.setVisible(true);
        //add to extended panel
        add(panel1,BorderLayout.CENTER);
        setVisible(true);
        regEx = Pattern.compile("[a-zA-Z0-9 ]+");
    }
    
    //create class to submit data and generate input fields
        SubmitListener submit_listener = new SubmitListener();
    protected class SubmitListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae){
            submitData();
        }
    }
    //close chart
        private void closeChart(){
            panel3.setVisible(false);
            panel3.removeAll();
            scrollPane.setVisible(true);
            frame.remove(panel3);
            frame.add(scrollPane);
            frame.repaint();
            frame.revalidate();
        }
    
        
        private void submitData(){            
        try{
           data = Integer.parseInt(num_of_dataset_field.getText()); 
                
            if(((regEx.matcher(get_title = title_field.getText()).matches()) &&(!get_title.equalsIgnoreCase("")))&&
                    ((regEx.matcher(get_x_axis = x_axis_field.getText()).matches()) &&(!get_x_axis.equalsIgnoreCase("")))&&
                    ((regEx.matcher(get_y_axis = y_axis_field.getText()).matches()) &&(!get_y_axis.equalsIgnoreCase("")))
                    )
                    {
                    panel1.setVisible(false);
                    remove(panel1);
                    panel2.setVisible(true);
                    add(panel2);
                    repaint();
               for(int i=0; i<data; i++){
                    panel2.add( xb_values[i]=new JTextField(4));
                    panel2.add(yb_values[i]=new JTextField(4));
                    panel2.add(zb_values[i]=new JTextField(5),"wrap");
                    placeHolder = new PlaceHolder(xb_values[i], "value");
                    placeHolder = new PlaceHolder(yb_values[i], "key");
                    placeHolder = new PlaceHolder(zb_values[i], "category");
                    }
                    panel2.add(back2Fields,"span,growx,wrap");
                    panel2.add(view_chart,"span,growx");                    
                    
                    scrollPane = new JScrollPane(this,
                    ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                    frame.remove(this);
                    frame.add(scrollPane);            
                    frame.repaint();
                    frame.revalidate();
                
                }
            
            else{
                JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
            }
    }
            catch(NumberFormatException | NullPointerException nfe){
             JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
    } 

        private void drawChart(){
            try{
            JFreeChart barChart = ChartFactory.createBarChart(
         get_title,
         get_x_axis,
         get_y_axis,
         createDataset(data,xb_values,yb_values,zb_values),
         PlotOrientation.VERTICAL,           
         true, true, false);
        
                  //create chart panel
        ChartPanel chartpanel = new ChartPanel(barChart);
        chartpanel.setDomainZoomable(true);
         CategoryPlot plot = (CategoryPlot) barChart.getPlot();

        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        
        panel3 = new JPanel();
        panel3.setLayout(ml);
        panel3.add(chartpanel,"wrap");
        panel3.add(backFromChart,"growx , span");
        scrollPane.setVisible(false);
        frame.remove(scrollPane);
        panel3.setVisible(true);
        //remove(panel2);
        //add(panel3);
        //repaint();
        //frame.remove(sp);
        frame.add(panel3);
        frame.repaint();
        frame.revalidate();
            }
            
            catch(NumberFormatException | NullPointerException nfe){
             JOptionPane.showMessageDialog(frame, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
        }
        }
        
      private  CategoryDataset createDataset(int confirm, JTextField [] x,JTextField[] y,JTextField[] z) {
       // final XYSeries series = new XYSeries(get_parameter);
           DefaultCategoryDataset dataset = new DefaultCategoryDataset();
           
           for (int i=0; i<data; i++){
                get_x = x[i].getText();
                get_y = y[i].getText();
                get_z = z[i].getText();
                int get_x_values = Integer.parseInt(get_x);
                dataset.addValue(get_x_values, get_y,get_z);
            }            
           
            
            return dataset;
        }

        
    
    
    JTextField [] xb_values = get_xbFields();
     JTextField [] yb_values = get_ybFields();
     JTextField [] zb_values = get_zbFields();
     private JTextField[] get_xbFields(){
        JTextField xfields[]={
            x1	,
        x2	,x3	,x4	,x5	,x6	,x7	,x8	,
        x9	,x10	,x11	,x12	,x13	,x14	,x15	,x16	,x17	,x18	,x19	,x20	,x21	,x22	,x23	,
        x24	,x25	,x26	,x27	,x28	,x29	,x30	,x31	,x32	,x33	,x34	,x35	,x36	,x37	,x38	,
        x39	,x40	,x41	,x42	,x43	,x44	,x45	,x46	,x47	,x48	,x49	,x50
        };
            return xfields;
 }
    private JTextField[] get_ybFields(){
        JTextField yfields[]={y1	,
        y2	,y3	,y4	,y5	,y6	,y7	,y8	,
        y9	,y10	,y11	,y12	,y13	,y14	,y15	,y16	,y17	,y18	,y19	,y20	,y21	,y22	,y23	,
        y24	,y25	,y26	,y27	,y28	,y29	,y30	,y31	,y32	,y33	,y34	,y35	,y36	,y37	,y38	,
        y39	,y40	,y41	,y42	,y43	,y44	,y45	,y46	,y47	,y48	,y49	,y50
        };
            return yfields;
    }

    private JTextField[] get_zbFields(){
        JTextField zfields[]={
            z1	,
        z2	,z3	,z4	,z5	,z6	,z7	,z8	,z9	,z10	,z11	,z12	,z13	,z14	,z15	,z16	,z17	,z18	,z19	,z20	,z21	,z22	,z23	,
        z24	,z25	,z26	,z27	,z28	,z29	,z30	,z31	,z32	,z33	,z34	,z35	,z36	,z37	,z38	,
        z39	,z40	,z41	,z42	,z43	,z44	,z45	,z46	,z47	,z48	,z49	,z50 };
          return zfields;
        }
    
    private Pattern regEx ;
    private JScrollPane scrollPane;
    private String get_title,get_category,get_x_axis,get_y_axis,get_x,get_y,get_z;
    private int data;
    private JFrame frame;
    private JScrollPane sp;
    private JPanel panel1,panel2,panel3;
    private MigLayout ml;
    private JButton back2Home,back2Fields,backFromChart,submit,submit2,view_chart;
    private JTextField num_of_dataset_field,title_field,category_field,x_axis_field,y_axis_field;
    private JLabel num_of_dataset_label,title_label,category_label,x_axis_label,y_axis_label;
    private TitledBorder titledBorder = titledBorder = BorderFactory.createTitledBorder(null,
                "<html><b>BAR CHART</b></html>", TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
   private PlaceHolder placeHolder,placeHolder2;
    
     JTextField x1	,
        x2	,x3	,x4	,x5	,x6	,x7	,x8	,x9	,x10	,x11	,x12	,x13	,x14	,x15	,x16	,x17	,x18	,x19	,x20	,x21	,x22	,x23	,
        x24	,x25	,x26	,x27	,x28	,x29	,x30	,x31	,x32	,x33	,x34	,x35	,x36	,x37	,x38	,
        x39	,x40	,x41	,x42	,x43	,x44	,x45	,x46	,x47	,x48	,x49	,x50,y1	,
        y2	,y3	,y4	,y5	,y6	,y7	,y8	,y9	,y10	,y11	,y12	,y13	,y14	,y15	,y16	,y17	,y18	,y19	,y20	,y21	,y22	,y23	,
        y24	,y25	,y26	,y27	,y28	,y29	,y30	,y31	,y32	,y33	,y34	,y35	,y36	,y37	,y38	,
        y39	,y40	,y41	,y42	,y43	,y44	,y45	,y46	,y47	,y48	,y49	,y50,z1	,
        z2	,z3	,z4	,z5	,z6	,z7	,z8	,z9	,z10	,z11	,z12	,z13	,z14	,z15	,z16	,z17	,z18	,z19	,z20	,z21	,z22	,z23	,
        z24	,z25	,z26	,z27	,z28	,z29	,z30	,z31	,z32	,z33	,z34	,z35	,z36	,z37	,z38	,
        z39	,z40	,z41	,z42	,z43	,z44	,z45	,z46	,z47	,z48	,z49	,z50;

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aquadike;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author CHRISGEEK
 */
public class TidalLevel extends JPanel{
    private JPanel tidalPanel;
    private Font answerFont;
    private JFrame frame;
    private JScrollPane sp;
        TitledBorder titledBorder = BorderFactory.createTitledBorder(null,
                "<html><b>TIDAL LEVEL</b></html>", TitledBorder.CENTER,
                TitledBorder.DEFAULT_POSITION);
    MigLayout ml;
    public TidalLevel(JFrame frame,JScrollPane sp){
        this.frame = frame;
        this.sp = sp;
        initTidalComponents();
        setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", 
                javax.swing.border.TitledBorder.CENTER,javax.swing.border.TitledBorder.DEFAULT_POSITION)); 
    }
    private void initTidalComponents(){
        ml = new MigLayout();
        border_title = "TIDAL LEVEL";
        answerFont = new Font("Tahoma", Font.BOLD, 12);
        tidalPanel = new JPanel();
        tidalPanel.setLayout(ml);
        tidalPanel.setBorder(titledBorder);
        h_label = new JLabel("<html><b>h value</b></html>");
        tp_label = new JLabel("<html><b>tp value</b></html>");
        rp_label = new JLabel("<html><b>rp value</b></html>");
        t_label = new JLabel("<html><b>t value</b></html>");
        ht_label = new JLabel("<html><b>HT :</b></html>");
        ht_label.setForeground(Color.BLUE);
        ht_label.setFont(answerFont);
        ht_answer = new JLabel("");        
        ht_answer.setForeground(Color.BLUE);
        ht_answer.setFont(answerFont);
        h_field = new JTextField(5);
        tp_field = new JTextField(5);
        t_field = new JTextField(5);
        rp_field = new JTextField(5);
        calc_tidal = new JButton("<html><b>CALCULATE</b></html>");
        calc_tidal.addActionListener(new CalculateTidalLevel());
        back = new JButton("<html><b>BACK</b></html>");
        back.addActionListener((ActionEvent ae) -> {
            removeTidalPanel();
        });
        tidalPanel.add(h_label,"gap unrelated");
        tidalPanel.add(h_field,"wrap");
        tidalPanel.add(tp_label,"gap unrelated");
        tidalPanel.add(tp_field,"wrap");
        tidalPanel.add(t_label,"gap unrelated");
        tidalPanel.add(t_field,"wrap");
        tidalPanel.add(rp_label,"gap unrelated");
        tidalPanel.add(rp_field,"wrap");
        tidalPanel.add(ht_label,"gap unrelated");
        tidalPanel.add(ht_answer,"wrap");
        tidalPanel.add(calc_tidal);
        tidalPanel.add(back,"wrap");
        ht_label.setVisible(false);
        ht_answer.setVisible(false);
      
        add(tidalPanel,BorderLayout.CENTER);
        // setVisible(true);
        regEx = Pattern.compile("[0-9]*\\.?[0-9]*"); //[^\\s]
    }
    Pattern regEx;
    protected class CalculateTidalLevel implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ae) {
            getTidalFieldvalues();
            
        }
    }
    
        public void removeTidalPanel(){
            setVisible(false);
            frame.remove(this);
            sp.setVisible(true);
            frame.add(sp,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();
        }        
    
    public void getTidalFieldvalues(){
                get_h_field = h_field.getText();
                get_tp_field = tp_field.getText();
                get_rp_field = rp_field.getText();
                get_t_field = t_field.getText();
                try{                
                 h = Double.parseDouble(get_h_field);
                 tp = Double.parseDouble(get_tp_field);
                 t = Double.parseDouble(get_t_field);
                 rp = Double.parseDouble(get_rp_field);
                 ht = (h + ((tp/2)*Math.sin((Math.PI*t)/rp)));
                ht_label.setVisible(true);
                ht_answer.setVisible(true);
                ht_answer.setText(String.valueOf(ht));
            }
            catch(NumberFormatException | NullPointerException nfe){
                 JOptionPane.showMessageDialog(this, "INVALID INPUT(S)","INPUT ERROR",JOptionPane.ERROR_MESSAGE);
            }                
    }    
    private JTextField h_field,tp_field,t_field,rp_field;
    private JLabel h_label,tp_label,t_label,rp_label,ht_label,ht_answer;
    private JButton calc_tidal,back;
    String get_tp_field,get_rp_field,get_t_field,get_h_field,border_title;
    private double h,tp,rp,t,ht;
}

package aquadike;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
/**
 *
 * @author Chrisgeeq@gmail.com
 */

public class AquaDike {
    
    private static JFrame frame;
    private static JScrollPane scrollPane;
    public String dimension;    
    private static JScrollPane scrollPane2;
    private static void initComponents(){        
        frame = new JFrame("AQUA-DIKE");
//                try{
//            frame.setIconImage(ImageIO.read(new File("aqua_icon32.jpg")));
//        }
//        catch(IOException exc){
//            exc.printStackTrace();
//        }
        SedimentPanel sedimentPanel = new SedimentPanel(frame,scrollPane);
        //PanelTitle pt = new PanelTitle(sedimentPanel);
                
//        scrollPane2 = new JScrollPane(data_correlation,
//                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane = new JScrollPane(sedimentPanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        TidalLevel tidalLevel= new TidalLevel(frame,scrollPane);
        TidalVelocity tidalVelocity= new TidalVelocity(frame,scrollPane);
        DataCorrelation data_correlation = new DataCorrelation(frame, scrollPane);
        CorrelationCoefficient correlation_coef = new CorrelationCoefficient(frame, scrollPane);
        XyLineChart xyLine_chart = new XyLineChart(frame,scrollPane);
        BarChart bar_chart = new BarChart(frame,scrollPane);
        BedLevel bed_level = new BedLevel(frame, scrollPane);
    JMenu parameterMenu,dimensionMenu,tideMenu,chartMenu,statAnalysisMenu,dxfMenu;
    JMenuBar menuBar;
    SoftBevelBorder softBevelborder;
    JMenuItem sedimentMenuItem,velocityXMenuItem,velocityYMenuItem,bedlevelMenuItem,d1MenuItem,d2MenuItem,d3MenuItem,tidallevelMenuItem,
              dxfMenuItem,tidalvelocityMenuItem,linechartMenuItem,barchartMenuItem,datacorrelationMenuItem,datacoefMenuItem;
    Font menuFont;   
        
        //initialize wrapper panel and add to JFrame         
         frame.add(scrollPane,BorderLayout.CENTER);//add panel to frame
        //initialize JMenuItems
        sedimentMenuItem = new JMenuItem("SEDIMENT CONCENTRATION");
        velocityXMenuItem = new JMenuItem("VELOCITY(X-DIRECTION)");
        velocityYMenuItem = new JMenuItem("VELOCITY(Y-DIRECTION)");
        //add listeners for parameter items
       
        
        bedlevelMenuItem = new JMenuItem("BEDLEVEL FORM");
        d1MenuItem = new JMenuItem("1D VALUES");
        d2MenuItem = new JMenuItem("2D VALUES");
        d3MenuItem = new JMenuItem("3D VALUES");
        dxfMenuItem = new JMenuItem("IMPORT DXF");
        tidallevelMenuItem = new JMenuItem("TIDAL LEVEL");
        tidalvelocityMenuItem = new JMenuItem("TIDAL VELOCITY");
        linechartMenuItem = new JMenuItem("XY LINECHART");
        barchartMenuItem = new JMenuItem("BAR CHART");
        datacorrelationMenuItem = new JMenuItem("DATA CORRELATION");
        datacoefMenuItem = new JMenuItem("CORRELATION COEFFICIENT");
        
        //initilize menu border
        softBevelborder =new SoftBevelBorder(BevelBorder.LOWERED);
        menuFont = new Font("Tahoma", Font.BOLD, 14);
        // add menuitem properties
        
        dxfMenuItem.setBorder(softBevelborder);
        dxfMenuItem.setBorderPainted(true);
        dxfMenuItem.setForeground(Color.black);
        dxfMenuItem.setOpaque(true);
        
        sedimentMenuItem.setBorder(softBevelborder);
        sedimentMenuItem.setBorderPainted(true);
        sedimentMenuItem.setForeground(Color.black);
        sedimentMenuItem.setOpaque(true);
        
        velocityXMenuItem.setBorder(softBevelborder);
        velocityXMenuItem.setBorderPainted(true);
        velocityXMenuItem.setForeground(Color.black);
        velocityXMenuItem.setOpaque(true);
        
        velocityYMenuItem.setBorder(softBevelborder);
        velocityYMenuItem.setBorderPainted(true);
        velocityYMenuItem.setForeground(Color.black);
        velocityYMenuItem.setOpaque(true);
        
        bedlevelMenuItem.setBorder(softBevelborder);
        bedlevelMenuItem.setBorderPainted(true);
        bedlevelMenuItem.setForeground(Color.black);
        bedlevelMenuItem.setOpaque(true);
        bedlevelMenuItem.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            frame.remove(scrollPane);
            //tidalLevel.titledBorder.setTitle("New Title");
            bed_level.setVisible(true);
            frame.add(bed_level,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();
        });
        
        d1MenuItem.setBorder(softBevelborder);
        d1MenuItem.setBorderPainted(true);
        d1MenuItem.setForeground(Color.black);
        d1MenuItem.setOpaque(true);
        //d1MenuItem.addActionListener(new View_1D());
        
        d2MenuItem.setBorder(softBevelborder);
        d2MenuItem.setBorderPainted(true);
        d2MenuItem.setForeground(Color.black);
        d2MenuItem.setOpaque(true);
        //d2MenuItem.addActionListener(new View_2D());
        
        d3MenuItem.setBorder(softBevelborder);
        d3MenuItem.setBorderPainted(true);
        d3MenuItem.setForeground(Color.black);
        d3MenuItem.setOpaque(true);
        //d3MenuItem.addActionListener(new View_3D());
        
        
        tidallevelMenuItem.setBorder(softBevelborder);
        tidallevelMenuItem.setBorderPainted(true);
        tidallevelMenuItem.setForeground(Color.black);
        tidallevelMenuItem.setOpaque(true);
        tidallevelMenuItem.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            frame.remove(scrollPane);
            //tidalLevel.titledBorder.setTitle("New Title");
            tidalLevel.setVisible(true);
            frame.add(tidalLevel,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();
        });        
        
        tidalvelocityMenuItem.setBorder(softBevelborder);
        tidalvelocityMenuItem.setBorderPainted(true);
        tidalvelocityMenuItem.setForeground(Color.black);
        tidalvelocityMenuItem.setOpaque(true);
        
        tidalvelocityMenuItem.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            frame.remove(scrollPane);
            tidalVelocity.setVisible(true);
            frame.add(tidalVelocity,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();
        });
        
        
        linechartMenuItem.setBorder(softBevelborder);
        linechartMenuItem.setBorderPainted(true);
        linechartMenuItem.setForeground(Color.black);
        linechartMenuItem.setOpaque(true);
        linechartMenuItem.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            frame.remove(scrollPane);
            xyLine_chart.setVisible(true);
            frame.add(xyLine_chart,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();
        });
        
        barchartMenuItem.setBorder(softBevelborder);
        barchartMenuItem.setBorderPainted(true);
        barchartMenuItem.setForeground(Color.black);
        barchartMenuItem.setOpaque(true);
        barchartMenuItem.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            frame.remove(scrollPane);
            bar_chart.setVisible(true);
            frame.add(bar_chart,BorderLayout.CENTER);
            frame.repaint();
            frame.revalidate();
        });
        
        datacorrelationMenuItem.setBorder(softBevelborder);
        datacorrelationMenuItem.setBorderPainted(true);
        datacorrelationMenuItem.setForeground(Color.black);
        datacorrelationMenuItem.setOpaque(true);
        datacorrelationMenuItem.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            frame.remove(scrollPane);
            data_correlation.setVisible(true);
            frame.add(data_correlation);
            frame.repaint();
            frame.revalidate();
        });
        
        datacoefMenuItem.setBorder(softBevelborder);
        datacoefMenuItem.setBorderPainted(true);
        datacoefMenuItem.setForeground(Color.black);
        datacoefMenuItem.setOpaque(true);
        datacoefMenuItem.addActionListener((ActionEvent ae) -> {
            scrollPane.setVisible(false);
            frame.remove(scrollPane);
            correlation_coef.setVisible(true);
            frame.add(correlation_coef);
            frame.repaint();
            frame.revalidate();
        });
        
        velocityXMenuItem.addActionListener((ActionEvent ae) -> {
            sedimentPanel.disable_d_Fields(false);
           sedimentPanel.setTitle("VELOCITY(X-DIRECTION)");
           if(sedimentPanel.checkBox.isSelected()==true){
               sedimentPanel.checkBox.setSelected(false);
           }
            
        });
        
                velocityYMenuItem.addActionListener((ActionEvent ae) -> {
            sedimentPanel.disable_d_Fields(false);
           sedimentPanel.setTitle("VELOCITY(Y-DIRECTION)");
           if(sedimentPanel.checkBox.isSelected()==true){
               sedimentPanel.checkBox.setSelected(false);
           }
            
        });
        sedimentMenuItem.addActionListener((ActionEvent ae) -> {
            sedimentPanel.disable_d_Fields(true);
            sedimentPanel.setTitle("SEDIMENT CONCENTRATION");
                       if(sedimentPanel.checkBox.isSelected()==true){
               sedimentPanel.checkBox.setSelected(false);


           }
        });
        
        //initialize JMenu and JMenuItems
        parameterMenu = new JMenu("HYDRAULIC ENGINEERING");
        dimensionMenu = new JMenu("DIMENSION");
        tideMenu = new JMenu("TIDE PREDICTION");
        chartMenu = new JMenu("CHARTS");
        //dxfMenu = new JMenu("DXF");
        statAnalysisMenu = new JMenu("DATA ANALYSIS");
        menuBar = new JMenuBar();
        menuBar.setBorder(softBevelborder);
        
        //add border properties to JMenu
//        dxfMenu.setBorder(softBevelborder);
//        dxfMenu.setBorderPainted(true);
//        dxfMenu.setFont(menuFont);
        parameterMenu.setBorder(softBevelborder);
        parameterMenu.setBorderPainted(true);
        parameterMenu.setFont(menuFont);
        dimensionMenu.setBorder(softBevelborder);
        dimensionMenu.setBorderPainted(true);
        dimensionMenu.setFont(menuFont);
        tideMenu.setBorder(softBevelborder);
        tideMenu.setBorderPainted(true);
        tideMenu.setFont(menuFont);
        chartMenu.setBorder(softBevelborder);
        chartMenu.setBorderPainted(true);
        chartMenu.setFont(menuFont);
        statAnalysisMenu.setBorder(softBevelborder);
        statAnalysisMenu.setBorderPainted(true);
        statAnalysisMenu.setFont(menuFont);
        //add JMenuItems to JMenu
        //dxfMenu.add(dxfMenuItem);
        parameterMenu.add(sedimentMenuItem);
        parameterMenu.add(velocityXMenuItem);
        parameterMenu.add(velocityYMenuItem);
        parameterMenu.add(bedlevelMenuItem);
        dimensionMenu.add(d1MenuItem);
        dimensionMenu.add(d2MenuItem);
        dimensionMenu.add(d3MenuItem);
        tideMenu.add(tidallevelMenuItem);
        tideMenu.add(tidalvelocityMenuItem);
        chartMenu.add(linechartMenuItem);
        chartMenu.add(barchartMenuItem);
        statAnalysisMenu.add(datacorrelationMenuItem);
        statAnalysisMenu.add(datacoefMenuItem);        
        
        //add JMenu to menubar
        menuBar.add(parameterMenu);
        //menuBar.add(dimensionMenu);
        menuBar.add(tideMenu);
        menuBar.add(chartMenu);
        menuBar.add(statAnalysisMenu);
//        menuBar.add(dxfMenu);
        
        frame.setJMenuBar(menuBar);
        
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        //jframe properties
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);        
        frame.setSize(600,550);
        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
        frame.setVisible(true);
        frame.pack();
        //add window listener
        frame.addWindowListener(new WindowAdapter(){
             @Override
             public void windowClosing(WindowEvent e){
                 int confirm=JOptionPane.showConfirmDialog(frame,"<html><u><b><font color='black'>"
                + "Sure To Exit?<font><b></u></html>"
                ,"Confirm Exit",JOptionPane.YES_NO_OPTION);
                if(confirm == JOptionPane.YES_OPTION){
                    frame.dispose();
                }
             }
        });
        
        
    }        
    public static void main(String[] args)    {
            EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(AquaDike.class.getName()).log(Level.SEVERE, null, ex);
                }
}
            }
            );
            
        try {
           for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                UIManager.setLookAndFeel(info.getClassName());
                break;
            }
          }
        }  
        catch (IllegalAccessException | ClassNotFoundException | UnsupportedLookAndFeelException e) {
            System.err.print("look and Feel not supported");
        }
        catch (InstantiationException e) {
            // handle exception
        }

        // AquaDike aquadike = new AquaDike(sp);
          SwingUtilities.invokeLater(() -> initComponents());
        
    }
    
    //Variables Declaration- do not modify
   
   // private JPanel wrapperPanel;
    //WrapperPanel wrapper_panel;
    //SedimentPanel sediment_panel;
}
